#include <pthread.h>
#include "DataTransferMaster.h"

void PrintStack(char * function);

void SetWorkingDir();

void Calc(); // Vichislenie vlajnosti

char WriteUBI();

char Write2Master();

void Aout(); //

char Reader(); //ctenie faylov nastroek

char Rabota(); // Regim raboty

char Graduirovka(); // Regim graduirovki

char Kalibrovka(); // Regim kalibrovki

char Nastrojka(); // Regim nastrojki

char NGrad(); // Regim nastrojki

char NSlug(); // Regim nastrojki

char Porog(); // Regim nastrojki

char Rezonans(); // Poisk rezonansa

bool NewMedium(); // New medium write

char SchablonNSlug(); // Regim nastrojki

char SchablonPorog(); // Regim nastrojki

char ComCMode(char UstrAdr,char UstrFuncArgument); // Ustanjvit regim raboti

char ComReadIntReg(unsigned char UstrAdr,unsigned char StartAdr, unsigned char Length); // Chtenie registrov

char ReadDataSPMCurrent(); // Chtenie tekuchih dannih SPM

char ReadDataSPMSet(); // Chtenie nastroek SPM

char ComWriteIntReg(char UstrAdr,char StartAdr, char Length); // Zapis registrov

char WriteDataSPMSet(); // Zapis nastroek SPM

char ReadKey(); // Chtenie klaviatury immediately

char ReadKeyBuffered(); // Chtenie klaviatury

char EEPROMSave(char UstrAdr); // Zapis v EEPROM

char ClrScr(); // Ochistka ekrana

char SetCursor(char X, char Y);

char PrintXYL(char X, char Y, int length); // Pechat stroki simvolov

char PrintXY(char X, char Y, const char * str);

char PrintXY(char X, char Y, const char * str, int length);

char SchablonRabota(); // Schablon ekrana raboty

char SchablonGrad(); // Schablon ekrana graduirovki

char SchablonKalibr(); // Schablon ekrana kalibrovki

char SchablonNastrojka(); // Schablon ekrana

char SchablonNGrad(); // Schablon ekrana

void SchablonKeyboard(char * title, char * buffer); //Schablon ekrana New medium

char CRCcount(char rw,unsigned char Ln); // Vichislenie CRC

int IniCom(const char * portname, int baundRate);

char IniCom1(); //Inicializacia Com1

char IniCom2(); //Inicializacia Com2

char * CombinePath(const char * firstPart, const char * secondPart);

void TimedIncrementT_Gen_T_Sr0_T_Sr1_W(bool setAverage, int t_Gen = 0, int t_Sr0 = 0, int t_Sr1 = 0, float w = 0.0f);

static void * ReadKeyThreadProc (void * vptr_args);

unsigned long ClockToMilliseconds(clock_t value);

char WaitNoKey();

char WaitKey();

char WaitAnyKey();

void WaitNoKeyImmediately();

void WaitKeyImmediately();

bool ShutdownServer();

bool CheckFilling();

void SleepWithService(long sleeptime);

void CheckShutdown();

char VirtualKeyboard(char * buffer);

enum FileType {Grad, Nastr, Kalibr, Set};

char * GetNumberedFileName(FileType type);

char * GetFullRow(char * buffer);

char * SetMediumRow(int newIndex = -1);

char * GetCenteredRow(char * buffer);

char * Trim(char * source);

void SetDefaultValues();

void SaveGrad();

void SaveNastr();

void SaveKalibr();

void SaveSet();

void SaveAll();

bool ReadDefaultMedium();

void ReadMedium();

void SaveMedium();

void SetMedium(char * newMediumName);

bool HasSomeDefaultFiles();

void CompliteAndSendData(CommandEntity * entity);

void InitialiseBuffer(unsigned char * const buffer, int length);

void clearArrays();

void set_blocking(int fd, int should_block);

void utf8cp1251(char * from, char * to, int length);

int set_interface_attribytes(int fd, int baudRate);

bool ExistReadBytes(int fd, int timeout_sec);

void Sleep(int mileseconds_time);

void PrintLock();

void PrintUnlock();

char ComDataWrite[260];

char ComDataRead[260];

char Text[255];

char CP1251[255];

int RegistrSPM_int[26];

float RegistrSPM_float[4];

int RegistrTM_int [11];

int Gamma0 = 0, Gamma1 = 0;    //Period mejdu pikami                     *0.1 mkS

int Gamma0_k = 0, Gamma1_k = 0;//Period mejdu pikami pri kalibrovke      *0.1 mkS

int Haw0 = 0, Haw1 = 0;        //Shirina impulsa na polovine amplitudy   *0.1 mkS

int Am0 = 0, Am1 = 0;          //Amplituda impulsa                       *1000 V


int Haw0_k = 0, Haw1_k = 0;    //Shirina impulsa na polovine amplitudy   *0.1 mkS

//pri kalibrovke
int Am0_k = 0, Am1_k = 0;      //Amplituda impulsa pri kalibrovke        *1000 V

int T_Gen = 0, T_Gen_k = 0;    //Temperatura generatora                  *100  C

int T_Sr0 = 0, T_Sr1 = 0;      //Temperatura sredy                       *100  C

int T_Sr0_k = 0;           //Temperatura sredy pri kalibrovke        *100  C

int Ain0 = 0, Ain1 = 0;        //Tekuchee znachenie na analogovom vhode  *1000 V

int Saw_A0 = 0;            //Amplituda podporki                      *1000 V

int Saw_A1 = 0;            //Amplituda pily                          *1000 V

int Ku0 = 0, Ku1 = 0;          //Koefficient usilenija                   2^x

int Taq = 0;               //Period sbora dannih                     x*20mkS

int Fwdt = 0;              //Shirina okna filtra

int Pcount = 0;            //Kol-vo periodov usrednenija

int BaudRateSPM = 0;       //Skorost obmena SPM

int HostSPM = 0;           //Adres SPM

int ValidReqSPM = 0;       //Schetchik uspeshnih zaprosov SPM

int CRCReqSPM = 0;         //Schetchik error CRC SPM

int ExcReqSPM = 0;         //Schetchik error obrabotki SPM

int VO = 0, VZ = 0, VI = 0, AK = 0, Tak = 0, Aout_tip = 0;

float W = 0, AA = 0, BB = 0, a0 = 0, a1 = 0, a2 = 0, a3 = 0,a4 = 0,a5 = 0,a6 = 0,a7 = 0,a8 = 0,a9 = 0,k1 = 0,Wmin = 0,Wmax = 0;

float Va = 0, kgt = 0, kgv = 0, T = 0,tgn = 0,tsn = 0; //delta_gamma; tgn;

float delta_gamma = 0, delta_gamma2 = 0;

int Kol_dat = 0, Nom_dat1 = 0, Nom_dat2 = 0, Nom_ind = 0;

float N_Rez = 0, K_Rez = 0, Step_Rez = 0;

int UstRele = 0;

char Rel = 0, Rele3 = 0, Rele3_old = 0;

int Nzp = 0;

float Poralfa = 0, Porbeta = 0, Porgamma = 0;

long int xtim = 0, xtim_n = 0;

char Regim = 0;

int hCom1 = -1,hCom2 = -1;

char applicationPath[1024];

volatile char keyBuffer = 0;

const long servicetime = 400;

pthread_mutex_t g_LockData;

unsigned char controlleradr = 0x001;

const int MBSIZE = 4;

unsigned char masterbuffer[4];

volatile bool dataSent = true;
 
#define MEAN_TIME 12000 // Mean time is 12 sec

#define PATH_SEPARATOR "/"

#define READKEY_INTERVAL 100

#define NET_ZAPOLNENIYA_PING_PERIOD 60*1000

char * abc_ru = "1234567890 abcdefghijklmnopqrstuvwxyz";

static const char * notation[] = {"a", "b", "v", "g", "d", "ye", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "ts", "ch", "sh", "sch", "tz", "y", "mz", "e", "yu", "ya", "x", "yy", "iy", "yo"};

static const char * emptyrow = "                    ";

const int ROW_WIDTH = 21;

char mediumName[ROW_WIDTH];

const int MEDIUM_COUNT = 10;

char mediums[MEDIUM_COUNT][ROW_WIDTH];

int media_count = 0;

int NomGrad = 0;

char fullrow[ROW_WIDTH];

const bool IS_EXPERIMENT = true; // if device is manually controlled then you can back from current programm state without wait for this state finish.

char trimStr[ROW_WIDTH];

char newMediumStr[ROW_WIDTH];

DataTransferMaster master(RS485, "COM2", 12 * 1000);

pthread_t readkey_thread;

char combinePathStr[255];

volatile int isexit = 0;

char translated[255];

char translatedFileName[255];

char centeredStr[21];

const char * SERIALPORT1 = "/dev/ttyS0";

const char * SERIALPORT2 = "/dev/ttyS1";

#define USE_REMOTE_CONTROL false

#define USE_VERBOSE_IO false

#define USE_VERBOSE_STACK false

#define USE_VERBOSE_LOCKS false