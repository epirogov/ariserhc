#ifndef _DataTransferMaster
#define _DataTransferMaster

#include <pthread.h>
#include "PrivateMessanger.h"
#include "CommandEntity.h"
#include "CommandResponseFactory.h"

//public delegate void CompliteTemplateEventHandler (void * sender, TypedEventArgs<CommandEntity *> ^ e);
class DataTransferMaster : public PrivateMessanger
{
  private :
    pthread_mutex_t g_syncReceive;//was not used
    pthread_cond_t g_syncCond;//was not used
    pthread_t listener_thread;
  public:
    ModbusProvider * provider;
    void (*CompliteTemplate)(CommandEntity *);
    CommandResponseFactory * responseFactoy;
    volatile bool listen;
    DataTransferMaster(DeviceTypeEnum deviceType, const char * device, int listenPeriod);
    DataTransferMaster(const char * device, int listenPeriod);
    ~DataTransferMaster();
    void Open(int hCom2);
    int ScanController(unsigned char address);
    int RegisterController(unsigned char address, unsigned char deviceType);
    int DeviceWasHalted(unsigned char address);
    int RequestData(unsigned char address, time_t startDate, int duration);
    int SendDataPacket(unsigned char address, int offset, unsigned char count, unsigned char * data, int datalength);
    int DataTransactionComplited(unsigned char address);
    int BeginRemoteControl(unsigned char address);
    int SendDeviceFiles(unsigned char address, int fileMask);
    int RequestDeviceFile(unsigned char address, int fileCode);
    int SaveDeviceFiles(unsigned char address, int fileMask);
    int DeviceFileUpload(unsigned char address, int fileCode);
    int CompliteRemoteControl(unsigned char address);
    void Close();
    void SendResponse(CommandEntity * responseCommand);
    int RetriveResponse(CommandEntity * entity);
//private :
//	void * WatchCycle (void * vptr_args);
};

#endif