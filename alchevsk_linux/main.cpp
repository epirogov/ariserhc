#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/stat.h>
#include <unistd.h>
#include <termios.h>
#include <iconv.h>

#include "MWTSoft2.h"
#include "DataTransferMaster.h"
#include "DeviceTypeEnum.h"
#include "CommandEntity.h"
#include "DataTransferCommandEnum.h"
#include "ControllerCommandsEnum.h"
#include "spm3service.h"

int main(int argc, char* argv[])
{	
	PrintStack("main");	
	g_LockData = PTHREAD_MUTEX_INITIALIZER;
	clearArrays();	
	if(argc == 1)
	{
	  SetWorkingDir();
	}
	else
	{
	  strcpy(applicationPath, argv[1]);
	  applicationPath[strlen(argv[1])] = 0;
	}

	//NomUBI=10;
	IniCom1();
	
	if(USE_REMOTE_CONTROL)
	{
	  IniCom2();
	  master.Open(hCom2);
	  master.CompliteTemplate = &CompliteAndSendData;
	  SleepWithService(1000);
	}
	
	ComCMode(0x2,0x2);
	ComCMode(0x1,0x0);
	//AA=0;
	//BB=1;
	//a0=1234.5678;
	//a1=1;
	//a2=1;
	//a3=1;
	//a4=4; a5=5; a6=6; a7=7; a8=8; a9=9; k1=1; Wmin=10; Wmax=80;

	//Write2Master();

	//Am0_k=20000; Haw0_k=30000; Gamma0_k=40000;
	//T_Gen_k=2100; T_Sr0_k=2300;
	//UstRele=123;
	//VO=5;VZ=12345;VI=5; AK=0; Tak=100;
	//Aout_tip=2;  NomGrad=1;
	//Va=1234.567;kgt=1234.567;kgv=1234.567;T=1234.567;
	//N_Rez=3.0; K_Rez=6.5; Step_Rez=0.1;

	SetDefaultValues();
	if(!ReadDefaultMedium())
	{
		NewMedium();
		if(!ReadDefaultMedium())
		{
			SetMedium("по умолчанию");
			SaveAll();
		}
	}
	else
	{
		Reader();
	}

	Va=Saw_A1;

	//Ku0=2;Ku1=2;
	//Saw_A0=3000;Saw_A1=2000;
	WriteDataSPMSet();
	ReadDataSPMSet();
	//AA=60; Aout_tip=1;

	Regim='1';
	
	do {
	      switch(Regim)
	      {
		case '1':
		  Regim=Rabota();
		  break;
		case '3':
		  Regim=Graduirovka();
		  break;
		case '5':
		  Regim=Kalibrovka();
		  break;
		case '7':
		  Regim=Nastrojka();
		  break;
		default:
		  Regim=ReadKeyBuffered();
	      }
	      
	} while (true);

	//delete mutexes,close files.
	isexit = 1;
	if(pthread_join(readkey_thread, NULL))
	{
		exit(1);
	}
	
	close(hCom1);
	close(hCom2);
}


char PrintXYL(char X, char Y, int length) // Pechat stroki simvolov
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
	//          13-err adr komandy

	//-----Komanda-----
	//0-UstrAdr - adres ustrojstva (1 char)- =2-terminal;
	//1-UstrCodeFunc - Kod funkcii (1 char)- =0x68;
	//2-koordinata X (0-19) gorizont
	//3-koordinata Y (0-15) vertikal
	//4- 3+N - stroka simvolov
	//3+N-CRC
	//4+N-CRC
	//-----Otvet-----
	//0-UstrAdr - adres ustrojstva (1 char)- =UstrAdr v komande;
	//1-UstrCodeFunc - Vozvrat koda funkcii (1 char)- =0x68 - esli OK;
	//=0xE8 - esli error
	//2 - esli error - kod oshibki    0x01 - err koda func
	//0x02 - err adr reg
	//0x03 - err data
	//2 - esli OK - CRC
	//3 - CRC
	//4 - esli error - CRC
	//4 - esli OK - net
{
	PrintStack("PrintXYL");
	PrintLock();
	pthread_mutex_lock(&g_LockData);

	int i;
	char len, pos=0;

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=0x02;//char 0
	ComDataWrite[pos++]=0x68;//char 1
	ComDataWrite[pos++]=X;//char 2
	ComDataWrite[pos++]=Y; //char 3
	
	utf8cp1251(Text, CP1251, length);

	for (i=0;i<length;i++)
	{
		ComDataWrite[pos++]=CP1251[i];
	};
	//pos=pos+length;
	CRCcount('w',pos);
	
	//Peredacha komandnoj stroki i priem otveta
	len=pos+2;
	int ret = write(hCom1,ComDataWrite,len);
	len=4;
	ret = read(hCom1,ComDataRead,len);
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);
	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4	
	if (ComDataWrite[0]!=ComDataRead[0])
	{
		return 10;
	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{
		if (ComDataRead[1]!=ComDataWrite[1]+0x80)
		{
			return 11;
		} 
		else
		{	    
			return ComDataRead[2];
		};
	}
	else 
	{
		CRCcount('r',len); // vichislenie CRC
	};
	
	return 0;
}



//read key only if con exists in buffer
char ReadKeyBuffered()
{
	PrintStack("ReadKeyBuffered");	
	if(keyBuffer == 'Z')
	{
		keyBuffer = ReadKey();
		CheckShutdown();	
	}
	char currentKey = keyBuffer;
	keyBuffer = 'Z';	
	return currentKey;
}



char ReadKey() // Read keyboard
{  
	PrintStack("ReadKey");	
	int Keyint = 0;
	char Keychar = 'Z';
	if(ComReadIntReg(0x2,0x30,0x1) != 0)
	{
	  return Keychar;
	}
	Keyint=ComDataRead[2]*256+ComDataRead[3];
	ComDataRead[2]=0;ComDataRead[3]=0;
	switch(Keyint) 
	{
	  case 16:
	     Keychar='0';
	     break;
	  case 2:
	    Keychar='1';
	    break;
	  case 32:
	    Keychar='2';
	    break;
	  case 512:
	    Keychar='3';
	    break;
	  case 4:
	    Keychar='4';
	    break;
	  case 64:
	    Keychar='5';
	    break;
	  case 1024:
	    Keychar='6';
	    break;
	  case 8:
	    Keychar='7';
	    break;
	  case 128:
	    Keychar='8';
	    break;
	  case 2048:
	    Keychar='9';
	    break;
	  case 1:
	    Keychar='S';
	    break;
	  case 256:
	    Keychar='C';
	    break;
	  case 32768:
	    Keychar='*';
	    break;
	  case 16384:
	    Keychar='-';
	    break;
	  case 8192:
	    Keychar='+';
	    break;
	  case 4096:
	    Keychar='=';
	    break;
	  case 32785:
	    Keychar='H';
	    break;
	  case 0:
	    Keychar='Z';
	    break;
	};
	
	return Keychar;
}



void Calc() // Vichislenie vlajnosti
{
	PrintStack("Calc");
	
	float difftime0 = 0.0f;
	tgn=a8;
	tsn=a9;
	if (kgv==0) 
	{
	  kgv=1;
	}
	if (Va==0) 
	{
	  Va=1;
	}
		
	delta_gamma =(kgt/kgv)*(T/Va)*(T_Gen-T_Gen_k);
	delta_gamma2=k1*(T_Sr0-tsn);
	difftime0=(Gamma0-Gamma0_k);
	//difftime0=Gamma0/1000;
	difftime0=difftime0+delta_gamma+delta_gamma2;
	//printf("Am0=");printf("%u",Am0);printf(" \n");
	W=AA+BB*(a0+a1*(Am0-Am0_k)*10+a2*(Haw0-Haw0_k)/1000+a3*difftime0/1000+a4*(T_Gen-T_Gen_k)/100+a5*(T_Sr0-T_Sr0_k)/100);
	printf("W=%f\n",W);
}



char Kalibrovka() // Regim kalibrovki
{
	PrintStack("Kalibrovka");
	
	int i;
	char Key, tempchar;
	float y,z;
	
	ClrScr();
	SchablonKalibr();
Kalibr:
	char PosX=0xA, PosY=0x10;

	SetCursor(0xA,0x10);
	Key=0x0;
	if (VO!=0)
	{
		ComReadIntReg(0x2,0x31,0x1);
		Rel=ComDataRead[3];
		Rel=Rel ^  0x01;
		RegistrTM_int[1]=Rel;
		ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"Очистка   ");
		xtim_n=clock();
		do 
		{
			xtim=clock();
			tempchar=ReadKeyBuffered();
			if (tempchar=='1'||
			  tempchar=='3'||
			  tempchar=='5'||
			  tempchar=='7'||
			  tempchar=='9') 
			{
				Key=tempchar;		  
			};
			if (IS_EXPERIMENT 
			  && Key=='1'
			||Key=='3'
			||Key=='7') 
			{
				return Key;
			};

		} while ((xtim/1000-xtim_n/1000) <VO);
		Rel=Rel ^ 0x01;
		RegistrTM_int[1]=Rel;
		ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"          ");             
	};
	SleepWithService(1000);
	PrintXY(0,3,"Измерение ");
	SleepWithService(50);
	tempchar=ReadDataSPMCurrent();
	Calc();
	WriteUBI();
	dataSent = true;   

	Aout();
	if (W>(((float)UstRele)/10))
	{
		Rele3=1;     
	} 
	else 
	{
		Rele3=0;    
	};
	if (Rele3 !=Rele3_old)
	{
		Rel=Rel^0x04;     
	};
	RegistrTM_int[1]=Rel;
	ComWriteIntReg(0x2,0x31,0x1);
	Rele3_old=Rele3;

	printf("W=%f\nTgen=%u\nTsr=%u\nT3=%u\nAlfa0=%u\nBeta0=%u\nGamma0=%u\n",W,T_Gen,T_Sr0,T_Sr1,Am0,Haw0,Gamma0);
	y=Am0_k*10; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i-1]=z+48;     
	};
	PrintXYL(7,5,5);
	y=Haw0_k; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[0]=Text[1];
	Text[1]=Text[2];
	Text[2]='.';
	PrintXYL(7,6,5);
	y=Gamma0_k; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[0]=Text[1];
	Text[1]=Text[2];
	Text[2]='.';
	PrintXYL(7,7,5);
	//
	y=Am0*10; 
	for (i=5;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i-1]=z+48;     
	};
	PrintXYL(7,9,5);
	y=Haw0; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[0]=Text[1];
	Text[1]=Text[2];
	Text[2]='.';
	PrintXYL(7,10,5);
	y=Gamma0; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[0]=Text[1];
	Text[1]=Text[2];
	Text[2]='.';
	PrintXYL(7,11,5);
	PrintXY(0,3,"          ");

	if (T_Gen_k<0) 
	{
		Text[0]='-';     
	} 
	else 
	{
		Text[0]='+';    
	};
	y=T_Gen_k; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10);
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[1]=Text[2];
	Text[2]=Text[3];
	Text[3]='.';
	PrintXYL(15,5,5);

	if (T_Sr0_k<0) 
	{
		Text[0]='-';     
	} 
	else 
	{
		Text[0]='+';    
	};
	y=T_Sr0_k; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[1]=Text[2];
	Text[2]=Text[3];
	Text[3]='.';
	PrintXYL(15,6,5);

	//
	if (T_Gen<0) 
	{
		Text[0]='-';     
	}  
	else 
	{
		Text[0]='+';    
	};
	y=T_Gen; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[1]=Text[2];
	Text[2]=Text[3];
	Text[3]='.';
	PrintXYL(15,9,5);


	if (T_Sr0<0) 
	{
		Text[0]='-';     
	} 
	else 
	{
		Text[0]='+';    
	};
	y=T_Sr0; 
	for (i=5;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		Text[i]=z+48;     
	};
	Text[1]=Text[2];
	Text[2]=Text[3];
	Text[3]='.';
	PrintXYL(15,10,5);

	do 
	{
		if (Key=='9'||
		  Key=='3'||
		  Key=='5'||
		  Key=='7'||
		  Key=='1')
		{	  
			if (Key=='5') 
			{
				Key=ReadKeyBuffered();
				goto Kalibr;      
			};
			if (Key=='9') 
			{
				SaveKalibr();				   
				SleepWithService(100); 
				Reader();
				Va=Saw_A1;
				Key=WaitNoKey();
				PrintXY(11,1,"Save Ok");
				SleepWithService(1000);
				PrintXY(11,1,"       ");
				SetCursor(PosX,PosY);
			}; //Save
			if (Key=='1'||
			  Key=='3'||
			  Key=='7') 
			{
				return Key;
			};
		};
		Key=WaitKey();
	} while (Key=='1'||
	Key=='3'||
	Key=='5'||
	Key=='7'||
	Key=='9');

Exk:
	return Key;
}



char Rabota() // Regim raboty
{
	PrintStack("Rabota");
  	int i;
	char  Key = 0, tempchar;
	float y,z;
	ClrScr();
	SchablonRabota();	
	SetCursor(0xA,0x10);
Rab:
	if (VO!=0)
	{
		ComReadIntReg(0x2,0x31,0x1);
		Rel=ComDataRead[3];
		Rel=Rel ^  0x01;
		RegistrTM_int[1]=Rel;
		ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"Очистка   ");
		xtim_n=clock();
		do
		{
			xtim=clock();
			tempchar=ReadKeyBuffered();
			if (tempchar=='3'||
			  tempchar=='5'||
			  tempchar=='7') 
			{
				Key=tempchar;		   
			};
			if (IS_EXPERIMENT && 
			  Key=='3'||
			  Key=='5'||
			  Key=='7') 
			{
				return Key;		  
			};
		} while ((xtim/1000-xtim_n/1000) <VO);
		Rel=Rel ^ 0x01;
		RegistrTM_int[1]=Rel;
		ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"          ");             
	};
	SleepWithService(1000);
	if (VZ!=0)
	{
		ComReadIntReg(0x2,0x31,0x1);
		Rel=ComDataRead[3];
		Rel=Rel ^  0x02;
		RegistrTM_int[1]=Rel;
		tempchar=ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"Загрузка  ");
		xtim_n=clock();
		do 
		{
			xtim=clock();
			tempchar=ReadKeyBuffered();
			if (tempchar=='3'||
			  tempchar=='5'||
			  tempchar=='7') 
			{
				Key=tempchar;		   
			};
			if (IS_EXPERIMENT &&
			  Key=='3'||
			  Key=='5'||
			  Key=='7') 
			{
				return Key;
			};

		} while ((xtim/1000-xtim_n/1000) <VZ);
		Rel=Rel ^ 0x02;
		RegistrTM_int[1]=Rel;
		ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"          ");
	};

	xtim_n=clock();
	do
	{
		xtim=clock();
		tempchar=ReadKeyBuffered();
		if (tempchar=='3'||
		  tempchar=='5'||
		  tempchar=='7')
		{
			Key=tempchar;
		};
		if (IS_EXPERIMENT &&
		  Key=='3'||
		  Key=='5'||
		  Key=='7') 
		{
			return Key;
		};

		PrintXY(0,3,"Измерение ");
		SleepWithService(servicetime);

		tempchar=ReadDataSPMCurrent();
		Calc();
		TimedIncrementT_Gen_T_Sr0_T_Sr1_W(false, T_Gen, T_Sr0, T_Sr1, W);

		WriteUBI();

		if (CheckFilling() &&W>=Wmin && W<=Wmax)
		{
			y=W*1000;
			for (i=5;i!=0;i--)
			{
				z=fmod(y,10); 
				y=floor(y/10); 
				Text[i]=z+48;		     
			};
			Text[0]=Text[1];
			Text[1]=Text[2];
			PrintXYL(14,5,2);
			Text[0]='.';
			Text[1]=Text[3];
			Text[2]=Text[4];
			PrintXYL(16,5,3);
		}
		else if (W<Wmin)
		{
			PrintXY(11,5,"<Wmin     ");	     
		}
		else if (W>Wmax) 
		{
			PrintXY(11,5,">Wmax     ");	     
		};

		if (T_Sr0<0) 
		{
			Text[0]='-';	     
		} 
		else 
		{
			Text[0]='+';	     
		};
		y=T_Sr0; 
		for (i=5;i!=0;i--)
		{
			z=fmod(y,10); 
			y=floor(y/10); 
			Text[i]=z+48;	     
		};
		Text[1]=Text[2];
		Text[2]=Text[3];
		Text[3]='.';
		PrintXYL(13,8,6);
		PrintXY(0,3,"          ");
		PrintXY(3,4,"              ");
		
		if(!CheckFilling())
		{
			PrintXY(14,5,"           ");
			PrintXY(3,4,"Нет заполнения");
		}
	} while ((xtim/1000-xtim_n/1000) <VI);

	TimedIncrementT_Gen_T_Sr0_T_Sr1_W(true);
	if(!CheckFilling())
	{
		W = 0.0f;
	}

	Aout();

	if (W>(((float)UstRele)/10))
	{
		Rele3=1;     
	} 
	else 
	{
		Rele3=0;    
	};
	if (Rele3 !=Rele3_old)
	{
		Rel=Rel^0x04;     
	};
	RegistrTM_int[1]=Rel;
	ComWriteIntReg(0x2,0x31,0x1);
	Rele3_old=Rele3;
	tempchar=ReadKeyBuffered();
	if (tempchar=='3'||
	  tempchar=='5'||
	  tempchar=='7') 
	{
		Key=tempchar;	  
	};
	if (Key=='3'||
	  Key=='5'||
	  Key=='7') 
	{
		return Key;
	};

	goto Rab;

Exr:
	return Key;
}



char Graduirovka() // Regim graduirovki
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
	//          13-err adr komandy


{
	PrintStack("Graduirovka");
	int i;
	char Key=0;
	char tempchar;
	float y,z;
	ClrScr();
	SchablonGrad();
	
Grad:
	if (VO!=0)
	{SetCursor(0xA,0xF);
	ComReadIntReg(0x2,0x31,0x1);
	Rel=ComDataRead[3];
	Rel=Rel ^  0x01;
	RegistrTM_int[1]=Rel;
	ComWriteIntReg(0x2,0x31,0x1);
	PrintXY(0,3,"Очистка   ");
	xtim_n=clock();
	do
	{
		xtim=clock();
		tempchar=ReadKeyBuffered();
		if (tempchar=='1'||
		  tempchar=='5'||
		  tempchar=='7'||
		  tempchar=='9')
		{
			Key=tempchar;		   
		};
		if (IS_EXPERIMENT &&
		  Key=='1'||
		  Key=='5'||
		  Key=='7') 
		{
			return Key;
		};

	} while ((xtim/1000-xtim_n/1000) <VO);
	Rel=Rel ^ 0x01;
	RegistrTM_int[1]=Rel;
	ComWriteIntReg(0x2,0x31,0x1);
	PrintXY(0,3,"          ");
	SetCursor(0xA,0x10);
	};
	SleepWithService(1000);
	if (VZ!=0)
	{
		SetCursor(0xA,0xF);
		ComReadIntReg(0x2,0x31,0x1);
		Rel=ComDataRead[3];
		Rel=Rel ^  0x02;
		RegistrTM_int[1]=Rel;
		tempchar=ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"Загрузка  ");
		xtim_n=clock();
		do
		{
			xtim=clock();
			tempchar=ReadKeyBuffered();
			if (tempchar=='1'||
			  tempchar=='5'||
			  tempchar=='7'||
			  tempchar=='9')
			{
				Key=tempchar;		   
			};
			if (IS_EXPERIMENT &&
			  Key=='1'||
			  Key=='5'||
			  Key=='7')
			{
				return Key;
			};

		} while ((xtim/1000-xtim_n/1000) <VZ);
		Rel=Rel ^ 0x02;
		RegistrTM_int[1]=Rel;
		ComWriteIntReg(0x2,0x31,0x1);
		PrintXY(0,3,"          ");
		SetCursor(0xA,0x10);
	};
	xtim_n=clock();
	do 
	{
		xtim=clock();
		tempchar=ReadKeyBuffered();
		if (tempchar=='1'||
		  tempchar=='5'||
		  tempchar=='7'||
		  tempchar=='9') 
		{
			Key=tempchar;	 
		};
		if (IS_EXPERIMENT &&
		  Key=='1'||
		  Key=='5'||
		  Key=='7')
		{
			return Key;
		};

		PrintXY(0,3,"Измерение ");
		SleepWithService(50);
		tempchar=ReadDataSPMCurrent();
		Calc();
		WriteUBI();
		TimedIncrementT_Gen_T_Sr0_T_Sr1_W(false, T_Gen, T_Sr0, T_Sr1, W);
		printf("Beta0=%u\n",Haw0);
		printf("Gamma0=%u\n",Gamma0);

		y=W*1000; 
		for (i=5;i!=0;i--)
		{
			z=fmod(y,10); 
			y=floor(y/10); 
			Text[i]=z+48;     
		};
		Text[0]=Text[1];
		Text[1]=Text[2];
		Text[2]='.';
		PrintXYL(14,4,5);

		if (T_Gen<0) 
		{
			Text[0]='-';     
		} 
		else 
		{
			Text[0]='+';    
		};
		y=T_Gen; 
		for (i=5;i!=0;i--)
		{
			z=fmod(y,10); 
			y=floor(y/10); 
			Text[i]=z+48;     
		};
		Text[1]=Text[2];
		Text[2]=Text[3];
		Text[3]='.';
		PrintXYL(13,6,6);

		if (T_Sr0<0) 
		{
			Text[0]='-';     
		} 
		else 
		{
			Text[0]='+';    
		};
		y=T_Sr0; 
		for (i=5;i!=0;i--) 
		{
			z=fmod(y,10);
			y=floor(y/10);
			Text[i]=z+48;     
		};
		Text[1]=Text[2];
		Text[2]=Text[3];
		Text[3]='.';
		PrintXYL(13,7,6);

		if (T_Sr1<0) 
		{
			Text[0]='-';     
		} 
		else 
		{
			Text[0]='+';    
		};
		y=T_Sr1;
		for (i=5;i!=0;i--)
		{
			z=fmod(y,10); 
			y=floor(y/10); 
			Text[i]=z+48;     
		};
		Text[1]=Text[2];
		Text[2]=Text[3];
		Text[3]='.';
		PrintXYL(13,8,6);

		//
		y=Am0*10; 
		for (i=5;i!=0;i--) 
		{
			z=fmod(y,10);
			y=floor(y/10);
			Text[i-1]=z+48;     
		};
		PrintXYL(14,10,5);
		y=Haw0; 
		for (i=5;i!=0;i--) 
		{
			z=fmod(y,10); 
			y=floor(y/10); 
			Text[i]=z+48;     
		};
		Text[0]=Text[1];
		Text[1]=Text[2];
		Text[2]='.';
		PrintXYL(14,11,5);
		y=Gamma0; 
		for (i=5;i!=0;i--) 
		{
			z=fmod(y,10);
			y=floor(y/10); 
			Text[i]=z+48;     
		};
		Text[0]=Text[1];
		Text[1]=Text[2];
		Text[2]='.';
		PrintXYL(14,12,5);
		PrintXY(0,3,"          ");
	} 
	while ((xtim/1000-xtim_n/1000) <VI);

	TimedIncrementT_Gen_T_Sr0_T_Sr1_W(true);

	Aout();

	if (W>(((float)UstRele)/10))
	{
		Rele3=1;     
	} else 
	{
		Rele3=0;    
	};
	if (Rele3 !=Rele3_old)
	{
		Rel=Rel^0x04;     
	};
	RegistrTM_int[1]=Rel;
	ComWriteIntReg(0x2,0x31,0x1);
	Rele3_old=Rele3;

	tempchar=ReadKeyBuffered();
	if (tempchar=='9'||
	  tempchar=='5'||
	  tempchar=='7'||
	  tempchar=='1') 
	{
		Key=tempchar;     
	};

	if (Key=='9')  //Save
	{
		printf("Save");
		printf(" \n");
		Key='Z'; 
		SleepWithService(2000);      
	};
	if (Key=='1'||Key=='5'||Key=='7') 
	{
		return Key;
	};

	goto Grad;

Exg:
	return Key;
}



char Nastrojka() // Regim nastrojki
{
	PrintStack("Nastrojka");
	ClrScr();
	SchablonNastrojka();
	int i, j;
	char PosY,PosX, Key;
	char temp[7][3];
	char tempchar;
	float y,z;

	PosY=0;
	PosY++;
	PosY++;
	y=VZ;
	for (i=3;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;

	};
	PosY++;
	y=VO;
	for (i=3;i!=0;i--) 
	{
		z=fmod(y,10);
		y=floor(y/10);
		temp[PosY][i-1]=z+48;   
	};
	PosY++;
	y=VI; 
	for (i=3;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	PosY++;
	y=Tak; 
	for (i=3;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	PosY++;
	y=UstRele;
	for (i=3;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	PosY++;

Nastr:
	strcpy(Text, SetMediumRow());
	PrintXYL(8, 0, strlen(SetMediumRow()) < 13 ? strlen(SetMediumRow()) : 12 );

	for (j=0;j<7;j++)
	{
		if (j<=1) continue;
		if(j==6)
		{
			strcpy(Text, emptyrow);
			for (i=0;i<2;i++)
			{
				Text[i]=temp[j][i];
			};
			Text[2]='.';
			for (i=0;i<1;i++)
			{
				Text[3+i]=temp[j][2+i];
			};
			PrintXYL(15,j,4);
			continue;
		}
		strcpy(Text, emptyrow);
		for (i=0;i<3;i++)
		{
			Text[i]=temp[j][i];		 
		};
		PrintXYL(16,j,3);		 
	};

	if (AK==0) 
	{
		Text[2]='ч';
		Text[1]='у';
		Text[0]='р';     
	} 
	else 
	{
		Text[2]='т';
		Text[1]='в';
		Text[0]='а';    
	};
	PrintXYL(14,7,3);
	//if(Aout_tip == 0) Aout_tip = 1;
	if (Aout_tip==1) 
	{
		Text[3]=' ';
		Text[2]='5';
		Text[1]='-';
		Text[0]='0';     
	};
	if (Aout_tip==2) 
	{
		Text[3]='0';
		Text[2]='2';
		Text[1]='-';
		Text[0]='0';     
	};
	if (Aout_tip==3) 
	{
		Text[3]='0';
		Text[2]='2';
		Text[1]='-';
		Text[0]='4';     
	};
	PrintXYL(14,8,4);

	PosX=0; 
	PosY=0;
	SetCursor(PosX, PosY);

	do
	{
		tempchar=ReadKeyBuffered();

		if (tempchar=='9'||
		  tempchar=='5'||
		  tempchar=='3'||
		  tempchar=='1'||
		  tempchar=='2'||
		  tempchar=='8'||
		  tempchar=='S') 
		{
			Key=tempchar;     
		};

		if (Key=='2') 
		{
			PosY++; 
			if (PosY>12) 
				PosY=0;
			SetCursor(PosX, PosY);
			do
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');      
		};
		if (Key=='8') 
		{
			PosY--; 
			if (PosY>20)
				PosY=12; 
			SetCursor(PosX, PosY);
			do
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');};
		if (Key=='S') 
		{
			if (PosY==7) 
			{
				AK++; 
				if (AK>1) 
					AK=0;
				if (AK==0) 
				{
					Text[2]='ч';
					Text[1]='у';
					Text[0]='р';	  
				}
				else 
				{
					Text[2]='т';
					Text[1]='в';
					Text[0]='а';	  
				};
				PrintXYL(14,7,3);
				do
				{
					Key=ReadKeyBuffered();	  
				} while (Key!='Z');

			};
			if (PosY==8)
			{
				Aout_tip++;
				if (Aout_tip>3) 
					Aout_tip=1;
				if (Aout_tip==1)
				{
					Text[3]=' ';
					Text[2]='5';
					Text[1]='-';
					Text[0]='0';	  
				};
				if (Aout_tip==2)
				{
					Text[3]='0';
					Text[2]='2';
					Text[1]='-';
					Text[0]='0';	  
				};
				if (Aout_tip==3) 
				{
					Text[3]='0';
					Text[2]='2';
					Text[1]='-';
					Text[0]='4';	  
				};
				PrintXYL(14,8,4);
				do
				{
					Key=ReadKeyBuffered();	  
				} while (Key!='Z');	
			};
			struct timespec tw = {0, 500 * 1000000};
			struct timespec tr;
			nanosleep(&tw, &tr); 
			if (PosY==9)
			{
				ClrScr();
				nanosleep(&tw, &tr); 
				SchablonNSlug();
				NSlug();
				ClrScr();
				nanosleep(&tw, &tr); 
				SchablonNastrojka();
				do 
				{
					Key=ReadKeyBuffered();	  
				} while (Key!='Z');
				goto Nastr;	
			};
			if (PosY==11) 
			{
				ClrScr();
				nanosleep(&tw, &tr); 
				SchablonPorog();
				Porog();
				ClrScr();
				nanosleep(&tw, &tr); 
				SchablonNastrojka();
				do 
				{
					Key=ReadKeyBuffered();	  
				} while (Key!='Z');
				goto Nastr;	
			};
			if (PosY==0)
			{
				if(media_count > 1)
				{
					SetMediumRow(1);
					SetMediumRow();
					strcpy(mediumName, fullrow);
					mediumName[strlen(fullrow)] = 0;
					Reader();
					return '7';	  
				}	
			}
			if (PosY==2||
			  PosY==3||
			  PosY==4||
			  PosY==5)
			{
				PosX=16;
				Key='Z';
				for (i=PosX;i<PosX+3;i++)                    
				{
					Key='Z';
					SetCursor(i, PosY);
					do 
					{
						Key=ReadKeyBuffered();	    
					} while (Key=='Z'||Key=='S');
					Text[0]=Key;
					PrintXYL(i,PosY,1);
					temp[PosY][i-16]=Text[0];
					struct timespec tw = {1, 0};
					struct timespec tr;
					nanosleep(&tw, &tr);
					do 
					{
						Key=ReadKeyBuffered();	    
					} while (Key!='Z');	  
				};
				PosX=0;
				SetCursor(PosX, PosY);	
			};
			if(PosY==6)
			{
				PosX=16; 
				Key='Z';
				int index = 0;
				for (i=PosX-1;i<PosX+1;i++)
				{
					Key='Z';
					SetCursor(i, PosY);
					do 
					{
						Key=ReadKeyBuffered();	    
					} while (Key=='Z'||Key=='S');
					Text[0]=Key;
					PrintXYL(i,PosY,1);
					temp[PosY][index++]=Text[0];
					struct timespec tw = {1, 0};
					struct timespec tr;
					nanosleep(&tw, &tr);
					do 
					{
						Key=ReadKeyBuffered();	    
					} while (Key!='Z');	  
				};
				Key='Z';
				SetCursor(PosX+2, PosY);
				do
				{
					Key=ReadKeyBuffered();	  
				} while (Key=='Z'||Key=='S');
				Text[0]=Key;
				PrintXYL(PosX+2,PosY,1);
				temp[PosY][index]=Text[0];
				struct timespec tw1 = {1, 0};
				struct timespec tr;
				nanosleep(&tw1, &tr);
				do 
				{
					Key=ReadKeyBuffered();	  
				} while (Key!='Z');
				PosX=0;
				SetCursor(PosX, PosY);	
			}
			struct timespec tw2 = {0, 500 * 1000000};
			if (PosY==10)
			{
				ClrScr();
				nanosleep(&tw2, &tr);
				SchablonNGrad();
				NGrad();
				ClrScr();
				nanosleep(&tw2, &tr);
				SchablonNastrojka();
				do 
				{
					Key=ReadKeyBuffered();	  
				} while (Key!='Z');
				goto Nastr;	
			};
			if(PosY==12 && NewMedium())
			{
				return '7';	
			}      
		};
		if (Key=='9') 
		{
			PosY=0;
			PosY++;
			PosY++;
			VZ=(temp[PosY][0]-48)*100+(temp[PosY][1]-48)*10+temp[PosY][2]-48;
			PosY++;
			VO=(temp[PosY][0]-48)*100+(temp[PosY][1]-48)*10+temp[PosY][2]-48;
			PosY++;
			VI=(temp[PosY][0]-48)*100+(temp[PosY][1]-48)*10+temp[PosY][2]-48;
			PosY++;
			Tak=(temp[PosY][0]-48)*100+(temp[PosY][1]-48)*10+temp[PosY][2]-48;
			PosY++;
			UstRele=(temp[PosY][0]-48)*100+(temp[PosY][1]-48)*10+temp[PosY][2]-48;
			PosY++;
			SaveNastr();
			struct timespec tw = {0, 100 * 1000000};
			struct timespec tw1 = {1, 0};
			struct timespec tr;
			nanosleep(&tw, &tr);
			Reader();
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');
			PrintXY(11,1,"Save Ok");
			nanosleep(&tw1, &tr);
			PrintXY(11,1,"       ");
			SetCursor(PosX,PosY);
		}; //Save
		if (Key=='1'||
		  Key=='3'||
		  Key=='5')
		{
			return Key;
		};     
	} while (true);

	goto Nastr;

Exn:
	return Key;
}



char Porog() // Regim slugebnojn nastrojki
{
	PrintStack("Porog");
	int i,j;
	char PosY,PosX, Key;
	char temp[15][8];
	char tempchar;
	float y,z;

	struct timespec tw = {0, 200 * 1000000};
	struct timespec tr;
	PosY=3;
	y=Poralfa*1000; 
	for (i=8;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;    
	};

	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';

	PosY++;
	y=Porbeta*1000; 
	for (i=8;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;    
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	
	y=Porgamma*1000; 
	for (i=8;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;    
	};

	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	PosY++;
	
	for (j=3;j<6;j++)
	{ 
		strcpy(Text, emptyrow);
		for (i=0;i<8;i++)
		{
			Text[i]=temp[j][i];  
		};
		PrintXYL(12,j,8);
	};



	if (Nzp==0) 
	{
		PrintXY(9,7,"Не использ.");
		struct timespec tw1 = {0, 100 * 1000000};
		nanosleep(&tw1, &tr);    
	};

	if (Nzp==1) 
	{
		PrintXY(9,7,"alfa > por ");
		nanosleep(&tw, &tr);    
	};
	if (Nzp==2) 
	{
		PrintXY(9,7,"alfa < por ");
		nanosleep(&tw, &tr);    
	};
	if (Nzp==3) 
	{
		PrintXY(9,7,"beta > por ");
		nanosleep(&tw, &tr);    
	};
	if (Nzp==4) 
	{
		PrintXY(9,7,"beta < por ");
		nanosleep(&tw, &tr);    
	};
	if (Nzp==5) 
	{
		PrintXY(9,7,"gamma > por");
		nanosleep(&tw, &tr);    
	};
	if (Nzp==6) 
	{
		PrintXY(9,7,"gamma < por");
		nanosleep(&tw, &tr);    
	};

	PosX=0; 
	PosY=3; 
	SetCursor(PosX, PosY);
	do {
		tempchar=ReadKeyBuffered();
		if (tempchar=='5'||
		  tempchar=='9'||
		  tempchar=='C'||
		  tempchar=='2'||
		  tempchar=='8'||
		  tempchar=='S') 
		{
			Key=tempchar;     
		};

		if (Key=='2') 
		{
			PosY++; 
			if (PosY>7) 
			{
			  PosY=3;
			}
				
			SetCursor(PosX, PosY);
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');      
		};
		if (Key=='8') 
		{
			PosY--; 
			if (PosY<3) 
			{
			  PosY=7;
			}
				
			SetCursor(PosX, PosY);
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');      
		};
		if (Key=='S') 
		{
			if (PosY==3||
			  PosY==4||
			  PosY==5)
			{
				PosX=12;
				Key='Z';
				for (i=PosX;i<PosX+8;i++)
				{
					Key='Z';
					SetCursor(i, PosY);          
					if (i==16) 
						goto npl1;
					do 
					{
						Key=ReadKeyBuffered();	      
					} while (Key=='Z'||Key=='S');
					Text[0]=Key;
					PrintXYL(i,PosY,1);
					temp[PosY][i-12]=Text[0];

					nanosleep(&tw, &tr);
					do 
					{
						Key=ReadKeyBuffered();	      
					} while (Key!='Z');

npl1:;
				};
			};
			if (PosY==7)
			{          
				Nzp++; 
				if (Nzp>6) 
				{
					Nzp=0;	  
				};


				if (Nzp==0) 
				{
					PrintXY(9,7,"Не использ.");
				};
				if (Nzp==1) 
				{
					PrintXY(9,7,"alfa > por ");
				};
				if (Nzp==2) 
				{
					PrintXY(9,7,"alfa < por ");
				};
				if (Nzp==3) 
				{
					PrintXY(9,7,"beta > por ");
				};
				if (Nzp==4) 
				{
					PrintXY(9,7,"beta < por ");					
				};
				if (Nzp==5) 
				{
					PrintXY(9,7,"gamma > por");					
				};
				if (Nzp==6) 
				{
					PrintXY(9,7,"gamma < por");
					
				};
				nanosleep(&tw, &tr);	  
				do 
				{
					Key=ReadKeyBuffered();	  
				} while (Key!='Z');	
			};
			PosX=0;
			SetCursor(PosX, PosY);                

		};
		if (Key=='9')
		{
			PosY=3;

			Poralfa=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;
			Porbeta=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;
			Porgamma=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;

			SaveSet();

			nanosleep(&tw, &tr);
			Reader();
			PrintXY(13,2,"SaveOk");
			struct timespec tw1 = {1, 0};
			nanosleep(&tw1, &tr);
			PrintXY(13,2,"      ");
			SetCursor(PosX,PosY);
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');       
		}; //Save
		if (Key=='7') 
		{
			PosY=0;
			Saw_A0=(temp[PosY][0]-48)*1000000+(temp[PosY][1]-48)*100000+(temp[PosY][2]-48)*10000+(temp[PosY][3]-48)*1000+
				(temp[PosY][5]-48)*100+(temp[PosY][6]-48)*10+(temp[PosY][7]-48)*1;
			PosY++;

			Saw_A1=(temp[PosY][0]-48)*1000000+(temp[PosY][1]-48)*100000+(temp[PosY][2]-48)*10000+(temp[PosY][3]-48)*1000+
				(temp[PosY][5]-48)*100+(temp[PosY][6]-48)*10+(temp[PosY][7]-48)*1;
			PosY++;
			struct timespec tw = {5, 0};
			struct timespec tw1 = {0, 500 * 1000000};
			struct timespec tr;
			nanosleep(&tw, &tr);

			WriteDataSPMSet();
			EEPROMSave(0x01);
			nanosleep(&tw1, &tr);
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');      
		};
		if (Key=='5') 
		{
			ClrScr(); 
			struct timespec tw = {0, 500 * 1000000};
			struct timespec tr;
			nanosleep(&tw, &tr);
			Rezonans();
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');       
		}; //Rezonans

		if (Key=='C') 
		{
			Key='Z';
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z'); 
			goto Exnsl;      
		};    
	} while (true);

Exnsl:
	return 0;
}



char NSlug() // Regim slugebnojn nastrojki
{
	PrintStack("NSlug");
	
	int i,j;
	char PosY,PosX, Key,  tempchar;
	char temp[15][8];	
	float y,z;
	
	PosX=0; 
	PosY=0; 
	
	SetCursor(PosX, PosY);
	struct timespec tw = {0, 200 * 1000000};
	struct timespec tr;
	y=Saw_A0; 
	for (i=8;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;

	y=Saw_A1; 
	for (i=8;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	PosY++;

	y=Va*1000; 
	for (i=8;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;

	if (kgt<0)
	{
		y=kgt*(-1000);   
	} 
	else
	{
		y=kgt*1000;  
	};
	for (i=8;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	if (kgt<0) 
	{
		temp[PosY][0]='-';   
	} 
	else 
	{
		temp[PosY][0]='+';  
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;

	if (kgv<0) 
	{
		y=kgv*(-1000);   
	} 
	else 
	{
		y=kgv*1000;  
	};
	for (i=8;i!=0;i--) 
	{
		z=fmod(y,10);
		y=floor(y/10);
		temp[PosY][i-1]=z+48;   
	};
	if (kgv<0) 
	{
		temp[PosY][0]='-';   
	} 
	else
	{
		temp[PosY][0]='+';  
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;

	y=T*1000; 
	for (i=8;i!=0;i--)
	{
		z=fmod(y,10);
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	PosY++;
	PosY++;
	PosY++;
	PosY++;

	y=controlleradr; 
	for (i=2;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	PosY++;
	y=Ku0; 
	for (i=2;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10);
		temp[PosY][i-1]=z+48;   
	};
NastrSlug:
	for (j=0;j<7;j++)
	{ 
		strcpy(Text, emptyrow);
		for (i=0;i<8;i++)
		{
			Text[i]=temp[j][i];        
		};
		if(j != 2)
		{
			PrintXYL(12,j,8);
		}
	};
	j=11;
	strcpy(Text, emptyrow);
	for (i=0;i<2;i++)
	{
		Text[i]=temp[j][i];        
	};
	PrintXYL(18,j,2);
	j=12;
	strcpy(Text, emptyrow);
	for (i=0;i<2;i++)
	{
		Text[i]=temp[j][i];        
	};
	PrintXYL(18,j,2);
	PosX=0; PosY=0;

	do 
	{
		tempchar=ReadKeyBuffered();
		if (tempchar=='5'||
		  tempchar=='9'||
		  tempchar=='C'||
		  tempchar=='2'||
		  tempchar=='8'||
		  tempchar=='S') 
		{
			Key=tempchar;     
		};

		if (Key=='2') 
		{
			PosY++; 
			if (PosY>12) 
				PosY=0; 
			SetCursor(PosX, PosY);
			do
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');      
		};
		if (Key=='8') 
		{
			PosY--; 
			if (PosY>20) 
				PosY=12;
			SetCursor(PosX, PosY);
			do
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');      
		};
		if (Key=='S')
		{
			if (PosY==11||
			  PosY==12)
			{
				PosX=18; 
				Key='Z';
				for (i=PosX;i<PosX+2;i++)
				{
					Key='Z';
					SetCursor(i, PosY);
					do 
					{
						Key=ReadKeyBuffered();	    
					} while (Key=='Z'||Key=='S');
					Text[0]=Key;
					PrintXYL(i,PosY,1);
					temp[PosY][i-18]=Text[0];
					struct timespec tw = {0, 200 * 1000000};
					struct timespec tr;
					nanosleep(&tw, &tr);
					do 
					{
						Key=ReadKeyBuffered();	    
					} while (Key!='Z');	  
				};	
			} else
			{
				PosX=12; 
				Key='Z';
				for (i=PosX;i<PosX+8;i++)
				{
					if (i==16) 
						goto nsl1;
					Key='Z';
					SetCursor(i, PosY);
					do 
					{
						Key=ReadKeyBuffered();	    
					} while (Key=='Z'||Key=='S');
					Text[0]=Key;
					PrintXYL(i,PosY,1);
					temp[PosY][i-12]=Text[0];

					nanosleep(&tw, &tr);
					do
					{
						Key=ReadKeyBuffered();	    
					} while (Key!='Z');
nsl1:;	  
				};	
			};
			PosX=0;
			SetCursor(PosX, PosY);      
		};
		if (Key=='9') 
		{
			PosY=0;
			Saw_A0=(temp[PosY][0]-48)*1000000+(temp[PosY][1]-48)*100000+(temp[PosY][2]-48)*10000+(temp[PosY][3]-48)*1000+
				(temp[PosY][5]-48)*100+(temp[PosY][6]-48)*10+(temp[PosY][7]-48)*1;
			PosY++;
			Saw_A1=(temp[PosY][0]-48)*1000000+(temp[PosY][1]-48)*100000+(temp[PosY][2]-48)*10000+(temp[PosY][3]-48)*1000+
				(temp[PosY][5]-48)*100+(temp[PosY][6]-48)*10+(temp[PosY][7]-48)*1;
			PosY++;
			PosY++;
			Va=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;
			kgt=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001);
			PosY++;

			kgv=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001);
			PosY++;

			T=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;
			controlleradr=(temp[11][0]-48)*10+temp[11][1]-48;
			Ku0=(temp[12][0]-48)*10+temp[12][1]-48;
			Ku1=(temp[12][0]-48)*10+temp[12][1]-48;
			SaveSet();
			struct timespec tw = {0, 100 * 1000000};
			struct timespec tw1 = {0, 500 * 1000000};
			struct timespec tw2 = {1, 0};
			struct timespec tr;
			nanosleep(&tw, &tr); 
			Reader();
			Va=Saw_A1;
			WriteDataSPMSet();
			PrintXY(13,2,"SaveOk");
			nanosleep(&tw2, &tr); 
			PrintXY(13,2,"      ");
			SetCursor(PosX,PosY);
			do 
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');      
		}; //Save
		if (Key=='7')
		{ 
			PosY=0;
			Saw_A0=(temp[PosY][0]-48)*1000000+(temp[PosY][1]-48)*100000+(temp[PosY][2]-48)*10000+(temp[PosY][3]-48)*1000+
				(temp[PosY][5]-48)*100+(temp[PosY][6]-48)*10+(temp[PosY][7]-48)*1;
			PosY++;
			Saw_A1=(temp[PosY][0]-48)*1000000+(temp[PosY][1]-48)*100000+(temp[PosY][2]-48)*10000+(temp[PosY][3]-48)*1000+
				(temp[PosY][5]-48)*100+(temp[PosY][6]-48)*10+(temp[PosY][7]-48)*1;
			PosY++;

			struct timespec tw = {0, 500 * 1000000};
			struct timespec tw1 = {5, 0};
			struct timespec tr;
			nanosleep(&tw1, &tr);
			WriteDataSPMSet();
			EEPROMSave(0x01);
			nanosleep(&tw, &tr);
			do
			{
				Key=ReadKeyBuffered();	   
			} while (Key!='Z');      
		};
		if (Key=='5') 
		{
			ClrScr();
			struct timespec tw = {0, 500 * 1000000};
			struct timespec tr;
			nanosleep(&tw, &tr);
			Rezonans();
			do
			{
				Key=ReadKeyBuffered();	
			} while (Key!='Z');       
		}; //Rezonans

		if (Key=='C')
		{
			Key='Z';
			do 
			{
				Key=ReadKeyBuffered();	
			} 
			while (Key!='Z'); 
			goto Exnsl;      
		};
	}
	while (true);

Exnsl:
	return Key;
}



char Rezonans() // Poisk rezonansa
{
	PrintStack("Rezonans");
	int i,j;
	char PosY,PosX, Key;
	char temp[15][8];
	char tempchar;
	float y,z;

	struct timespec tw = {0, 200 * 1000000};
	struct timespec tr;

	PrintXY(0,0, "Поиск резонанса     ");
	PrintXY(0,3, "Подпорка            ");
	PrintXY(0,5, "Альфа               ");
	PrintXY(0,6, "Бета                ");
	PrintXY(0,7, "Гамма               ");
	PrintXY(0,9, "Нач. поиска        В");
	PrintXY(0,10,"Кон. поиска        В");
	PrintXY(0,11,"Шаг поиска         В");
	PrintXY(0,14,"F3 -Поиск           ");
	PrintXY(0,15,"Sh-Ред F5-Сохр С-вых");

	PosY=9;
	y=N_Rez*1000;
	for (i=8;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	y=K_Rez*1000; 
	for (i=8;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10);
		temp[PosY][i-1]=z+48;   
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	y=Step_Rez*1000; 
	for (i=8;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;   
	};
	temp[PosY][0]=temp[PosY][1];
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	for (j=9;j<12;j++)
	{ 
		strcpy(Text, emptyrow);
		for (i=0;i<8;i++)          
		{
			Text[i]=temp[j][i];     
		};
		PrintXYL(12,j,8);   
	};
	PosX=0; 
	PosY=9;
	SetCursor(PosX,PosY);

	do 
	{
		tempchar=ReadKeyBuffered();
		if (tempchar=='9'||tempchar=='5'||tempchar=='C'||tempchar=='2'||tempchar=='8'||tempchar=='S') 
		{
			Key=tempchar;	
		};
		//5 (F3) - Poisk rezonansa 9 (F5) - save nastr C - vihod  S - redaktirovanie 2 - vniz 8 - vverh
		if (Key=='2')
		{
			PosY++; 
			if (PosY>11) 
				PosY=9;
			SetCursor(PosX, PosY);
			do 
			{
				Key=ReadKeyBuffered();	    
			} 
			while (Key!='Z');	  
		};
		if (Key=='8') 
		{
			PosY--; 
			if (PosY<9)
				PosY=11; 
			SetCursor(PosX, PosY);
			do
			{
				Key=ReadKeyBuffered();	    
			}
			while (Key!='Z');	  
		};
		if (Key=='C') 
		{
			Key='Z';
			do 
			{
				Key=ReadKeyBuffered();	    
			} 
			while (Key!='Z'); 
			goto Exrez;	  
		};
		if (Key=='S') 
		{
			PosX=12;
			Key='Z';
			for (i=PosX;i<PosX+8;i++)
			{
				if (i==16) 
					goto nr1;

				Key='Z';
				SetCursor(i, PosY);
				do
				{
					Key=ReadKeyBuffered();	      
				} 
				while (Key=='Z'||Key=='S');
				Text[0]=Key;
				PrintXYL(i,PosY,1);
				temp[PosY][i-12]=Text[0];

				nanosleep(&tw, &tr);
				do 
				{
					Key=ReadKeyBuffered();	      
				}
				while (Key!='Z');
nr1:;	    
			};
			PosX=0;
			SetCursor(PosX, PosY);	  
		};
		if (Key=='9') 
		{
			PosY=9;

			N_Rez=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;
			K_Rez=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;
			Step_Rez=(temp[PosY][0]-48)*1000+(temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001;
			PosY++;
			SaveNastr();
			struct timespec tw2 = {0, 100 * 1000000};
			struct timespec tr;
			nanosleep(&tw2, &tr);
			Reader();
			do 
			{
				Key=ReadKeyBuffered();	   
			} 
			while (Key!='Z');
			PrintXY(11,1,"Save Ok");
			struct timespec tw = {1, 0};
			nanosleep(&tw, &tr);
			PrintXY(11,1,"       ");
			SetCursor(PosX,PosY);
			PosX=0;
			PosY=0;
			SetCursor(PosX, PosY);
		}; //Save
		if (Key=='5') 
		{
			PosY=0;
			Saw_A0=N_Rez*1000;
			ComCMode(0x01,0x01);			
			do 
			{
				PosY=3;
				y=Saw_A0;
				for (i=8;i!=0;i--) 
				{
					z=fmod(y,10);
					y=floor(y/10); 
					temp[PosY][i-1]=z+48;
				};
				temp[PosY][0]=temp[PosY][1];
				temp[PosY][1]=temp[PosY][2];
				temp[PosY][2]=temp[PosY][3];
				temp[PosY][3]=temp[PosY][4];
				temp[PosY][4]='.';
				for (i=0;i<8;i++)
				{
					Text[i]=temp[PosY][i];
					PrintXYL(12,PosY,8);
				};
				nanosleep(&tw, &tr);
				WriteDataSPMSet();
				ComCMode(0x01,0x00);				
				ReadDataSPMCurrent();
				nanosleep(&tw, &tr);
				PosY=5;
				y=Am0*1000; 
				for (i=8;i!=0;i--) 
				{
					z=fmod(y,10); 
					y=floor(y/10); 
					temp[PosY][i-1]=z+48;	    
				};
				temp[PosY][0]=temp[PosY][1];
				temp[PosY][1]=temp[PosY][2];
				temp[PosY][2]=temp[PosY][3];
				temp[PosY][3]=temp[PosY][4];
				temp[PosY][4]='.';
				PosY++;
				y=Haw0; 
				for (i=8;i!=0;i--)
				{
					z=fmod(y,10); 
					y=floor(y/10); 
					temp[PosY][i-1]=z+48;
				};
				temp[PosY][0]=temp[PosY][1];
				temp[PosY][1]=temp[PosY][2];
				temp[PosY][2]=temp[PosY][3];
				temp[PosY][3]=temp[PosY][4];
				temp[PosY][4]='.';
				PosY++;
				y=Gamma0;
				for (i=8;i!=0;i--) 
				{
					z=fmod(y,10); 
					y=floor(y/10);
					temp[PosY][i-1]=z+48;
				};
				temp[PosY][0]=temp[PosY][1];
				temp[PosY][1]=temp[PosY][2];
				temp[PosY][2]=temp[PosY][3];
				temp[PosY][3]=temp[PosY][4];
				temp[PosY][4]='.';
				PosY++;
				for (j=5;j<8;j++)
				{
					for (i=0;i<8;i++)
					{
						Text[i]=temp[j][i];
						PrintXYL(12,j,8);
					};	    
				};  //
				Saw_A0=Saw_A0+Step_Rez*1000;	  
			} 
			while (Saw_A0<K_Rez*1000); //Cicl Rezonans
			do
			{
				Key=ReadKeyBuffered();
			} 
			while (Key!='Z');	
		};// Key 5

	}
	while (true);
Exrez:
	return Key;
}


bool NewMedium()
{
	PrintStack("NewMedium");
	newMediumStr[0] = 0;
	ClrScr();

	char tempchar = VirtualKeyboard(newMediumStr);
	if(tempchar == '9')
	{
		strcpy(mediumName, newMediumStr);
		mediumName[strlen(newMediumStr)] = 0;
		for(int i = 0; i < MEDIUM_COUNT; i++)
		{
			if(mediums[i][0] == 0)
			{
				strcpy(mediums[i], mediumName);
				mediums[i][strlen(mediumName)] = 0;

				media_count++;
				SetMediumRow(2);
				break;
			};
		}

		SetDefaultValues();
		SaveAll();
		return true;
	}

	return false;
}


char VirtualKeyboard(char * buffer)
{
	buffer[0]=0;
	PrintStack("VirtualKeyboard");
	ClrScr();
	SchablonKeyboard(" Новая градуировка: ", buffer);

	char tempchar = 0;
	int charindex = 0, index = 0;

	SetCursor(0,9);
	
	do
	{
		Sleep(800);
		tempchar = ReadKey();
		switch(tempchar)
		{
		case '4' :
			if(charindex > 0) 
				charindex--;
			Sleep(500);
			break;
		case '6' :
			if(charindex < strlen(abc_ru) - 1) 
				charindex++;
			Sleep(500);
			break;
		case '8' :
			if(charindex > 19) 
				charindex -= 20;
			Sleep(500);
			break;
		case '2' :
			if(charindex <= 15)
				charindex += 20;
			Sleep(500);
			break;
		case 'S' :
			if(index < 19)
			{
				buffer[index++] = abc_ru[charindex];
				buffer[index] = 0;
				PrintXY(0, 2, buffer, index);
			};
			break;
		case 'C' :
			printf("%s\n",buffer);
			if(index == 0) 
			{
			    buffer[0] = 0;
			    PrintXY(0, 2, emptyrow);
			}
			if(index > 0) 
			{
			    buffer[index--] = 0;
			}
			PrintXY(0, 2, buffer, index);
			break;
		}

		if(tempchar == '4' || tempchar == '6' || tempchar == '8' || tempchar == '2')
		{
			SetCursor(charindex % 20, charindex / 20 + 9);
			Sleep(500);
		}
		if(tempchar == '9' && strlen(buffer) > 0)
		{
			return tempchar;
		}
	}
	while(true);
}

////////////////////////////
char NGrad() // Regim nastrojki
{
	PrintStack("NGrad");
	int i,j;
	char PosY,PosX, Key;
	char temp[15][9];
	char tempchar;
	float y,z;
	PosX=0; PosY=0; SetCursor(PosX, PosY);
	PosY=0;
	struct timespec tw3 = {0, 200 * 1000000};
	struct timespec tr;
	if (a0<0)
	{
		y=a0*(-10000);   
	} 
	else 
	{
		y=a0*10000;  
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;  
	};
	if (a0<0) 
	{
		temp[PosY][0]='-';    
	}
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a1<0) 
	{
		y=a1*(-10000);
	} 
	else 
	{
		y=a1*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;
	};
	if (a1<0) 
	{
		temp[PosY][0]='-';
	} 
	else 
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a2<0) 
	{
		y=a2*(-10000);
	} 
	else 
	{
		y=a2*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;
	};
	if (a2<0) 
	{
		temp[PosY][0]='-';
	} 
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a3<0) 
	{
		y=a3*(-10000);   
	} 
	else 
	{
		y=a3*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10);
		temp[PosY][i-1]=z+48;
	};
	if (a3<0) 
	{
		temp[PosY][0]='-';
	} 
	else 
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a4<0) 
	{
		y=a4*(-10000);
	}
	else 
	{
		y=a4*10000;
	};
	for (i=9;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;
	};
	if (a4<0) 
	{
		temp[PosY][0]='-';
	} 
	else 
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a5<0) 
	{
		y=a5*(-10000);
	} 
	else 
	{
		y=a5*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10);
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;
	};
	if (a5<0) 
	{
		temp[PosY][0]='-';
	} 
	else 
	{
		temp[PosY][0]='+';    
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a6<0) 
	{
		y=a6*(-10000);
	} 
	else 
	{
		y=a6*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;
	};
	if (a6<0) 
	{
		temp[PosY][0]='-';
	} 
	else 
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a7<0) 
	{
		y=a7*(-10000);
	}
	else
	{
		y=a7*10000;
	};
	for (i=9;i!=0;i--)
	{
		z=fmod(y,10);
		y=floor(y/10);
		temp[PosY][i-1]=z+48;
	};
	if (a7<0) 
	{
		temp[PosY][0]='-';
	} 
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a8<0)
	{
		y=a8*(-10000);
	}
	else
	{
		y=a8*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10);
		y=floor(y/10);
		temp[PosY][i-1]=z+48;
	};
	if (a8<0) 
	{
		temp[PosY][0]='-';
	}
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (a9<0)
	{
		y=a9*(-10000);
	} 
	else
	{
		y=a9*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10);
		temp[PosY][i-1]=z+48;
	};
	if (a9<0) 
	{
		temp[PosY][0]='-';
	}
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (AA<0) 
	{
		y=AA*(-10000);
	}
	else
	{
		y=AA*10000;
	};
	for (i=9;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;
	};
	if (AA<0) 
	{
		temp[PosY][0]='-';
	} 
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (BB<0) 
	{
		y=BB*(-10000);
	} 
	else 
	{
		y=BB*10000;
	};
	for (i=9;i!=0;i--)
	{
		z=fmod(y,10);
		y=floor(y/10);
		temp[PosY][i-1]=z+48;
	};
	if (BB<0)
	{
		temp[PosY][0]='-';
	}
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (k1<0) 
	{
		y=k1*(-10000);
	} 
	else 
	{
		y=k1*10000;
	};
	for (i=9;i!=0;i--)
	{
		z=fmod(y,10); 
		y=floor(y/10);
		temp[PosY][i-1]=z+48;
	};
	if (k1<0)
	{
		temp[PosY][0]='-';
	} 
	else 
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (Wmax<0) 
	{
		y=Wmax*(-10000);
	} 
	else 
	{
		y=Wmax*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10);
		temp[PosY][i-1]=z+48;
	};
	if (Wmax<0)
	{
		temp[PosY][0]='-';
	}
	else 
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;
	if (Wmin<0) 
	{
		y=Wmin*(-10000);
	}
	else
	{
		y=Wmin*10000;
	};
	for (i=9;i!=0;i--) 
	{
		z=fmod(y,10); 
		y=floor(y/10); 
		temp[PosY][i-1]=z+48;
	};
	if (Wmin<0)
	{
		temp[PosY][0]='-';
	} 
	else
	{
		temp[PosY][0]='+';
	};
	temp[PosY][1]=temp[PosY][2];
	temp[PosY][2]=temp[PosY][3];
	temp[PosY][3]=temp[PosY][4];
	temp[PosY][4]='.';
	PosY++;

NastrGrad:
	for (j=0;j<15;j++)
	{ 
		strcpy(Text, emptyrow);
		for (i=0;i<9;i++)
		{
			Text[i]=temp[j][i];        
		};
		PrintXYL(6,j,9);
	};
	PosX=0; PosY=0;

	do {
		tempchar=ReadKeyBuffered();
		if (tempchar=='9'||
		  tempchar=='C'||
		  tempchar=='2'||
		  tempchar=='8'||
		  tempchar=='S') 
		{
		  Key=tempchar;
		};

		if (Key=='2')
		{
			PosY++; 
			if (PosY>14) 
				PosY=0; 
			SetCursor(PosX, PosY);
			do
			{
				Key=ReadKeyBuffered();
			}
			while (Key!='Z');
		};
		if (Key=='8') 
		{
			PosY--;
			if (PosY>20)
				PosY=14;
			SetCursor(PosX, PosY);
			do
			{
				Key=ReadKeyBuffered();
			} 
			while (Key!='Z');};
		if (Key=='S')
		{
			PosX=6; Key='Z';
			for (i=PosX;i<PosX+9;i++)
			{
				if (i==10) 
					goto ng1;
				Key='Z';
				SetCursor(i, PosY);
				do 
				{
ng3:
					Key=ReadKeyBuffered();
				}
				while (Key=='Z'||Key=='S');
				if (i==6) 
				{
					if (Key=='+'||Key=='-')
					{
						goto ng2;
					} 
					else 
					{
						goto ng3;
					};
				};
ng2:
				Text[0]=Key;
				PrintXYL(i,PosY,1);
				temp[PosY][i-6]=Text[0];	
				nanosleep(&tw3, &tr);
				do
				{
					Key=ReadKeyBuffered();
				}
				while (Key!='Z');
ng1:;
			};
			PosX=0;
			SetCursor(PosX, PosY);
		};
		if (Key=='9') 
		{ 
			PosY=0;
			a0=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001 +(temp[PosY][8]-48)*0.0001);
			PosY++;
			a1=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a2=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a3=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a4=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a5=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a6=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a7=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a8=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			a9=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			AA=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			BB=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			k1=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			Wmax=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			Wmin=(44-temp[PosY][0])*((temp[PosY][1]-48)*100+(temp[PosY][2]-48)*10+(temp[PosY][3]-48)+
				(temp[PosY][5]-48)*0.1+(temp[PosY][6]-48)*0.01+(temp[PosY][7]-48)*0.001+(temp[PosY][8]-48)*0.0001);
			PosY++;
			SaveGrad();
			struct timespec tw1 = {0, 100 * 1000000};
			struct timespec tr1;
			nanosleep(&tw1, &tr1); 
			Reader();
			do
			{
				Key=ReadKeyBuffered();
			}
			while (Key!='Z');
			PrintXY(14,1,"SaveOk");
			struct timespec tw = {1, 0};
			struct timespec tr;
			nanosleep(&tw, &tr);
			PrintXY(14,1,"      ");
			SetCursor(PosX,PosY);
		};//Save
		if (Key=='C') 
		{
			Key='Z';
			do
			{
				Key=ReadKeyBuffered();
			}
			while (Key!='Z'); 
			goto Exng;
		};     
	} 
	while (true);
	goto NastrGrad;
Exng:
	return Key;
}



char SchablonNSlug() // Schablon ekrana

{
	PrintStack("SchablonNSlug");
	
	PrintXY(0,0, "Подпорка   =        ");
	PrintXY(0,1, "Амплитуда  =        "); 
	PrintXY(0,3, "Va  =               ");
	PrintXY(0,4, "kgt =               ");
	PrintXY(0,5, "kgv =               ");
	PrintXY(0,6, "T   =               ");
	PrintXY(0,8, "Кол-во датчиков     ");
	PrintXY(0,9, "Ном. датчика 1      ");
	PrintXY(0,10,"Ном. датчика 2      ");
	PrintXY(0,11,"Ном. прибора        ");
	PrintXY(0,12,"К-т усиления        ");
	PrintXY(0,14,"F3 -Поиск резонанса ");
	PrintXY(0,15,"Sh-Ред F5-Сохр С-вых");
	return 0;
}



char SchablonPorog() // Schablon ekrana

{
	PrintStack("SchablonPorog");
	
	PrintXY(0,0, "    Выбор порога    ");
	PrintXY(0,3, "Порог alfa          ");
	PrintXY(0,4, "Порог beta          ");
	PrintXY(0,5, "Порог gamma         ");
	PrintXY(0,7, "Условие             ");
	PrintXY(0,15,"Sh-Ред F5-Сохр С-вых");
	return 0;
}



char SchablonNGrad() // Schablon ekrana
{
	PrintStack("SchablonNGrad");
	
	PrintXY(0,0,"a0  =               ");
	PrintXY(0,1,"a1  =               ");
	PrintXY(0,2,"a2  =               ");
	PrintXY(0,3,"a3  =               ");
	PrintXY(0,4,"a4  =               ");
	PrintXY(0,5,"a5  =               ");
	PrintXY(0,6,"a6  =               ");
	PrintXY(0,7,"a7  =               ");
	PrintXY(0,8,"Tg n=               ");
	PrintXY(0,9,"Ts n=               ");
	PrintXY(0,10,"AA  =               ");
	PrintXY(0,11,"BB  =               ");
	PrintXY(0,12,"k1  =               ");
	PrintXY(0,13,"Wmax=               ");
	PrintXY(0,14,"Wmin=               ");
	PrintXY(0,15,"Sh-Ред F5-Сохр С-вых");
	return 0;
}



char SchablonNastrojka() // Schablon ekrana
{
	PrintStack("SchablonNastrojka");
	
	PrintXY(0,0,"Среда :             ");
	//
	PrintXY(0,2,"Время заполн.      с");
	PrintXY(0,3,"Время очист.       с");
	PrintXY(0,4,"Время измер.       с");
	PrintXY(0,5,"Период калибр.     с");
	PrintXY(0,6,"Уставка реле     . %");
	PrintXY(0,7,"Тип калибр.         ");
	PrintXY(0,8,"Тип ток. вых.     мА");
	PrintXY(0,9,"Служебн. настр.     ");
	PrintXY(0,10,"Градуир. коэфф.     ");
	PrintXY(0,11,"Выбор порога        ");
	PrintXY(0,12,"Новая среда         ");
	//
	PrintXY(0,13,"Sh-Редактировать    ");
	PrintXY(0,14,"F1-Работа F2-Град.  ");
	PrintXY(0,15,"F3-Калибр F5-Сохран.");
	return 0;
}



char SchablonRabota() // Schablon ekrana
{
	PrintStack("SchablonRabota");
	
	PrintXY(0,0,GetCenteredRow(mediumName));
	//
	PrintXY(0,2,"Режим: Работа       ");
	PrintXY(0,5,"ВЛАЖНОСТЬ:         %");
	PrintXY(0,8,"Т среды:           С");
	PrintXY(0,14,"F2-Град.  F3-Калибр.");
	PrintXY(0,15,"F4-Настройка        ");
	return 0;
}



char SchablonGrad() // Schablon ekrana
{
	PrintStack("SchablonGrad");
	
	PrintXY(0,0,GetCenteredRow(mediumName));
	//
	PrintXY(0,2, "Режим: Градуировка  ");
	PrintXY(0,4, "Влажность:         %");
	PrintXY(0,6, "Т датчика:         С");
	PrintXY(0,7, "Т среды:           С");
	PrintXY(0,8, "Т датчика2:        С");
	//
	PrintXY(0,10,"Альфа:              ");
	PrintXY(0,11,"Бета :              ");
	PrintXY(0,12,"Гамма:              ");
	//
	PrintXY(0,14,"F1-Работа F3-Калибр.");
	PrintXY(0,15,"F4-Настр. F5-Сохран.");
	return 0;
}



char SchablonKalibr() // Schablon ekrana
{
	PrintStack("SchablonKalibr");
	
	PrintXY(0,0,GetCenteredRow(mediumName));
	//
	PrintXY(0,2, "Режим: Калибровка   ");
	PrintXY(0,5, "Альфа0:     Тг:     ");
	PrintXY(0,6, "Бета0 :     Тс:     ");
	PrintXY(0,7, "Гамма0:             ");
	//
	PrintXY(0,9, "Альфа :     Тг:     ");
	PrintXY(0,10,"Бета  :     Тс:     ");
	PrintXY(0,11,"Гамма :             ");
	//
	PrintXY(0,13,"F3-Калибровка       ");
	PrintXY(0,14,"F1-Работа F2-Град.  ");
	PrintXY(0,15,"F4-Настр. F5-Сохран.");
	return 0;
}



void SchablonKeyboard(char* title, char* buffer)
{
	PrintStack("SchablonKeyboard");
	PrintXY(0,0,title);
	PrintXY(0,2,emptyrow);
	PrintXY(0,5, "Up-Вверх Dn-Вниз    ");
	PrintXY(0,6, "Lf-Влево Rg-Вправо  ");
	PrintXY(0,9, "1234567890 abcdefghi");
	PrintXY(0,10,"jklmnopqrstuvwxyz   ");
	PrintXY(0,11,"                    ");
	PrintXY(0,12,"Sh-Вставка C-Стереть");
	PrintXY(0,13,"F5-Готово           ");
	PrintXY(0,14,"F1-Работа F2-Град.  ");
	PrintXY(0,15,"F3-Калибр. F4-Настр.");
}

char PrintXY(char X, char Y, const char * str) // Pechat stroki simvolov	
{
    return PrintXY(X, Y, str, strlen(str));
}

char PrintXY(char X, char Y, const char * str, int length) // Pechat stroki simvolov	
{
	PrintStack("PrintXY");
	
	PrintLock();
	pthread_mutex_lock(&g_LockData);

	int i;
	char len, pos=0;

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=0x02;//char 0
	ComDataWrite[pos++]=0x68;//char 1
	ComDataWrite[pos++]=X;//char 2
	ComDataWrite[pos++]=Y;//char 3
	
	utf8cp1251((char *)str, Text, length);
	
	for (i=0;i<length;i++)
	{
		ComDataWrite[pos++]=Text[i]; 
	};
	//pos=pos+length;
	CRCcount('w',pos);
	
	//Peredacha komandnoj stroki i priem otveta
	len=pos+2;
	int ret = write(hCom1,ComDataWrite,len);
	len=4;
	ret = read(hCom1,ComDataRead,len);
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);
	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4
	if (ComDataWrite[0]!=ComDataRead[0])
	{
		return 10;
	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{
		if (ComDataRead[1]!=ComDataWrite[1]+0x80)
		{
			return 11;
		} 
		else
		{	    
			return ComDataRead[2];
		};
	}
	else 
	{
		CRCcount('r',len);
	};
	
	return 0;
}


char SetCursor(char X, char Y) // Ustanovka kursora
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
	//          13-err adr komandy

	//-----Komanda-----
	//0-UstrAdr - adres ustrojstva (1 char)- =2-terminal;
	//1-UstrCodeFunc - Kod funkcii (1 char)- =0x67;
	//2-koordinata X (0-19) gorizont
	//3-koordinata Y (0-15) vertikal
	//4-CRC
	//5-CRC
	//-----Otvet-----
	//0-UstrAdr - adres ustrojstva (1 char)- =UstrAdr v komande;
	//1-UstrCodeFunc - Vozvrat koda funkcii (1 char)- =0x67 - esli OK;
	//=0xE7 - esli error
	//2 - esli error - kod oshibki    0x01 - err koda func
	//0x02 - err adr reg
	//0x03 - err data
	//2 - esli OK - CRC
	//3 - CRC
	//4 - esli error - CRC
	//4 - esli OK - net
{
	PrintStack("SetCursor");
	PrintLock();
	pthread_mutex_lock(&g_LockData);

	int i;
	char len;
	char pos=0;

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=0x02;//char 0
	ComDataWrite[pos++]=0x67; //char 1
	ComDataWrite[pos++]=X;//char 2 
	ComDataWrite[pos++]=Y;//char 3 
	CRCcount('w',pos);


	//Peredacha komandnoj stroki i priem otveta
	len=pos+2;
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++) 
	  {
		printf("%x ",ComDataWrite[i]);		
	  };
	}
	
	int ret = write(hCom1,ComDataWrite,len);
	ret = read(hCom1,ComDataRead,len);      
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);
	
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++)
	  {
		printf("read :            %x ", ComDataRead[i]);
	  };
	}

	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4
	//len=4;
	if (ComDataWrite[0]!=ComDataRead[0])
	{	
		return 10;
	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{
		if (ComDataRead[1]!=ComDataWrite[1]+0x80) 
		{
			return 11;
		} 
		else
		{
			return ComDataRead[2];
		};
	}
	else
	{
		CRCcount('r',len); // vichislenie CRC
	};           

	//proverka CRC
	if (ComDataRead[len]!=ComDataRead[len+2] ||
		ComDataRead[len+1]!=ComDataRead[len+1+2])
	{
		return 12;
	}

	return 0;
}



char ClrScr() // Ochistka ekrana
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
	//          13-err adr komandy

	//-----Komanda-----
	//0-UstrAdr - adres ustrojstva (1 char)- =2-terminal;
	//1-UstrCodeFunc - Kod funkcii (1 char)- =0x66;
	//2-CRC
	//3-CRC
	//-----Otvet-----
	//0-UstrAdr - adres ustrojstva (1 char)- =UstrAdr v komande;
	//1-UstrCodeFunc - Vozvrat koda funkcii (1 char)- =0x66 - esli OK;
	//=0xE6 - esli error
	//2 - esli error - kod oshibki    0x01 - err koda func
	//0x02 - err adr reg
	//0x03 - err data
	//2 - esli OK - CRC
	//3 - CRC
	//4 - esli error - CRC
	//4 - esli OK - net
{
	PrintStack("ClrScr");
	PrintLock();
	pthread_mutex_lock(&g_LockData);

	int i;
	char len;
	char pos=0;

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=0x02;//char 0
	ComDataWrite[pos++]=0x66; //char 1
	CRCcount('w',pos);


	//Peredacha komandnoj stroki i priem otveta
	len=pos+2;
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++) 
	  {
	    printf("%x ",ComDataWrite[i]);
	  };
	}
	int ret = write(hCom1,ComDataWrite,len);
	len=4;
	ret = read(hCom1,ComDataRead,len);
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);
	
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++)
	  {
		printf("read          %x ",ComDataRead[i]);
	  };
	}

	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4
	if (ComDataWrite[0]!=ComDataRead[0])
	{	
		return 10;
	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{
		if (ComDataRead[1]!=ComDataWrite[1]+0x80) 
		{
			return 11;
		} 
		else
		{
			return ComDataRead[2];	    
		};
	}
	else
	{
		CRCcount('r',len);// vichislenie CRC
	};            

	//proverka CRC
	if (ComDataRead[len]!=ComDataRead[len+2] ||
		ComDataRead[len+1]!=ComDataRead[len+1+2])
	{
		return 12;
	}
	return 0;
}



char EEPROMSave(char UstrAdr) // Zapis v EEPROM
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
	//          13-err adr komandy

	//-----Komanda-----
	//0-UstrAdr - adres ustrojstva (1 char)- =1-dathik; =2-terminal;
	//1-UstrCodeFunc - Kod funkcii (1 char)- =0x65;
	//2-CRC
	//3-CRC
	//-----Otvet-----
	//0-UstrAdr - adres ustrojstva (1 char)- =UstrAdr v komande;
	//1-UstrCodeFunc - Vozvrat koda funkcii (1 char)- =0x65 - esli OK;
	//=oxE5 - esli error
	//2 - esli error - kod oshibki    0x01 - err koda func
	//0x02 - err adr reg
	//0x03 - err data
	//2 - esli OK - CRC
	//3 - CRC
	//4 - esli error - CRC
	//4 - esli OK - net
{
	PrintStack("EEPROMSave");
	PrintLock();
	pthread_mutex_lock(&g_LockData);

	char len;
	char pos=0;

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=UstrAdr;
	ComDataWrite[pos++]=0x65; 
	CRCcount('w',pos);


	//Peredacha komandnoj stroki i priem otveta
	len=pos+2;
	int ret = write(hCom1,ComDataWrite,len);
	len=4;

	ret = read(hCom1,ComDataRead,len);
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);
		
	printf("read :        %d",ret);
	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4
	if (ComDataWrite[0]!=ComDataRead[0])
	{
		return 10;
	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{
	  if (ComDataRead[1]!=ComDataWrite[1]+0x80)
	  {
		return 11;
	  } 
	  else
	  {
		return ComDataRead[2];
	  };
	}
	else
	{
		CRCcount('r',len);
	};

	//proverka CRC
	if (ComDataRead[len]!=ComDataRead[len+2] ||
		ComDataRead[len+1]!=ComDataRead[len+1+2])
	{
		return 12;
	}
	return 0;
}



char WriteDataSPMSet() // Zapis nastroek SPM
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
{
	PrintStack("WriteDataSPMSet");
	char UstrAdr=1, StartAdr=0x0B,  temp, i = 0x0B;
	char Length=4;//<<< KOD PEREDACHI Dannih v DATCHIK (dolgen bit=2)
	
	RegistrSPM_int[i++]=Saw_A0;
	RegistrSPM_int[i++]=Saw_A1;
	RegistrSPM_int[i++]=Ku0;
	RegistrSPM_int[i++]=Ku1;
	RegistrSPM_int[i++]=Taq;
	RegistrSPM_int[i++]=Fwdt;
	RegistrSPM_int[i++]=Pcount;
	// todo : is commented registres used ?
	//BaudRateSPM=RegistrSPM_int[i];i++;
	//RegistrSPM_int[i]=HostSPM;i++;
	//RegistrSPM_int[i]=ValidReqSPM;i++;
	//RegistrSPM_int[i]=CRCReqSPM;i++;
	//RegistrSPM_int[i]=ExcReqSPM;i++;

	temp=ComWriteIntReg(UstrAdr,StartAdr,Length);
	
	Sleep(500);	
	if (temp !=0)
	{
	  return temp;
	}
	return 0;
}



char ComWriteIntReg(char UstrAdr,char StartAdr, char Length) // Zapis registrov
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
	//          13-err adr komandy

	//-----Komanda-----
	//0-UstrAdr - adres ustrojstva (1 char)- =1-dathik; =2-terminal;
	//1-UstrCodeFunc - Kod funkcii (1 char)- =0x45;
	//2-0        - start char zapisi - (2 char)
	//3-StartAdr - start char zapisi
	//4...(3+Length*2) - data zapisivaemie
	//(4+Length*2)...(5+Length*2)-CRC ;
	//-----Otvet-----
	//0-UstrAdr - adres ustrojstva (1 char)- =UstrAdr v komande;
	//1-UstrCodeFunc - Vozvrat koda funkcii (1 char)- =0x45 - esli OK;
	//=oxC5 - esli error
	//2 - esli error - kod oshibki    0x01 - err koda func
	//0x02 - err adr reg
	//0x03 - err data
	//2 - esli OK - CRC
	//3 - CRC
	//4 - esli error - CRC
	//4 - esli OK - net
{
	PrintStack("ComWriteIntReg");
	
	if (UstrAdr<1 || UstrAdr>2) 
	{
		return 10;
	};
	if (UstrAdr==1)
	{
		if (StartAdr>0x16 || StartAdr+Length>0x17)
		{
			return 13;
		};
	};
	if (UstrAdr==2)
	{
		if (StartAdr<0x30)
		{
			return 13;
		};
		if (StartAdr>0x35 || StartAdr+Length>0x36)
		{
			return 13;
		};
	};
	PrintLock();
	pthread_mutex_lock(&g_LockData);
	int i;
	char len,  pos=0;	

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=UstrAdr;//char 0
	ComDataWrite[pos++]=0x45;//char 1
	ComDataWrite[pos++]=0x00;//char 2
	ComDataWrite[pos++]=StartAdr;//char 3
	

	if (UstrAdr==1)
	{
	  for (i=0; i<Length; i++)
	  {
		ComDataWrite[pos++]=floor((float)RegistrSPM_int[StartAdr+i]/256);
		ComDataWrite[pos++]=fmod((float)RegistrSPM_int[StartAdr+i],256);
	  };
	};
	if (UstrAdr==2)
	{
	  for (i=0; i<Length; i++)
	  {
		ComDataWrite[pos++]=floor((float)RegistrTM_int[StartAdr-0x30+i]/256);
		ComDataWrite[pos++]=fmod((float)RegistrTM_int[StartAdr-0x30+i],256);
	  };
	};

	CRCcount('w',pos);

	//Peredacha komandnoj stroki i priem otveta
	len=pos+2;
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++) 
	  {
	    printf("%x ",ComDataWrite[i]);
	  };
	}
	
	int ret = write(hCom1,ComDataWrite,len);
	len=4;
	if(ExistReadBytes(hCom1, 1))
	{
	    ret = read(hCom1,ComDataRead,len);
	    printf("read :             %d", ret);
	}
	else
	{
	    printf("Ignore read!\n");
	}
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);
	  	
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++) 
	  {
		printf("read :         %x ",ComDataRead[i]);
	  };
	}

	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4
	if (ComDataWrite[0]!=ComDataRead[0])
	{	
		return 10;
	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{
	  if (ComDataRead[1]!=ComDataWrite[1]+0x80) 
	  {
		return 11;
	  } else              
	  {
		return ComDataRead[2];

	  };
	}
	else
	{
		CRCcount('r',len);// vichislenie CRC
	};

	//proverka CRC
	if (ComDataRead[len]!=ComDataRead[len+2] ||
		ComDataRead[len+1]!=ComDataRead[len+1+2])
	{
		return 12;
	}
	return 0;
}



char ReadDataSPMSet() // Chtenie nastroek SPM
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
{
	PrintStack("ReadDataSPMSet");
	char UstrAdr=1,StartAdr=0x0B,Length=12, temp,i=0x0B;
	
	temp=
	ComReadIntReg(UstrAdr,StartAdr,Length);
	if (temp !=0) 
	{
	  return temp;
	}
	Ku0=RegistrSPM_int[i++];
	Ku1=RegistrSPM_int[i++];
	Taq=RegistrSPM_int[i++];
	Fwdt=RegistrSPM_int[i++];
	Pcount=RegistrSPM_int[i++];
	BaudRateSPM=RegistrSPM_int[i++];
	HostSPM=RegistrSPM_int[i++];
	ValidReqSPM=RegistrSPM_int[i++];
	CRCReqSPM=RegistrSPM_int[i++];
	ExcReqSPM=RegistrSPM_int[i];	
	Sleep(500);
	return 0;
}



char ReadDataSPMCurrent() // Chtenie tekuchih dannih SPM
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC


{
	PrintStack("ReadDataSPMCurrent");
	char UstrAdr=1, StartAdr=0,Length=11, temp, i = 0;
	
	temp=ComReadIntReg(UstrAdr,StartAdr,Length);
	if (temp !=0) 
	{
	  return temp;
	}
	Gamma0=RegistrSPM_int[i++]*10;//mks
	Gamma1=RegistrSPM_int[i++]*10;
	Haw0=RegistrSPM_int[i++]*10;
	Haw1=RegistrSPM_int[i++]*10;
	Am0=RegistrSPM_int[i++];//mV
	Am1=RegistrSPM_int[i++];
	T_Gen=RegistrSPM_int[i++];//C *100
	T_Sr0=RegistrSPM_int[i++];
	T_Sr1=RegistrSPM_int[i++];
	Ain0=RegistrSPM_int[i++]; //mV
	Ain1=RegistrSPM_int[i++];

	return 0;
}



char ComReadIntReg(unsigned char UstrAdr,unsigned char StartAdr, unsigned char Length) // Chtenie registrov
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC
	//          13-err adr komandy

	//-----Komanda-----
	//0-UstrAdr - adres ustrojstva (1 char)- =1-dathik; =2-terminal;
	//1-UstrCodeFunc - Kod funkcii (1 char)- =0x44;
	//2-0        - start char chtenija - (2 char)
	//3-StartAdr - start char chtenija
	//4-Length - dlina (char) bloka chtenija
	//5-6-CRC ;
	//-----Otvet-----
	//0-UstrAdr - adres ustrojstva (1 char)- =UstrAdr v komande;
	//1-UstrCodeFunc - Vozvrat koda funkcii (1 char)- =0x44 - esli OK;
	//=oxC4 - esli error
	//2- esli error - kod oshibki  0x01 - err koda func
	//0x02 - err adr reg
	//0x03 - err data
	//2...(Length*2)+1  - esli OK - data
	//3-4-esli error - CRC
	//(Length*2)+2... (Length*2)+3 - esli OK - CRC
{
	PrintStack("ComReadIntReg");
	
	if (UstrAdr<1 || UstrAdr>2) 
	{
		return 10;
	};
	if (UstrAdr==1)
	{
	  if (StartAdr>0x16 || StartAdr+Length>0x17) 
	  {
		return 13;
	  };
	};
	if (UstrAdr==2)
	{
	  if (StartAdr<0x30) 
	  {
		  return 13;

	  };
	  if (StartAdr>0x35 || StartAdr+Length>0x36) 
	  {
		  return 13;
	  };	  
	};
	PrintLock();
	pthread_mutex_lock(&g_LockData);
	
	int i;
	char lenr,  pos=0;

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=UstrAdr;           //char 0
	ComDataWrite[pos++]=0x44;             //char 1
	ComDataWrite[pos++]=0x00;              //char 2
	ComDataWrite[pos++]=StartAdr;          //char 3
	ComDataWrite[pos++]=Length;            //char 4
	//for debug
	//      ComDataWrite[pos]=0x9c; pos++; //CRC for read one #30 register
	//      ComDataWrite[pos]=0xf0; pos++;

	CRCcount('w',pos);                         //char 5-6

	lenr=pos+2;


	//Peredacha komandnoj stroki i priem otveta
	int ret = write(hCom1,ComDataWrite,lenr);
	printf("write\n");
	lenr=(Length*2)+4;
	if(ExistReadBytes(hCom1, 1))
	{
	    ret = read(hCom1,ComDataRead,lenr);
	    printf("read\n");
	}
	else
	{
	    printf("read ignored!\n");
	}
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);

	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4
	if (ComDataWrite[0]!=ComDataRead[0])
	{
		return 10;

	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{if (ComDataRead[1]!=ComDataWrite[1]+0x80) 
	{
		return 11;

	} else
	{
		return ComDataRead[2];		
	};}
	else {
		CRCcount('r',(Length*2+2));
	};

	//proverka CRC
	if (ComDataRead[(Length*2)+2]!=ComDataRead[(Length*2)+2+2] ||
		ComDataRead[(Length*2)+3]!=ComDataRead[(Length*2)+3+2])
	{
		return 12;

	}
	if (UstrAdr==1){for (i=0; i<Length; i++)
	{
	  RegistrSPM_int[StartAdr+i]=ComDataRead[i*2+2]*256+ComDataRead[i*2+3];
	};
	};
	if (UstrAdr==2){for (i=0; i<Length; i++)
	{
	  RegistrTM_int[StartAdr+i]=ComDataRead[i*2+2]*256+ComDataRead[i*2+3];
	};
	};
	return 0;
}



char ComCMode(char UstrAdr,char UstrFuncArgument) // Ustanovka regima raboti
	// vozvrat: 0-OK,
	//          1-err koda func, 2- err adr reg, 3-err data;
	//          10-err adr ustr, 11-err koda func, 12-err CRC


	//-----Komanda-----
	//0-UstrAdr - adres ustrojstva (1 char)- =1-dathik; =2-terminal; =3-PC Com2;
	//1-UstrCodeFunc - Kod funkcii (1 char)- =0x42;
	//2-UstrMode - regim raboty (1 char)- =0- Ini i ciclicheskoe izmerenie (SPM);
	// =1- Ini i odnokratniy prohod piloy (SPM);
	// =2- Reset (SPM & TM);
	//3-4-UstrCRC - (2 char);
	//-----Otvet-----
	//0-UstrAdr - adres ustrojstva (1 char)- =UstrAdr v komande;
	//1-UstrCodeFunc - Vozvrat koda funkcii (1 char)- =0x42 - esli OK;
	//=oxC2 - esli error
	//2- esli error dalee kod oshibki  0x01 - err koda func
	//0x02 - err adr reg
	//0x03 - err data
	//2-3 (ili 3-4)-UstrCRC - (2 char);
{
	PrintStack("ComCMode");
	PrintLock();
	pthread_mutex_lock(&g_LockData);
	int i;
	char pos=0,length;
	
	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=UstrAdr;           //char 0
	ComDataWrite[pos++]=0x42;             //char 1
	ComDataWrite[pos++]=UstrFuncArgument;  //char 2
	CRCcount('w',pos);                         //char 3-4
	length=pos+2;

	//Peredacha komandnoj stroki i priem otveta
	if (UstrAdr==0x1 || UstrAdr==0x2)
	{
		printf("write\n");
		int ret = write(hCom1,ComDataWrite,length);
		if(ExistReadBytes(hCom1, 1))
		{
		  ret = read(hCom1,ComDataRead,length - 1); //todo : this is read have no data
		  printf("read :             %d", ret);
		}
		else
		{
		  printf("Ignore read!\n");
		}
	};
	if (UstrAdr==3)
	{
		//WriteFile(hCom2,&ComDataWrite,length,&ret,NULL);
		//ReadFile(hCom2,&ComDataRead,length-1,&ret,NULL);
	};
	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4
	//Rasshifrovka otveta
	//  if (ComDataWrite[0]!=ComDataRead[0]) return 10;
	//  if (ComDataWrite[1]!=ComDataRead[1])
	//      {if (ComDataRead[1]!= ComDataWrite[1]+0x80) {return 11;} else
	//           {return ComDataRead[2];};}
	//                else {CRCcount('r',2);};            // vichislenie CRC
	//  for (i=0;i<length+2;i++) {printf("%x",ComDataRead[i]);printf(" ");};
	//   printf("\n");
	//proverka CRC
	// if (ComDataRead[2]!=ComDataRead[2+2] || ComDataRead[3]!=ComDataRead[3+2]) return 12;
	PrintUnlock();
	pthread_mutex_unlock(&g_LockData);
	
	return 0;
}



char CRCcount(char rw, unsigned char Ln) // Vichislenie CRC
	// vozvrat: 0-vipolneno,
	// rw=='w' - vichislenie CRC dla heredachi (CRC dobavlaetsa v konce posilki)
	// rw=='r' - vichislenie CRC otveta (CRC dobavlaetsa posle prinjatogo CRC)
	// Len - chislo char CRC kotorih vichislaetsja
{
	PrintStack("CRCcount");
	unsigned char CRCcharLOW=0xFF ,CRCcharHIGH=0xFF;
	int i,index;

	if (rw=='w')
	{
		for (i=0; i<Ln; i++)
		{
			index=(CRCcharHIGH^(ComDataWrite[i]&0xff))&0xff;
			CRCcharHIGH=(CRCcharLOW^crc_table[index])&0xff;
			CRCcharLOW=crc_table[index+256];
		}
		ComDataWrite[Ln+1]=CRCcharLOW;
		ComDataWrite[Ln]=CRCcharHIGH;

		//
		printf("\n");  printf("\nTX -> ");
		if(USE_VERBOSE_IO)
		{
		  for (i=0;i<Ln+2;i++) 
		  {
		    printf("%02X ",ComDataWrite[i]&0xff);
		  }; 
		}
		printf("\n");

	}
	if (rw=='r')
	{
		for (i=0; i<Ln; i++)
		{
			index=(CRCcharHIGH^(ComDataRead[i]&0xff))&0xff;
			CRCcharHIGH=(CRCcharLOW^crc_table[index])&0xff;
			CRCcharLOW=crc_table[index+256];
		}
		ComDataRead[Ln+3]=CRCcharLOW;
		ComDataRead[Ln+2]=CRCcharHIGH;

		//
		if(USE_VERBOSE_IO)
		{
		  printf("\nRX <- ");
		  for (i=0;i<Ln+2;i++) 
		  { 
		    printf("%02X ",ComDataRead[i]&0xff);		  		  
		  };
		  printf("\n");
		}
	}

	return 0;
}



int IniCom(const char * portname, int baudRate)
{
	PrintStack("IniCom");
	
	int fd = open(portname, O_RDWR | O_NOCTTY | O_NDELAY);
	if(fd < 0)
	{
		return fd;
	}
	
	set_interface_attribytes(fd, baudRate);
	return fd;
}

char 
IniCom1()   //initializacija COM1	
{
	PrintStack("IniCom1");
	
	hCom1 = IniCom(SERIALPORT1, B19200);
	if(hCom1 < 0)
	{
		//todo error
		printf("Port1 init error...");
		exit(1);
	}
	
	if(pthread_create(&readkey_thread, NULL, ReadKeyThreadProc, NULL) != 0)
	{
		exit(1);
	}
	ReadKey();
	return 0;
};



char IniCom2()   //initializacija COM2	
{
	PrintStack("IniCom2");
	
	hCom2 = IniCom(SERIALPORT1, B19200);
	if(hCom2 < 0)
	{
		//todo error
		printf("Port2 init error...");
		exit(1);
	}
	ReadKey();
	return 0;
};



char Reader() //ctenie faylov nastroek
{ 
	PrintStack("Reader");
	bool exists = false;
	ReadMedium();
	char gradPath[256];
	strcpy(gradPath, CombinePath(applicationPath,  GetNumberedFileName(Grad)));
	char nastrPath[256];
	strcpy(nastrPath, CombinePath(applicationPath,  GetNumberedFileName(Nastr)));
	char kalibrPath[256];
	strcpy(kalibrPath, CombinePath(applicationPath,  GetNumberedFileName(Kalibr)));
	char setPath[256];
	strcpy(setPath, CombinePath(applicationPath,  GetNumberedFileName(Set)));
	FILE *fpg;
	fpg=fopen(gradPath, "r+b");
	if(fpg != NULL)
	{
		fread(&a0, sizeof(float), 1, fpg);
		fread(&a1, sizeof(float), 1, fpg);
		fread(&a2, sizeof(float), 1, fpg);
		fread(&a3, sizeof(float), 1, fpg);
		fread(&a4, sizeof(float), 1, fpg);
		fread(&a5, sizeof(float), 1, fpg);
		fread(&a6, sizeof(float), 1, fpg);
		fread(&a7, sizeof(float), 1, fpg);
		fread(&a8, sizeof(float), 1, fpg);
		fread(&a9, sizeof(float), 1, fpg);
		fread(&AA, sizeof(float), 1, fpg);
		fread(&BB, sizeof(float), 1, fpg);
		fread(&k1, sizeof(float), 1, fpg);
		fread(&Wmax, sizeof(float), 1, fpg);
		fread(&Wmin, sizeof(float), 1, fpg);
		fclose(fpg);
		exists = true;
	}

	FILE *fpn; fpn=fopen(nastrPath, "r+b");
	int fake;
	if(fpn != NULL)
	{
		fread(&fake, sizeof(int), 1, fpn);
		fread(&VZ, sizeof(int), 1, fpn);
		fread(&VO, sizeof(int), 1, fpn);
		fread(&VI, sizeof(int), 1, fpn);
		fread(&Tak, sizeof(int), 1, fpn);
		fread(&UstRele, sizeof(int), 1, fpn);
		fread(&AK, sizeof(int), 1, fpn);
		fread(&Aout_tip, sizeof(int), 1, fpn);
		fread(&N_Rez, sizeof(float), 1, fpn);
		fread(&K_Rez, sizeof(float), 1, fpn);
		fread(&Step_Rez, sizeof(float), 1, fpn);
		fclose(fpn);
		exists = true;
	}

	FILE *fpk; fpk=fopen(kalibrPath, "r+b");
	if(fpk != NULL)
	{
		fread(&Am0_k, sizeof(int), 1, fpk);
		fread(&Haw0_k, sizeof(int), 1, fpk);
		fread(&Gamma0_k, sizeof(int), 1, fpk);
		fread(&T_Gen_k, sizeof(int), 1, fpk);
		fread(&T_Sr0_k, sizeof(int), 1, fpk);
		fclose(fpk);
		exists = true;
	}

	bool rewrite = false;
	FILE *fps; fps=fopen(setPath, "r+b");
	if(fps != NULL)
	{
		fread(&Va, sizeof(float), 1, fps);
		fread(&kgt, sizeof(float), 1, fps);
		fread(&kgv, sizeof(float), 1, fps);
		fread(&T, sizeof(float), 1, fps);
		fread(&Kol_dat, sizeof(int), 1, fps);
		fread(&Nom_dat1, sizeof(int), 1, fps);
		fread(&Nom_dat2, sizeof(int), 1, fps);
		fread(&Nom_ind, sizeof(int), 1, fps);
		fread(&Saw_A0, sizeof(int), 1, fps);
		fread(&Saw_A1, sizeof(int), 1, fps);
		fread(&controlleradr, sizeof(int), 1, fps);
		fread(&Ku0, sizeof(int), 1, fps);
		fread(&Ku1, sizeof(int), 1, fps);
		fread(&Nzp, sizeof(int), 1, fps);
		fread(&Poralfa, sizeof(float), 1, fps);
		fread(&Porgamma, sizeof(float), 1, fps);
		if(!feof(fps))
		{
			fread(&Porbeta, sizeof(float), 1, fps);
		}
		else
		{
			Porbeta = 0;
			rewrite = true;
		}
		fclose(fps);
		if(rewrite)
		{
			SaveSet();
		}
		exists = true;
	}
	return 0;
}

//////////////////////////////////////
//modified by SL 18.06.09
//modified by SL 23.12.2009 add data output throught COM2
char Write2Master() //write to master host
{
	PrintStack("Write2Master");
	/*
	GetCommState(hCom2,&dcb2);
	dcb2.BaudRate =9600;
	dcb2.ByteSize = 8;
	dcb2.Parity = NOPARITY;
	dcb2.StopBits = ONESTOPBIT;
	dcb2.fDtrControl = DTR_CONTROL_DISABLE;
	dcb2.fRtsControl = RTS_CONTROL_ENABLE;

	fSuccess = SetCommState( hCom2, &dcb2);

	// printf("%x",temp[23]);

	temp[0]='A';temp[1]='T';temp[2]=' ';
	//temp[3]='1';temp[4]='4';
	temp[5]=' ';
	// y=2 -  NAZNACHENIE NOMERA PRIBORA DLA UDALENNOGO KOMPUTERA (2);

	y=1; for (i=2;i!=0;i--) {z=fmod(y,10); y=floor(y/10); temp[i+2]=z+48;};
	temp[6]='1';temp[7]=' ';temp[8]='1';temp[9]=' ';
	y=W*100; for (i=5;i!=0;i--) {z=fmod(y,10); y=floor(y/10); temp[i+15]=z+48;};
	temp[16]=temp[17];temp[17]=temp[18];temp[18]='.';
	temp[21]=' ';
	y=T_Sr0; for (i=5;i!=0;i--) {z=fmod(y,10); y=floor(y/10); temp[i+9]=z+48;};
	temp[14]=temp[13];temp[13]='.';
	temp[15]=' ';
	temp[22]='$';
	CRC=0xA5;for (i=0;i<23;i++) {CRC=CRC+temp[i];};
	temp[23]=CRC;
	len=24;
	for (i=0;i<24;i++) {printf("%c",temp[i]);};
	//printf("%x",temp[23]);
	*/
	
	printf("\nhumidity %f %i", W, T_Sr0);
	if(CheckFilling())
	{

		unsigned char wHigth = (unsigned char)W;
		unsigned char wLow = (unsigned char)(((W - (unsigned char)W)) * 100);

		unsigned char tHigth = (unsigned char)(T_Sr0 / 100);
		unsigned char tLow = (unsigned char)(T_Sr0 -(T_Sr0/100) * 100);

		masterbuffer[0] = wHigth;
		masterbuffer[1] = wLow;
		masterbuffer[2] = tHigth;
		masterbuffer[3] = tLow;
		dataSent = false;
	}
	else
	{
		masterbuffer[0] = 0;
		masterbuffer[1] = 0;
		masterbuffer[2] = 0;
		masterbuffer[3] = 0;

		dataSent = false;
	}

	return 0;
}
//write to remote indicator
char WriteUBI() //display temperature and humidity on remote indicator
{
	PrintStack("WriteUBI");
	
	int i;
	char len, pos=0, h4,h3,h2,h1;
	float prt1 = 0, prt2;

	// Podgotovka komandnoj stroki             //Write:
	ComDataWrite[pos++]=0x03; 	          //select indicator
	ComDataWrite[pos++]=0x10;   	          //write multiple registers
	ComDataWrite[pos++]=0x00;
	ComDataWrite[pos++]=0x00;          	 //starting adress
	ComDataWrite[pos++]=0x00;
	ComDataWrite[pos++]=0x02;          	 //quantity
	ComDataWrite[pos++]=0x04;          	 //char count


	//humidity
	if (W<100)
	{
	  prt1=W/100;
	  prt2=floor(prt1*10);
	  h4=(char)(prt2);
	  prt1=prt1*10-prt2;
	  prt2=floor(prt1*10);
	  h3=(char)(prt2);
	  prt1=prt1*10-prt2;
	  prt2=floor(prt1*10);
	  h2=(char)(prt2);
	  prt1=prt1*10-prt2;
	  prt2=floor(prt1*10);
	  h1=(char)(prt2);
	  prt1=prt1*10-prt2;
	  ComDataWrite[pos++]=(char)(16*h2+h1);
	  ComDataWrite[pos++]=(char)(16*h4+h3);
	}
	else
	{
		ComDataWrite[pos++]=0xDD;
		ComDataWrite[pos++]=0xAC;
	};
	//temperature
	prt1=(float)(T_Sr0)/10000;
	prt2=floor(prt1*10);
	h4=(char)(prt2);
	prt1=prt1*10-prt2;
	prt2=floor(prt1*10);
	h3=(char)(prt2);
	prt1=prt1*10-prt2;
	prt2=floor(prt1*10);
	h2=(char)(prt2);
	prt1=prt1*10-prt2;
	prt2=floor(prt1*10);
	h1=(char)(prt2);
	prt1=prt1*10-prt2;
	ComDataWrite[pos++]=(char)(16*h2+h1);
	ComDataWrite[pos++]=(char)(16*h4+h3);
	//add CRC
	CRCcount('w',pos);


	//Peredacha komandnoj stroki i priem otveta
	len=(char)(pos+2);
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++) 
	  {
	    printf("%x ",ComDataWrite[i]);
	  };
	}
	int ret = write(hCom2,ComDataWrite,len);
	len=8;
	ret = read(hCom2,ComDataRead,len);
	
	if(USE_VERBOSE_IO)
	{
	  for (i=0;i<len;i++) 
	  {
	    printf("%x ",ComDataRead[i]);
	  };
	}

	//Read:
	//OK:    Error:
	//adres //char 0 char 0
	//kod funkcii //char 1 char 1(modificirovan)
	//kod error   //       char 2
	//CRC         //char 2 char 3
	// CRC        //char 3 char 4	
	if (ComDataWrite[0]!=ComDataRead[0])
	{
		return 10;
	}
	if (ComDataWrite[1]!=ComDataRead[1])
	{if (ComDataRead[1]!=ComDataWrite[1]+0x80) 
	{
		return 11;
	} else
	{
		return ComDataRead[2];
	};}
	else 
	{
	  CRCcount('r',len);            // vichislenie CRC
	};

	//proverka CRC
	if (ComDataRead[len]!=ComDataRead[len+2] ||
		ComDataRead[len+1]!=ComDataRead[len+1+2])
	{
		return 12;
	}
	return 0;
}



void Aout() //
{
	PrintStack("Aout");
	if(Aout_tip <1 || Aout_tip> 3) 
	{
	  printf("Auto tip out of range\n");
	  return;
	}
	char CRC;
	int ACout0,ACout1, DacData;
	float Kout;
	
	if (Aout_tip==1) //0-5
	{
	  ACout0=60; 
	  ACout1=70;
	  Kout=1715/(Wmax-Wmin != 0 ? Wmax-Wmin : 1);
	  DacData=(int)(floor((W-Wmin)*Kout));
	  if (DacData>1220)
	  {
	    DacData=60;
	  };
	  if (DacData<60)
	  {
	    DacData=60;
	  };
	};
	
	if (Aout_tip==2)  //0-20
	{
	  ACout0=60;
	  ACout1=55;
	  Kout=3680/(Wmax-Wmin != 0 ? Wmax-Wmin : 1);
	  DacData=(int)(floor((W-Wmin)*Kout));
	  if (DacData>3610)
	  {
	    DacData=0;
	  };
	  if (DacData<0)
	  {
	    DacData=0;
	  };
	};
	if (Aout_tip==3)  //4-20
	{
	  ACout0=60; 
	  ACout1=65; 
	  Kout=2920/(Wmax-Wmin != 0 ? Wmax-Wmin : 1);
	  DacData=(int)(floor(((W-Wmin)*Kout)/1+750));
	  if (DacData>3610)
	  {
	    DacData=3610;
	  };
	  if (DacData<750)
	  {
	    DacData=750;
	  };
	};
	struct timespec tw = {0, 200 * 1000000};
	struct timespec tr;
	nanosleep(&tw, &tr);
	RegistrTM_int[4]=ACout0;
	nanosleep(&tw, &tr);
	RegistrTM_int[2]=DacData;
	ComWriteIntReg(0x2,0x32,0x1);
	nanosleep(&tw, &tr);
	RegistrTM_int[4]=ACout1;
	nanosleep(&tw, &tr);
	RegistrTM_int[2]=DacData;
	ComWriteIntReg(0x2,0x33,0x1);
	nanosleep(&tw, &tr);	
}



char * CombinePath(const char * firstPart, const char * secondPart)
{
	PrintStack("CombinePath");
	strcpy(combinePathStr, firstPart);
	strcat(combinePathStr, PATH_SEPARATOR);
	strcat(combinePathStr, secondPart);
	return combinePathStr;
}



void TimedIncrementT_Gen_T_Sr0_T_Sr1_W(bool setAverage, int t_Gen, int t_Sr0, int t_Sr1, float w)
{
	PrintStack("TimedIncrementT_Gen_T_Sr0_T_Sr1_W");
	static int means_count = 0;
	static int T_Gen_sum = 0;
	static int T_Sr0_sum = 0, T_Sr1_sum = 0;
	static float W_sum = 0;   

	if(!setAverage)
	{	   
		means_count++;
		T_Gen_sum += t_Gen;
		T_Sr0_sum += t_Sr0;
		T_Sr1_sum += t_Sr1;
		W_sum += w;
	}
	else   
	{
		if(means_count > 0)
		{
			T_Gen = (int)floor((float)T_Gen_sum / means_count);	   
			T_Sr0 = (int)floor((float)T_Sr0_sum / means_count);
			T_Sr1 = (int)floor((float)T_Sr1_sum / means_count);
			W = W_sum / means_count;
		}
		means_count = 0;
		T_Gen_sum = T_Sr0_sum = T_Sr1_sum = 0;
		W_sum = 0;
	}
}



static void * ReadKeyThreadProc(void * vptr_args)
{
	PrintStack("ReadKeyThreadProc");
	while(!isexit)
	{
		Sleep(400);		
		char tempchar = ReadKeyBuffered();
		printf(" readkey %c\n", tempchar);
		if(tempchar != 'Z')
		{
			keyBuffer = tempchar;
		}
	}

	return 0;
}



char WaitNoKey()
{
	PrintStack("WaitNoKey");
	
	do
	{
		Sleep(300);
		char key = ReadKeyBuffered();
		if(key == 'Z')
		{
		  return key;
		}	
	}
	while (true);
}



char WaitKey()
{
	PrintStack("WaitKey");
	
	do 
	{
		Sleep(300);
		char key = ReadKeyBuffered();
		if(key!='Z'&&key!='S')
		{
		  return key;
		}
	}
	while (true);
}



char WaitAnyKey()
{	
	PrintStack("WaitAnyKey");
	
	char key = 0;
	do
	{
		Sleep(300);
		char key = ReadKeyBuffered();
	}
	while (key=='Z');
	return key;
}



bool CheckFilling()
{
	PrintStack("CheckFilling");
	
	printf("Haw0 = %i",Haw0);
	bool filling = Nzp == 0 ||
		!(Nzp==1 && Am0>Poralfa) &&
		!(Nzp==2 && Am0<Poralfa) &&
		!(Nzp==3 && (Haw0 / 1000.0)>Porbeta) &&
		!(Nzp==4 && (Haw0 / 1000.0)<Porbeta) &&
		!(Nzp==5 && (Gamma0 / 1000.0)>Porgamma) &&
		!(Nzp==6 && (Gamma0 / 1000.0)<Porgamma);
	return filling;
}



bool ShutdownServer()
{
	PrintStack("ShutdownServer");
	// release resources
	isexit = 1;
	if(pthread_join(readkey_thread, NULL))
	{
		exit(1);
	}
	
	close(hCom1);
	close(hCom2);
	execl("/usr/bin/sudo", "/usr/bin/sudo", "/sbin/shutdown", "-h", "now", (char *)NULL);

}



void SleepWithService(long sleeptime)
{	
	PrintStack("SleepWithService");
	
	while(sleeptime >= servicetime)
	{
		sleeptime -= servicetime;
		struct timespec tw = {servicetime / 1000, (servicetime - (servicetime / 1000)*1000) * 1000000};
		struct timespec tr;
		nanosleep(&tw, &tr);
		if(sleeptime >= READKEY_INTERVAL)
		{
			sleeptime -= READKEY_INTERVAL;
			tw = {READKEY_INTERVAL / 1000, (READKEY_INTERVAL - (READKEY_INTERVAL / 1000)*1000) * 1000000};
			nanosleep(&tw, &tr);			
		}
	}

	if(sleeptime > 0)
	{
		Sleep(sleeptime);		
	}
}



void CheckShutdown()
{
	PrintStack("CheckShutdown");
	
	if(keyBuffer == 'H')
	{
		ClrScr();
		if(ShutdownServer())
		{
			exit(0);
		}
	}
}



char * GetFileRootName(FileType type)
{
	PrintStack("GetFileRootName");
	
	switch(type)
	{
	case Grad :
		return "Grad";
	case Nastr :
		return "Nastr";
	case Kalibr :
		return "Kalibr";
	case Set :
		return "Set";
	}
	return "Default";
}



char * GetNumberedFileName(FileType type)
{
	PrintStack("GetNumberedFileName");
	
	char * rootstr = GetFileRootName(type);
	char mediumstr[2];
	mediumstr[0] = NomGrad + '0';
	mediumstr[1] = 0;

	strcpy(translatedFileName, rootstr);
	if(strlen(mediumstr) > 0)
	{
		strcat(translatedFileName, "_");
	}
	strcat(translatedFileName, mediumstr);
	strcat(translatedFileName, ".dat");

	return translatedFileName;
}



char * GetFullRow(char * buffer)
{
	PrintStack("GetFullRow");
	
	int bufferlength = strlen(buffer) < ROW_WIDTH - 1 ? strlen(buffer) : ROW_WIDTH - 1;
	strcpy(fullrow, emptyrow);
	strncpy(fullrow, buffer, bufferlength);
	fullrow[bufferlength] = 0;
	strncat(fullrow, emptyrow, ROW_WIDTH - strlen(buffer) - 1);
	fullrow[ROW_WIDTH - 1] = 0;

	return fullrow;
}



char * GetCenteredRow(char * buffer)
{
	PrintStack("GetCenteredRow");
	
	int rigntPadding = strlen(buffer);
	for(rigntPadding = strlen(buffer); rigntPadding > 0; rigntPadding--)
	{
	  if(buffer[rigntPadding - 1] != ' ')
	  {
	    break;
	  } 
	};

	int leftPadding = (ROW_WIDTH - 1 - rigntPadding) / 2;
	char * leftPaddingStr = new char[rigntPadding + leftPadding  + 1];

	strncpy(leftPaddingStr, emptyrow, leftPadding );
	leftPaddingStr[leftPadding ] = 0;
	strncat(leftPaddingStr, buffer, rigntPadding);
	leftPaddingStr[rigntPadding + leftPadding] = 0;


	char * fullrow = GetFullRow(leftPaddingStr);
	delete [] leftPaddingStr;
	strcpy(centeredStr, fullrow);
	centeredStr[strlen(fullrow)] = 0;
	return centeredStr;
}



bool HasSomeDefaultFiles()
{
	PrintStack("HasSomeDefaultFiles");
	
	struct stat fileinfo;
	return !stat(CombinePath(applicationPath, "Grad.dat"),(struct stat *)&fileinfo) || !stat( CombinePath(applicationPath, "Nastr.dat"),(struct stat *)&fileinfo) || !stat(CombinePath(applicationPath, "Kalibr.dat"),(struct stat *)&fileinfo) || !stat(CombinePath(applicationPath, "Set.dat"),(struct stat *)&fileinfo);
}



void SetDefaultValues()
{
	PrintStack("SetDefaultValues");
	
	a0 = a1 =a2 =a3 =a4 =a5 =a6 = a7 = a8 = a9 = AA = BB = k1 = Wmin = Wmax = 0;
	
	//NomGrad = 0;
	VZ = VO = VI = Tak = UstRele = AK = N_Rez = K_Rez = Step_Rez = 0;
	Aout_tip = 1;
	
	Am0 = Haw0 = Gamma0 = T_Gen = T_Sr0 = 0;
	
	Am0_k = Haw0_k = Gamma0_k = T_Gen_k = T_Sr0_k = 0;
	
	Va = kgt = kgv = T = Kol_dat =Nom_dat1 = Nom_dat2 = Nom_ind = Saw_A0 = Saw_A1 = 0;
	
	controlleradr = 0;
	Ku0 = Ku1 = Nzp = Poralfa = Porbeta = Porgamma = 0;
}



void SaveGrad()
{
	PrintStack("SaveGrad");
	
	char * gradPath = CombinePath(applicationPath, GetNumberedFileName(Grad));
	FILE *fp; fp=fopen(gradPath, "w+");

	if(fp == NULL) 
	{
	  printf("Error save grad\n");
	  return;
	}
	
	fprintf(fp, "a0 %f\n", &a0);
	fprintf(fp, "a1 %f\n", &a1);
	fprintf(fp, "a2 %f\n", &a2);
	fprintf(fp, "a3 %f\n", &a3);
	fprintf(fp, "a4 %f\n", &a4);
	fprintf(fp, "a5 %f\n", &a5);
	fprintf(fp, "a6 %f\n", &a6);
	fprintf(fp, "a7 %f\n", &a7);
	fprintf(fp, "a8 %f\n", &a8);
	fprintf(fp, "a9 %f\n", &a9);
	fprintf(fp, "AA %f\n", &AA);
	fprintf(fp, "BB %f\n", &BB);
	fprintf(fp, "k1 %f\n", &k1);
	fprintf(fp, "Wmax %f\n", &Wmax);
	fprintf(fp, "Wmin %f\n", &Wmin);
	
	fclose(fp);
}



void SaveNastr()
{
	PrintStack("SaveNastr");
	
	char * nastrPath = CombinePath(applicationPath, GetNumberedFileName(Nastr));
	FILE *fp; fp=fopen(nastrPath, "w+");

	int fake = 0;
	if(fp == NULL) 
	{
	  printf("Nastroika not saved\n");
	  return;
	}
	//fwrite(&NomGrad, sizeof(int), 1, fp);
	
	fprintf(fp, "fake %d\n", &fake);
	fprintf(fp, "VZ %d\n", &VZ);
	fprintf(fp, "VO %d\n", &VO);
	fprintf(fp, "VI %d\n", &VI);
	fprintf(fp, "Tak %d\n", &Tak);
	fprintf(fp, "UstRele %d\n", &UstRele);
	fprintf(fp, "AK %d\n", &AK);
	fprintf(fp, "Aout_tip %d\n", &Aout_tip);
	fprintf(fp, "N_Rez %f\n", &N_Rez);
	fprintf(fp, "K_Rez %f\n", &K_Rez);
	fprintf(fp, "Step_Rez %f\n", &Step_Rez);

	fclose(fp);
}



void SaveKalibr()
{
	PrintStack("SaveKalibr");
	
	char * kalibrPath = CombinePath(applicationPath, GetNumberedFileName(Kalibr));
	FILE *fp; fp=fopen(kalibrPath, "w+");

	if(fp == NULL) 
	{
	  printf("Kalibrovka not saved\n");
	  return;
	}
	
	fprintf(fp, "Am0 %d\n", &Am0);
	fprintf(fp, "Haw0 %d\n", &Haw0);
	fprintf(fp, "Gamma0 %d\n", &Gamma0);
	fprintf(fp, "T_Gen %d\n", &T_Gen);
	fprintf(fp, "T_Sr0 %d\n", &T_Sr0);
	
	fclose(fp);
}



void SaveSet()
{
	PrintStack("SaveSet");
	
	char * setPath = CombinePath(applicationPath, GetNumberedFileName(Set));
	FILE *fp; fp=fopen(setPath, "w+b");

	if(fp == NULL) 
	{
	  printf("Set was not saved\n");
	  return;
	}
	
	fprintf(fp, "Va %f\n", &Va);
	fprintf(fp, "kgt %f\n", &kgt);
	fprintf(fp, "kgv %f\n", &kgv);
	fprintf(fp, "T %f\n", &T);
	fprintf(fp, "Kol_dat %d\n", &Kol_dat);
	fprintf(fp, "Nom_dat1 %d\n", &Nom_dat1);
	fprintf(fp, "Nom_dat2 %d\n", &Nom_dat2);
	fprintf(fp, "Nom_ind %d\n", &Nom_ind);
	fprintf(fp, "Saw_A0 %d\n", &Saw_A0);
	fprintf(fp, "Saw_A1 %d\n", &Saw_A1);
	fprintf(fp, "controlleradr %d\n", &controlleradr);
	fprintf(fp, "Ku0 %d\n", &Ku0);
	fprintf(fp, "Ku1 %d\n", &Ku1);
	fprintf(fp, "Nzp %d\n", &Nzp);
	fprintf(fp, "Poralfa %f\n", &Poralfa);
	fprintf(fp, "Porbeta %f\n", &Porbeta);
	fprintf(fp, "Porgamma %f\n", &Porgamma);
	
	fclose(fp);
}



void SaveMedium()
{
	PrintStack("SaveMedium");
	char * mediumPath = CombinePath(applicationPath, "Media.dat");
	FILE * file = fopen(mediumPath, "w+");
	if(file == NULL) return;

	for(int i = 0; i < MEDIUM_COUNT; i++)
	{
		if(mediums[i][0] != 0) 
		{
			fprintf(file, "%s\n", &mediums[i]);			
		}		
	}

	fclose(file);
}



void ReadMedium()
{
	PrintStack("ReadMedium");
	
	for(int i =0; i < MEDIUM_COUNT; i++)
	{
		mediums[i][0] = 0;
	}

	char * mediumPath = CombinePath(applicationPath, "Media.dat");
	FILE * file = fopen(mediumPath, "r");
	if(file == NULL) return;

	media_count = 0;
	
	while(!feof(file))
	{	
		fgets(mediums[media_count++],18,file);		
	}

	fclose(file);
}



void SetMedium(char * newMediumName)
{	
	PrintStack("SetMedium");
	
	GetFullRow(newMediumName);
	strcpy(mediumName, fullrow);
	mediumName[strlen(fullrow)] = 0;	

	for(int i =0; i < MEDIUM_COUNT; i++)
	{
		if(mediums[i][0] == 0) 
		{
			strcpy(mediums[i], mediumName);
			mediums[i][strlen(mediumName)] = 0;
			break; 
		};
	}
}

bool ReadDefaultMedium()
{
	PrintStack("ReadDefaultMedium");
	
	char * mediumPath = CombinePath(applicationPath, "Media.dat");
	FILE * file = fopen(mediumPath, "r");

	if(file == NULL)
	{		
		return false;
	}
	int reallength = fread(mediumName, sizeof(char), ROW_WIDTH - 1, file);
	mediumName[ROW_WIDTH - 1] = 0;

	fclose(file);
	if(reallength)
	{
		mediumName[reallength] = 0;
		SetMedium(mediumName);
		return true;
	}
	return false;
}



void SaveAll()
{
	PrintStack("SaveAll");
	
	SaveMedium();
	SaveGrad();
	SaveNastr();
	SaveKalibr();
	SaveSet();
}



char * SetMediumRow(int newIndex)
{
	PrintStack("SetMediumRow");
	
	if(newIndex == 1)
	{
		NomGrad++;
		NomGrad%=media_count;
	}
	if(newIndex == 2)
	{
		NomGrad=media_count - 1;
	}

	return GetFullRow(mediums[NomGrad]);
}



void CompliteAndSendData(CommandEntity * entity)
{
	PrintStack("CompliteAndSendData");
	
	//printf("\r\nentity address %i\r\n",entity->GetAddress());
	if(controlleradr == entity->GetAddress())
	{
		switch(entity->identifier)
		{
		case BeginDataSend :
			master.SendResponse(entity);
			break;
		case SendDataPacket :
			if(!dataSent)
			{
				master.SendDataPacket(
					controlleradr,
					0,
					4,
					masterbuffer,
					4);			
				dataSent = true;
			}
			else
			{
				master.DataTransactionComplited(controlleradr);
			}
		case DataPacketReceived:
			dataSent = false;
			break;
		case RegisterController :
			master.RegisterController(controlleradr, RS485);
			break;
		}
	}
}



char * Trim(char * source)
{
	PrintStack("Trim");
	
	trimStr[0] = 0;
	int i = 0;
	int index = 0;
	while(source[i] != 0 && source[i] == ' ' )
	{
		i++;
	}

	while(source[i] != 0 && source[i] != ' ' )
	{
		trimStr[index++] = source[i++];
	}

	return trimStr;
}



void clearArrays()
{
	PrintStack("clearArrays");
	
	for(int i =0; i<MEDIUM_COUNT; i++) mediums[i][0] = 0;
	memset(&trimStr[0], 0, sizeof(trimStr));
	memset(&fullrow[0], 0, sizeof(fullrow));
	memset(&masterbuffer[0], 0, sizeof(masterbuffer));
	memset(&RegistrTM_int[0], 0, sizeof(RegistrTM_int));
	memset(&RegistrSPM_float[0], 0, sizeof(RegistrSPM_float));
	memset(&RegistrSPM_int[0], 0, sizeof(RegistrSPM_int));
	memset(&Text[0], 0, sizeof(Text));
	memset(&ComDataRead[0], 0, sizeof(ComDataRead));
	memset(&ComDataWrite[0], 0, sizeof(ComDataWrite));
	memset(&newMediumStr[0], 0, sizeof(newMediumStr));
	memset(&mediumName[0], 0, sizeof(mediumName));
	memset(&masterbuffer[0], 0, MBSIZE);
	strcpy(mediumName, emptyrow);
	mediumName[strlen(emptyrow)] = 0;
}



int set_interface_attribytes(int fd, int baudRate)
{
	PrintStack("set_interface_attribytes");
	struct termios options;
	//alternate is : fcntl(fd,F_SETFL,FNDELAY); //no wait for char input
	fcntl(fd,F_SETFL,0); //wait for char input

	//get current attr
	tcgetattr(fd,&options);

	//set speed
	cfsetispeed(&options,baudRate);
	cfsetospeed(&options,baudRate);
	
	//receiver enable & set local mode
	options.c_cflag |= (CLOCAL | CREAD);
	
	//set symbol format 8N1
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
	options.c_cflag &= ~CRTSCTS;

	//line options
	options.c_lflag &= ~( ICANON | ECHO | ECHOE | ISIG);

	//output options\
	//alternate is : options.c_oflag &= ~OLCUC; //no output processing
	options.c_oflag &= ~OPOST;

	//input options
	options.c_iflag = IGNBRK; //set no break condition, all others are clear

	//character options
	options.c_cc[VMIN] = 1; //min 1 char to read
	options.c_cc[VTIME] = 5; //500 ms timeout
	//alternate is : options.c_cc[VTIME] = 0; //block port

	//set attr
	tcsetattr(fd,TCSANOW,&options);
	return 0;
}


// THIS IS LINUX FILES, BUT LCD WANT CP1251 INSTEAD OF UTF-8
void utf8cp1251(char * from, char * to, int length)
{
	PrintStack("utf8cp1251");
	
	size_t fl, tl;
	fl = tl = length == 0 ? (size_t)strlen(from) : (size_t)length;
	iconv_t d;
	d = iconv_open("CP1251", "UTF-8");
	
	iconv(d, &from, &fl, &to, &tl);
	iconv_close(d);
}



void Sleep(int mileseconds_time)
{
  struct timespec tw = {mileseconds_time / 1000, (mileseconds_time - (mileseconds_time / 1000)*1000) * 1000000};
  struct timespec tr;
  nanosleep(&tw, &tr);
}



bool ExistReadBytes(int fd, int timeout_sec)
{
	fd_set read_fds;
	FD_ZERO(&read_fds);
	FD_SET(fd, &read_fds);
		
	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	if(select(fd+1, &read_fds, NULL, NULL, &timeout) == 1)
	{
	    return true;
	}
	else
	{
	    return false;
	}
}


void SetWorkingDir()
{
  if(getcwd(applicationPath, sizeof(applicationPath)) == NULL)
	{
		perror(strcat("getcwd function error : ", strerror(errno)));
		exit(1);
	}
}



void PrintStack(char * function)
{
    if(USE_VERBOSE_STACK)
    {
	printf("%s\n",function);
    }
}



void PrintLock()
{
    if(USE_VERBOSE_LOCKS)
    {
	printf("pthread_mutex_lock\n");
    }
}



void PrintUnlock()
{
    if(USE_VERBOSE_LOCKS)
    {
	printf("pthread_mutex_unlock\n");
    }
}