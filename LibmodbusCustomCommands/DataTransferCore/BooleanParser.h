#pragma once

namespace LibmodbusCustomCommands
{
	public ref class BooleanParser
	{
	    public :
	    static bool Parse(const char * value);
	    static bool TryParse(const char * value, bool & result);
	};
}
