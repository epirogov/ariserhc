#pragma once

namespace LibmodbusCustomCommands
{
	public ref class CommandHeader
	{
	    private :
	    System::Byte address;
	    System::Byte function;
	    bool hasSubfunction;
	    System::Byte subfunction;
	    public :

		CommandHeader()
		{
		}

	    CommandHeader(System::Byte address, System::Byte function, System::Byte subfunction, bool hasSubfunction);
	    ~CommandHeader();

	    System::Byte GetAddress();
	    System::Byte GetFunction();
		void SetFunction(System::Byte function);
	    bool HasSubfunction();
		void SethasSubfunction(bool hasSubfunction);
	    System::Byte GetSubFunction();
		void SetSubFunction(System::Byte subfunction);

	};
}