#pragma once
#include <stdio.h>
#include <sys/stat.h>

namespace LibmodbusCustomCommands
{
	enum FileReaderMode {STATICFILE, FD, PIPE};

	public ref class FileReaderBase
	{
	    protected :
	    static const int PIPE_MODE = 0x1180;
	    char * fileName;
	    int fd;
	    FILE * file;
	    FileReaderMode mode;
	    bool readed;

	    public :
	    FileReaderBase();
	    FileReaderBase(const char * fileName, FileReaderMode mode);
	    FileReaderBase(int fd, FileReaderMode mode);
	    ~FileReaderBase();
	    void Open();
	    void Close();

	    protected :
	    size_t GetFileSize(FILE * file);
	    size_t GetFileSize(int td);
	    size_t GetFileSize();
	    void OnlyOnceRead();
	    void CreateInternals(const char * fileName, FileReaderMode mode);
	    void CreateInternals(int fd, FileReaderMode mode);
	    virtual void SetNoData() = 0;
	};
}
