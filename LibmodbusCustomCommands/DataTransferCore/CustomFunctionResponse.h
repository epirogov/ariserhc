#pragma once

namespace LibmodbusCustomCommands
{
	public ref class CustomFunctionResponse
	{
	    public :
	    System::Byte address;
	    System::Byte function;
	    System::Byte subfunction;
	    System::Byte * data;
	    System::Byte datalength;

	    public :
	    CustomFunctionResponse(System::Byte address, System::Byte function, System::Byte subfunction, System::Byte * data, System::Byte datalength);
	    ~CustomFunctionResponse();

	};
}
