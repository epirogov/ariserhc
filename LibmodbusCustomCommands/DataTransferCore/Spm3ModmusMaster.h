#pragma once
#include <stdlib.h>
#include "CMODEEnum.h"
#include "ReadBufferEnum.h"
#include "SpeedEnum.h"
#include "ModbusProvider.h"
#include "CommandEntity.h"
#include "CommandFactory.h"
#include "PrivateMessanger.h"

using namespace System::IO::Ports;

namespace LibmodbusCustomCommands
{
	public ref class Spm3ModbusMaster : public PrivateMessanger
	{
	    private :
	    System::Byte address;
	    ModbusProvider ^ provider;
		CommandFactory ^ commandFactory;
	    public :
	    Spm3ModbusMaster(System::Byte address,SerialPort ^ serialPort); //also provider
	    ~Spm3ModbusMaster();

	    void Open();
	    int CMODE(CMODEEnum mode);
	    int ReadBuffer(ReadBufferEnum channel);
	    int GetRegisters(short startAddress, System::Byte count);
	    int SetRegisters(short startAddress, System::Byte count, const System::Byte * registers, System::Byte registerslength);
	    int WriteEEPROM();    
	    int SetSpeed(SpeedEnum speed);
	    int SendId();
	    void Close();

	    static void CalculateStartAddressAndSizeRegisters(System::Byte * data, System::Byte startAddressHight, System::Byte startAddressLow, System::Byte count, short & startAddress, System::Byte & realBytesCount, System::Byte * & startData);
	    static void CalculateStartAddressAndSizeRegisters(System::Byte * data, short startAddress, System::Byte count, System::Byte & realBytesCount, System::Byte * & startData);
	    static void CalculateStartAddressAndSizeRegisters(short startAddress, System::Byte count, System::Byte & realBytesCount);

	};
}