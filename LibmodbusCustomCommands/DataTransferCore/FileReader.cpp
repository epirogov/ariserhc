#include "StdAfx.h"
#include <list>
#include <string.h>
#include <stdio.h>
#include "FileReader.h"

using namespace std;

using namespace LibmodbusCustomCommands;

FileReader::FileReader() : FileReaderBase("", STATICFILE)
{
}

FileReader::FileReader(const char * filename) : FileReaderBase(filename, STATICFILE)
{
}

char * FileReader::GetContent()
{
    return this->body;
}

void FileReader::ReadToEnd()
{
    this->OnlyOnceRead();

    if(this->file == NULL)
    {
        this->SetNoData();
        return;
    }

    if(this->mode == STATICFILE || this->mode == FD)
    {
        ReadStaticSize();
    }
    else if(this->mode == PIPE)
    {
        ReadBuffered();
    }
    else
    {
        ReadStaticSize();
    }
}

void FileReader::ReadFirstLine()
{
    this->OnlyOnceRead();

    if(this->file == NULL)
    {
        this->SetNoData();
        return;
    }

    size_t sizeChar = this->GetFileSize(file);
    this->body = new char[sizeChar + 1];
    //getline(&this->body, &sizeChar, file);
	int i  = 0;
	int count = 0;	
	do
	{
		int count = fread(this->body + i++, sizeof(char), sizeChar, this->file);		
	}
	while(this->body[i] != '\r' || this->body[i] != '\n');
    this->body[i] = 0;
}

void FileReader::ReadStaticSize()
{
    size_t sizeChar = this->GetFileSize();
    this->body = new char[sizeChar + 1];
    fread(this->body, sizeof(char), sizeChar, this->file);
    this->body[sizeChar] = 0;
}

void FileReader::ReadLineStaticSize()
{
    size_t sizeChar = this->GetFileSize();
    this->body = new char[sizeChar + 1];
    fgets(this->body, sizeChar, this->file);
    this->body[sizeChar] = 0;
}

void FileReader::ReadBuffered()
{
    ReadBufferedCondition(gcnew ConditionMethod(FileReader::EofCondition));
}

void FileReader::ReadLineBuffered()
{
    ReadBufferedCondition(gcnew ConditionMethod(FileReader::EolCondition));
}

void FileReader::ReadBufferedCondition(ConditionMethod ^ condition)
{
    list<char *> buffer;
    size_t size = 0;

    int fullSize = 0;
    char * buff = NULL;
    do
    {
        buff = new char [256];
        size = fread(buff, 256,1,this->file);
        fullSize += strlen(buff);
        buffer.push_back(buff);
    }
    while (condition(buff));

    this->body = new char [fullSize + 1];
    strcpy(this->body,"");

    list<char *>::iterator current = buffer.begin();
    for (int i=0; i<buffer.size(); i++)
    {
        strcat(this->body, *current);
        delete [] *current;
        current++;
    }
    this->body[fullSize] = 0;

}

bool FileReader::EofCondition(char * buffer)
{
    return strlen(buffer) > 0;
}

bool FileReader::EolCondition(char * buffer)
{
    return strlen(strchr(buffer, '\n')) > 0;
}

void FileReader::SetNoData()
{
    this->body = new char [1];
    strcpy(this->body,"");
}
