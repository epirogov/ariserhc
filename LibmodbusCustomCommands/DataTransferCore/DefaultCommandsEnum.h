namespace LibmodbusCustomCommands
{
	public enum class DefaultCommandsEnum
	{
		DefaultResponse,
		ErrorCRC,
		ErrorFunction,
		ErrorData
	};
}