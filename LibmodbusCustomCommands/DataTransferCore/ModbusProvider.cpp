#include "StdAfx.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <Winsock2.h>

#include "ModbusProvider.h"

using namespace std;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::IO::Ports;
using namespace System::Threading;

using namespace LibmodbusCustomCommands;

#define BILLION 1000000000

ModbusProvider::ModbusProvider(DeviceTypeEnum deviceType, SerialPort ^ serialPort, int baud, Parity parity, int data_bit, StopBits stop_bit, List<CustomFunctionMapping ^> ^ mapping)
{
	eventDataReceived = gcnew ManualResetEvent(false);
	
	this->port1 = serialPort;
	this->port1->DataReceived  += gcnew SerialDataReceivedEventHandler(this, &LibmodbusCustomCommands::ModbusProvider::ModbusProvider_Port1_DataReceived);
	dataReceivedFlag = false;
	//table_crc_hi;
	
    this->deviceType = deviceType;
    this->baud = baud;
    this->parity = parity;    
    this->data_bit = data_bit;
    this->stop_bit = stop_bit;
    this->mapping = mapping;    
}

void ModbusProvider::CreateTables()
{
	table_crc_hi = new unsigned char [256];
	table_crc_lo = new unsigned char [256];

	unsigned char array_hi[] = {
        0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
        0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
        0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,
        0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
        0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,
        0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,
        0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,
        0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
        0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
        0x80,0x22,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
        0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,
        0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
        0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
        0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
        0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,
        0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
        0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
        0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
        0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,
        0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
        0x00,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,
        0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
        0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,
        0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
        0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
        0x80,0x41,0x00,0xc1,0x81,0x40,
	};

	unsigned char array_lo[] = {
        0x00,0xc0,0xc1,0x01,0xc3,0x03,0x02,0xc2,0xc6,0x06,
        0x07,0xc7,0x05,0xc5,0xc4,0x04,0xcc,0x0c,0x0d,0xcd,
        0x0f,0xcf,0xce,0x0e,0x0a,0xca,0xcb,0x0b,0xc9,0x09,
        0x08,0xc8,0xd8,0x18,0x19,0xd9,0x1b,0xdb,0xda,0x1a,
        0x1e,0xde,0xdf,0x1f,0xdd,0x1d,0x1c,0xdc,0x14,0xd4,
        0xd5,0x15,0xd7,0x17,0x16,0xd6,0xd2,0x12,0x13,0xd3,
        0x11,0xd1,0xd0,0x10,0xf0,0x30,0x31,0xf1,0x33,0xf3,
        0xf2,0x32,0x36,0xf6,0xf7,0x37,0xf5,0x35,0x34,0xf4,
        0x3c,0xfc,0xfd,0x3d,0xff,0x3f,0x3e,0xfe,0xfa,0x3a,
        0x3b,0xfb,0x39,0xf9,0xf8,0x38,0x28,0xe8,0xe9,0x29,
        0xeb,0x2b,0x2a,0xea,0xee,0x2e,0x2f,0xef,0x2d,0xed,
        0xec,0x2c,0xe4,0x24,0x25,0xe5,0x27,0xe7,0xe6,0x26,
        0x22,0xe2,0xe3,0x23,0xe1,0x21,0x20,0xe0,0xa0,0x60,
        0x61,0xa1,0x63,0xa3,0xa2,0x62,0x66,0xa6,0xa7,0x67,
        0xa5,0x65,0x64,0xa4,0x6c,0xac,0xad,0x6d,0xaf,0x6f,
        0x6e,0xae,0xaa,0x6a,0x6b,0xab,0x69,0xa9,0xa8,0x68,
        0x78,0xb8,0xb9,0x79,0xbb,0x7b,0x7a,0xba,0xbe,0x7e,
        0x7f,0xbf,0x7d,0xbd,0xbc,0x7c,0xb4,0x74,0x75,0xb5,
        0x77,0xb7,0xb6,0x76,0x72,0xb2,0xb3,0x73,0xb1,0x71,
        0x70,0xb0,0x50,0x90,0x91,0x51,0x93,0x53,0x52,0x92,
        0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,0x9c,0x5c,
        0x5d,0x9d,0x5f,0x9f,0x9e,0x5e,0x5a,0x9a,0x9b,0x5b,
        0x99,0x59,0x58,0x98,0x88,0x48,0x49,0x89,0x4b,0x8b,
        0x8a,0x4a,0x4e,0x8e,0x8f,0x4f,0x8d,0x4d,0x4c,0x8c,
        0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,
        0x43,0x83,0x41,0x81,0x80,0x40
	};

	for(int i =0; i < 265; i++)
	{
		table_crc_hi[i] = array_hi[i];
		table_crc_lo[i] = array_lo[i];
	}	
}

ModbusProvider::~ModbusProvider()
{
	this->Close();
    delete this->mapping;
}

void ModbusProvider::ModbusProvider_Port1_DataReceived(Object ^ sender, SerialDataReceivedEventArgs ^ e)
{	
	eventDataReceived->Set();
}

void ModbusProvider::Initialize()
{	
    this->modbus_init_rtu();
	this->VerifyCOMPorts();	
	this->modbus_connect_rtu();
}

void ModbusProvider::SendData(System::Byte * query, System::Byte querylength, System::Byte * response, System::Byte responseLength, System::Byte & realResponseLength, bool longRespone)
{
	this->modbus_send(query, querylength);	
    //this->receive_msg(responseLength, response, realResponseLength, longRespone);		
}

void ModbusProvider::Close()
{
    this->modbus_close_rtu();
}

/* Sends a query/response over a serial or a TCP communication */
int ModbusProvider::modbus_send(System::Byte *query, int query_length)
{	
	this->port1->Write(this->ToByteArray(query, query_length), 0, query_length);	
	return query_length;
}


/* Fast CRC */
short ModbusProvider::crc16(array<Byte> ^ buffer, short buffer_length)
{
        System::Byte crc_hi = 0xFF; /* high CRC byte initialized */
        System::Byte crc_lo = 0xFF; /* low CRC byte initialized */
        unsigned int i; /* will index into CRC lookup */

        /* pass through message buffer */
		int index = 0;
        for(i =0; i < buffer_length; i++)
		{
                index = crc_hi ^ buffer[i]; /* calculate the CRC  */
                crc_hi = crc_lo ^ table_crc_hi[index];
                crc_lo = table_crc_lo[index];

				//Console::WriteLine("{0} {1} {2} {3} {4}", i, crc_hi, crc_lo, table_crc_hi[i], table_crc_lo[i]);
        }

        return (short)crc_hi << 8 | crc_lo;
}

/*
int ModbusProvider::WaitData(struct timeval * tv)
{
	TimeSpan ^ waitTime = gcnew TimeSpan(tv->tv_sec * 1000000 + tv->tv_usec * 1000);
	if(!Monitor::Wait(this->syncReceive, *waitTime))
	{
		return COM_TIME_OUT;
	}
	else
	{
		return -1;
	}

	// Linux reqlisation

    fd_set * rfds = new fd_set();
    // Add a file descriptor to the set
    FD_ZERO(rfds);
    FD_SET(mb_param->fd, rfds);

    int select_ret;
    while ((select_ret = select(mb_param->fd+1, rfds, NULL, NULL, tv)) == -1) {
            if (errno == EINTR) {
                    printf("A non blocked signal was caught\n");
                    //Necessary after an error
                    FD_ZERO(rfds);
                    FD_SET(mb_param->fd, rfds);
            }
            else
            {
                break;
            }
    }

    delete rfds;

    if (select_ret == 0) {
            //Call to error_treat is done later to manage exceptions
            return COM_TIME_OUT;
    }
    else
    {
        //error_treat(mb_param, SELECT_FAILURE, "Select failure");
        return SELECT_FAILURE;
    }	
}
*/


/* Waits a reply from a modbus slave or a query from a modbus master.
   This function blocks for timeout seconds if there is no reply.

   In
   - msg_length_computed must be set to MSG_LENGTH_UNDEFINED if undefined

   Out
   - msg is an array of System::Byte to receive the message
   - p_msg_length, the variable is assigned to the number of
     characters received. This value won't be greater than
     msg_length_computed.

   Returns 0 in success or a negative value if an error occured.
*/
int ModbusProvider::receive_msg(int msg_length_computed, System::Byte *msg, System::Byte & p_msg_length, bool longRequest)
{	
	array<Byte> ^ bytes;
	int read_ret;	
	
	if(longRequest)
	{
		eventDataReceived->WaitOne(10000);	
	}
	else
	{
		if(!eventDataReceived->WaitOne(5000))
		{
			return -1;
		}
	}
	eventDataReceived->Reset();
	bytes = gcnew array<Byte>(256);				
	read_ret = this->port1->Read(bytes, 0, 256);							
	
	if(read_ret > 2)
	{
		/*if( check_crc16(bytes, read_ret) != 0)
		{		
			throw gcnew Exception("INVALID_CRC");
			return INVALID_CRC;
		}*/
	
		//System::Text::StringBuilder ^ b = gcnew System::Text::StringBuilder();
		int startIndex = 0;
		if(bytes[0] == 255)
		{
			startIndex = 1;
		}
		for(int i =startIndex; i < read_ret; i++)
		{			
			msg[i - startIndex] = bytes[i];
			//b->Append(bytes[i].ToString());
			//b->Append(" ");
		}

		

		//throw gcnew Exception(b->ToString());    
		p_msg_length = read_ret - startIndex;		
		return 0;	
	}
	else
	{
		return -1;
	}
	// OK
	
}


/* Computes the length of the header following the function code */
System::Byte ModbusProvider::compute_query_length_header(int function)
{
        int length = 0;
        
		for each(CustomFunctionMapping ^ current in this->mapping)
	    {
			if(current->function == function)
            {
                    return current->headerlength;
            }
		}
        
        /*
        if (function <= FC_FORCE_SINGLE_COIL ||
            function == FC_PRESET_SINGLE_REGISTER)
                // Read and single write
                length = 4;
        else if (function == FC_FORCE_MULTIPLE_COILS ||
                 function == FC_PRESET_MULTIPLE_REGISTERS)
                // Multiple write
                length = 5;
        else
                length = 0;
        */
        return length;
}


/* Computes the length of the data to write in the query */
int ModbusProvider::compute_query_length_data(System::Byte *msg)
{
        int function = msg[HEADER_LENGTH_RTU + 1];
        int length = 0;

		for each(CustomFunctionMapping ^ current in this->mapping)
		{
			if(current->function == function)
            {
                    return current->datalength + CHECKSUM_LENGTH_RTU;
            }
		}
        
        /*if (function == FC_FORCE_MULTIPLE_COILS ||
            function == FC_PRESET_MULTIPLE_REGISTERS)
                length = msg[HEADER_LENGTH_RTU + 6];
        else
                length = 0;

        length += mb_param->checksum_length;

       */
        return length;
}


/* If CRC is correct returns 0 else returns INVALID_CRC */
int ModbusProvider::check_crc16(array<Byte> ^ msg, int length)
{
        int ret;
        short crc_calc;
        short crc_received;

		if(length <= 2)
			return 0;
        crc_calc = crc16(msg, length - 2);
        crc_received = (short)msg[length - 2] << 8 | msg[length - 1];        
		//throw gcnew Exception(String::Format("{0} != {1}, {2}", crc_calc, crc_received, length));
        /* Check CRC of msg */
        if (crc_calc == crc_received) {
                ret = 0;
        } else {
                char s_error[64];
                sprintf(s_error,
                        "invalid crc received %0X - crc_calc %0X",
                        crc_received, crc_calc);
                ret = INVALID_CRC;
                //error_treat(mb_param, ret, s_error);
        }

        return ret;
}

/* Initializes the modbus_param_t structure for RTU
   - device: "/dev/ttyS0"
   - baud:   9600, 19200, 57600, 115200, etc
   - parity: "even", "odd" or "none"
   - data_bits: 5, 6, 7, 8
   - stop_bits: 1, 2
*/
void ModbusProvider::modbus_init_rtu()
{
	/*this->port1->PortName = gcnew String(device);
	this->port1->BaudRate = baud;
	this->port1->Parity = parity;
	this->port1->DataBits = data_bit;
	this->port1->StopBits = stop_bit;
	this->port1->ReadTimeout = 500;
	this->port1->WriteTimeout = 500;	
*/
	/*
        memset(mb_param, 0, sizeof(modbus_param_t));
        strcpy(mb_param->device, device);
        mb_param->baud = baud;
        strcpy(mb_param->parity, parity);
        mb_param->debug = false;
        mb_param->data_bit = data_bit;
        mb_param->stop_bit = stop_bit;
        mb_param->type_com = RTU;
        mb_param->header_length = HEADER_LENGTH_RTU;
        mb_param->checksum_length = CHECKSUM_LENGTH_RTU;
	*/
}

void ModbusProvider::OpenPort(Object ^ arg)
{	
	ModbusProvider ^ provider = (ModbusProvider ^)arg;
	try
	{
		if(!provider->port1->IsOpen)
		{
			provider->port1->Open();
			provider->port1->DiscardOutBuffer();
	        provider->port1->DiscardInBuffer();
		}

		
		unsigned char message_length = 0;
		unsigned char * buffer = new unsigned char[256];
		provider->receive_msg(256, buffer, message_length, false);
		delete [] buffer;
	}
	catch (IOException ^ ex)
	{
		return;
	}
}

int ModbusProvider::modbus_connect_rtu()
{	
	do
	{
		ParameterizedThreadStart ^ threadStart = gcnew ParameterizedThreadStart(ModbusProvider::OpenPort);		
		Thread ^ thread = gcnew Thread(threadStart);		
		thread->IsBackground = true;
		thread->Start(this);
		thread->Join(COM_WAIT_TIMEOUT);
		if(thread->ThreadState == ThreadState::Running)
		{
			Console::WriteLine("Device busy.");			
		}
	}
	while(!this->port1->IsOpen);

	/*
        struct termios tios;
        speed_t speed;

        if (mb_param->debug) {
                printf("Opening %s at %d bauds (%s)\n",
                       mb_param->device, mb_param->baud, mb_param->parity);
        }

        // The O_NOCTTY flag tells UNIX that this program doesn't want
        //   to be the "controlling terminal" for that port. If you
        //   don't specify this then any input (such as keyboard abort
        //   signals and so forth) will affect your process

        //   Timeouts are ignored in canonical input mode or when the
        //   NDELAY option is set on the file via open or fcntl
        mb_param->fd = open(mb_param->device, O_RDWR | O_NOCTTY | O_NDELAY);
        if (mb_param->fd < 0) {
                perror("open");
                printf("ERROR Can't open the device %s (errno %d)\n",
                       mb_param->device, errno);
                return -1;
        }

        // Save
        tcgetattr(mb_param->fd, &(mb_param->old_tios));

        memset(&tios, 0, sizeof(struct termios));

        // C_ISPEED     Input baud (new interface)
        //   C_OSPEED     Output baud (new interface)
       
        switch (mb_param->baud) {
        case 110:
                speed = B110;
                break;
        case 300:
                speed = B300;
                break;
        case 600:
                speed = B600;
                break;
        case 1200:
                speed = B1200;
                break;
        case 2400:
                speed = B2400;
                break;
        case 4800:
                speed = B4800;
                break;
        case 9600:
                speed = B9600;
                break;
        case 19200:
                speed = B19200;
                break;
        case 38400:
                speed = B38400;
                break;
        case 57600:
                speed = B57600;
                break;
        case 115200:
                speed = B115200;
                break;
        default:
                speed = B9600;
                printf("WARNING Unknown baud rate %d for %s (B9600 used)\n",
                       mb_param->baud, mb_param->device);
        }

        // Set the baud rate
        if ((cfsetispeed(&tios, speed) < 0) ||
            (cfsetospeed(&tios, speed) < 0)) {
                perror("cfsetispeed/cfsetospeed\n");
                return -1;
        }

        // C_CFLAG      Control options
        //   CLOCAL       Local line - do not change "owner" of port
        //   CREAD        Enable receiver
       
        tios.c_cflag |= (CREAD | CLOCAL);
        // CSIZE, HUPCL, CRTSCTS (hardware flow control)

        // Set data bits (5, 6, 7, 8 bits)
        //   CSIZE        Bit mask for data bits
        
        tios.c_cflag &= ~CSIZE;
        switch (mb_param->data_bit) {
        case 5:
                tios.c_cflag |= CS5;
                break;
        case 6:
                tios.c_cflag |= CS6;
                break;
        case 7:
                tios.c_cflag |= CS7;
                break;
        case 8:
        default:
                tios.c_cflag |= CS8;
                break;
        }

        // Stop bit (1 or 2)
        if (mb_param->stop_bit == 1)
                tios.c_cflag &=~ CSTOPB;
        else // 2
                tios.c_cflag |= CSTOPB;

        // PARENB       Enable parity bit
        //   PARODD       Use odd parity instead of even
        if (strncmp(mb_param->parity, "none", 4) == 0) {
                tios.c_cflag &=~ PARENB;
        } else if (strncmp(mb_param->parity, "even", 4) == 0) {
                tios.c_cflag |= PARENB;
                tios.c_cflag &=~ PARODD;
        } else {
                // odd
                tios.c_cflag |= PARENB;
                tios.c_cflag |= PARODD;
        }

        // Read the man page of termios if you need more information.

        // This field isn't used on POSIX systems
        //   tios.c_line = 0;
       

        // C_LFLAG      Line options

        //   ISIG Enable SIGINTR, SIGSUSP, SIGDSUSP, and SIGQUIT signals
        //   ICANON       Enable canonical input (else raw)
        //   XCASE        Map uppercase \lowercase (obsolete)
        //   ECHO Enable echoing of input characters
        //   ECHOE        Echo erase character as BS-SP-BS
        //   ECHOK        Echo NL after kill character
        //   ECHONL       Echo NL
        //   NOFLSH       Disable flushing of input buffers after
        //   interrupt or quit characters
        //   IEXTEN       Enable extended functions
        //   ECHOCTL      Echo control characters as ^char and delete as ~?
        //   ECHOPRT      Echo erased character as character erased
        //   ECHOKE       BS-SP-BS entire line on line kill
        //   FLUSHO       Output being flushed
        //   PENDIN       Retype pending input at next read or input char
        //   TOSTOP       Send SIGTTOU for background output

        //   Canonical input is line-oriented. Input characters are put
        //   into a buffer which can be edited interactively by the user
        //   until a CR (carriage return) or LF (line feed) character is
        //   received.

        //   Raw input is unprocessed. Input characters are passed
        //   through exactly as they are received, when they are
        //   received. Generally you'll deselect the ICANON, ECHO,
        //   ECHOE, and ISIG options when using raw input
        

        // Raw input
        tios.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

        // C_IFLAG      Input options

        //  Constant     Description
        //   INPCK        Enable parity check
        //   IGNPAR       Ignore parity errors
        //   PARMRK       Mark parity errors
        //   ISTRIP       Strip parity bits
        //   IXON Enable software flow control (outgoing)
        //   IXOFF        Enable software flow control (incoming)
        //   IXANY        Allow any character to start flow again
        //   IGNBRK       Ignore break condition
        //   BRKINT       Send a SIGINT when a break condition is detected
        //   INLCR        Map NL to CR
        //   IGNCR        Ignore CR
        //   ICRNL        Map CR to NL
        //   IUCLC        Map uppercase to lowercase
        //   IMAXBEL      Echo BEL on input line too long
        
        if (strncmp(mb_param->parity, "none", 4) == 0) {
                tios.c_iflag &= ~INPCK;
        } else {
                tios.c_iflag |= INPCK;
        }

        // Software flow control is disabled
        tios.c_iflag &= ~(IXON | IXOFF | IXANY);

        // C_OFLAG      Output options
        //   OPOST        Postprocess output (not set = raw output)
        //   ONLCR        Map NL to CR-NL

        //   ONCLR ant others needs OPOST to be enabled
        

        // Raw ouput
        tios.c_oflag &=~ OPOST;

        // C_CC         Control characters
        //   VMIN         Minimum number of characters to read
        //   VTIME        Time to wait for data (tenths of seconds)

        //   UNIX serial interface drivers provide the ability to
        //   specify character and packet timeouts. Two elements of the
        //   c_cc array are used for timeouts: VMIN and VTIME. Timeouts
        //   are ignored in canonical input mode or when the NDELAY
        //   option is set on the file via open or fcntl.

        //   VMIN specifies the minimum number of characters to read. If
        //   it is set to 0, then the VTIME value specifies the time to
        //   wait for every character read. Note that this does not mean
        //   that a read call for N bytes will wait for N characters to
        //   come in. Rather, the timeout will apply to the first
        //   character and the read call will return the number of
        //   characters immediately available (up to the number you
        //   request).

        //   If VMIN is non-zero, VTIME specifies the time to wait for
        //   the first character read. If a character is read within the
        //   time given, any read will block (wait) until all VMIN
        //   characters are read. That is, once the first character is
        //   read, the serial interface driver expects to receive an
        //   entire packet of characters (VMIN bytes total). If no
        //   character is read within the time allowed, then the call to
        //   read returns 0. This method allows you to tell the serial
        //   driver you need exactly N bytes and any read call will
        //   return 0 or N bytes. However, the timeout only applies to
        //   the first character read, so if for some reason the driver
        //   misses one character inside the N byte packet then the read
        //   call could block forever waiting for additional input
        //   characters.

        //   VTIME specifies the amount of time to wait for incoming
        //   characters in tenths of seconds. If VTIME is set to 0 (the
        //   default), reads will block (wait) indefinitely unless the
        //   NDELAY option is set on the port with open or fcntl.
        
        // Unused because we use open with the NDELAY option
        //tios.c_cc[VMIN] = 0;
        //tios.c_cc[VTIME] = 0;

        if(this->deviceType == RS534)
        {
            if (tcsetattr(mb_param->fd, TCSANOW, &tios) < 0) {
                    perror("tcsetattr\n");
                    return -1;
            }
        }

        return 0;
		*/
	return 0;
}



/* Closes the file descriptor in RTU mode */
void ModbusProvider::modbus_close_rtu()
{
	if(!this->port1->IsOpen) return;
	this->port1->Close();
	/*
        if (this->deviceType == RS534 && tcsetattr(mb_param->fd, TCSANOW, &(mb_param->old_tios)) < 0)
                perror("tcsetattr");

        close(mb_param->fd);
		*/
}


timespec * ModbusProvider::CreateTime(int aditionalInterval)
{	
    timespec * waittime = new timespec();

	DateTime ^ timeCurrent = DateTime::Now;
	DateTime ^ timeStart = gcnew DateTime(timeCurrent->Year, timeCurrent->Month, timeCurrent->Day, timeCurrent->Hour, timeCurrent->Minute, timeCurrent->Second);
	waittime->tv_nsec = (timeCurrent->Ticks -  timeStart->Ticks) * 100;
	waittime->tv_sec = 0;
    waittime->tv_nsec += aditionalInterval;

    if(waittime->tv_nsec > BILLION)
    {
        waittime->tv_sec++;
        waittime->tv_nsec-=BILLION;
    }

    return waittime;
}


timespec * ModbusProvider::CreateDelay(int delayInterval)
{
    timespec * delay = new timespec();

    delay->tv_sec = 0;
    delay->tv_nsec = delayInterval;

    return delay;
}


array<Byte> ^ ModbusProvider::ToByteArray(Byte * rawarray, unsigned long length)
{
	array<Byte> ^ convertedArray = gcnew array<Byte>(length);
	for(unsigned long i = 0; i < length; i++)
	{
		convertedArray[i] = rawarray[i];
	}

	return convertedArray;
}

void ModbusProvider::VerifyCOMPorts()
{
	/*
	if(this->port1->IsOpen)
		return;
	try
	{
		this->modbus_connect_rtu();
		portAvailable = true;
	}
	catch (IOException ^ ex)
	{
		portAvailable = false;
		return;
	}
	finally
	{
		this->modbus_close_rtu();
	}
	*/
}

Byte ModbusProvider::ToByte(Char value)
{
	return (Byte)value;
}