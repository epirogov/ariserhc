#pragma once
#include <list>

using namespace std;

namespace LibmodbusCustomCommands
{
	public ref class BooleanArrayParser
	{
	    public :
	    static void Parse(const char * content, bool * & array, int & lenght);
	    static bool Validate(const char * content);
	    static list<char*> * TrimString(const char * source, char delimiter);
	};
}