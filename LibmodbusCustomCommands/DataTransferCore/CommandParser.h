#pragma once
#include "CommandEntity.h"

using namespace System;

namespace LibmodbusCustomCommands
{
	public ref class CommandParser
	{
	    public :
	    static CommandEntity ^ Parse(Byte * rawcommand, unsigned int rawcommandlength);
	};
}
