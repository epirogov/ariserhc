#pragma once
#include "CommandEntity.h"
#include "CommandFactory.h"
#include "ModbusProvider.h"

namespace LibmodbusCustomCommands
{
	public ref class PrivateMessanger
	{
		protected :
		CommandFactory ^ commandFactory;
	    protected :
	    CommandEntity ^ SendWithResponse(CommandEntity ^ queryentity, ModbusProvider ^ provider, bool longRespone);
	};
}