#include "StdAfx.h"
#include "CommandEntity.h"
#include "ModbusProvider.h"

using namespace LibmodbusCustomCommands;

CommandEntity::CommandEntity(System::Byte address, System::Byte function, System::Byte subfunction, bool hasSubfunction, System::Byte * data, int datalength, int responselength) : CommandHeader(address, function, subfunction, hasSubfunction)
{
	this->CreateInternals(data,datalength,responselength);
    this->identifier = -1;
}

CommandEntity::CommandEntity(System::Byte address, System::Byte function, System::Byte subfunction, bool hasSubfunction, System::Byte * data, int datalength, int responselength, int identifier)  : CommandHeader(address, function, subfunction, hasSubfunction)
{
	this->CreateInternals(data,datalength,responselength);
	this->identifier = identifier;
}

void CommandEntity::CreateInternals(System::Byte * data, int datalength, int responselength)
{
	this->data = data;
    this->datalength = datalength;
    this->responselength = responselength;	
}

CommandEntity::~CommandEntity()
{
    delete [] this->data;
}

void CommandEntity::Build(System::Byte * rawcommand, int & rawcommandlength)
{
    int commandlengthWithoutCRC = 1 + 1 + (this->HasSubfunction() ? 1 : 0) + this->datalength;
    rawcommandlength = commandlengthWithoutCRC + 2;    
    rawcommand[0] = this->GetAddress();
    rawcommand[1] = this->GetFunction();
    int index = 2;
    if(this->HasSubfunction())
    {
        rawcommand[2] = this->GetSubFunction();
        index = 3;
    }

    for(int i = 0; i < this->datalength; i++)
    {
        rawcommand[index++] = this->data[i];
    }

    short crc = ModbusProvider::crc16(ModbusProvider::ToByteArray(rawcommand, commandlengthWithoutCRC), commandlengthWithoutCRC);	
    rawcommand[index++] = (unsigned int)crc >> 8;
    rawcommand[index++] = (unsigned int)crc & 0xFF;
}