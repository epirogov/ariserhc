#pragma once
#include <stdlib.h>
#include <time.h>
#include "CustomFunctionMapping.h"
#include "DeviceTypeEnum.h"
#include "timespec.h"

#define PORT_SOCKET_FAILURE     -0x0D
#define MSG_LENGTH_UNDEFINED -1
#define TIME_OUT_BEGIN_OF_TRAME 4
#define SELECT_FAILURE          -0x0E
#define CONNECTION_CLOSED       -0x12
#define MAX_MESSAGE_LENGTH     256
#define TOO_MANY_DATA           -0x0F
#define TIME_OUT_END_OF_TRAME   500000
#define INVALID_CRC             -0x10
#define COM_TIME_OUT           -0x0C
#define HEADER_LENGTH_RTU           0
#define CHECKSUM_LENGTH_RTU      2
#define COM_WAIT_TIMEOUT 10000000000

using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO::Ports;
using namespace System::Threading;

namespace LibmodbusCustomCommands
{
	public ref class ModbusProvider
	{
		public :
		/* Table of CRC values for high-order byte */
	    static unsigned char * table_crc_hi;
		
		/* Table of CRC values for low-order byte */
	    static unsigned char * table_crc_lo;
		
	    private :		
		
		SerialDataReceivedEventHandler ^ dataReceived;
		volatile bool dataReceivedFlag;
	    DeviceTypeEnum deviceType;	    
	    int baud;
	    Parity parity;
	    int data_bit;
	    StopBits stop_bit;

		SerialPort ^ port1;	
		
		ManualResetEvent ^ eventDataReceived;		
		List<CustomFunctionMapping ^> ^ mapping;
	    public :		
		ModbusProvider(DeviceTypeEnum deviceType, SerialPort ^ serialPort, int baud, Parity parity, int data_bit, StopBits stop_bit, List<CustomFunctionMapping ^> ^ mapping);
	    ~ModbusProvider();
		static void CreateTables();
	    void Initialize();
	    void SendData(System::Byte * query, System::Byte querylength, System::Byte * response, System::Byte responseLength, System::Byte & realResponseLength, bool longRespone);
	    void Close();		 
	    static short crc16(array<Byte> ^ buffer, short buffer_length);
	    int modbus_send(System::Byte *query, int query_length);
	    int receive_msg(int msg_length_computed, System::Byte *msg, System::Byte & p_msg_length, bool longRequest);
	    static timespec * CreateTime(int aditionalInterval);
	    static timespec * CreateDelay(int delayInterval);
		static array<Byte> ^ ToByteArray(Byte * rawarray, unsigned long length);
		static int check_crc16(array<Byte> ^msg, int length);
	    private :
	    System::Byte compute_query_length_header(int function);
	    int compute_query_length_data(System::Byte *msg);	    
	    
	    void modbus_init_rtu();
	    int modbus_connect_rtu();
	    void modbus_close_rtu();
		void ModbusProvider_Port1_DataReceived(Object ^ sender, SerialDataReceivedEventArgs ^ e);	
		void VerifyCOMPorts();
		static void OpenPort(Object ^ arg);
		static Byte ToByte(Char value);
	};
}