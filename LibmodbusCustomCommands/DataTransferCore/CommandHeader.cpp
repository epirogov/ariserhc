#include "StdAfx.h"
#include "CommandHeader.h"

using namespace LibmodbusCustomCommands;

CommandHeader::CommandHeader(System::Byte address, System::Byte function, System::Byte subfunction, bool hasSubfunction)
{
    this->address = address;
    this->function = function;
    this->subfunction = subfunction;
    this->hasSubfunction = hasSubfunction;
}

CommandHeader::~CommandHeader()
{
}

System::Byte CommandHeader::GetAddress()
{
    return this->address;
}

System::Byte CommandHeader::GetFunction()
{
    return this->function;
}

bool CommandHeader::HasSubfunction()
{
    return this->hasSubfunction;
}

System::Byte CommandHeader::GetSubFunction()
{
    if(this->hasSubfunction)
    {
        return this->subfunction;
    }

    return 0;
}

void CommandHeader::SetFunction(System::Byte function)
{
	this->function = function;
}

void CommandHeader::SethasSubfunction(bool hasSubfunction)
{
	this->hasSubfunction = hasSubfunction;
}

void CommandHeader::SetSubFunction(System::Byte subfunction)
{
	this->subfunction = subfunction;
}