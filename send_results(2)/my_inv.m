function iM=my_inv(M)
if numel(M)==1,
    iM=1/M;
    return
end
ia=1/M(1);
B=M(1,2:end);
C=M(2:end,1);
D=M(2:end,2:end);
iM=[ia+ia*B*my_inv(D-C*ia*B)*C*ia -ia*B*my_inv(D-C*ia*B);
    -my_inv(D-C*ia*B)*C*ia                  my_inv(D-C*ia*B)];