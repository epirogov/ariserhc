function ms=my_param_comb(D,a)

ms=ones(size(D,1),1);
for k=1:length(a),
    ms=ms.*D(:,a(k));
end
