function e=my_err_func(D,W,CF,tMI,r)

TT=[];
for i=1:r,
    TT=[TT my_param_comb(D,CF(tMI(i),:))];
end
h=inv(TT'*TT)*TT'*W;
e=mean(sqrt(((TT*h-W)./W).^2));