function M=my_perm_coef_eq(n,p)

M=ones(1,p);
while prod(M(end,:))<n^p
    v=M(end,:);
    v(p)=v(p)+1;
    ind=find(rem(v,n+1)==0);
    while ~isempty(ind)
        v(ind(1)-1)=v(ind(1)-1)+1;
        v(ind(1):end)=v(ind(1)-1);
        ind=find(rem(v,n+1)==0);
    end
    M=[M;v];
end
