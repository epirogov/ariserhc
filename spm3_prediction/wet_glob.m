close all
clear all
clc
% load data: 
% CD - calibrating data
% MD - measured data
% W - wet (target data)
load 'WET2.mat'

%initial order of model
p=0;
%initial output error (array for target data)
mE=1;
%persent of error tolerance
%here the error is 2%
e=0.02;

%data normalization
D=[ones(size(MD,1),1) (MD-CD)./CD];
sz=size(D);

% find the order of the model, the number of coefficients and its values
while mean(mE)>e,
    %increase order
    p=p+1;
    %find all coefficients
    CF=my_perm_coef_eq(sz(2),p);
    %find the number of coefficients
    %set of coefficients is empty
    MI=[];
    %residual set has all coefficients
    MU=1:size(CF,1);
    r1=length(MI);
    r2=length(MU);

    while abs(r1-r2)
        %add coefficients which decrease the fitting error
        [MI,MU]=my_add_coef(D,W,CF,e,MI,MU);
        
        %subscribe coefficients which do not exceed the tolerance error
        [MI,MU]=my_sub_coef(D,W,CF,e,MI,MU);
        r2=r1;
        r1=length(MI);
    end
    %set of important coefficients
    CF=CF(MI,:);
    %create new dataset from initial data and combinational coefficients  
    TT=[];
    for i=1:size(CF,1),
        TT=[TT my_param_comb(D,CF(i,:))];
    end
    %find linear coefficients
    %when you write you code in C, you have to use recursive function
    %my_inv.m
    h=inv(TT'*TT)*TT'*W;
    %find error
    mE=sqrt(((TT*h-W)./W).^2);
end

%compare final result
[TT*h W]
%order of model
p
%the number of linear coefficients
length(MI)
%compare different errors
[mean(mE) median(mE) mode(mE) max(mE)]
%plot errors
%some data have spike errors (probably those data were measured with error)
plot(mE)