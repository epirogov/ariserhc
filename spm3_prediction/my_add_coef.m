function [MI,MU]=my_add_coef(D,W,CF,e,MI,MU)
sd=size(D);
sc=size(CF);
r=length(MI);
mn=1;
while (mn>e)&&(r<sd(1))&&(r<sc(1)),
    mE=[];
    for m=1:length(MU),
        tMI=[MI MU(m)];
        mE(m)=my_err_func(D,W,CF,tMI,r+1);
    end 
    [mn,ind]=min(mE);
    MI=[MI MU(ind(1))];
    MU=setdiff(MU,MI);
    r=length(MI);
end