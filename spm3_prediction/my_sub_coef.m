function [MI,MU]=my_sub_coef(D,W,CF,e,MI,MU)

r=length(MI);
done=0;
while ~done,
    mE=[];
    for m=1:r,
        tMI=[MI(1:m-1) MI(m+1:end)];
        mE(m)=my_err_func(D,W,CF,tMI,r-1);
    end
    [mn,ind]=min(mE);
    if mn<=e,
        MU=[MU MI(ind(1))];
        MI=setdiff(MI,MU);
        r=r-1;
    else
        done=1;
    end
end
