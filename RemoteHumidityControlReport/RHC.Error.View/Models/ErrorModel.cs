﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RHC.Error.View.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string ExceptionType { get; set; }

        public string FullMessage { get; set; }

        public DateTime Date { get; set; }
    }
}
