﻿using RHC.Error.View.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RHC.Error.View.Models
{
    public class ErrorsModel
    {
        public List<ErrorModel> Errors { get { return new ErrorController().GetErrors(); } }
    }
}
