﻿using LibraryBusinessLogic;
using RHC.Error.View.Models;
using RHC.View;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace RHC.Error.View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            new MainWindow { DataContext = new ErrorsModel() }.Show();

            if (!Repository.CheckConnection())
            {
                var error = new NoConnectionWindow();
                error.ShowDialog();
            }

            base.OnStartup(e);
        }
    }
}
