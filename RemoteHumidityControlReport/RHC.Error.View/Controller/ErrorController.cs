﻿using LibraryBusinessLogic;
using RHC.Error.View.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace RHC.Error.View.Controller
{
    internal class ErrorController
    {
        internal List<ErrorModel> GetErrors()
        {
            try
            {
                return Repository.Select<ErrorModel>("GetErrors", new List<SqlParameter>()).Where(e => e.ExceptionType != "RemoteHumidityControlReport.Logic.Exceptions.ObviousException").ToList();
            }
            catch (Exception)
            {
            }

            return new List<ErrorModel>();
        }
    }
}
