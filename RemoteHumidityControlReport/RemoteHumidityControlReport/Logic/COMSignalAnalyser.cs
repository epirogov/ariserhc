﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.Logic
{
	/// <summary>
	/// Description of COMSygnalAnalyser.
	/// </summary>
	public static class ComSignalAnalyser
	{
		public static ComSignalTypeEnum Detect(byte [] rawData)
		{
			if(rawData[0] == 0x2 && rawData[1] == 0x44 && rawData[2] == 0x0 && rawData[3] == 0x30 && rawData[4] == 0x1)
			{
				return ComSignalTypeEnum.KeyboardScan;
			}
			
			if(rawData[0] == 0x2 && rawData[1] == 0x67)
			{
				return ComSignalTypeEnum.SetCursor;
			}
			
			if(rawData[0] == 0x2 && rawData[1] == 0x68)
			{
				return ComSignalTypeEnum.PrintXy;
			}			
			
			if(rawData[0] == 0x2 && rawData[1] == 0x66)
			{
				return ComSignalTypeEnum.ClrScr;
			}
				
			return ComSignalTypeEnum.Unknown;
		}
	}
}
