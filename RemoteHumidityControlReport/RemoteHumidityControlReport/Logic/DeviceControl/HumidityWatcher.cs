﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.Linq;
using LibmodbusCustomCommands;
using System.IO.Ports;
using LibraryBusinessLogic;
using System.Data.SqlClient;
using RemoteHumidityControlReport.DataAccess;
using RemoteHumidityControlReport.Entity;
using RemoteHumidityControlReport.Logic.Exceptions;
using System.Configuration;

namespace RemoteHumidityControlReport.Logic.DeviceControl
{
    public class HumidityWatcher
    {        
    	private readonly ControllerConnector _connector;
    	
        private TimeSpan _duration;
        private readonly int _period;

        private readonly List<Spm3DeviceInfo> _controllers = new List<Spm3DeviceInfo>();
        private int _controllerIndex;
        public event EventHandler<GEventArgs<CommandEntity>> DataSent;
        public event EventHandler<GEventArgs<IEnumerable<ControllerGeneralDataEntity>>> DataReceived;
        public event EventHandler<GEventArgs<byte>> ControllerFounded;
        public event EventHandler<EventArgs> ControllerRequest;

        public HumidityWatcher()
        {
            _period = 30;
            int.TryParse(ConfigurationManager.AppSettings["Period"], out _period);
        }

        public HumidityWatcher(SerialPort serialPort, TimeSpan duration)
        {
            _period = 30;
            int.TryParse(ConfigurationManager.AppSettings["Period"], out _period);

            _connector = new ControllerConnector(serialPort, _controllers, _period);
            _duration = duration;
            _connector.DataSent += connector_DataSent;
            _connector.DataReceived += OnDataReceived;
            _connector.ControllerFounded += connector_ControllerFounded;
            _connector.ControllerUnsubscribe += connector_ControllerUnsubscribe;
            _connector.ControllerRequest += connector_ControllerRequest;
        }

        void connector_ControllerRequest(object sender, EventArgs e)
        {
            if (ControllerRequest != null)
            {
                ControllerRequest(this, e);
            }           
        }

        public List<Spm3DeviceInfo> Controllers
        {
            get
            {
                return _controllers;
            }
        }

        void connector_DataSent(object sender, GEventArgs<CommandEntity> e)
        {
            if (DataSent != null)
            {
                DataSent(this, e);
            }
        }

        private void OnDataReceived(object sender, GEventArgs<CommandEntity> data)
        {
        	unsafe
        	{
        		var byteArray = new byte[data.Value1.datalength];
        		
        		for(var i = 0; i < data.Value1.datalength; i++)
        		{
        			byteArray[i] = data.Value1.data[i];
        		}

                float humidity = byteArray[0] + byteArray[1] / 100.0f;
                float temperature = byteArray[2] + byteArray[3] / 100.0f;
                
	        	if(DataReceived != null)
                {                    
		        	DataReceived(
		                    this,
                            new GEventArgs<IEnumerable<ControllerGeneralDataEntity>>(new[]
                    {
                        new ControllerGeneralDataEntity(data.Value1.GetAddress(),
                                                    humidity,
                                                    temperature,
                                                    DateTime.Now)
                    }));

                    try
                    {
                        Repository.NoQuery("WriteControllerData", new List<SqlParameter> { 
                                new SqlParameter { DbType = System.Data.DbType.Int32, Direction = System.Data.ParameterDirection.Input, ParameterName = "@AddressId", SqlDbType = System.Data.SqlDbType.Int, Value = data.Value1.GetAddress()},
                                new SqlParameter { DbType = System.Data.DbType.Double, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Humidity", SqlDbType = System.Data.SqlDbType.Float, Value = humidity},
                                new SqlParameter { DbType = System.Data.DbType.Double, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Temperature", SqlDbType = System.Data.SqlDbType.Float, Value = temperature},
                                new SqlParameter { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Date", SqlDbType = System.Data.SqlDbType.DateTime, Value = DateTime.Now}});                        
                    }
                    catch (Exception ex1)
                    {
                        ErrorNotifier.SetError(ex1);
                    }
                }
        	}
	        _controllerIndex = (_controllerIndex + 1) % _controllers.Count;
        }
        
        private bool RegisterController(int contollerId)
        {
            if (_controllers.All(controller => controller.Address != contollerId))
            {                
                _controllers.Add(new Spm3DeviceInfo { Address = contollerId });
                return true;
            }
            return false;
        }

        private void UnsubscribeController(int contollerId)
        {
            if (_controllers.Any(controller => controller.Address == contollerId))
            {
                _controllers.Remove(_controllers.SingleOrDefault(controller => controller.Address == contollerId));
            }
        }

        public void Start()
        {
            if (_controllers.Count > 0)
            {
	            _connector.SetInterval((long)_duration.TotalMilliseconds);
	            _connector.Star();
	            _controllerIndex = 0;                                
            }
        }        

        public void Stop()
        {
            _connector.Stop();            
        }

        public bool ScanControllers()
        {
            return _connector.StartScaning();
        }

        private void connector_ControllerUnsubscribe(object sender, GEventArgs<byte> e)
        {
            UnsubscribeController(e.Value1);            
        }

        private void connector_ControllerFounded(object sender, GEventArgs<byte> e)
        {
            if (RegisterController(e.Value1) && ControllerFounded != null)
            {
                ControllerFounded(this, e);
            }
        }

        public SerialPort Serialport
        {
            get
            {
                return _connector.SerialPort;
            }
        }

        internal void RequestData(Spm3DeviceInfo sPm3DeviceInfo)
        {
            _connector.ReadPack();
        }
    }
}
