﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.Logic.DeviceControl
{
    public class Spm3DeviceInfo
    {
        public int Address
        {
            get;
            set;
        }

        public string DeviceName
        {
            get
            {
                return string.Format("Устройство № {0}", Address);
            }
        }

        public bool? State
        {
            get;
            set;
        }
    }
}
