﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.IO;

namespace RemoteHumidityControlReport.Logic.Exceptions
{
    public static class ErrorNotifier
    {
        public static event EventHandler<GEventArgs<Exception>> OnError;

        public static void SetError(Exception ex)
        {
        	if (LogToFile /*&& !(ex is ObviousException)*/)
        	{
        		File.AppendAllText(@"c:\exceptions.txt", string.Format("{0}{1}{2}",ex.Message, Environment.NewLine, ex.StackTrace));
        	}
        	
            if (OnError != null)
            {
                OnError(new Object(), new GEventArgs<Exception>(ex));
            }
        }
        
        public static bool LogToFile
        {
        	get;
        	set;
        }        	
    }
}
