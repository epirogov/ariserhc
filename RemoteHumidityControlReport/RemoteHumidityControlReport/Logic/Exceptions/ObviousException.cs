﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Runtime.Serialization;

namespace RemoteHumidityControlReport.Logic.Exceptions
{
    public class ObviousException : Exception
    {
        public ObviousException()
        {
        }

        public ObviousException(string message)
            : base(message)
        {
        }

        public ObviousException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ObviousException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
