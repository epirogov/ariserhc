﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Remote humidity control / Report")]
[assembly: AssemblyDescription(@"Product has research purposes, do not use any decompiler software see arise-project.org.ua istead.

Vendor : thegra.kiev.ua

Single manufactor rights.
")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MWT Dnepropetrovsk")]
[assembly: AssemblyProduct("Remote Humidity Control. Report.")]
[assembly: AssemblyCopyright("MWT Dnepropetrovsk, Eugene Pirogov Copyright ©  2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ad8439b4-d43c-4c9d-bf4d-6dc8b915f9b3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.0")]
