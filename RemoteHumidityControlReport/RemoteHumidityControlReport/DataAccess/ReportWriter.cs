﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using RemoteHumidityControlReport.Entity;

namespace RemoteHumidityControlReport.DataAccess
{
    public static class ReportWriter
    {
        public static void WriteReport(string filename, IEnumerable<ControllerGeneralDataEntity> data)        
        {
            var controllerGeneralDataEntities = data as ControllerGeneralDataEntity[] ?? data.ToArray();
            if(!controllerGeneralDataEntities.Any())
                return;

            _Application oXl = new Application { Visible = true};
            var oWb = (_Workbook)oXl.Workbooks.Add();
            var oSheet = (_Worksheet)oWb.ActiveSheet;

            oSheet.Cells[1, 1] = "Отчет влагомер SPM3";
            oSheet.Cells[2, 1] = string.Format("Прибор № {0} ",
                controllerGeneralDataEntities.First().Address);
            oSheet.Cells[3, 1] = string.Format("Период от {0} до {1} ", 
            controllerGeneralDataEntities.Min(dataEntity => dataEntity.Date), 
            controllerGeneralDataEntities.Max(dataEntity => dataEntity.Date));

            oSheet.Cells[4, 1] = "Дата";
            oSheet.Cells[4, 2] = "Влажность";
            oSheet.Cells[4, 3] = "Температура";
            int index = 6;
            controllerGeneralDataEntities.ToList().ForEach(dataEntity => 
            {
                oSheet.Cells[index, 1] = dataEntity.Date;
                oSheet.Cells[index, 2] = dataEntity.Humidity;
                oSheet.Cells[index++, 3] = dataEntity.Temperature;                
            });

            oXl.SaveWorkspace(filename);
        }
    }
}
