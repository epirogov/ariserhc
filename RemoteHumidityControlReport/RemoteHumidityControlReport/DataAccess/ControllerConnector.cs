﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using LibmodbusCustomCommands;
using System.Threading;
using System.IO.Ports;
using RemoteHumidityControlReport.Logic;
using RemoteHumidityControlReport.Logic.Exceptions;
using RemoteHumidityControlReport.Logic.DeviceControl;
using System.IO;
using System.Configuration;
using Timer = System.Timers.Timer;

namespace RemoteHumidityControlReport.DataAccess
{
	/// <summary>
	/// Description of ControllerConnector.
	/// </summary>
	public class ControllerConnector
	{
		//const int ScanController = 0x1;
		const int RegisterController = 0x2;
		const int DeviceWasHalted = 0x4;
		const int RequiestData = 0x8;
		const int DataPacketReceived = 0x10;
		//const int DataPacketBrocken = 0x20;
		const int DataTransactionComplited = 0x40;
		//const int BeginDataSend = 0x80;
		//const int SendDataPacket = 0x100;
		const int EndDataSend = 0x200;
		const int NoMedium = 0x400;

		private readonly Timer _timer = new Timer();		
		private readonly DataTransferMaster _master;
		volatile bool _isAsynchronousResponse;
		volatile byte _controllerId;
		
		private readonly object _waitScanning = new object();
		private readonly object _waitResponse = new object();        
		private readonly int _period = 30;
		private readonly List<int> _requiredDevices = new List<int>();
		//private int controllerIndex;

		public event EventHandler<GEventArgs<CommandEntity>> DataSent;
		public event EventHandler<GEventArgs<CommandEntity>> DataReceived;
		public event EventHandler<GEventArgs<byte>> ControllerFounded;
		public event EventHandler<GEventArgs<byte>> ControllerUnsubscribe;
		public event EventHandler<EventArgs> ControllerRequest;
		public readonly CommandFactory CommandFactory = new CommandFactory();
		

		public SerialPort SerialPort
		{
			get;
			set;
		}

		public ControllerConnector(SerialPort serialPort, List<Spm3DeviceInfo> controllers, int period)
		{
			//File.WriteAllText(@"C:\rhc.log", @"Data" + DateTime.Now.ToLongTimeString() + Environment.NewLine);
			_period = period;
			SerialPort = serialPort;
			SerialPort.DataReceived += serialPort_DataReceived;
			_master = new DataTransferMaster(serialPort, 30 * 1000);
			//master.Open();                
			ControllerIDs = controllers;

			var thread = new Thread(ReadPack) {IsBackground = true};
		    thread.Start();
			_master.DataSent += master_DataSent;
			_master.CompliteTemplate += master_CompliteTemplate;

			var addresses = ConfigurationManager.AppSettings["RequredDeviceAddresses"];			
			int address1;
			if (addresses.Split(';').All(address => int.TryParse(address, out address1)))
			{
				_requiredDevices = addresses.Split(';').Select(int.Parse).ToList();
			}
		}

		
        private readonly object _syncDataRead = new object();

		void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
            if (Monitor.TryEnter(_syncDataRead))
            {
                //File.AppendAllText(@"C:\rhc.log", "Data" + DateTime.Now.ToLongTimeString() + Environment.NewLine);
                //Thread thread = new Thread(new ThreadStart(delegate()
                //    {
                try
                {
                    if (_isAsynchronousResponse)
                    {
                        var bufferUnion = new byte[0];
                        var registerController = false;

                        ReleaseData();
                        try
                        {
                            var buffer = new byte[256];
                            var buffer1 = new byte[256];

                            Thread.Sleep(500);
                            var length = SerialPort.Read(buffer, 0, 256);
                            buffer = buffer.Take(length).ToArray();
                            registerController = (_requiredDevices.Count > 0 ||
                            ControllerIDs.Any(controller => controller.State == true)) &&
                            buffer
                            .Select((value, i) => new { Value = value, Index = i })
                            .Any(data =>
                            (_requiredDevices.Contains(data.Value) ||
                            ControllerIDs.Any(controller => controller.Address == data.Value)) &&
                            buffer.Count() > data.Index + 2 &&
                            buffer[data.Index + 1] == 0x51);
                            if (!registerController && length < 7)
                            {
                                Thread.Sleep(100);
                                if (SerialPort.BytesToRead == 0)
                                {
                                    Monitor.Exit(_syncDataRead);
                                    return;
                                }
                                int length1 = SerialPort.Read(buffer1, 0, 256);
                                buffer1 = buffer1.Take(length1).ToArray();
                                bufferUnion = buffer.Union(buffer1).ToArray();

                                registerController = (_requiredDevices.Count > 0 ||
                                ControllerIDs.Any(controller => controller.State == true)) &&
                                bufferUnion
                                .Select((value, i) => new { Value = value, Index = i })
                                .Any(data =>
                                (_requiredDevices.Contains(data.Value) ||
                                ControllerIDs.Any(controller => controller.Address == data.Value)) &&
                                bufferUnion.Count() > data.Index + 2 &&
                                bufferUnion[data.Index + 1] == 0x51);
                            }
                            else
                            {
                                bufferUnion = buffer;
                            }
                        }
                        catch (TimeoutException)
                        {
                        }
                        catch (InvalidOperationException ex)
                        {
                            ErrorNotifier.SetError(ex);
                        }

                        if (registerController)
                        {
                            int registeredController = bufferUnion
                                .Select((v, i) => new { V = v, I = i })
                                .Where(value => value.I > 0 &&
                                    (_requiredDevices.Contains(value.V) ||
                                    ControllerIDs.Any(controller => controller.Address == value.V)) &&
                                    bufferUnion.Count() > value.I + 2 &&
                                    bufferUnion[value.I + 1] == 0x51)
                                .Select(value => value.V)
                                .FirstOrDefault();

                            if (registeredController > 0)
                            {
                                if (_requiredDevices.Contains(registeredController) ||
                            	    ControllerIDs.Any(controller => controller.State.GetValueOrDefault()))
                                {
                                    if (_requiredDevices.Contains(registeredController))
                                    {
                                        _requiredDevices.Remove(registeredController);
                                    }
                                    

                                    if (ControllerFounded != null)
                                    {
                                        ControllerFounded(this, new GEventArgs<byte>((byte)registeredController));
                                    }
                                    if (ControllerIDs.Any(controller => controller.Address == registeredController))
                                    {
                                        ControllerIDs.First(controller => controller.Address == registeredController).State = null;
                                    }                                    
                                }
                            }
                            Monitor.Exit(_syncDataRead);
                            return;
                        }

                        int index = bufferUnion
                                .Select((v, i) => new { V = v, I = i })
                                .Where(value => value.I > 0 &&                                    
                                    ControllerIDs.Any(c => c.Address == bufferUnion[value.I - 1]) &&
                                    value.V == 0x54)
                                .Min(value => value.I);

                        if (index == 0 || ControllerIDs.All(c => c.Address != bufferUnion[index - 1]) ||
                            bufferUnion.Count() < 5 + index)
                        {
                            Monitor.Exit(_syncDataRead);
                            return;
                        }

                        var address = bufferUnion[index - 1];
                        
                        var result = bufferUnion.Skip(index + 1).Take(4);
                        //bufferUnion
                        //    .Select((v, i) => new { V = v, I = i})
                        //    .Where(value => value.V == 0x54)
                        //    .Min(value => value.I) + 1);

                        var enumerable = result as byte[] ?? result.ToArray();
                        ErrorNotifier.SetError(
                                        new ObviousException(
                                            string.Format(
                                            "Data Packet Received adr [{0}] {1} ",
                                            address,
                                            string.Join(",", enumerable.Select(value => value.ToString(CultureInfo.InvariantCulture))))));
                        if (DataReceived != null)
                        {
                            unsafe
                            {
                                fixed (byte* entityData = enumerable.ToArray())
                                {
                                    DataReceived(this,
                                        new GEventArgs<CommandEntity>(
                                            CommandFactory
                                            .CreateDataPacketReceived(
                                            address,
                                            entityData,
                                            4)));
                                }
                            }
                        }

                    }
                }
                catch (Exception)
                {
                }
                //}));
                //thread.IsBackground = true;
                //thread.Start();            
                //if (thread.IsAlive && !thread.Join(10000))
                //{
                //    this.serialPort.DiscardInBuffer();
                //    //thread.Interrupt();                
                //}

                Monitor.Exit(_syncDataRead);
            }

			
		}

		void master_DataSent(object sender, TypedEventArgs<CommandEntity> e)
		{
			if (DataSent != null)
			{
				DataSent(this, new GEventArgs<CommandEntity>(e.Value1));
			}
		}

		public List<Spm3DeviceInfo> ControllerIDs 
		{
			get;
			set;
		}

	    public void SetInterval(long interval)
		{
			//timer.Stop();			
			//timer.Interval =  interval;
			//timer.Start();
		}
		
		public void Star()
		{
			if(_timer.Interval > 0)
			{
				_timer.Start();
			}
		}
		
		public void Stop()
		{
            _master.Close();
			//timer.Stop();
		}
		
		public bool StartScaning()
		{
			//bool result = false;

			_isAsynchronousResponse = false;
			for (byte i = 1; i < 6; i++)
			{
				if (ControllerRequest != null)
				{
					ControllerRequest(this, new EventArgs());
				}
				try
				{
					_master.ScanController(i);
					WaitScaning();
					//if (WaitScaning() && requiredDevices.Contains(i))
					//{
					//   ControllerIDs.Add(new SPM3DeviceInfo() { Address = i, State = true });
					//};
				}
				catch (TimeoutException)
				{
					try
					{
						_master.ScanController(i);
						WaitScaning();
						//if (!WaitScaning() && requiredDevices.Contains(i))
						//{
						//   ControllerIDs.Add(new SPM3DeviceInfo() { Address = i, State = true });
						//}                        
					}  
					catch(Exception)
					{
					}
				}
				catch (InvalidOperationException ex)
				{
					ErrorNotifier.SetError(ex);
				}                
			}
			_isAsynchronousResponse = true;

			return true;
		}     
		

		private bool WaitData(out TimeSpan waitTime)
		{
			var periodTime = new TimeSpan(0, 0, _period);
			var time = DateTime.Now;
			Monitor.Enter(_waitResponse);
			Monitor.Wait(_waitResponse, periodTime);
			Monitor.Exit(_waitResponse);
			waitTime = DateTime.Now - time;
			return waitTime < periodTime;
		}

		private void ReleaseData()
		{
			Monitor.Enter(_waitResponse);
			Monitor.PulseAll(_waitResponse);
			Monitor.Exit(_waitResponse);
		}

		private void WaitScaning()
		{
			Monitor.Enter(_waitScanning);
			Monitor.Wait(_waitScanning, 4 * 1000);
			Monitor.Exit(_waitScanning);
		}

		private void ReleaseScaning()
		{
			Monitor.Enter(_waitScanning);
			Monitor.PulseAll(_waitScanning);
			Monitor.Exit(_waitScanning);
		}

	    public void ReadPack()
		{
			_isAsynchronousResponse = true;

			while (true)
			{

				if (ControllerIDs.Count <= _controllerId && _requiredDevices.Count <= _controllerId)
				{
					_controllerId = 0;
				}
				
				if (!_isAsynchronousResponse || _requiredDevices.Count == 0 && ControllerIDs.Count == 0)
				{
					Thread.Sleep(5000);
					continue;
				}
				
				//ReleaseScaning();

				//Action requiestData = () => master.RequestData(controllerId, 0, 50 * 1000);
				//TimedOperation(requiestData);            

				//WaitScaning();
				
				unsafe
				{
					var waitTime = new TimeSpan();
					Action sendDataPacket = () =>
					{
						fixed (byte* data = new byte[0])
						{
							try
							{
								bool isRequest = false;
								if (ControllerIDs.Count > _controllerId && ControllerIDs[_controllerId].State != true)
								{
									_master.SendDataPacket((byte)ControllerIDs[_controllerId].Address, 0, 0, data, 0);
									if (!WaitData(out waitTime))
									{
										ControllerIDs[_controllerId].State = ControllerIDs[_controllerId].State.HasValue;
										//MessageBox.Show("Failed " + ControllerIDs[controllerId].State.ToString());
										if (ControllerIDs[_controllerId].State == true)
										{
											isRequest = true;
										}
									}
									else
									{
										ControllerIDs[_controllerId].State = null;                                
									}
								}
								else
								{
									isRequest = true;
								}

								if (isRequest || _requiredDevices.Count > _controllerId)
								{
									try
									{
										if (ControllerRequest != null)
										{
											ControllerRequest(this, new EventArgs());
										}
										if(ControllerIDs.Count() > _controllerId && ControllerIDs[_controllerId].State == true)
										{
											_master.ScanControllerRequest((byte)ControllerIDs[_controllerId].Address);
											ErrorNotifier.SetError(
											new ObviousException(
												string.Format(
												"Request Controller {0} ", (byte)ControllerIDs[_controllerId].Address
												)));
                                            Thread.Sleep(1000);
										}
										else
										{
											Thread.Sleep(5000);
											try
											{
											_master.ScanControllerRequest((byte)_requiredDevices[_controllerId]);
											ErrorNotifier.SetError(
											new ObviousException(
												string.Format(
												"Request Controller {0} ", (byte)_requiredDevices[_controllerId]
												)));
											}
											catch(ArgumentOutOfRangeException)
											{
												
											}
											Thread.Sleep(5000);
										}
										
									}
									catch (TimeoutException)
									{
									}
									catch (InvalidOperationException ex)
									{
										ErrorNotifier.SetError(ex);
									}
								}
							}
							catch (TimeoutException)
							{
							}
							catch (InvalidOperationException ex)
							{
								ErrorNotifier.SetError(ex);
							}
						}
					};

					sendDataPacket();
					if(new TimeSpan(0, 0, _period / 2) > waitTime)
						Thread.Sleep(new TimeSpan(0, 0, _period / 2) - waitTime);
					//while (!transactionComplited)
					//{
					//    master.SendDataPacket(controllerId, 0, 0, data, 0);
					//    WaitScaning();
					//}

				}
				_controllerId++;
			}
		}

		void master_CompliteTemplate(object sender, TypedEventArgs<CommandEntity> e)
		{
		    //MessageBox.Show(e.Value1.identifier.ToString());
			switch (e.Value1.identifier)
			{
				case  RegisterController:
					ErrorNotifier.SetError(new ObviousException(string.Format("Register Controller {0}", e.Value1.GetAddress())));
					_requiredDevices.Remove(e.Value1.GetAddress());
					if (ControllerFounded != null)
					{   
						ControllerFounded(this, new GEventArgs<byte>(e.Value1.GetAddress()));                        
					}
					ReleaseScaning();
					if (ControllerIDs.Any(controller => controller.Address == e.Value1.GetAddress()))
					{
                        ControllerIDs.First(controller => controller.Address == e.Value1.GetAddress()).State = null;
					}
					break;
				case DeviceWasHalted:
					ErrorNotifier.SetError(new ObviousException("Device Was Halted"));
					ControllerUnsubscribe(this, new GEventArgs<byte>(e.Value1.GetAddress()));
					break;
				case RequiestData:
					ErrorNotifier.SetError(new ObviousException("Requiest Data"));
					ReleaseScaning();
					break;
				case DataPacketReceived:
					ReleaseScaning();
					break;
				case DataTransactionComplited:
					ErrorNotifier.SetError(new ObviousException("Data Transaction Complited"));                    
					ReleaseScaning();
					break;
				case EndDataSend:
					ErrorNotifier.SetError(new ObviousException("End Data Send"));
					break;
				case NoMedium:
					ErrorNotifier.SetError(new ObviousException("No Medium"));					
					ReleaseScaning();
					break;
				default :
					//ErrorNotifier.SetError(
					//    new ObviousException( 
					//        string.Format(
					//        "Identifier{0}, Function {1}, Has SubFunction {2}, SubFunction{3}, Data length {4}",
					//        e.Value1.identifier, 
					//        e.Value1.GetFunction(), 
					//        e.Value1.HasSubfunction(), 
					//        e.Value1.GetSubFunction(), 
					//        e.Value1.datalength)));                    
					ReleaseScaning();
					break;
			}
		}
	}
}
