﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO.Ports;
using RemoteHumidityControlReport.Entity;
using RemoteHumidityControlReport.Logic;
using RemoteHumidityControlReport.VievInterface;
using RemoteHumidityControlReport.View;
using System.ComponentModel;
using RemoteHumidityControlReport.Logic.DeviceControl;
using RemoteHumidityControlReport.Logic.Exceptions;

namespace RemoteHumidityControlReport.Controller
{
    /// <summary>
    /// Description of ArchiveController.
    /// </summary>
    public class ArchiveController
    {
        public event EventHandler<GEventArgs<Spm3DeviceInfo>> ControllerFounded;        

        [Browsable(false)]
        public HumidityWatcher Watcher
        {
            get;
            set;
        }        

        public ArchiveController(IDataUserControl control, SerialPort serialPort)
        {
            ArciveView = control;
            ArciveView.SetController(this);
            if (serialPort != null)
            {
                Watcher = new HumidityWatcher(serialPort, new TimeSpan(0, 0, 12));
                Watcher.ControllerFounded += watcher_ControllerFounded;
                Watcher.DataReceived += watcher_DataReceived;
                Watcher.ControllerRequest += watcher_ControllerRequest;
            }
        }

        void watcher_ControllerRequest(object sender, EventArgs e)
        {
            ArciveView.OnControllerRequest();
        }

        void watcher_DataReceived(object sender, GEventArgs<IEnumerable<ControllerGeneralDataEntity>> e)
        {
            ArciveView.BindData(e.Value1);            
        }

        void watcher_ControllerFounded(object sender, GEventArgs<byte> e)
        {
            if (ControllerFounded != null)
            {
                ControllerFounded(this, new GEventArgs<Spm3DeviceInfo>(new Spm3DeviceInfo { Address = e.Value1 }));
            }
        }
        
        public IDataUserControl ArciveView
        {
            get;
            private set;
        }

        public bool ScanDevices()
        {
            bool result = Watcher.ScanControllers();
            if (result)
            {
                Watcher.Start();
            }
            return result;
        }

        public List<Spm3DeviceInfo> Controllers
        {
            get
            {
                return Watcher.Controllers;
            }
        }

        public bool OpenPort(string serialPortName)
        {            
            Watcher.Serialport.PortName = serialPortName;            
            if (Watcher.Serialport.IsOpen)
            {                    
                Watcher.Serialport.Close();
            }
            
            if (SerialPort.GetPortNames().All(port => serialPortName != port))
            {
                return false;
            }

            //Thread thread = new Thread(new ThreadStart(delegate()
            //{
                try
                {
                    if (!Watcher.Serialport.IsOpen)
                    {                        
                        Watcher.Serialport.Open();                    
                    }
                }
                catch (Exception ex)
                {                    
                    ErrorNotifier.SetError(ex);
                }
            //}));
            //thread.IsBackground = true;
            //thread.Start();
            //if(thread.IsAlive && !thread.Join(10000))
            //{
            //    this.watcher.serialport.DiscardInBuffer();
            //    //thread.Interrupt();                
            //}

            return true;
        }

        public void SelectSerialPort()
        {
            var form = new SelectSerialPortForm(SerialPort.GetPortNames());
            form.ShowDialog();
            if (form.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                OpenPort(form.SelectedPort);
            }
        }

        internal void RequestData(Spm3DeviceInfo sPm3DeviceInfo)
        {
            Watcher.RequestData(sPm3DeviceInfo);
        }
    }
}
