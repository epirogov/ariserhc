﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.StateManagement.Content
{
    public interface IContentState : IContentAction
    {
        ContentStateEnum StateValue
        {
            get;
            set;
        }

        ContentStateManager StateManager
        {
            get;
            set;
        }        
    }
}
