﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.StateManagement.Content
{
    public class RemoteControlState : BaseContentState
    {
        public RemoteControlState()
            : base(ContentStateEnum.RemoteControl)
        {
        }

        public override void OpenArchive()
        {
            SetState<ArchiveState>();
        }

        public override void OpenCharts()
        {
            SetState<ChartsState>();
        }

        public override void OpenRemoteControl()
        {
        }

        public override void OpenReport()
        {
            SetState<ReportState>();
        }
    }
}
