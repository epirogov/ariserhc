﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.StateManagement.Content
{
    public class ReportState : BaseContentState
    {
        public ReportState()
            : base(ContentStateEnum.Report)
        {
        }

        public override void OpenArchive()
        {
            SetState<ArchiveState>();
        }

        public override void OpenCharts()
        {
            SetState<ChartsState>();
        }

        public override void OpenRemoteControl()
        {
            SetState<RemoteControlState>();
        }

        public override void OpenReport()
        {
        }
    }
}
