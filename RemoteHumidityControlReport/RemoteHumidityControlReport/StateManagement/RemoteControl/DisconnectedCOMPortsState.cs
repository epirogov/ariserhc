﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */



namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of DisconnectedCOMPortsState.
	/// </summary>
	public class DisconnectedComPortsState : BaseComPortsState, IStd4408MainFormState
	{
		public DisconnectedComPortsState()
		{
			CreateInternals(null);
		}	
		
		public DisconnectedComPortsState(Std4408MainFormStateController controller)
		{
			CreateInternals(controller);
		}	
		
		public void ConnectComPorts()
		{
			Controller.SetComPortsState<ConnectedComPortsState>();
		}
		
		public void DisconnectComPorts()
		{
			
		}
		
		private void CreateInternals(Std4408MainFormStateController controller)
		{
			CreateInternals(controller, Std4408StateEnum.DisconnectedComPorts);			
		}
	}
}
