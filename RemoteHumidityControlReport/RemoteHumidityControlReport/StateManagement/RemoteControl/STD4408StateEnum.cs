﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	public enum Std4408StateEnum {DisconnectedComPorts, ConnectedComPorts};
}