﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of ThreeKeyScenarioState.
	/// </summary>
	public class ThreeKeyScenarioState : BaseKeyScenarioState, IKeyScenarioState
	{
		public ThreeKeyScenarioState()
		{
			CreateInternals(null);
		}
		
		public ThreeKeyScenarioState(Std4408MainFormStateController controller)
		{
			CreateInternals(controller);
		}
		
		private void CreateInternals(Std4408MainFormStateController controller)
		{
			CreateInternals(controller, KeyScenarioStateEnum.ThreeKey);
		}
		
		public void SetOneKeyScenario()
		{
			Controller.SetKeyScenarioState<OneKeyScenarioState>();
		}
		
		public void SetThreeKeyScenario()
		{
			
		}
	}
}
