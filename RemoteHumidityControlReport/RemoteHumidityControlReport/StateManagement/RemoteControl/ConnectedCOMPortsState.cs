﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of ConnectedCOMPortsState.
	/// </summary>
	public class ConnectedComPortsState : BaseComPortsState, IStd4408MainFormState
	{
		public ConnectedComPortsState()
		{			
			CreateInternals(null);
		}
		
		public ConnectedComPortsState(Std4408MainFormStateController controller)
		{			
			CreateInternals(controller);
		}	
		
		public void ConnectComPorts()
		{			
		}
		
		public void DisconnectComPorts()
		{
			Controller.SetComPortsState<DisconnectedComPortsState>();
		}
		
		private void CreateInternals(Std4408MainFormStateController controller)
		{
			CreateInternals(controller, Std4408StateEnum.ConnectedComPorts);			
		}
	}
}
