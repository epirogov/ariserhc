﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of KeyScenarioStateEnu.
	/// </summary>
	public enum KeyScenarioStateEnum
	{
		OneKey,
		ThreeKey
	}
}
