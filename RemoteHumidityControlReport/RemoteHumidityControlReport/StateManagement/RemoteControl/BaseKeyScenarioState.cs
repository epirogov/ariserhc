﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of BaseKeysScenarioState.
	/// </summary>
	public class BaseKeyScenarioState
	{
	    public Std4408MainFormStateController Controller {
			get;
			set;
		}
		
		public KeyScenarioStateEnum StateValue
		{
			get;
			set;
		}
		
		protected void CreateInternals(Std4408MainFormStateController controller, KeyScenarioStateEnum stateValue)
		{
			Controller = controller;			
			StateValue = stateValue;
		}
	}
}
