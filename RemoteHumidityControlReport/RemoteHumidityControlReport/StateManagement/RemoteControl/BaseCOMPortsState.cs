﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of BaseCOMPortsState.
	/// </summary>
	public class BaseComPortsState
	{
	    public Std4408MainFormStateController Controller {
			get;
			set;
		}
		
		public Std4408StateEnum StateValue {
			get;
			private set;
		}
		
		protected void CreateInternals(Std4408MainFormStateController controller, Std4408StateEnum stateValue)
		{
			Controller = controller;			
			StateValue = stateValue;
		}
	}
}
