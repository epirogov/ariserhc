﻿using LibraryBusinessLogic;
using RemoteHumidityControlReport.Logic.Exceptions;
using System;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using RemoteHumidityControlReport.View;

namespace RemoteHumidityControlReport
{
    static class Program
    {
        public static MainForm MainForm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
           Application.EnableVisualStyles();
           Application.SetCompatibleTextRenderingDefault(false);

           AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
           Application.ThreadException += Application_ThreadException;
           
            
            Repository.ConnectionString = ConfigurationManager.ConnectionStrings["SPM3Database"].ConnectionString;
            string[] portNames = SerialPort.GetPortNames();
                //MessageBox.Show(ConfigurationManager.AppSettings["SerialPortName"]);
                string portname = ConfigurationManager.AppSettings["SerialPortName"];
                bool isReport = String.Compare(ConfigurationManager.AppSettings["SerialPortName"], "DENY", StringComparison.OrdinalIgnoreCase) == 0;
            try{
                	if(!portNames.Contains(portname))
                	{
		                var form = new AboutBox1(portNames, isReport);
		                form.ShowDialog();

                        portname = form.SelectedPort;
                        isReport = isReport && string.IsNullOrEmpty(portname);       	                    
                	}
                }
                catch (Exception)
                {
//                    DisableScanning();
//                    return;
                }


            MainForm = new MainForm(portname, isReport);
            Application.Run(MainForm);
        }


        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
        
            var ex = e.ExceptionObject as Exception;
            if (ex == null || (!(ex is ObviousException) &&
                ex.Message.Contains("Retrieving the COM class factory for component with CLSID")))
            {
                //File.WriteAllText(@"c:\exception.txt", ex.Message + " " + ex.StackTrace);

                ErrorNotifier.SetError((Exception)e.ExceptionObject);                
            }
        }


        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {        	
            if (!(e.Exception is ObviousException) &&
                !e.Exception.Message.Contains("Retrieving the COM class factory for component with CLSID"))
            {
                ErrorNotifier.SetError(e.Exception);
            }
            else
            {
                ErrorNotifier.SetError(new ObviousException( e.Exception.Message ));                
            }
        }
    }
}
