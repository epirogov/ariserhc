﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using RemoteHumidityControlReport.Controller;
using RemoteHumidityControlReport.Logic;
using RemoteHumidityControlReport.Logic.Exceptions;
using System.Reflection;
using System.Configuration;
using RemoteHumidityControlReport.Logic.DeviceControl;
using LibraryBusinessLogic;
using System.Data.SqlClient;
using RemoteHumidityControlReport.Properties;

namespace RemoteHumidityControlReport.View
{
    public partial class MainForm : Form
    {
        ArchiveController _controller;
        readonly List<Exception> _exceptions = new List<Exception>();

        private readonly string _portname;
        private readonly bool _isReport;
        private bool _allowExit;
        public MainForm(string portname, bool isReport)
        {
            if (DesignMode) return;
            _portname = portname;
            _isReport = isReport;
            InitializeComponent();
            ErrorNotifier.OnError += ErrorNotifier_OnError;
            bindingSourceErrors.DataSource = _exceptions;

            dataGridViewErrors.DataSource = bindingSourceErrors;
            splitContainer.Panel2Collapsed = true;
        }

        void ErrorNotifier_OnError(object sender, GEventArgs<Exception> e)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(() => PrintError(e.Value1)));
            }
            else
                PrintError(e.Value1);
        }

        public SerialPort SerialPort
        {
            get;
            set;
        }

        void Watcher_DataSent(object sender, GEventArgs<LibmodbusCustomCommands.CommandEntity> e)
        {
            //if (e.Value1 == null)
            //{
            //    return;
            //}
            //unsafe{
            //    fixed (byte** command = new byte * [1])
            //    {
            //        fixed(byte * buffer = new byte[256])
            //        {
            //            command[0] = buffer;
            //            int commandlength = 0;
            //            e.Value1.Build(command, &commandlength);

            //            StringBuilder b = new StringBuilder();
            //            for(int i =0; i<commandlength; i++)
            //            {
            //                b.Append((*command)[i].ToString("X2"));
            //                b.Append(", ");
            //            }

            //            Action printRequest = () => this.rtbRequest.Text = b.ToString() + Environment.NewLine + this.rtbRequest.Text;
            //            if (rtbRequest.InvokeRequired)
            //            {
            //                rtbRequest.Invoke(new MethodInvoker(delegate()
            //                {
            //                    printRequest();
            //                }));
            //            }
            //            else
            //            {
            //                printRequest();
            //            }
            //        }
            //    }            

            //};
            //e.Value1
        }

        private void btnScanController_Click(object sender, EventArgs e)
        {
            var thread = new Thread(new ThreadStart(delegate
                {
                    var result = reportUserControl.ScanDevices();
                    //MessageBox.Show(result.ToString());
                    if (!result)
                    {
                        _exceptions.Add(new ObviousException("Devices not founded"));
                        stateUserControl.SetYellow();
                    }
                    else
                    {
                        stateUserControl.SetGreen();
                    }


                })) {IsBackground = true};

            thread.Start();
        }

        private int _count;
        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //this.rtbData.Text = string.Join(",  ", "41 54 20 30 32 20 31 20 31 20 30 30 30 2E 30 20 30 30 2E 30 30 20 24 BE".Split(' ').Select(value => toBits(byte.Parse(value, NumberStyles.HexNumber))).ToArray());

            //byte[] data = new byte[0];
            //try
            //{
            //    data = serialPort.ReadExisting().Select(value => (byte)value).ToArray();
            //}
            //catch (TimeoutException)
            //{
            //    throw;
            //}
            //Action printResponse = () => this.rtbData.Text = string.Join(", ", data.Select((value, i) => value.ToString("X2")
            //    + (((i + count) % 16 == 0) ? Environment.NewLine : string.Empty))) + this.rtbData.Text;
            ////.ToString("X2")
            ////toBits(value)
            //if (this.rtbData.InvokeRequired)
            //{
            //    this.rtbData.Invoke(new MethodInvoker(delegate
            //    {
            //        printResponse();
            //    }));
            //}
            //else
            //{
            //    printResponse();
            //}

            //this.rtbData.Text = ModbusProvider.check_crc16(serialPort.ReadExisting().Select(value => (byte)value).ToArray()).ToString() ;            
            //short crc = ModbusProvider.crc16(data, (short)(data.Length - 2));


            //right reading
            //byte[] data = new byte[256];
            //int ret1 = 0;
            //ret1 = serialPort.Read(data, 0, 256);
            //data = data.Take(ret1).ToArray();   
            //short crc  = ModbusProvider.crc16(data, (short)(data.Length -2));

            //this.rtbData.Text = string.Format("{0} {1} = {2}, {3}", (crc & 0xFF00).ToString("x"), (crc & 0xFF).ToString("x"), crc, data.Length);
            //this.rtbData.Text = string.Join(", ", data.Select(value => value.ToString("x")).Take(ret1).ToArray());


            //this.rtbData.Text = this.rtbData.Text + data.Length + " " + (crc >> 8).ToString("x") + " " + (crc & 0xFF).ToString("x");
            //this.rtbData.Text += (count++).ToString();

            //count += data.Length;
            Invoke(new MethodInvoker(delegate {
                rtbData.Text = string.Format("{0}: {1}\r\n{2}", ++_count, SerialPort.BytesToRead, rtbData.Text);
            }));        
        }


        private void trackerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            splitContainer.Panel2Collapsed = !splitContainer.Panel2Collapsed;
        }

        private void PrintError(Exception ex)
        {
            _exceptions.Add(ex);
            bindingSourceErrors.ResetBindings(false);

            try
            {
                Repository.NoQuery("WriteError", new List<SqlParameter> { 
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Message", SqlDbType = System.Data.SqlDbType.NVarChar, Size = 1000, Value = ex.Message},
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@StackTrace", SqlDbType = System.Data.SqlDbType.NVarChar, Size = Int32.MaxValue, Value = (ex.StackTrace ?? string.Empty)},
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@ExceptionType", SqlDbType = System.Data.SqlDbType.NVarChar, Size = 1000, Value = ex.GetType().FullName},
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@FullMessage", SqlDbType = System.Data.SqlDbType.NVarChar, Size = Int32.MaxValue, Value = ex.ToString()} });
            }
            catch (Exception ex1)
            {
                _exceptions.Add(ex1);
            }
        }        

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (DesignMode) return;
            btnScanController.Enabled = false;

            if (!_isReport)
            {

                SerialPort = new SerialPort(_portname, 9600, Parity.None, 8, StopBits.One);
                Thread.Sleep(200);
                SerialPort.DataReceived += serialPort_DataReceived;
                SerialPort.RtsEnable = true;
                SerialPort.ReceivedBytesThreshold = 1;
                SerialPort.ReadTimeout = 20000;
            }
            else
            {
                if (Container != null) SerialPort = new SerialPort(Container);
            }

            _controller = new ArchiveController(reportUserControl, SerialPort);
            lvDevices.ValueMember = "Address";
            lvDevices.DisplayMember = "DeviceName";
            bindingSourceDevices.DataSource = _controller.Controllers;
            lvDevices.DataSource = bindingSourceDevices;
            reportUserControl.Watcher.DataSent += Watcher_DataSent;
            _controller.ControllerFounded += controller_ControllerFounded;

            string initialPortName = ConfigurationManager.AppSettings["SerialPortName"];
            var th = new Thread(new ThreadStart(delegate
                {





                    if (!_isReport)
                    {
                        _controller.OpenPort(_portname);
                    }

                    //                this.stateUserControl.Invoke(new MethodInvoker(delegate()
                    //                                                                {
                    //                 exceptions.Add(ex);
                    //                 stateUserControl.SetYellow();}));
                    if (!_isReport)
                        btnScanController.Invoke(new MethodInvoker(delegate {
                                                                                btnScanController.Enabled = true;
                        }));
                    if (initialPortName == _portname)
                    {
                        _controller.ScanDevices();
                    }

                })) {IsBackground = true};
            th.Start();
        }


        void controller_ControllerFounded(object sender, GEventArgs<Spm3DeviceInfo> e)
        {
            if (InvokeRequired)
            {
                lvDevices.Invoke(new MethodInvoker(delegate
                    {
                    bindingSourceDevices.ResetBindings(false);
                    reportUserControl.Address1 = _controller.Controllers[0].Address;
                    if (_controller.Controllers.Count > 1)
                    {
                        reportUserControl.Address2 = _controller.Controllers[0].Address;
                    }
                }));
            }            
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void dataGridViewErrors_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (DesignMode) return;
            
            if (e.RowIndex <= 0 || e.RowIndex >= dataGridViewErrors.Rows.Count) return;
            e.CellStyle.BackColor = Color.LightGray;

            var exceptionType = dataGridViewErrors.Rows[e.RowIndex].DataBoundItem.GetType();
            if (dataGridViewErrors.Rows[e.RowIndex].DataBoundItem is Exception &&
                (dataGridViewErrors.Rows[e.RowIndex].DataBoundItem as Exception).Message.Contains("Retrieving the COM class factory for component with CLSID"))
            {
                e.CellStyle.BackColor = Color.LightGray;
            }
            else if (exceptionType.Assembly != Assembly.GetExecutingAssembly())
            {
                e.CellStyle.BackColor = Color.Red;
            }
        }

        private void serialPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.SelectSerialPort();
        }

        private void receiveDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvDevices.SelectedIndex != -1)
            {
                _controller.RequestData((Spm3DeviceInfo)lvDevices.Items[lvDevices.SelectedIndex]);

            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DesignMode) return;
            e.Cancel = !_allowExit;
            Hide();
        }

        private void cmsDeviceBounds_Opening(object sender, CancelEventArgs e)
        {

        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Visible = !Visible;
            if (!Visible) return;
            Show();
            Activate();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvDevices == null || lvDevices.Items.Count == 0)
            {
                if (reportUserControl.Controller != null && reportUserControl.Controller.Watcher != null)
                {
                    reportUserControl.Controller.Watcher.Stop();
                }
                _allowExit = true;
            }
            else if (lvDevices.Items.Count > 0 &&
                MessageBox.Show(
                Resources.MainForm_exitToolStripMenuItem_Click_Вы_действительно_уверены_что_необходимо_остановить_запись_данных_,
                Resources.MainForm_exitToolStripMenuItem_Click_Предупреждение,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (reportUserControl.Controller != null)
                {
                    reportUserControl.Controller.Watcher.Stop();
                }
                //if (serialPort != null && serialPort.IsOpen)
                //{
                //    serialPort.DiscardOutBuffer();
                //    serialPort.Close();
                //}
                _allowExit = true;
            }

            if (_allowExit)
            {
                Close();
            }
        }
    }
}
