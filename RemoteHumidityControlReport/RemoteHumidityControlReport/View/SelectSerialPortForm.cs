﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace RemoteHumidityControlReport.View
{
    public partial class SelectSerialPortForm : Form
    {
        public SelectSerialPortForm(IEnumerable<string> portNames)
        {
            InitializeComponent();
            lvSerialPortList.Items.AddRange(portNames.Select(portName => new ListViewItem(portName)).ToArray());
        }

        public string SelectedPort
        {
            get
            {
                return lvSerialPortList.SelectedItems[0].Text;
            }
        }
        
        private void SelectSerialPortForm_FormClosing(object sender, FormClosingEventArgs e)
        {            
            if (DialogResult != DialogResult.OK||
                lvSerialPortList.SelectedItems.Count == 0 && DialogResult == DialogResult.OK)
            {
                DialogResult = DialogResult.Ignore;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

    }
}
