﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using RemoteHumidityControlReport.Controller;
using RemoteHumidityControlReport.Entity;
using RemoteHumidityControlReport.Logic.DeviceControl;
using RemoteHumidityControlReport.Logic.Exceptions;
using LibraryBusinessLogic;
using System.Data.SqlClient;
using RemoteHumidityControlReport.DataAccess;
using System.Configuration;
using RemoteHumidityControlReport.VievInterface;

namespace RemoteHumidityControlReport.View
{
    public partial class ReportUserControl : UserControl, IDataUserControl
    {
        private readonly List<ControllerGeneralDataEntity> _dataEntities1 = new List<ControllerGeneralDataEntity>();
        private readonly List<ControllerGeneralDataEntity> _dataEntities2 = new List<ControllerGeneralDataEntity>();

        public ReportUserControl()
        {
            InitializeComponent();

            string addresses = ConfigurationManager.AppSettings["RequredDeviceAddresses"];
            int address1;
            if (addresses.Split(';').All(address => int.TryParse(address, out address1)))
            {
                var requiredDevices = addresses.Split(';').Select(int.Parse).ToList();
                if (requiredDevices.Count == 1)
                {
                    dataGridView2.Visible = false;
                    splitContainer1.Panel2Collapsed = true;
                }
            }

            bindingSource.DataSource = _dataEntities1;
            bindingSource1.DataSource = _dataEntities2;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView2.AutoGenerateColumns = false;
            dataGridView2.DataSource = bindingSource1;            
            dataGridView1.DataSource = bindingSource;
            dataGridView2.DataSource = bindingSource1;

            var timer = new System.Threading.Timer(delegate
                {
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate
                            {
                                if (!cbCurrent.Checked)
                                {
                                    if (!backgroundWorker1.IsBusy)
                                    {
                                        try
                                        {
                                            backgroundWorker1.RunWorkerAsync();
                                        }
                                        catch (InvalidOperationException)
                                        {
                                        }
                                    }
                                }
                            }));
                    }
                
                }, null, 6000, 6000);

            if (String.Compare(ConfigurationManager.AppSettings["SerialPortName"], "DENY", StringComparison.OrdinalIgnoreCase) == 0)
            {
                cbCurrent.Checked = false;
                cbCurrent.Visible = false;
                gbInterval.Visible = true;
            }
        }

        [Browsable(false)]
        public ArchiveController Controller
        {
            get;
            set;
        }

        [Browsable(false)]
        public HumidityWatcher Watcher
        {
            get
            {
                if (!DesignMode)
                {
                    return Controller.Watcher;
                }
                return null;
            }
            set
            {
            }
        }

        public void BindData(IEnumerable<ControllerGeneralDataEntity> datapack)
        {
            try
            {
                //ErrorNotifier.SetError(new ObviousException(datapack.FirstOrDefault().Address.ToString()));
                var controllerGeneralDataEntities = datapack as ControllerGeneralDataEntity[] ?? datapack.ToArray();
                var controllerGeneralDataEntity = controllerGeneralDataEntities.FirstOrDefault();
                if (controllerGeneralDataEntity != null && controllerGeneralDataEntity.Address == Controller.Controllers[0].Address)
                {
                    _dataEntities1.InsertRange(0, controllerGeneralDataEntities.Reverse());
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(() => bindingSource.ResetBindings(false)));
                    }
                    else
                    {
                        bindingSource.ResetBindings(false);
                    }
                }
                else
                {
                    _dataEntities2.InsertRange(0, controllerGeneralDataEntities.Reverse());
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(() => bindingSource1.ResetBindings(false)));
                    }
                    else
                    {
                        bindingSource1.ResetBindings(false);
                    }
                }

                if (_dataEntities1.Count > 500)
                {
                    DateTime lastHour = _dataEntities1.First().Date.AddHours(-1);

                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate
                            { _dataEntities1.RemoveAll(entity => entity.Date < lastHour); bindingSource.ResetBindings(false); }));
                    }
                    else
                    {
                        bindingSource.ResetBindings(false);
                    }
                }

                if (_dataEntities2.Count > 500)
                {
                    DateTime lastHour = _dataEntities2.First().Date.AddHours(-1);

                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate
                            { _dataEntities2.RemoveAll(entity => entity.Date < lastHour); bindingSource1.ResetBindings(false); }));
                    }
                    else
                    {
                        _dataEntities2.RemoveAll(entity => entity.Date < lastHour);
                        bindingSource1.ResetBindings(false);
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorNotifier.SetError(ex);
            }
        }

        public void SetController(ArchiveController controller)
        {
            Controller = controller;
        }

        public bool ScanDevices()
        {
            if (Controller == null) return false;
            return Controller.ScanDevices();
        }

        
        private void cbCurrent_CheckedChanged(object sender, EventArgs e)
        {
            gbInterval.Visible = !cbCurrent.Checked;
            if (gbInterval.Visible)
            {
                BindStore();                
            }
            else
            {
                bindingSource.DataSource = _dataEntities1;
                bindingSource1.DataSource = _dataEntities2;            
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            BindStore();            
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            BindStore();            
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //  MessageBox.Show("Отчет по первому устройству?", "Отчет", MessageBoxButtons., MessageBoxIcon.Question) == DialogResult.Yes
            //StreamWriter writer = new StreamWriter(saveFileDialog1.FileName);


            //if(data.Count() == 0)
            //{
            //    writer.Close();
            //    return;
            //}
            //writer.Write(data.First().Address.ToString());
            //writer.Write("\r\n");
            //writer.Write("Влажность %, Температура Среды C, Время");
            //writer.Write("\r\n");
            //writer.Write(string.Join("\r\n", data.Select(entity => string.Format("{0},{1},{2}", entity.Humidity.ToString().Replace(",", "."), entity.Temperature.ToString().Replace(",", "."), entity.Date))));
            //writer.Close();

            var form = new ReportChoiceForm((int)nudAddress1.Value, (int)nudAddress2.Value );
            if(form.ShowDialog() == DialogResult.OK)
            {
                bool isLeft = form.IsLeft;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    IEnumerable<ControllerGeneralDataEntity> data = isLeft ? (IEnumerable<ControllerGeneralDataEntity>)bindingSource.DataSource : (IEnumerable<ControllerGeneralDataEntity>)bindingSource1.DataSource;
                    ReportWriter.WriteReport(saveFileDialog1.FileName, data);                    
                }
            }            
        }

        public int Address1 { get { return (int)nudAddress1.Value; } set { nudAddress1.Value = value; } }

        public int Address2 { get { return (int)nudAddress2.Value; } set { nudAddress2.Value = value; } }
        
        public void OnControllerRequest()
        {
            //Program.mainForm.SetControllerProgress();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var address1 = (byte)Address1;
            var address2 = (byte)Address2;
            List<ControllerGeneralDataEntity> data = null;
            try
            {
                data = Repository.Select<ControllerGeneralDataEntity>("GetControllerData", new List<SqlParameter> { 
                                new SqlParameter { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@DateFrom", SqlDbType = System.Data.SqlDbType.DateTime, Value = dateTimePicker1.Value},
                                new SqlParameter { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@DateTo", SqlDbType = System.Data.SqlDbType.DateTime, Value = dateTimePicker2.Value}
                                 });
            }
            catch (Exception ex)
            {
                ErrorNotifier.SetError(ex);
            }

            if (data != null)
            {
                IEnumerable<IGrouping<byte, ControllerGeneralDataEntity>> entityGroups = data
                    .Where(entity => entity.Address == address1 || entity.Address == address2)
                    .OrderBy(entity => entity.Address).GroupBy(entity => entity.Address);
                e.Result = entityGroups;                
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var entityGroups = 
                    (IEnumerable<IGrouping<byte, ControllerGeneralDataEntity>>)e.Result;
            if (entityGroups == null)
            {
                return;
            }

            if (entityGroups.Any(group => group.Key == Address1))
            {
                bindingSource.DataSource = entityGroups.Single(group => group.Key == Address1 );
            }
            if (entityGroups.Any(group => group.Key == Address2))
            {
                bindingSource1.DataSource = entityGroups.Single(group => group.Key == Address2);
            }
        }

        private void nudAddress1_ValueChanged(object sender, EventArgs e)
        {
            BindStore();
        }

        private void nudAddress2_ValueChanged(object sender, EventArgs e)
        {
            BindStore();
        }

        private void BindStore()
        {
            var eDoWork = new DoWorkEventArgs(null);
            var eComplited = new RunWorkerCompletedEventArgs(eDoWork.Result, null, false);
            backgroundWorker1_DoWork(null, eDoWork);
            backgroundWorker1_RunWorkerCompleted(null, eComplited);
        }
    }
}
