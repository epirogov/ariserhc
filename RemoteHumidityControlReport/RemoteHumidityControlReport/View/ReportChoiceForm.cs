﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace RemoteHumidityControlReport.View
{
    public partial class ReportChoiceForm : Form
    {
        public ReportChoiceForm(int first, int second)
        {
            InitializeComponent();

            var addresses = ConfigurationManager.AppSettings["RequredDeviceAddresses"];
            int address1;
            if (addresses.Split(';').All(address => int.TryParse(address, out address1)))
            {
                var requiredDevices = addresses.Split(';').Select(int.Parse).ToList();
                if (requiredDevices.Count == 1)
                {
                    radioButton2.Visible = false;
                }
            }

            radioButton1.Text = first.ToString(CultureInfo.InvariantCulture);
            radioButton2.Text = second.ToString(CultureInfo.InvariantCulture);
        }

        public bool IsLeft
        {
            get
            {
                return radioButton1.Checked;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
