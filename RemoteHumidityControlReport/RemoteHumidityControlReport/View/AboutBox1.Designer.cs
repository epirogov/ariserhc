﻿namespace RemoteHumidityControlReport.View
{
    partial class AboutBox1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox1));
        	System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("COM1");
        	this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
        	this.labelProductName = new System.Windows.Forms.Label();
        	this.labelVersion = new System.Windows.Forms.Label();
        	this.labelCopyright = new System.Windows.Forms.Label();
        	this.labelCompanyName = new System.Windows.Forms.Label();
        	this.textBoxDescription = new System.Windows.Forms.TextBox();
        	this.okButton = new System.Windows.Forms.Button();
        	this.lvSerialPortList = new System.Windows.Forms.ListView();
        	this.label1 = new System.Windows.Forms.Label();
        	this.tableLayoutPanel.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// tableLayoutPanel
        	// 
        	this.tableLayoutPanel.ColumnCount = 2;
        	this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.70588F));
        	this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.29412F));
        	this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.Controls.Add(this.labelProductName, 1, 0);
        	this.tableLayoutPanel.Controls.Add(this.labelVersion, 1, 1);
        	this.tableLayoutPanel.Controls.Add(this.labelCopyright, 1, 2);
        	this.tableLayoutPanel.Controls.Add(this.labelCompanyName, 1, 3);
        	this.tableLayoutPanel.Controls.Add(this.textBoxDescription, 1, 4);
        	this.tableLayoutPanel.Controls.Add(this.okButton, 1, 5);
        	this.tableLayoutPanel.Controls.Add(this.lvSerialPortList, 0, 1);
        	this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
        	this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
        	this.tableLayoutPanel.Name = "tableLayoutPanel";
        	this.tableLayoutPanel.RowCount = 6;
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.504132F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.82645F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        	this.tableLayoutPanel.Size = new System.Drawing.Size(510, 265);
        	this.tableLayoutPanel.TabIndex = 0;
        	this.tableLayoutPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel_Paint);
        	// 
        	// labelProductName
        	// 
        	this.labelProductName.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.labelProductName.Location = new System.Drawing.Point(233, 0);
        	this.labelProductName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
        	this.labelProductName.MaximumSize = new System.Drawing.Size(0, 17);
        	this.labelProductName.Name = "labelProductName";
        	this.labelProductName.Size = new System.Drawing.Size(274, 17);
        	this.labelProductName.TabIndex = 19;
        	this.labelProductName.Text = "Remote Humidity Control";
        	this.labelProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// labelVersion
        	// 
        	this.labelVersion.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.labelVersion.Location = new System.Drawing.Point(233, 26);
        	this.labelVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
        	this.labelVersion.MaximumSize = new System.Drawing.Size(0, 17);
        	this.labelVersion.Name = "labelVersion";
        	this.labelVersion.Size = new System.Drawing.Size(274, 17);
        	this.labelVersion.TabIndex = 0;
        	this.labelVersion.Text = "Version : 1.0.0 RC1";
        	this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// labelCopyright
        	// 
        	this.labelCopyright.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.labelCopyright.Location = new System.Drawing.Point(233, 52);
        	this.labelCopyright.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
        	this.labelCopyright.MaximumSize = new System.Drawing.Size(0, 17);
        	this.labelCopyright.Name = "labelCopyright";
        	this.labelCopyright.Size = new System.Drawing.Size(274, 17);
        	this.labelCopyright.TabIndex = 21;
        	this.labelCopyright.Text = "Copyright : MWT Dnepropetrovsk, Eugene Pirogov";
        	this.labelCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// labelCompanyName
        	// 
        	this.labelCompanyName.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.labelCompanyName.Location = new System.Drawing.Point(233, 78);
        	this.labelCompanyName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
        	this.labelCompanyName.MaximumSize = new System.Drawing.Size(0, 17);
        	this.labelCompanyName.Name = "labelCompanyName";
        	this.labelCompanyName.Size = new System.Drawing.Size(274, 17);
        	this.labelCompanyName.TabIndex = 22;
        	this.labelCompanyName.Text = "Company : PE Eugene Pirogov";
        	this.labelCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// textBoxDescription
        	// 
        	this.textBoxDescription.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.textBoxDescription.Location = new System.Drawing.Point(233, 106);
        	this.textBoxDescription.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
        	this.textBoxDescription.Multiline = true;
        	this.textBoxDescription.Name = "textBoxDescription";
        	this.textBoxDescription.ReadOnly = true;
        	this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
        	this.textBoxDescription.Size = new System.Drawing.Size(274, 128);
        	this.textBoxDescription.TabIndex = 23;
        	this.textBoxDescription.TabStop = false;
        	this.textBoxDescription.Text = "Product has research purposes, do not use any decompiler software see arise-proje" +
        	"ct.org.ua istead.\r\n\r\nVendor : thegra.kiev.ua\r\n\r\nSingle manufactor rights.";
        	// 
        	// okButton
        	// 
        	this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
        	this.okButton.Location = new System.Drawing.Point(432, 240);
        	this.okButton.Name = "okButton";
        	this.okButton.Size = new System.Drawing.Size(75, 22);
        	this.okButton.TabIndex = 24;
        	this.okButton.Text = "&OK";
        	this.okButton.Click += new System.EventHandler(this.okButton_Click);
        	// 
        	// lvSerialPortList
        	// 
        	this.lvSerialPortList.Activation = System.Windows.Forms.ItemActivation.OneClick;
        	this.lvSerialPortList.Alignment = System.Windows.Forms.ListViewAlignment.Default;
        	this.lvSerialPortList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lvSerialPortList.BackgroundImage")));
        	this.lvSerialPortList.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.lvSerialPortList.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.lvSerialPortList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.lvSerialPortList.ForeColor = System.Drawing.SystemColors.WindowText;
        	this.lvSerialPortList.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
        	        	        	listViewItem2});
        	this.lvSerialPortList.LabelWrap = false;
        	this.lvSerialPortList.Location = new System.Drawing.Point(3, 29);
        	this.lvSerialPortList.MultiSelect = false;
        	this.lvSerialPortList.Name = "lvSerialPortList";
        	this.tableLayoutPanel.SetRowSpan(this.lvSerialPortList, 4);
        	this.lvSerialPortList.Size = new System.Drawing.Size(221, 205);
        	this.lvSerialPortList.TabIndex = 25;
        	this.lvSerialPortList.UseCompatibleStateImageBehavior = false;
        	this.lvSerialPortList.View = System.Windows.Forms.View.SmallIcon;
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.label1.Location = new System.Drawing.Point(3, 0);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(221, 26);
        	this.label1.TabIndex = 26;
        	this.label1.Text = "Выберите порт :";
        	// 
        	// AboutBox1
        	// 
        	this.AcceptButton = this.okButton;
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(528, 283);
        	this.Controls.Add(this.tableLayoutPanel);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.Name = "AboutBox1";
        	this.Padding = new System.Windows.Forms.Padding(9);
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "Wellcome";
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AboutBox1_FormClosing);
        	this.Load += new System.EventHandler(this.AboutBox1_Load);
        	this.tableLayoutPanel.ResumeLayout(false);
        	this.tableLayoutPanel.PerformLayout();
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label labelProductName;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.ListView lvSerialPortList;
        private System.Windows.Forms.Label label1;
    }
}
