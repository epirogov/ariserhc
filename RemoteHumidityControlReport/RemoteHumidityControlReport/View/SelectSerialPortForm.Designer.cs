﻿namespace RemoteHumidityControlReport.View
{
    partial class SelectSerialPortForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectSerialPortForm));
            this.lvSerialPortList = new System.Windows.Forms.ListView();
            this.lbAvailableSerialPorts = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvSerialPortList
            // 
            this.lvSerialPortList.Location = new System.Drawing.Point(12, 29);
            this.lvSerialPortList.MultiSelect = false;
            this.lvSerialPortList.Name = "lvSerialPortList";
            this.lvSerialPortList.Size = new System.Drawing.Size(231, 175);
            this.lvSerialPortList.TabIndex = 0;
            this.lvSerialPortList.UseCompatibleStateImageBehavior = false;
            // 
            // lbAvailableSerialPorts
            // 
            this.lbAvailableSerialPorts.AutoSize = true;
            this.lbAvailableSerialPorts.Location = new System.Drawing.Point(13, 9);
            this.lbAvailableSerialPorts.Name = "lbAvailableSerialPorts";
            this.lbAvailableSerialPorts.Size = new System.Drawing.Size(98, 13);
            this.lbAvailableSerialPorts.TabIndex = 1;
            this.lbAvailableSerialPorts.Text = "Доступные порты";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(168, 210);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // SelectSerialPortForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 239);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lbAvailableSerialPorts);
            this.Controls.Add(this.lvSerialPortList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectSerialPortForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подключение устройства";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SelectSerialPortForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvSerialPortList;
        private System.Windows.Forms.Label lbAvailableSerialPorts;
        private System.Windows.Forms.Button btnOk;
    }
}