﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using RemoteHumidityControlReport.VievInterface;

namespace RemoteHumidityControlReport.View
{
    public partial class StateUserControl : UserControl, ISemaphoreControl
    {
        private readonly Color[] _colors = new Color[3];
        public StateUserControl()
        {
            InitializeComponent();
            _colors[0] = panel1.BackColor;
            _colors[1] = panel2.BackColor;
            _colors[2] = panel3.BackColor;
        }


        public void ResetOthers(int state)
        {
            panel1.BackColor = _colors[0];
            panel2.BackColor = _colors[1];
            panel3.BackColor = _colors[2];

            //panels.Where((value, i) => (i + 1) != state).ToList().ForEach(panel => panel.BackColor = SystemColors.Control);            
        }

        public void SetGreen()
        {            
            Action setState = () =>{
                ResetOthers(1);
                panel1.BackColor = Color.Green;                
            };
            if (panel1.InvokeRequired)
            {
                panel1.Invoke(new MethodInvoker(() => setState()));
            }
            else
            {
                setState();
            }
        }

        public void SetYellow()
        {
            Action setState = () =>
            {
                ResetOthers(2);
                panel2.BackColor = Color.Yellow;                
            };

            if (panel2.InvokeRequired)
            {
                panel2.Invoke(new MethodInvoker(() => setState()));
            }
            else
            {
                setState();
            }
        }

        public void SetRed()
        {
            Action setState = () => {
                ResetOthers(3);
                panel3.BackColor = Color.Red;
                
            };

            if (panel3.InvokeRequired)
            {
                panel3.Invoke(new MethodInvoker(() => setState()));
            }
            else
            {
                setState();
            }            
        }

        public void Reset()
        {
            SetGreen();
        }
    }
}
