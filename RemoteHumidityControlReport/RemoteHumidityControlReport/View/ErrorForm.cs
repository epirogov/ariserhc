﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Windows.Forms;

namespace RemoteHumidityControlReport.View
{
    public partial class ErrorForm : Form
    {
        public ErrorForm(Exception ex)
        {
            InitializeComponent();

            richTextBox1.Text = ex.Message;
            richTextBox2.Text = ex.StackTrace;
        }
    }
}
