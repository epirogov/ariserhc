﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.VievInterface
{
    interface ISemaphoreControl
    {
        void SetGreen();
        void SetYellow();
        void SetRed();
        void ResetOthers(int state);
        void Reset();
    }
}
