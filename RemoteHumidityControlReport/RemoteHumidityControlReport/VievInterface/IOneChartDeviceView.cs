﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.Entity;

namespace RemoteHumidityControl.ViewInterface
{
    public interface IOneChartDeviceView
    {
        void PrintData(int controllerId, IEnumerable<ControllerGeneralDataEntity> data);
    }
}
