﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System.Collections.Generic;
using RemoteHumidityControlReport.Controller;
using RemoteHumidityControlReport.Entity;
using RemoteHumidityControlReport.Logic.DeviceControl;

namespace RemoteHumidityControlReport.VievInterface
{
    public interface IDataUserControl
    {
        void BindData(IEnumerable<ControllerGeneralDataEntity> datapack);
        HumidityWatcher Watcher
        {
            get;
            set;
        }
        void SetController(ArchiveController controller);
        bool ScanDevices();
        void OnControllerRequest();
        
    }
}
