﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.Controller;

namespace RemoteHumidityControl.ViewInterface
{
    public interface IRemoteAccessView
    {
        RemoteAccessController Controller
        {
            get;
            set;
        }
 
        void ConnectRemoteControl();
        void DisconnectRemoteControl();
        void PrintSTD4408Input(string text);
        void SetShift();
        void UnsetShift();
        void DisplayText(string text);
        void EnableCursor(int selectionStart);
        void DisableCursor();
    }
}
