﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;

namespace RemoteHumidityControlReport.Entity
{
    public class ControllerGeneralDataEntity
    {        
        public ControllerGeneralDataEntity()
        {
        }

        public ControllerGeneralDataEntity(byte address, float humidity, float temperature, DateTime date)
        {
            Address = address;
            Humidity = humidity;
            Temperature = temperature;
            Date = date;
        }

        public byte Address
        {
            get;
            set;
        }

        public float Humidity
        {
            get;
            set;
        }

        public float Temperature
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

    }
}
