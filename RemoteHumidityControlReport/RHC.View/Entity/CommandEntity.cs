﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 03.03.2013
 * Time: 13:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace RemoteHumidityControlReport.Entity
{
	/// <summary>
	/// Description of CommandEntity.
	/// </summary>
	public class CommandEntity
	{
	    public Byte [] data {get;set;}
	    public int datalength {get; set;}
	    public int responselength {get;set;}
		public Byte address {get;set;}
	    public Byte function {get;set;}
	    public bool hasSubfunction {get;set;}
	    public Byte subfunction {get;set;}
	}
}
