﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System.Collections.Generic;
using RemoteHumidityControlReport.Entity;
using RemoteHumidityControlReport.StateManagement.Content;

namespace RemoteHumidityControlReport.VievInterface
{
    public interface IMainView : IContentAction
    {
        void PrintData(int controllerId, IEnumerable<ControllerGeneralDataEntity> data);        
    }
}
