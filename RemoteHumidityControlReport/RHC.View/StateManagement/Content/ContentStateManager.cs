﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using RemoteHumidityControlReport.Logic;

namespace RemoteHumidityControlReport.StateManagement.Content
{
    public class ContentStateManager : IContentAction
    {
        public event EventHandler<GEventArgs<ContentStateEnum>> OpenContent;        
 
        public ContentStateManager()
        {
            State = new ChartsState {StateManager = this};
        }

        public IContentState State
        {
            get;
            set;
        }        

        public void  OpenArchive()
        {
            State.OpenArchive();
        }

        public void  OpenCharts()
        {
            State.OpenCharts();
        }

        public void  OpenRemoteControl()
        {
            State.OpenRemoteControl();
        }

        public void  OpenReport()
        {
            State.OpenReport();
        }

        public void OnOpenContent()
        {
            if (OpenContent != null)
            {
                OpenContent(this, new GEventArgs<ContentStateEnum>(State.StateValue));
            }
        }
    }
}
