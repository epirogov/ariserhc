﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.StateManagement.Content
{
    public class ChartsState : BaseContentState
    {
        public ChartsState()
            : base(ContentStateEnum.Charts)
        {
        }

        public override void OpenArchive()
        {
            SetState<ArchiveState>();
        }

        public override void OpenCharts()
        {
            StateManager.OnOpenContent();
        }

        public override void OpenRemoteControl()
        {
            SetState<RemoteControlState>();
        }

        public override void OpenReport()
        {
            SetState<ReportState>();
        }
    }
}
