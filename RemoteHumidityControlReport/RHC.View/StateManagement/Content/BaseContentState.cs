﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.StateManagement.Content
{
    public abstract class BaseContentState : IContentState
    {
        protected BaseContentState(ContentStateEnum state)
        {
            StateValue = state;
        }

        public ContentStateEnum StateValue
        {
            get;
            set;
        }

        public ContentStateManager StateManager
        {
            get;
            set;
        }

        abstract public void OpenArchive();
        abstract public void OpenCharts();
        abstract public void OpenRemoteControl();
        abstract public void OpenReport();

        public void SetState<TStateType>() where TStateType : IContentState, new()
        {
            var newState = new TStateType {StateManager = StateManager};
            newState.StateManager.State = newState;
            newState.StateManager.OnOpenContent();            
        }
    }
}
