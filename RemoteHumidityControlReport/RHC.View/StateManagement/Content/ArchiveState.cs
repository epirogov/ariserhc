﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.StateManagement.Content
{
    public class ArchiveState : BaseContentState
    {
        public ArchiveState()
            : base(ContentStateEnum.Archive)
        {
        }

        public override void OpenArchive()
        {
        }

        public override void OpenCharts()
        {
            SetState<ChartsState>();
        }

        public override void OpenRemoteControl()
        {
            SetState<RemoteControlState>();
        }

        public override void OpenReport()
        {
            SetState<ReportState>();
        }
    }
}
