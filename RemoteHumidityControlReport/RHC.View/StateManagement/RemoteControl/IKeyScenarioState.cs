﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of IKeyScenarioState.
	/// </summary>
	public interface IKeyScenarioState
	{
		Std4408MainFormStateController Controller
		{
			get;
			set;			
		}
		
		KeyScenarioStateEnum StateValue
		{
			get;			
		}
		
		void SetOneKeyScenario();
		void SetThreeKeyScenario();
	}
}
