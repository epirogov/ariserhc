﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of OneKeyScenarioState.
	/// </summary>
	public class OneKeyScenarioState : BaseKeyScenarioState, IKeyScenarioState
	{
		public OneKeyScenarioState()
		{
			CreateInternals(null);
		}
		
		public OneKeyScenarioState(Std4408MainFormStateController controller)
		{
			CreateInternals(controller);
		}		
		
		private void CreateInternals(Std4408MainFormStateController controller)
		{
			CreateInternals(controller, KeyScenarioStateEnum.OneKey);
		}
		
		public void SetOneKeyScenario()
		{
			
		}
		
		public void SetThreeKeyScenario()
		{
			Controller.SetKeyScenarioState<ThreeKeyScenarioState>();
		}
	}
}
