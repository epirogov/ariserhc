﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


using System;
using RemoteHumidityControlReport.Logic;

namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of STD4408MainFormStateController.
	/// </summary>
	public class Std4408MainFormStateController
	{
		
		private IStd4408MainFormState _currentComPortsState;
		private IKeyScenarioState _currentKeyScenarioState;

        public event EventHandler<GEventArgs<Std4408StateEnum>> NewComPortsStateEvent;
        public event EventHandler<GEventArgs<KeyScenarioStateEnum>> NewKeyScenarioStateEvent;

	    public void Initialize(Std4408StateEnum initComPortsState, KeyScenarioStateEnum initKeyScenarioState)
		{
			switch(initComPortsState)
			{
				case Std4408StateEnum.ConnectedComPorts:
					SetComPortsState<ConnectedComPortsState>();
					break;
				case Std4408StateEnum.DisconnectedComPorts:
					SetComPortsState<DisconnectedComPortsState>();
					break;
			}
			
			switch(initKeyScenarioState)
			{
				case KeyScenarioStateEnum.OneKey:
					SetKeyScenarioState<OneKeyScenarioState>();
					break;
				case KeyScenarioStateEnum.ThreeKey:
					SetKeyScenarioState<ThreeKeyScenarioState>();
					break;
			}
		}
		
		public Std4408StateEnum ComPortsState
		{
			get
			{
				return _currentComPortsState.StateValue;
			}
		}
		
		public KeyScenarioStateEnum KeyScenarioState
		{
			get
			{
				return _currentKeyScenarioState.StateValue;
			}
		}
		
		public void SetNextSate()
		{
			switch(_currentComPortsState.StateValue)
			{
				case Std4408StateEnum.ConnectedComPorts :
					SetComPortsState<DisconnectedComPortsState>();
					break;
				case Std4408StateEnum.DisconnectedComPorts :
					SetComPortsState<ConnectedComPortsState>();
					break;
			}
		}
		
		public void SetComPortsState<T>() where T : IStd4408MainFormState, new()
		{
			_currentComPortsState = new T {Controller = this } ;
			if(NewComPortsStateEvent != null)
			{
                NewComPortsStateEvent(this, new GEventArgs<Std4408StateEnum>(_currentComPortsState.StateValue));
			}
		}
		
		public void SetKeyScenarioState<T>() where T : IKeyScenarioState, new()
		{
			_currentKeyScenarioState = new T {Controller = this } ;
			if(NewKeyScenarioStateEvent != null)
			{
                NewKeyScenarioStateEvent(this, new GEventArgs<KeyScenarioStateEnum>(_currentKeyScenarioState.StateValue));
			}
		}
		
		public void SetOneKeyScenario()
		{
			_currentKeyScenarioState.SetOneKeyScenario();
		}
		
		public void SetThreeKeyScenario()
		{
			_currentKeyScenarioState.SetThreeKeyScenario();
		}
	}
}
