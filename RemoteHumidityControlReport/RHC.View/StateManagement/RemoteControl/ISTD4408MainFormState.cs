﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.StateManagement.RemoteControl
{
	/// <summary>
	/// Description of ISTD4408MainFormState.
	/// </summary>
	public interface IStd4408MainFormState
	{
		Std4408MainFormStateController Controller
		{
			get;
			set;			
		}
		
		Std4408StateEnum StateValue
		{
			get;			
		}
		
		void ConnectComPorts();
		void DisconnectComPorts();		
	}
}
