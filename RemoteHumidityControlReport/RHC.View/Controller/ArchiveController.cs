﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO.Ports;
using RemoteHumidityControlReport.Entity;
using RemoteHumidityControlReport.Logic;
using RemoteHumidityControlReport.VievInterface;
using System.ComponentModel;
using RemoteHumidityControlReport.Logic.DeviceControl;
using RemoteHumidityControlReport.Logic.Exceptions;
using System.Threading;
using System.Data.SqlClient;
using LibraryBusinessLogic;

namespace RemoteHumidityControlReport.Controller
{
    /// <summary>
    /// Description of ArchiveController.
    /// </summary>
    public class ArchiveController
    {
        public event EventHandler<GEventArgs<Spm3DeviceInfo>> ControllerFounded;

		private readonly List<ControllerGeneralDataEntity> _dataEntities1 = new List<ControllerGeneralDataEntity>();

		private readonly List<ControllerGeneralDataEntity> _dataEntities2 = new List<ControllerGeneralDataEntity>();
       
        public HumidityWatcher Watcher
        {
            get;
            set;
        }

		public int Address1 { get; set; }

		public int Address2 { get; set; }


		public DateTime From { get; set; }

		public DateTime To { get; set; }

        public ArchiveController(SerialPort serialPort)
        {
			From = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
			To = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(1);

            if (serialPort != null)
            {
                Watcher = new HumidityWatcher(serialPort, new TimeSpan(0, 0, 12));
                Watcher.ControllerFounded += watcher_ControllerFounded;
            }
        }

        void watcher_ControllerFounded(object sender, GEventArgs<byte> e)
        {
            if (ControllerFounded != null)
            {
                ControllerFounded(this, new GEventArgs<Spm3DeviceInfo>(new Spm3DeviceInfo { Address = e.Value1 }));
            }
        }

		public ArchiveController ArciveView
        {
            get;
            private set;
        }

        public bool ScanDevices()
        {
            return Watcher.ScanControllers();
        }

        public List<Spm3DeviceInfo> Controllers
        {
            get
            {
                return Watcher.Controllers;
            }
        }

        public bool OpenPort(string serialPortName)
        {            
            Watcher.Serialport.PortName = serialPortName;            
            if (Watcher.Serialport.IsOpen)
            {                    
                Watcher.Serialport.Close();
            }
            
            if (SerialPort.GetPortNames().All(port => serialPortName != port))
            {
                return false;
            }

            Thread thread = new Thread(new ThreadStart(delegate()
            {
                try
                {
                    if (!Watcher.Serialport.IsOpen)
                    {                        
                        Watcher.Serialport.Open();                                         
                    }
                }
                catch (Exception ex)
                {                    
                    ErrorNotifier.PrintError(ex);
                }
            }));
            thread.IsBackground = true;
            thread.Start();
            if(thread.IsAlive && !thread.Join(10000))
            {
                Watcher.Connector.SerialPort.DiscardInBuffer();
                thread.Interrupt();                
            }

            return true;
        }


		public void GetReport(object sender, DoWorkEventArgs e)
		{
			var address1 = (byte)Address1;
			var address2 = (byte)Address2;
			List<ControllerGeneralDataEntity> data = null;
			try
			{
				data = Repository.Select<ControllerGeneralDataEntity>("GetControllerData", new List<SqlParameter> { 
                                new SqlParameter { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@DateFrom", SqlDbType = System.Data.SqlDbType.DateTime, Value = From},
                                new SqlParameter { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@DateTo", SqlDbType = System.Data.SqlDbType.DateTime, Value = To}
                                 });
				e.Result = data;
			}
			catch (Exception ex)
			{
				ErrorNotifier.PrintError(ex);
				e.Cancel = true;
			}
		}
    }
}
