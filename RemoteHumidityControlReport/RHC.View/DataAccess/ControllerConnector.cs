﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.IO.Ports;
using RemoteHumidityControlReport.Logic;
using RemoteHumidityControlReport.Logic.Exceptions;
using RemoteHumidityControlReport.Logic.DeviceControl;
using RemoteHumidityControlReport.Entity;
using System.IO;
using System.Configuration;
using Timer = System.Timers.Timer;

namespace RemoteHumidityControlReport.DataAccess
{
	/// <summary>
	/// Description of ControllerConnector.
	/// </summary>
	public class ControllerConnector
	{
		private const int SCAN = 0x50; //04 50 03 4C
		private const int REGISTER = 0x51; //FF 04 51 01 CC 51			   
		private const int REQUEST = 0x54;	//04 54 00 00 00 00 00 53 14
		private const int RESPONSE = 0x54; //FF 04 54 41 54 20 30 34 20 31 20 31 20 30 30 35 2E 36 20 30 30 2E 30 30 20 24 CB 14 4C
		
		private readonly Timer _timer = new Timer();						
		volatile byte _controllerId;
		
		private readonly int _period = 30;
		private readonly List<int> _requiredDevices = new List<int>();
		//private int controllerIndex;

		public event EventHandler<GEventArgs<CommandEntity>> DataSent;
		public event EventHandler<GEventArgs<CommandEntity>> DataReceived;
		public event EventHandler<GEventArgs<byte>> ControllerFounded;
		public event EventHandler<GEventArgs<byte>> ControllerUnsubscribe;
		public event EventHandler<EventArgs> ControllerRequest;
		public readonly CommandFactory CommandFactory = new CommandFactory();
		

		public SerialPort SerialPort
		{
			get;
			set;
		}

		public ControllerConnector(SerialPort serialPort, List<Spm3DeviceInfo> controllers, int period)
		{
			
			_period = period;
			SerialPort = serialPort;			
			SerialPort.DataReceived += serialPort_DataReceived;
			
			
			ControllerIDs = controllers;

			var thread = new Thread(ReadPack) {IsBackground = true};
			thread.Start();

			var addresses = ConfigurationManager.AppSettings["RequredDeviceAddresses"];			
			int address1;
			if (addresses.Split(';').All(address => int.TryParse(address, out address1)))
			{
				_requiredDevices = addresses.Split(';').Select(int.Parse).ToList();
			}
		}

		
		private readonly object _syncDataRead = new object();

		public void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			try
			{
				lock (_syncDataRead)
				{

					var bufferUnion = new byte[0];
					var registerController = false;


					var buffer = new byte[256];
					var buffer1 = new byte[256];

					buffer = SerialPort.ReadExisting().ToCharArray().Select(c => (byte)c).ToArray();
					var length = buffer.Length;
					buffer = buffer.Take(length).ToArray();
					if (ControllerIDs.Count > 0 || _requiredDevices.Count > 0)
					{
						registerController = buffer
						.Select((value, i) => new { Value = value, Index = i })
						.Any(data =>
						(_requiredDevices.Contains(data.Value) ||
						ControllerIDs.Any(controller => controller.Address == data.Value)) &&
						buffer.Count() > data.Index + 2 &&
						buffer[data.Index + 1] == 0x51);
					}
					else
					{
						registerController = buffer
						.Select((value, i) => new { Value = value, Index = i })
						.Any(data => buffer.Count() > data.Index + 3 &&
						 buffer[data.Index] < 10 && buffer[data.Index + 1] == 0x51);
					}
					if (!registerController && length < 7)
					{
						Thread.Sleep(500);
						if (SerialPort.BytesToRead == 0)
						{
							return;
						}
						buffer1 = SerialPort.ReadExisting().ToCharArray().Select(c => (byte)c).ToArray();
						bufferUnion = buffer.Union(buffer1).ToArray();

						registerController = (_requiredDevices.Count > 0 ||
						ControllerIDs.Any(controller => controller.State == true)) &&
						bufferUnion
						.Select((value, i) => new { Value = value, Index = i })
						.Any(data =>
						(_requiredDevices.Contains(data.Value) ||
						ControllerIDs.Any(controller => controller.Address == data.Value)) &&
						bufferUnion.Count() > data.Index + 2 &&
						bufferUnion[data.Index + 1] == 0x51);
					}
					else
					{
						bufferUnion = buffer;
					}
					bufferUnion = buffer;


					if (registerController)
					{
						int registeredController = 0;
						if (ControllerIDs.Count > 0 || _requiredDevices.Count > 0)
							registeredController = bufferUnion
								.Select((v, i) => new { V = v, I = i })
								.Where(value => value.I > 0 &&
									(_requiredDevices.Contains(value.V) ||
									ControllerIDs.Any(controller => controller.Address == value.V)) &&
									bufferUnion.Count() > value.I + 2 &&
									bufferUnion[value.I + 1] == 0x51)
								.Select(value => value.V)
								.FirstOrDefault();
						else
						{
							registeredController = bufferUnion
							.Select((v, i) => new { V = v, I = i })
							.Where(value => value.I > 0 &&
								bufferUnion.Count() > value.I + 3 &&
								bufferUnion[value.I] < 10 && bufferUnion[value.I + 1] == 0x51)
							.Select(value => value.V)
							.FirstOrDefault();
						}

						if (registeredController > 0)
						{
							if (_requiredDevices.Contains(registeredController) ||
								ControllerIDs.Any(controller => controller.State.GetValueOrDefault()))
							{
								if (_requiredDevices.Contains(registeredController))
								{
									_requiredDevices.Remove(registeredController);
								}


								if (ControllerFounded != null)
								{
									ControllerFounded(this, new GEventArgs<byte>((byte)registeredController));
								}
								if (ControllerIDs.Any(controller => controller.Address == registeredController))
								{
									ControllerIDs.First(controller => controller.Address == registeredController).State = null;
								}
							}
							else
							{
								if (ControllerFounded != null)
								{
									ControllerFounded(this, new GEventArgs<byte>((byte)registeredController));
								}
								ControllerIDs.Add(new Spm3DeviceInfo { Address = registeredController, State = null });
							}
						}

						return;
					}




					int index = bufferUnion
							.Select((v, i) => new { V = v, I = i })
							.Where(value => value.I > 0 &&
								ControllerIDs.Any(c => c.Address == bufferUnion[value.I - 1]) &&
								value.V == 0x54)
							.Min(value => value.I);

					if (index == 0 || ControllerIDs.All(c => c.Address != bufferUnion[index - 1]) ||
						bufferUnion.Count() < 5 + index)
					{

						return;
					}

					var address = bufferUnion[index - 1];

					var result = bufferUnion.Skip(index + 1).Take(4);

					var enumerable = result as byte[] ?? result.ToArray();
					ErrorNotifier.PrintError(
									new ObviousException(
										string.Format(
										"Data Packet Received adr [{0}] {1} ",
										address,
										string.Join(",", enumerable.Select(value => value.ToString(CultureInfo.InvariantCulture))))));
					if (DataReceived != null)
					{
						DataReceived(this,
									 new GEventArgs<CommandEntity>(new CommandEntity { address = address, data = enumerable, datalength = enumerable.Count() }));
					}

				}
			}

			catch (TimeoutException)
			{
			}
			catch (InvalidOperationException ex)
			{
				ErrorNotifier.PrintError(ex);
			}
		}

		
		public List<Spm3DeviceInfo> ControllerIDs 
		{
			get;
			set;
		}

		public void SetInterval(long interval)
		{
			_timer.Stop();			
			_timer.Interval =  interval;
			_timer.Start();
		}
		
		public void Star()
		{
			if(_timer.Interval > 0)
			{
				_timer.Start();
			}
		}
		
		public void Stop()
		{
			if (SerialPort.IsOpen) SerialPort.Close();
		}
		
		public bool StartScaning()
		{
			var bounds = new byte [] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
			if(_requiredDevices.Count > 0)
			{
				bounds = _requiredDevices.Select(a => (byte)a).ToArray();
			}
			foreach(var address in bounds)
			{
				if (ControllerRequest != null)
				{
					ControllerRequest(this, new EventArgs());
				}
				try
				{
					ScanController(address);
					Thread.Sleep(6000);
				}
				catch (Exception)
				{
						
				}
				
			}			

			return true;
		}	 
		

		
		public void ScanController(byte address)
		{
			var buffer = new byte [4]; //04 50 03 4C
			buffer[0] = address;
			buffer[1] = SCAN;
			byte c1, c2;
			CrcTable.CalculateCrc(buffer, out c1, out c2);
			buffer[2] =c1;
			buffer[3] =c2;
			
			SerialPort.Write(buffer, 0, buffer.Length);
		}
		
		public void SendDataPacket(byte address)
		{			
			var buffer = new byte [9]; //04 54 00 00 00 00 00 53 14
			buffer[0] = address;
			buffer[1] = REQUEST;
			byte c1, c2;
			CrcTable.CalculateCrc(buffer, out c1, out c2);
			buffer[7] =c1;
			buffer[8] =c2;

			SerialPort.Write(buffer, 0, buffer.Length);
		}
		

		public void ReadPack()
		{
			while (true)
			{

				if (ControllerIDs.Count <= _controllerId && _requiredDevices.Count <= _controllerId)
				{
					_controllerId = 0;
				}
				
				if (_requiredDevices.Count == 0 && ControllerIDs.Count == 0)
				{
					Thread.Sleep(5000);
					continue;
				}
				
				
					try
							{								
								if(ControllerIDs.Count > _controllerId)
								{
									SendDataPacket((byte)ControllerIDs[_controllerId].Address);								
								}
							}
							catch (TimeoutException)
							{
							}
							catch (InvalidOperationException ex)
							{
								ErrorNotifier.PrintError(ex);
							}
					
					Thread.Sleep(_period * 1000);					
					
				_controllerId++;
			}
		}
	}
}
