﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.Linq;
using RemoteHumidityControlReport.Entity;

namespace RemoteHumidityControlReport.DataAccess
{
    public static class FakeControllerConnector
    {
        public static float Aa
        {
            get;
            set;
        }

        public static float Bb
        {
            get;
            set;
        }

        private static readonly List<ControllerGeneralDataEntity> StoredData = new List<ControllerGeneralDataEntity>();

        public static IEnumerable<ControllerGeneralDataEntity> ReadConlrollerData(int controllerId, TimeSpan period)
        {
            Aa = 25;
            Bb = 10;
            var fakeData = new List<ControllerGeneralDataEntity>();

            var startDate = DateTime.Now.Subtract(period);
            var rand = new Random();
            for (var i = 0; i < period.TotalSeconds / 12; i++)
            {
                fakeData.Add(new ControllerGeneralDataEntity { Humidity = Aa + rand.Next((int)Bb / 2) - (Bb / 2), Temperature = rand.Next(5) + 60, Date = startDate.AddSeconds(12 * i) });
            }

            StoredData.AddRange(fakeData.ToArray());
            return fakeData;
        }

        public static IEnumerable<ControllerGeneralDataEntity> GetDataForPeriod(DateTime from, DateTime to)
        {            
            return StoredData.Where(hd => hd.Date >= from && hd.Date <= to).OrderBy(hd => hd.Date);
        }
    }
}
