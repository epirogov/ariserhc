﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 2/24/2013
 * Time: 1:52 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows;
using System.Windows.Threading;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
using System.IO.Ports;
using System.Data.SqlClient;
using RemoteHumidityControlReport.Controller;
using RemoteHumidityControlReport.Logic.Exceptions;
using RemoteHumidityControlReport.Logic;
using RemoteHumidityControlReport.Logic.DeviceControl;
using System.Threading;
using System.Collections.ObjectModel;
using System.Windows.Input;
using QA.FileDialogs;
using LibraryBusinessLogic;
using System.ComponentModel;
using RemoteHumidityControlReport.Entity;
using RemoteHumidityControlReport.DataAccess;

namespace RHC.View.Models
{
	/// <summary>
	/// Description of MainModel.
	/// </summary>
	public class MainModel : ViewModelBase
	{
		ArchiveController _controller;

		ArchiveController controller;
		
		private string portName;
				
		private bool _isReport;

        private bool _allowExit;
        
        private DelegateCommand _report;
        
        private DelegateCommand _scan;

        private bool _archive;

        private ObservableCollection<DataModel> device1;

        private ObservableCollection<DataModel> device2;

        private ObservableCollection<DataModel> archive1;

        private ObservableCollection<DataModel> archive2;

        private DelegateCommand<string> _processFileCommand;

        public MainModel(bool isReport)
		{			
			_isReport = isReport;

            device1 = new ObservableCollection<DataModel>();

            device2 = new ObservableCollection<DataModel>();

            Devices = new ObservableCollection<DeviceModel>();            
		}
		
		
		public string PortName 
		{
			get
			{
				return portName;
			}
			set
			{
				portName = value; Load();
			}
		}
		

		 
		internal SerialPort Port { get; set; }



        public ObservableCollection<DataModel> Device1 
        {
            get
            {
                return Archive ? archive1 : device1;
            }
        }



        public ObservableCollection<DataModel> Device2
        {
            get
            {
                return Archive ? archive2 : device2;
            }
        }
        
        
        
        public bool HasSecond 
        { 
            get 
            {
                return ConfigurationManager.AppSettings["RequredDeviceAddresses"].Split(';').Length > 1; 
            } 
        }



        public ICommand Scan
        {
            get
            {
                return _scan ?? (_scan = new DelegateCommand(s =>
                {
                    ScanController();
                }));
            }
        }



        public ICommand OpenFileCommand
        {
            get
            {
                return _processFileCommand ?? (_processFileCommand = new DelegateCommand<string>(p =>
                {
                    var bg = new BackgroundWorker();
                    bg.DoWork += controller.GetReport;
                    bg.RunWorkerCompleted += (s, e) =>
                    {
                        if (e.Cancelled) return;
                        ReportWriter.WriteReport(p, (List<ControllerGeneralDataEntity>)e.Result);
                    };
                    bg.RunWorkerAsync();
                }));
            }
        }



        public bool Archive 
        {
            get
            {
                return _archive;
            }


            set
            {
                _archive = value;
                RaisePropertyChanged(() => Device1);
                RaisePropertyChanged(() => Device2);
                GetArchive();
            }
        }



        private void GetArchive()
        {
            if (_archive)
            {
                var bg = new BackgroundWorker();
                bg.DoWork += controller.GetReport;
                bg.RunWorkerCompleted += bg_RunWorkerCompleted;
                bg.RunWorkerAsync();
            }
        }



        void bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled) return;
            var groups = ((List<ControllerGeneralDataEntity>)e.Result).GroupBy(entity => entity.Address).OrderBy(g => g.Key);
            var a1 = groups.FirstOrDefault();
            var a2 = groups.LastOrDefault();
            if (a1 != null)
                archive1 = new ObservableCollection<DataModel>(a1.Select(d => new DataModel {  Date = d.Date, Humidity = d.Humidity, Temperature = d.Temperature }));
            if (a2 != null)
                archive2 = new ObservableCollection<DataModel>(a2.Select(d => new DataModel { Date = d.Date, Humidity = d.Humidity, Temperature = d.Temperature }));
            RaisePropertyChanged(() => Device1);
            RaisePropertyChanged(() => Device2);
        }
        


        public DateTime From 
        {
            get
            {
                return controller.From;
            }
            set
            {
                controller.From  = value;
                GetArchive();
            }
        }



        public DateTime To
        {
            get
            {
                return controller.To;
            }
            set
            {
                controller.To = value;
                GetArchive();
            }
        }



        public ObservableCollection<DeviceModel> Devices { get; private set; }
		  
		  
		private void ScanController()
        {
            var thread = new Thread(new ThreadStart(delegate
                {
                    var result = controller.ScanDevices();
                })) { IsBackground = true };

            thread.Start();
        }
		
		

		private void Load()
        {
            if(!SerialPort.GetPortNames().Contains(PortName)) _isReport = true;

            if (_isReport)
            {
                return;
            }

            Port = new SerialPort(PortName, int.Parse(ConfigurationManager.AppSettings["BaundRate"]), Parity.None, 8, StopBits.One);
            Port.RtsEnable = true;
            Port.ReceivedBytesThreshold = 1;
            Port.ReadTimeout = 2000;

            _controller = new ArchiveController(Port);                        
            controller.Watcher.DataReceived +=Watcher_DataReceived;
            _controller.ControllerFounded += controller_ControllerFounded;

            string initialPortName = ConfigurationManager.AppSettings["SerialPortName"];
            _controller.OpenPort(PortName);
            
           	ScanController();
        }

        private void Watcher_DataReceived(object sender, GEventArgs<IEnumerable<RemoteHumidityControlReport.Entity.ControllerGeneralDataEntity>> e)
        {
            
        	  Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send,
              new Action(() => 
        	  {
                   e.Value1.ToList().ForEach(v => {
                    var d = new DataModel { Date = v.Date, Humidity = v.Humidity, Temperature = v.Temperature };
                    Device1.Add(d);
                    
        	                                                      	                          });
                   
        	  }
                     
              ));
        	                          	
                    
        }
 

 
        private void controller_ControllerFounded(object sender, GEventArgs<Spm3DeviceInfo> e)
        {   
        	
        	 Application.Current.Dispatcher.BeginInvoke(
                                        DispatcherPriority.Send,
                                        new Action(() => {
                                                   	Devices.Add(new DeviceModel { Address = e.Value1.Address });                                                   
                                                   }
                                                   	));
                                                                
             
        }

	}
}
