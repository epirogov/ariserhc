﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 25.02.2013
 * Time: 6:43
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace RHC.View.Models
{
	public class ViewModelBase : INotifyPropertyChanged
    {
        private int _index;


        public int GetIndex()
        {
            return _index;
        }



        public void SetIndex(int val)
        {
            _index = val;
        }



        public void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }



        protected void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression.Body.NodeType != ExpressionType.MemberAccess) return;
            var memberExpr = propertyExpression.Body as MemberExpression;
            if (memberExpr != null)
            {
                var propertyName = memberExpr.Member.Name;
                RaisePropertyChanged(propertyName);
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
    }
}
