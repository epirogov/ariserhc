﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.02.2013
 * Time: 20:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace RHC.View.Models
{
	/// <summary>
	/// Description of SelectedPort.
	/// </summary>
	public class SerialPorts
	{
		public string SelectedSerialPort { get; set; }
		
		
		public string [] Ports { get; set; }
	}
}
