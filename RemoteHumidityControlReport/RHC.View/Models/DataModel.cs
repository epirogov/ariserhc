﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RHC.View.Models
{
    public class DataModel
    {
        internal double Humidity { get; set; }

        internal double Temperature { get; set; }

        internal DateTime Date { get; set; }
        
        public string Влажность { get{return string.Format("{0:F4}", Humidity);} }

        public string Температура { get{return string.Format("{0:F4}", Temperature);} }

        public string Дата { get { return Date.ToString("HH:mm:ss dd/MM/yyyy"); } }

    }
}
