﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.Logic
{
	/// <summary>
	/// Description of COMSygnalTypeEnum.
	/// </summary>
	public enum ComSignalTypeEnum
	{
		Unknown,
		Cmode,
		ReadBuffer,
		GetRegistersGeneral,
		
		KeyboardScan,
		SetCursor,
		PrintXy,
		ClrScr,
			
		SetRegisters,
		WriteToEeprom,
		SetSpeed,
		SendId		
	}
}
