﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.Logic
{
	/// <summary>
	/// Description of PrintedText.
	/// </summary>
	public class PrintedText : CursorPosition
	{
		public PrintedText(string text, int posX, int posY) : base(posX, posY)
		{
			Text = text;
		}
		
		public string Text
		{
			get;
			private set;
		}
	}
}
