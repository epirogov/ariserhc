﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

namespace RemoteHumidityControlReport.Logic.ViewEntities
{
    public class DataEntity
    {
        public string Response
        {
            get;
            set;
        }
    }
}
