﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


using System;

namespace RemoteHumidityControlReport.Logic
{
	public class GEventArgs<TValue1> : EventArgs
	{
	    public TValue1 Value1 { get; set; }
	    public GEventArgs(TValue1 value1)
	    {
	        Value1 = value1;
	    }
	}

    public class GEventArgs<TValue1, TValue2> : GEventArgs<TValue1>
    {
        public TValue2 Value2 { get; set; }
        public GEventArgs(TValue1 value1, TValue2 value2) : base(value1)
        {
            Value2 = value2;
        }
    }
}
