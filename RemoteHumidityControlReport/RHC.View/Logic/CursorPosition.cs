﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.Logic
{
	/// <summary>
	/// Description of CursorPosition.
	/// </summary>
	public class CursorPosition : BaseCommandData
	{
		public CursorPosition(int posX, int posY)
		{
			PosX = posX;
			PosY = posY;
		}
		
		public int PosX
		{
			get;
			protected set;
		}
		
		public int PosY
		{
			get;
			protected set;
		}
	}
}
