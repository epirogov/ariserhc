﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.Logic
{
	/// <summary>
	/// Description of BaseCommandData.
	/// </summary>
	public class BaseCommandData
	{
		private static int _version;
		
		public BaseCommandData()
		{
			Version = CreateVersion();
		}
		
		public int Version
		{
			get;
			private set;
		}
		
		private static int CreateVersion()
		{
			return _version++;
		}
	}
}
