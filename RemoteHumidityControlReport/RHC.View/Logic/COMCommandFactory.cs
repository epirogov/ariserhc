﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


using System.Text;
using System.Linq;

namespace RemoteHumidityControlReport.Logic
{
	/// <summary>
	/// Description of COMCommandFactory.
	/// </summary>
	public static class ComCommandFactory
	{		
		public static KeyboardScan BuildKeyboardScan(byte [] rawdata)
		{
			return new KeyboardScan();
		}
		
		public static CursorPosition BuildSetCursor(byte [] rawdata)
		{
			return  new CursorPosition(rawdata[2], rawdata[3]);
		}
		
		public static PrintedText BuildPrintXy(byte [] rawdata)
		{
			var stringArray = rawdata.ToList().GetRange(4, rawdata.Length - 6).ToArray();
			var text = Encoding.GetEncoding(1251).GetString(stringArray);						
			return new PrintedText(text, rawdata[2], rawdata[3]);
		}
		
		public static ClrScr BuildClrScr(byte [] rawdata)
		{
			return new ClrScr();
		}
	}
}
