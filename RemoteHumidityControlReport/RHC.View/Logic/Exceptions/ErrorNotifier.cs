﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.IO;
using LibraryBusinessLogic;

namespace RemoteHumidityControlReport.Logic.Exceptions
{
    public static class ErrorNotifier
    {
        public static void PrintError(Exception ex)
        {
        	ex = ex ?? new Exception();
            try
            {
                Repository.NoQuery("WriteError", new List<SqlParameter> { 
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Message", SqlDbType = System.Data.SqlDbType.NVarChar, Size = 1000, Value = ex.Message},
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@StackTrace", SqlDbType = System.Data.SqlDbType.NVarChar, Size = Int32.MaxValue, Value = (ex.StackTrace ?? string.Empty)},
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@ExceptionType", SqlDbType = System.Data.SqlDbType.NVarChar, Size = 1000, Value = ex.GetType().FullName},
                                new SqlParameter { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@FullMessage", SqlDbType = System.Data.SqlDbType.NVarChar, Size = Int32.MaxValue, Value = ex.ToString()} });
            }
            catch (Exception ex1)
            {
                
            }
        }     	
    }
}
