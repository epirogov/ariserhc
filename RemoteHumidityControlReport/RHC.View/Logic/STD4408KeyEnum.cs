﻿/*
 * Created by SharpDevelop.
 * User: Eugene Pirogov
 * Date: 25.04.2010
 * Time: 13:12
 * Web site : arise-project.org.ua
 */


namespace RemoteHumidityControlReport.Logic
{
	/// <summary>
	/// Description of STD4408KeyEnum.
	/// </summary>
	public enum Std4408KeyEnum
	{
		None,
		Shift, ZeroStar, Enter, Equal,
		OneF1, TwoDown, ThreeF2, Plus,
		FourLeft, FiveF3, SixRight, Minus,
		SevenF4, EightUp, NineF5, Multiply		
	}
}
