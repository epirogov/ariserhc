﻿using RHC.View.Models;
using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using RemoteHumidityControlReport.Logic.Exceptions;
using LibraryBusinessLogic;

namespace RHC.View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        MainModel mainModel;

        protected override void OnStartup(StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            var p = ConfigurationManager.AppSettings["SerialPortName"];
            mainModel = new MainModel(p == "DENY");
            var mainView = new MainWindow { DataContext = mainModel } ;
            mainView.Show();
          	
            if(string.IsNullOrEmpty( p ))
            {
            	var model = new SerialPorts { Ports = SerialPort.GetPortNames().OrderBy(n => n).ToArray() };
            	var portForm = new SerialPortWindow { DataContext = model };
            	portForm.ShowDialog();
            	
            	if(portForm.DialogResult == true && !string.IsNullOrEmpty(model.SelectedSerialPort))
            	{
            		mainModel.PortName = model.SelectedSerialPort;
            	}
            }
            else
            {
            	mainModel.PortName = p == "DENY" ? string.Empty : p;
            }

            if (!Repository.CheckConnection())
            {
                var error = new NoConnectionWindow();
                error.ShowDialog();
            }

            base.OnStartup(e);
        }


        protected override void OnExit(ExitEventArgs e)
        {
            if (mainModel.Port != null && mainModel.Port.IsOpen) mainModel.Port.Close();
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
        	ErrorNotifier.PrintError(e.ExceptionObject as Exception);
        }

        private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            ErrorNotifier.PrintError(e.Exception);
        }
    }
}
