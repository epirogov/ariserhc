﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RHC.View
{
    /// <summary>
    /// Interaction logic for SerialPortWindow.xaml
    /// </summary>
    public partial class SerialPortWindow : Window
    {
        public SerialPortWindow()
        {
            InitializeComponent();
            if (bool.Parse(ConfigurationManager.AppSettings["SoftwareOnly"]))
                Dispatcher.BeginInvoke(new Action(delegate
                {
                    var hwndSource = PresentationSource.FromVisual(this) as HwndSource;
                    if (null == hwndSource)
                    {
                        throw new InvalidOperationException("No HWND");
                    }
                    var hwndTarget = hwndSource.CompositionTarget;
                    if (hwndTarget != null)
                        hwndTarget.RenderMode = RenderMode.SoftwareOnly;
                }), System.Windows.Threading.DispatcherPriority.ContextIdle, null);
        }
        
		void Button1_Click(object sender, RoutedEventArgs e)
		{
			if(listBox1.SelectedItem != null)
			{
				DialogResult = true;
				Close();
			}
		}
    }
}
