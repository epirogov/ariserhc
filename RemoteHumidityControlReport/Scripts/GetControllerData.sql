USE [SPM3Database]
GO
/****** Object:  StoredProcedure [dbo].[GetControllerData]    Script Date: 01/03/2011 22:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetControllerData]
	@DateFrom datetime,
	@DateTo datetime
AS
BEGIN
	select [Address], [Humidity], [Temperature], [Date] from ControllerData 	
	where 	
	DATEDIFF(day, [Date], @DateTo) <= 0 and DATEDIFF(day, @DateFrom, [Date]) >= 0
	order by [Date] desc
END

