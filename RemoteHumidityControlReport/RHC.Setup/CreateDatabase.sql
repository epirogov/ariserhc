USE [master]
GO

USE [master]
GO
/****** Object:  Database [SPM3Database]    Script Date: 10/27/2010 18:58:48 ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'SPM3Database')
begin
/****** Object:  Database [SPM3Database]    Script Date: 10/27/2010 18:58:48 ******/
CREATE DATABASE [SPM3Database] ON  PRIMARY 
( NAME = N'SPM3Database', FILENAME = N'c:\RHC\SPM3Database.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SPM3Database_log', FILENAME = N'c:\RHC\SPM3Database_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)



IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SPM3Database].[dbo].[sp_fulltext_database] @action = 'enable'
end


ALTER DATABASE [SPM3Database] SET ANSI_NULL_DEFAULT OFF 


ALTER DATABASE [SPM3Database] SET ANSI_NULLS OFF 


ALTER DATABASE [SPM3Database] SET ANSI_PADDING OFF 


ALTER DATABASE [SPM3Database] SET ANSI_WARNINGS OFF 


ALTER DATABASE [SPM3Database] SET ARITHABORT OFF 


ALTER DATABASE [SPM3Database] SET AUTO_CLOSE OFF 


ALTER DATABASE [SPM3Database] SET AUTO_CREATE_STATISTICS ON 


ALTER DATABASE [SPM3Database] SET AUTO_SHRINK OFF 


ALTER DATABASE [SPM3Database] SET AUTO_UPDATE_STATISTICS ON 


ALTER DATABASE [SPM3Database] SET CURSOR_CLOSE_ON_COMMIT OFF 


ALTER DATABASE [SPM3Database] SET CURSOR_DEFAULT  GLOBAL 


ALTER DATABASE [SPM3Database] SET CONCAT_NULL_YIELDS_NULL OFF 


ALTER DATABASE [SPM3Database] SET NUMERIC_ROUNDABORT OFF 


ALTER DATABASE [SPM3Database] SET QUOTED_IDENTIFIER OFF 


ALTER DATABASE [SPM3Database] SET RECURSIVE_TRIGGERS OFF 


ALTER DATABASE [SPM3Database] SET  DISABLE_BROKER 


ALTER DATABASE [SPM3Database] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 


ALTER DATABASE [SPM3Database] SET DATE_CORRELATION_OPTIMIZATION OFF 


ALTER DATABASE [SPM3Database] SET TRUSTWORTHY OFF 


ALTER DATABASE [SPM3Database] SET ALLOW_SNAPSHOT_ISOLATION OFF 


ALTER DATABASE [SPM3Database] SET PARAMETERIZATION SIMPLE 


ALTER DATABASE [SPM3Database] SET READ_COMMITTED_SNAPSHOT OFF 


ALTER DATABASE [SPM3Database] SET  READ_WRITE 


ALTER DATABASE [SPM3Database] SET RECOVERY SIMPLE 


ALTER DATABASE [SPM3Database] SET  MULTI_USER 


ALTER DATABASE [SPM3Database] SET PAGE_VERIFY CHECKSUM  


ALTER DATABASE [SPM3Database] SET DB_CHAINING OFF 






/****** Object:  Table [dbo].[ControllerData]    Script Date: 10/27/2010 18:59:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ControllerData]') AND type in (N'U'))
DROP TABLE [dbo].[ControllerData]





/****** Object:  Table [dbo].[ControllerData]    Script Date: 10/27/2010 18:59:52 ******/

CREATE TABLE [dbo].[ControllerData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Address] [int] NOT NULL,
	[Humidity] [float] NOT NULL,
	[Temperature] [float] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_ControllerData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]




/****** Object:  Table [dbo].[ProgramException]    Script Date: 10/27/2010 19:39:35 ******/

CREATE TABLE [dbo].[ProgramException](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](1000) NOT NULL,
	[StackTrace] [nvarchar](max) NOT NULL,
	[ExceptionType] [nvarchar](1000) NOT NULL,
	[FullMessage] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL default (getdate())
 CONSTRAINT [PK_ProgramException] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

end
GO


GO

/****** Object:  StoredProcedure [dbo].[WriteControllerData]    Script Date: 10/27/2010 19:09:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WriteControllerData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WriteControllerData]
GO


GO

/****** Object:  StoredProcedure [dbo].[WriteControllerData]    Script Date: 10/27/2010 19:09:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WriteControllerData]
	@AddressId int,
	@Humidity float,
	@Temperature float,
	@Date datetime
AS
BEGIN
	insert into ControllerData ( [Address], [Date], [Humidity], [Temperature])
	values (@AddressId, @Date, @Humidity, @Temperature)
END

GO



GO

/****** Object:  Table [dbo].[ProgramException]    Script Date: 10/27/2010 19:39:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProgramException]') AND type in (N'U'))
DROP TABLE [dbo].[ProgramException]
GO


GO

/****** Object:  StoredProcedure [dbo].[WriteControllerData]    Script Date: 10/27/2010 19:45:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WriteControllerData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WriteControllerData]
GO


GO

/****** Object:  StoredProcedure [dbo].[WriteControllerData]    Script Date: 10/27/2010 19:45:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WriteControllerData]
	@AddressId int,
	@Humidity float,
	@Temperature float,
	@Date datetime
AS
BEGIN
	insert into ControllerData ( [Address], [Date], [Humidity], [Temperature])
	values (@AddressId, @Date, @Humidity, @Temperature)
END

GO



GO

/****** Object:  StoredProcedure [dbo].[WriteError]    Script Date: 10/27/2010 19:54:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WriteError]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WriteError]
GO


GO

/****** Object:  StoredProcedure [dbo].[WriteError]    Script Date: 10/27/2010 19:54:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WriteError]
	@Message [nvarchar](1000),
	@StackTrace [nvarchar](max),
	@ExceptionType [nvarchar](1000),
	@FullMessage [nvarchar](max)
AS
BEGIN
	insert into ProgramException ([Message], [StackTrace], [ExceptionType], [FullMessage])
	values (@Message, @StackTrace, @ExceptionType, @FullMessage)
END

GO



GO

/****** Object:  StoredProcedure [dbo].[GetControllerData]    Script Date: 10/27/2010 21:59:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetControllerData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetControllerData]
GO


GO
/****** Object:  StoredProcedure [dbo].[GetControllerData]    Script Date: 01/03/2011 22:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetControllerData]
	@DateFrom datetime,
	@DateTo datetime
AS
BEGIN
	select [Address], [Humidity], [Temperature], [Date] from ControllerData 	
	where [Date]
	 between CONVERT(datetime,@DateFrom,101) and CONVERT(datetime,@DateTo,101)
	order by [Date] desc
END


GO


/****** Object:  StoredProcedure [dbo].[GetErrors]    Script Date: 04.03.2013 21:09:39 ******/
DROP PROCEDURE [dbo].[GetErrors]
GO

/****** Object:  StoredProcedure [dbo].[GetErrors]    Script Date: 04.03.2013 21:09:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetErrors]
AS
BEGIN
	SELECT * from ProgramException
END

GO