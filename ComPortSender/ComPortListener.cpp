// ComPortSender.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <tchar.h>
#include <string.h>
#include <direct.h>
#include <conio.h>

using namespace std;
int _tmain(int argc, _TCHAR* argv[])
{	
	DCB dcb, dcb2;
	COMMTIMEOUTS CommTimeOuts;
	DWORD ret;	

	int com1Number,com2Number;
	char com1[10];
	char com2[10];
	char buf[10];
	cout<<"Read from:";
	cin>>com1Number;
	cout<<"Write to:";
	cin>>com2Number;
	strcpy(com1,"COM");
	strcpy(com2,"COM");
	strcat(com1,itoa(com1Number, buf, 10));
	strcat(com2,itoa(com2Number, buf, 10));

	HANDLE hCom2 = CreateFile( (LPCWSTR)(atoi((char *)argv[1]) == 1 ? "COM3" : "COM3"), GENERIC_READ | GENERIC_WRITE,
                      0,               // comm devices must be opened w/exclusive-access
                      NULL,            // no security attrs
                      OPEN_EXISTING,   // comm devices must use OPEN_EXISTING
                      0,               // not overlapped I/O
                      NULL             // hTemplate must be NULL for comm devices
                    );

   GetCommState(hCom2,&dcb);
      dcb.BaudRate =9600;
      dcb.ByteSize = 8;
      dcb.Parity = NOPARITY;
      dcb.StopBits = ONESTOPBIT;
      dcb.fDtrControl = DTR_CONTROL_DISABLE;
      dcb.fRtsControl = RTS_CONTROL_DISABLE;
   SetCommState( hCom2, &dcb);
   

   GetCommTimeouts (hCom2,&CommTimeOuts);
      CommTimeOuts.ReadIntervalTimeout=10;
      CommTimeOuts.ReadTotalTimeoutMultiplier=10;
      CommTimeOuts.ReadTotalTimeoutConstant=10;
   SetCommTimeouts (hCom2,&CommTimeOuts);   
   printf("Com2 complit\n");

   unsigned char * temp = new unsigned char[256]; 
   strcpy((char *)temp, "help");
   
   while(true)
   {
	   Sleep(1000);
	   if(atoi((char *)argv[1]) == 1)
	   {
			ReadFile(hCom2,temp,5,&ret,NULL);	
			temp[ret] = 0;
		   if(ret > 0)
		   {
			   for(int i =0; i < 10; i++)
			   {
				printf("%i", (int)temp[i]);
			   }
		   }
		   else
		   {
			   printf("%i",ret);
		   }
	   }
	   else
	   {
			WriteFile(hCom2, temp, 5, &ret, NULL);
	   }   
	   
   }

	return 0;
}

