#pragma once

using namespace System;

#include "FileReaderBase.h"

namespace LibmodbusCustomCommands
{
	public ref class BinaryFileReader
		: public FileReaderBase
	{
	public:
		BinaryFileReader();
	    BinaryFileReader(const char * filename);
	    ~BinaryFileReader();
	    void ReadToEnd(Byte * & array, int & length);
	};
}