#include "StdAfx.h"
#include <stdlib.h>
#include <string.h>
#include "CommandFactory.h"
#include "CommandResponseFactory.h"
#include "CustomFunctionMapping.h"
#include "ControllerCommandsEnum.h"
#include "DataTransferCommandEnum.h"
#include "RemoteAccessCommandsEnum.h"
#include "DefaultCommandsEnum.h"

using namespace System;
using namespace System::Threading;

using namespace LibmodbusCustomCommands;

CommandFactory::CommandFactory()
{
	syncRoot = gcnew Object();
}
CommandFactory::~CommandFactory()
{
}

CommandEntity ^ CommandFactory::CreateCMODE(Byte address, CMODEEnum mode)
{
    return gcnew CommandEntity(address, SMP3_CMODE, (System::Byte)mode, true, NULL, 0, SMP3_CMODE_RESPONSE_DATA_LENGTH);
}

CommandEntity ^ CommandFactory::CreateReadBuffer(Byte address, ReadBufferEnum channel)
{
    return gcnew CommandEntity(address, SMP3_READ_BUFFER, (System::Byte)channel, true, NULL, 0, SMP3_READ_BUFFER_RESPONSE_DATA_LENGTH);
}

CommandEntity ^ CommandFactory::CreateGetRegisters(System::Byte address, short startAddress, System::Byte count)
{
	static unsigned char rawdata[256];
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	
    rawdata[0] = startAddress >> 8;
    rawdata[1] = startAddress &0xFF;
    rawdata[2] = count;
	try
	{
		return  gcnew CommandEntity(address, SMP3_GET_REGISTERS, 0, false, rawdata, 3, SMP3_GET_REGISTERS_RESPONSE_DATA_LENGTH);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateSetRegisters(Byte address, short startAddress, Byte count, const Byte * registers, Byte registerslength)
{
	static unsigned char rawdata[256];
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	
    //System::Byte realBytesCount = 0;
    //CommandResponseFactory::CalculateStartAddressAndSizeRegisters(startAddress, count, realBytesCount);
    rawdata[0] = startAddress >> 8;
    rawdata[1] = startAddress & 0xFF;
    rawdata[2] = count;
    memcpy(rawdata + 3, registers, registerslength);

	try
	{
		return gcnew CommandEntity(address, SMP3_SET_REGISTERS, 0, false, rawdata, registerslength + 3, SMP3_SET_REGISTERS_RESPONSE_DATA_LENGTH);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateWriteToEEPROM(Byte address)
{
    return gcnew CommandEntity(address, SMP3_WRITE_EEPROM, 0, false, NULL, 0, SMP3_WRITE_EEPROM_RESPONSE_DATA_LENGTH);
}

CommandEntity ^ CommandFactory::CreateSetSpeed(Byte address, SpeedEnum mode)
{
    return gcnew CommandEntity(address, SMP3_SET_SPEED, (System::Byte)mode, true, NULL, 0, SMP3_SET_SPEED_RESPONSE_DATA_LENGTH);
}

CommandEntity ^ CommandFactory::CreateSendId(Byte address)
{
    return gcnew CommandEntity(address, SMP3_SEND_ID, 0, false, NULL, 0, SMP3_SEND_ID_RESPONSE_DATA_LENGTH);
}

CommandEntity ^ CommandFactory::CreateResponse(Byte * rawresponse, int count)
{
	System::Byte address = rawresponse[0];
    System::Byte function = rawresponse[1];
    System::Byte subfunction = 0;
    bool hassubfunction = false;
    System::Byte * data = rawresponse + 2 + hassubfunction;
    System::Byte datacount = count - 4 + hassubfunction;	

	switch(function)
    {
        case SMP3_CMODE :
            hassubfunction = false;
            break;
        case SMP3_READ_BUFFER :
            hassubfunction = false;
            datacount = count - 4;
            break;
        case SMP3_GET_REGISTERS :
            hassubfunction = false;
            datacount = count - 4;
            break;
        case SMP3_SET_REGISTERS :
            hassubfunction = false;
            break;
        case SMP3_WRITE_EEPROM :
            hassubfunction = false;
            break;
        case SMP3_SET_SPEED :
            hassubfunction = false;
            break;
        case SMP3_SEND_ID :
            hassubfunction = false;
            datacount = count - 4;
            break;
		case SCANCONTROLLER_FUNCTION :
			return CreateScanController(address);			
			break;
		case REGISTERCONTROLLER_FUNCTION :						
			return CreateRegisterController(address, rawresponse[1]);			
			break;
		case DEVICEWASHALTED_FUNCTION :
			return CreateDeviceWasHalted(address);			
			break;
		case REQUESTDATA_FUNCTION :
			return CreateRequestData(address, DateTime::Now, gcnew TimeSpan() );			
			break;
		case DATAPACKETRECEIVED_FUNCTION :
			return CreateDataPacketReceived(address, data, datacount);			
			break;
		case DATATRANSACTIONCOMPLITED_FUNCTION :
			return CreateDataTransactionComplited(address);			
			break;
		case BEGINREMOTECONTROL_FUNCTION :
			return CreateBeginRemoteControl(address);			
			break;
		case SENDDEVICEFILES_FUNCTION :
			return CreateSendDeviceFiles(address, 0);			
			break;
		case REQUESTDEVICEFILE_FUNCTION :
			return CreateRequestDeviceFile(address, 0);			
			break;
		case SAVEDEVICEFILES_FUNCTION :
			return CreateSaveDeviceFiles(address, 0);			
			break;
		case DEVICEFILEUPLOAD_FUNCTION :
			return CreateDeviceFileUpload(address, 0);			
			break;
		case COMPLITEREMOTECONTROL_FUNCTION :
			return CreateCompliteRemoteControl(address);			
			break;			
    }

    if(datacount > 0)
    {
        data = new System::Byte[datacount];
        memcpy(data, rawresponse + count - datacount - 2, datacount);
    }

    return gcnew CommandEntity(address, function, subfunction, hassubfunction, data, datacount, 0);
}

CommandEntity ^ CommandFactory::CreateScanController(Byte address)
{
	return gcnew CommandEntity(address, SCANCONTROLLER_FUNCTION, 0, false, NULL, 0, SCANCONTROLLER_FUNCTION_RESPONSE_DATA_LENGTH, (int)ControllerCommandsEnum::ScanController);
}

CommandEntity ^ CommandFactory::CreateRegisterController(Byte address, Byte deviceType)
{
	static unsigned char rawdata[256];    
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	
	rawdata[0] = 1;    // char * name = "TM3"; May me use crc to set value from name
	try
	{
		return gcnew CommandEntity(address, REGISTERCONTROLLER_FUNCTION, 0, false, rawdata, 1, REGISTERCONTROLLER_FUNCTION_RESPONSE_DATA_LENGTH, (int)ControllerCommandsEnum::RegisterController);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateDeviceWasHalted(Byte address)
{
	return gcnew CommandEntity(address, DEVICEWASHALTED_FUNCTION, 0, false, NULL, 0, DEVICEWASHALTED_FUNCTION_RESPONSE_DATA_LENGTH, (int)ControllerCommandsEnum::DeviceWasHalted);
}
		
CommandEntity ^ CommandFactory::CreateRequestData(Byte address, DateTime ^ startDate, TimeSpan ^ duration)
{		
	//Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	
	/*rawdata[0] = startDate->Ticks >> 56 & 0xFF;
	rawdata[1] = startDate->Ticks >> 48 & 0xFF;
	rawdata[2] = startDate->Ticks >> 40 & 0xFF;
	rawdata[3] = startDate->Ticks >> 32 & 0xFF;
	rawdata[4] = startDate->Ticks >> 24 & 0xFF;
	rawdata[5] = startDate->Ticks >> 16 & 0xFF;
	rawdata[6] = startDate->Ticks >> 8 & 0xFF;
	rawdata[7] = startDate->Ticks & 0xFF;	*/
	/*rawdata[8] = (Byte)duration->Minutes;
	rawdata[9] = (Byte)duration->Seconds;*/
	try
	{
		return gcnew CommandEntity(address, REQUESTDATA_FUNCTION, 0, false, NULL, 0, REQUESTDATA_FUNCTION_RESPONSE_DATA_LENGTH, (int)DataTransferCommandEnum::RequestData);
	}
	finally
	{		
		//Monitor::Pulse(syncRoot);
		//Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateDataPacketReceived(Byte address, System::Byte * data, System::Byte datacount)
{
	return gcnew CommandEntity(address, DATAPACKETRECEIVED_FUNCTION, 0, false, data, datacount, DATAPACKETRECEIVED_FUNCTION_RESPONSE_DATA_LENGTH, (int)DataTransferCommandEnum::DataPacketReceived);
}

CommandEntity ^ CommandFactory::CreateDataPacketBrocken(Byte address)
{
	return gcnew CommandEntity(address, DATAPACKETRECEIVED_FUNCTION, 0, true, NULL, 0, DATAPACKETRECEIVED_FUNCTION_RESPONSE_DATA_LENGTH, (int)DataTransferCommandEnum::DataPacketBrocken);
}

CommandEntity ^ CommandFactory::CreateDataTransactionComplited(Byte address)
{
	return gcnew CommandEntity(address, DATATRANSACTIONCOMPLITED_FUNCTION, 0, false, NULL, 0, DATATRANSACTIONCOMPLITED_FUNCTION_RESPONSE_DATA_LENGTH, (int)DataTransferCommandEnum::DataTransactionComplited);
}

CommandEntity ^ CommandFactory::CreateBeginDataSend(Byte address, TimeSpan ^ duration)
{
	static unsigned char rawdata[256];    
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	
	rawdata[0] = duration->Minutes;
	rawdata[1] = duration->Seconds;

	int identifier = (int)DataTransferCommandEnum::BeginDataSend;

	if(duration->Minutes == 0 && duration->Seconds == 0)
	{
		int identifier = (int)DataTransferCommandEnum::NoMedium;
	}

	try
	{
		return gcnew CommandEntity(address, REQUESTDATA_FUNCTION, 0, false, rawdata, 2, BEGINDATASEND_FUNCTION_RESPONSE_DATA_LENGTH, identifier);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateSendDataPacket(Byte address, int offset, Byte count, Byte * data, int datalength)
{
	static unsigned char rawdata[256];    
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	
	int rawdatalength = 5 + datalength;
	rawdata[0] = offset >> 24;
	rawdata[1] = offset >> 16 & 0xFF;
	rawdata[2] = offset >> 8 & 0xFF;
	rawdata[3] = offset & 0xFF;
	rawdata[4] = count;

	for(int i =0; i < datalength; i++)
	{
		rawdata[5 + i] = data[i];
	}

	try
	{
		return gcnew CommandEntity(address, SENDDATAPACKET_FUNCTION, 0, false, rawdata, rawdatalength, SENDDATAPACKET_FUNCTION_RESPONSE_DATA_LENGTH, (int)DataTransferCommandEnum::SendDataPacket);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateEndDataSend(Byte address, bool hasData)
{
	return gcnew CommandEntity(address, DATATRANSACTIONCOMPLITED_FUNCTION, hasData, true, NULL, 0, ENDDATASEND_FUNCTION_RESPONSE_DATA_LENGTH, (int)DataTransferCommandEnum::EndDataSend);
}

CommandEntity ^ CommandFactory::CreateNoMedium(Byte address)
{
	return CommandFactory::CreateBeginDataSend(address, gcnew TimeSpan(0));	
}

		
CommandEntity ^ CommandFactory::CreateBeginRemoteControl(Byte address)
{
	return gcnew CommandEntity(address, BEGINREMOTECONTROL_FUNCTION, 0, false, NULL, 0, BEGINREMOTECONTROL_FUNCTION_RESPONSE_DATA_LENGTH, (int)RemoteAccessCommandsEnum::BeginRemoteControl);
}

CommandEntity ^ CommandFactory::CreateRequestDeviceFile(Byte address, int fileCode)
{
	static unsigned char rawdata[256];    
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	

	rawdata[0] = fileCode >> 24;
	rawdata[1] = fileCode >> 16 & 0xFF;
	rawdata[2] = fileCode >> 8 & 0xFF;
	rawdata[3] = fileCode & 0xFF;	

	try
	{
		return gcnew CommandEntity(address, REQUESTDEVICEFILE_FUNCTION, 0, false, rawdata, 4, REQUESTDEVICEFILE_FUNCTION_RESPONSE_DATA_LENGTH, (int)RemoteAccessCommandsEnum::RequestDeviceFile);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}

}

CommandEntity ^ CommandFactory::CreateSaveDeviceFiles(Byte address, int fileMask)
{
	static unsigned char rawdata[256];    
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	
	rawdata[0] = fileMask >> 24;
	rawdata[1] = fileMask >> 16 & 0xFF;
	rawdata[2] = fileMask >> 8 & 0xFF;
	rawdata[3] = fileMask & 0xFF;	

	try
	{
		return gcnew CommandEntity(address, SAVEDEVICEFILES_FUNCTION, 0, false, rawdata, 4, SAVEDEVICEFILES_FUNCTION_RESPONSE_DATA_LENGTH, (int)RemoteAccessCommandsEnum::SaveDeviceFiles);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateDeviceFileUpload(Byte address, int fileCode)
{
	static unsigned char rawdata[256];    
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	

	rawdata[0] = fileCode >> 24;
	rawdata[1] = fileCode >> 16 & 0xFF;
	rawdata[2] = fileCode >> 8 & 0xFF;
	rawdata[3] = fileCode & 0xFF;		

	try
	{
		return gcnew CommandEntity(address, DEVICEFILEUPLOAD_FUNCTION, 0, false, rawdata, 4, DEVICEFILEUPLOAD_FUNCTION_RESPONSE_DATA_LENGTH, (int)RemoteAccessCommandsEnum::DeviceFileUpload);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateCompliteRemoteControl(Byte address)
{
	return gcnew CommandEntity(address, COMPLITEREMOTECONTROL_FUNCTION, 0, false, NULL, 0, COMPLITEREMOTECONTROL_FUNCTION_RESPONSE_DATA_LENGTH, (int)RemoteAccessCommandsEnum::CompliteRemoteControl);
}

CommandEntity ^ CommandFactory::CreateSendDeviceFiles(Byte address, int fileMask)
{
	static unsigned char rawdata[256];    
	Monitor::Enter(syncRoot);	
	//Monitor::Wait(syncRoot);	

	rawdata[0] = fileMask >> 24;
	rawdata[1] = fileMask >> 16 & 0xFF;
	rawdata[2] = fileMask >> 8 & 0xFF;
	rawdata[3] = fileMask & 0xFF;	
	try
	{
		return gcnew CommandEntity(address, SENDDEVICEFILES_FUNCTION, 0, false, rawdata, 4, SENDDEVICEFILES_FUNCTION_RESPONSE_DATA_LENGTH, (int)RemoteAccessCommandsEnum::SendDeviceFiles);
	}
	finally
	{
		//Monitor::Pulse(syncRoot);
		Monitor::Exit(syncRoot);
	}
}

CommandEntity ^ CommandFactory::CreateResponseTemplate(CommandEntity ^ request)
{
	switch(request->identifier)
	{		
	case ControllerCommandsEnum::ScanController :
			return CreateRegisterController(request->GetAddress(), 0);
			break;
	case ControllerCommandsEnum::RegisterController :
			return CreateDefaultResponse(request);
			break;
	case ControllerCommandsEnum::DeviceWasHalted :
			return CreateDefaultResponse(request);
			break;
	case DataTransferCommandEnum::DataPacketReceived :
			return CreateDefaultResponse(request);
			break;
	case DataTransferCommandEnum::DataPacketBrocken :
			return CreateDefaultResponse(request);
			break;
	case DataTransferCommandEnum::DataTransactionComplited :
			return CreateEndDataSend(request->GetAddress(), false);
			break;
	case DataTransferCommandEnum::BeginDataSend :
			return CreateDefaultResponse(request);
			break;
	case DataTransferCommandEnum::SendDataPacket :
			return CreateDefaultResponse(request);
			break;
	case DataTransferCommandEnum::EndDataSend :
			return CreateDefaultResponse(request);
			break;
	case DataTransferCommandEnum::NoMedium :
			return CreateDefaultResponse(request);
			break;
	case RemoteAccessCommandsEnum::BeginRemoteControl :
			return CreateDefaultResponse(request);
			break;
	case RemoteAccessCommandsEnum::RequestDeviceFile :
			return CreateDefaultResponse(request);
			break;
	case RemoteAccessCommandsEnum::SaveDeviceFiles :
			return CreateDefaultResponse(request);
			break;
	case RemoteAccessCommandsEnum::DeviceFileUpload :
			return CreateDefaultResponse(request);				 
			break;
	case RemoteAccessCommandsEnum::CompliteRemoteControl :
			return CreateDefaultResponse(request);
			break;
	case RemoteAccessCommandsEnum::SendDeviceFiles :
			return CreateDefaultResponse(request);
			break;
		default:
			return CreateDefaultResponse(request);
			break;
	}	
}

CommandEntity ^ CommandFactory::CreateDefaultResponse(CommandEntity ^ request)
{
	return gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, NULL, 0, 0, (int)DefaultCommandsEnum::DefaultResponse);
}