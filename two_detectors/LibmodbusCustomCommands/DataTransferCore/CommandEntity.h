#pragma once

#include "CommandHeader.h"

namespace LibmodbusCustomCommands
{
	public ref class CommandEntity : public CommandHeader
	{
	    public :
	    System::Byte * data;
	    int datalength;
	    int responselength;
		int identifier;		
		CommandEntity()
		{
		}
	    CommandEntity(System::Byte address, System::Byte function, System::Byte subfunction, bool hasSubfunction, System::Byte * data, int datalength, int responselength);
		CommandEntity(System::Byte address, System::Byte function, System::Byte subfunction, bool hasSubfunction, System::Byte * data, int datalength, int responselength, int identifier);
	    ~CommandEntity();

	    void Build(System::Byte * rawcommand, int & rawcommandlength);
		private:
		void CreateInternals(System::Byte * data, int datalength, int responselength);
	};
}