namespace LibmodbusCustomCommands
{
	public enum class ControllerTypeEnum { RemoteCentre = 'g', HumidityController = 'c' };
}