#include "StdAfx.h"
#include <iostream>
#include <time.h>
#include "DeviceSemulator.h"
#include "CommandResponseFactory.h"
#include "CommandEntity.h"
#include "CommandParser.h"

using namespace std;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Threading;

using namespace LibmodbusCustomCommands;
using namespace System::IO::Ports;

DeviceSemulator::DeviceSemulator(DeviceTypeEnum deviceType, SerialPort ^ serialPort, const char * datafilename)
{
    this->listen = false;
	List<CustomFunctionMapping ^> ^ mapping = gcnew List<CustomFunctionMapping ^>();
    mapping->Add(gcnew CustomFunctionMapping(SMP3_CMODE, 0, SMP3_CMODE_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_READ_BUFFER, 0, SMP3_READ_BUFFER_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_GET_REGISTERS, 0, SMP3_GET_REGISTERS_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_SET_REGISTERS, 0, SMP3_SET_REGISTERS_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_WRITE_EEPROM, 0, SMP3_WRITE_EEPROM_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_SET_SPEED, 0, SMP3_SET_SPEED_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_SEND_ID, 0, SMP3_SEND_ID_REQUEST_DATA_LENGTH));
	this->provider = gcnew ModbusProvider(deviceType, serialPort, 19200, Parity::None, 8, StopBits::One, mapping);
    this->responseFactory = gcnew CommandResponseFactory(datafilename);
}

DeviceSemulator::DeviceSemulator(SerialPort ^ serialPort, const char * datafilename) //: this(RS485, device, datafilename)
{
	this->listen = false;
	List<CustomFunctionMapping ^> ^ mapping = gcnew List<CustomFunctionMapping ^>();
    mapping->Add(gcnew CustomFunctionMapping(SMP3_CMODE, 0, SMP3_CMODE_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_READ_BUFFER, 0, SMP3_READ_BUFFER_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_GET_REGISTERS, 0, SMP3_GET_REGISTERS_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_SET_REGISTERS, 0, SMP3_SET_REGISTERS_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_WRITE_EEPROM, 0, SMP3_WRITE_EEPROM_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_SET_SPEED, 0, SMP3_SET_SPEED_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(SMP3_SEND_ID, 0, SMP3_SEND_ID_REQUEST_DATA_LENGTH));
	this->provider = gcnew ModbusProvider(DeviceTypeEnum::RS485, serialPort, 19200, Parity::None, 8, StopBits::One, mapping);
    this->responseFactory = gcnew CommandResponseFactory(datafilename);
}

DeviceSemulator::~DeviceSemulator()
{
    delete this->provider;
    this->responseFactory->Close();
    delete this->responseFactory;
}

void DeviceSemulator::Start()
{
    this->responseFactory->Open();
    this->provider->Initialize();
    this->listen = true;

	this->thread = gcnew Thread(gcnew ParameterizedThreadStart(DeviceSemulator::WatchCycle));
	this->thread->IsBackground = true;
	this->thread->Start(this);       
}

void DeviceSemulator::Stop()
{
    if(this->listen)
    {
        this->listen = false;
		this->thread->Join();        
        this->provider->Close();
    }
    this->responseFactory->Close();
}

void DeviceSemulator::WatchCycle(Object ^ arg)
{
	DeviceSemulator ^ deviceSemulator = (DeviceSemulator ^)arg;
    System::Byte * request = new System::Byte[256];
    System::Byte length = 256;
    int index = 0;
    cout<< "Start listen"<< endl;
    short oldcrc = -1;
    while(deviceSemulator->listen)
    {
        length = 256;

        deviceSemulator->provider->receive_msg(256, request, length, false);
        if(length > 0)
        {
            /*
            // resend message if not received
            short crc_received = (request[length - 2] << 8) | request[length - 1];
            if(oldcrc == crc_received)
            {
                deviceSemulator->provider->modbus_send(request, length);
                timespec * ts = new timespec();
                nanosleep(ModbusProvider::CreateDelay(COM_WAIT_TIMEOUT), ts);
                nanosleep(ts, NULL);
                oldcrc = -1;
                continue;
            }
            else
            {
                oldcrc = crc_received;
            }
            */
            cout << "Address : " << (int)request[0] << " function :" << (int)request[1] << " length :" << (int)length << endl;

            for(int i =0; i<length; i++)
            {
                cout << (int)request[i] << " ";
            }

            cout<< endl;
            cout.flush();

            CommandEntity ^ requestCommand = CommandParser::Parse(request, length);
            CommandEntity ^ responseCommand = deviceSemulator->responseFactory->Build(requestCommand);
            System::Byte rawresponse[256];
            int rawresponselength = 0;
            responseCommand->Build(rawresponse, rawresponselength);

            //cout << "Response Address : " << (int)rawresponse[0] << " function :" << (int)rawresponse[1] << " length :" << (int)rawresponselength << endl;

            /*for(int i =0; i<rawresponselength; i++)
            {
                cout << (int)rawresponse[i] << " ";
            }

            cout<< endl;
            cout.flush();*/

            deviceSemulator->provider->modbus_send(rawresponse, rawresponselength);
            //timespec * ts = new timespec();
			//Thread::Sleep(100);
            //nanosleep(ModbusProvider::CreateDelay(COM_WAIT_TIMEOUT), ts);
            ///nanosleep(ts, NULL);
        }


        //if(index++ > 7)
        //{<
        //    break;
        //}

    }
}
