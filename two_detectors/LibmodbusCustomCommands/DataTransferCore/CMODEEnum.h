#pragma once

namespace LibmodbusCustomCommands
{
	public enum class CMODEEnum {normal = 0x00, oneCycle = 0x01, reset_mcu = 0x02 };
}