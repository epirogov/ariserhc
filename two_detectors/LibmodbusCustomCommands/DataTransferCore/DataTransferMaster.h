#pragma once

#include "PrivateMessanger.h"
#include "CommandEntity.h"
#include "CommandResponseFactory.h"
#include "TypedEventArgs.h"

using namespace System;
using namespace System::IO::Ports;

namespace LibmodbusCustomCommands
{
	public delegate void CompliteTemplateEventHandler (Object ^ sender, TypedEventArgs<CommandEntity ^> ^ e);
	public ref class DataTransferMaster : public PrivateMessanger
	{
		private :
			ModbusProvider ^ provider;
			volatile bool listen;
			Thread ^ thread;
			CommandResponseFactory ^ responseFactoy;
		public:			
			event CompliteTemplateEventHandler ^ CompliteTemplate;
			event CompliteTemplateEventHandler ^ DataSent;
		public:			
			DataTransferMaster(DeviceTypeEnum deviceType, SerialPort ^ serialPort, int listenPeriod);
			DataTransferMaster(SerialPort ^ serialPort, int listenPeriod);
			~DataTransferMaster();

			void Open();
			int ScanController(unsigned char address);
			int ScanControllerRequest(unsigned char address);
			int RegisterController(unsigned char address, unsigned char deviceType);
			int DeviceWasHalted(unsigned char address);
			int RequestData(unsigned char address, DateTime ^ startDate, TimeSpan ^ duration);
			int SendDataPacket(unsigned char address, int offset, unsigned char count, unsigned char * data, int datalength);
			int DataPackedReseived(unsigned char address);
			int DataTransactionComplited(unsigned char address);
			int BeginRemoteControl(unsigned char address);
			int SendDeviceFiles(unsigned char address, int fileMask);
			int RequestDeviceFile(unsigned char address, int fileCode);
			int SaveDeviceFiles(unsigned char address, int fileMask);
			int DeviceFileUpload(unsigned char address, int fileCode);
			int CompliteRemoteControl(unsigned char address);
			void Close();

			void SendResponse(CommandEntity ^ responseCommand);
			int RetriveResponse(CommandEntity ^ entity);
			int RetriveLongResponse(CommandEntity ^ entity);		
	};
}