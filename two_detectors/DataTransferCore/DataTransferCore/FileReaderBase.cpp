#include "StdAfx.h"
#include <list>
#include <stdio.h>
#include <iostream>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <windows.h>
#include "FileReader.h"

using namespace std;

using namespace LibmodbusCustomCommands;

//const int FileReaderBase::PIPE_MODE = 0x1180;

FileReaderBase::FileReaderBase()
{
    CreateInternals("", STATICFILE);
}

FileReaderBase::FileReaderBase(const char * fileName, FileReaderMode mode)
{
    CreateInternals(fileName, mode);
}

FileReaderBase::FileReaderBase(int fd, FileReaderMode mode)
{
    CreateInternals(fd, mode);
}

FileReaderBase::~FileReaderBase()
{
    delete [] this->fileName;
}

void FileReaderBase::OnlyOnceRead()
{
    if(this->readed)
    {
        throw new exception();
    }

    readed = true;
}


void FileReaderBase::Open()
{
    if(this->mode == STATICFILE)
    {
        this->file = fopen(this->fileName,"r");
    }
    else if(this->mode == FD || this->mode == PIPE)
    {
        this->file = fdopen(fd,"r");
    }
}

void FileReaderBase::Close()
{
    fclose(file);
    if(this->mode == FD || this->mode == PIPE)
    {
        CloseHandle((HANDLE)fd);
    }
}

size_t FileReaderBase::GetFileSize(FILE * file)
{
    fseek(file, 0, SEEK_END);
    fpos_t size;
    fgetpos(file, &size);
    fseek(file, 0, SEEK_SET);
    return size/sizeof(char);
}

size_t FileReaderBase::GetFileSize(int td)
{
    struct stat buf;
    fstat(td, &buf);
    if(buf.st_mode == PIPE_MODE)
    {
        throw new exception();
    }

    return buf.st_size / sizeof(char);
}

size_t FileReaderBase::GetFileSize()
{
    if(this->mode == STATICFILE)
    {
        return this->GetFileSize(this->file);
    }
    else if(this->mode == FD)
    {
        return this->GetFileSize(this->fd);
    }
    else if(this->mode == PIPE)
    {
        throw new exception();
    }

    return this->GetFileSize(this->file);
}

void FileReaderBase::CreateInternals(const char * fileName, FileReaderMode mode)
{
    this->fileName = new char[strlen(fileName) + 1];
    strcpy(this->fileName, fileName);
    this->fd = -1;
    this->mode = mode;
    this->readed = false;
}

void FileReaderBase::CreateInternals(int fd, FileReaderMode mode)
{
    this->fd = fd;
    this->fileName = NULL;
    this->mode = mode;
    this->readed = false;
}
