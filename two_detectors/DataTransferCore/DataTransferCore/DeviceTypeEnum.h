#pragma once

namespace LibmodbusCustomCommands
{
	public enum class DeviceTypeEnum { RS485 = 0, PIPE = 1};
}