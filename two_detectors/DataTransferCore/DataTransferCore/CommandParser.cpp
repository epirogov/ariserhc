#include "StdAfx.h"
#include <stdlib.h>
#include <string.h>
#include "CustomFunctionMapping.h"
#include "CommandParser.h"
#include "ControllerCommandsEnum.h"
#include "DataTransferCommandEnum.h"
#include "RemoteAccessCommandsEnum.h"
#include "DefaultCommandsEnum.h"

using namespace System;

using namespace LibmodbusCustomCommands;

CommandEntity ^ CommandParser::Parse(Byte * rawcommand, unsigned int rawcommandlength)
{
    System::Byte address = rawcommand[0];
    System::Byte function = rawcommand[1];
    System::Byte subfunction = 0;
    bool hassubfunction = false;
    System::Byte * data = NULL;
    System::Byte datacount = 0;
	int identifier = (int)DefaultCommandsEnum::DefaultResponse;

    switch(function)
    {
        case SMP3_CMODE :
            hassubfunction = true;
            subfunction = rawcommand[2];
            break;
        case SMP3_READ_BUFFER :
            hassubfunction = true;
            subfunction = rawcommand[2];
            break;
        case SMP3_GET_REGISTERS :
            hassubfunction = false;
            datacount = rawcommandlength - 4;
            break;
        case SMP3_SET_REGISTERS :
            hassubfunction = false;
            datacount = rawcommandlength - 4;
            break;
        case SMP3_WRITE_EEPROM :
            hassubfunction = false;
            break;
        case SMP3_SET_SPEED :
            hassubfunction = true;
            subfunction = rawcommand[2];
            break;
        case SMP3_SEND_ID :
            hassubfunction = false;
            break;
			// datatransfer
		case SCANCONTROLLER_FUNCTION:
			hassubfunction = false;
			identifier = (int)ControllerCommandsEnum::ScanController;
			break;
		case REGISTERCONTROLLER_FUNCTION:
			hassubfunction = false;
			datacount = 1;
			identifier = (int)ControllerCommandsEnum::RegisterController;
			break;
		case DEVICEWASHALTED_FUNCTION:
			hassubfunction = false;
			identifier = (int)ControllerCommandsEnum::DeviceWasHalted;
			break;
		case REQUESTDATA_FUNCTION:
			if(rawcommandlength == 14)
			{
				hassubfunction = false;
				datacount = 10;
				identifier = (int)DataTransferCommandEnum::RequestData;
			}
			else if(rawcommandlength == 6)
			{
				hassubfunction = false;
				datacount = 2;
				identifier = rawcommand[2] == 0 && rawcommand[3] == 0 ? (int)DataTransferCommandEnum::NoMedium : (int)DataTransferCommandEnum::BeginDataSend;
			}
			break;
		case SENDDATAPACKET_FUNCTION:
			hassubfunction = false;
			datacount = rawcommandlength - 4;
			identifier = (int)DataTransferCommandEnum::SendDataPacket;
			break;
		case DATATRANSACTIONCOMPLITED_FUNCTION:
			if(rawcommandlength == 4)
			{
				hassubfunction = false;
				identifier = (int)DataTransferCommandEnum::DataTransactionComplited;
			}
			else if(rawcommandlength == 5)
			{
				subfunction = rawcommand[2];
				identifier = (int)DataTransferCommandEnum::EndDataSend;
			}
			break;
		case BEGINREMOTECONTROL_FUNCTION:
			hassubfunction = false;			
			identifier = (int)RemoteAccessCommandsEnum::BeginRemoteControl;
			break;
		case SENDDEVICEFILES_FUNCTION:
			hassubfunction = false;
			datacount = 4;
			identifier = (int)RemoteAccessCommandsEnum::SendDeviceFiles;
			break;
		case REQUESTDEVICEFILE_FUNCTION:
			hassubfunction = false;
			datacount = 4;
			identifier = (int)RemoteAccessCommandsEnum::RequestDeviceFile;
			break;
		case SAVEDEVICEFILES_FUNCTION:
			hassubfunction = false;
			datacount = 4;
			identifier = (int)RemoteAccessCommandsEnum::SaveDeviceFiles;
			break;
		case DEVICEFILEUPLOAD_FUNCTION:
			hassubfunction = false;
			datacount = 4;
			identifier = (int)RemoteAccessCommandsEnum::DeviceFileUpload;
			break;
		case COMPLITEREMOTECONTROL_FUNCTION:
			hassubfunction = false;
			identifier = (int)RemoteAccessCommandsEnum::CompliteRemoteControl;
			break;
	}

    if(datacount > 0)
    {
        data = new System::Byte[datacount];
        memcpy(data, rawcommand + rawcommandlength - datacount - 2, datacount);
    }

	if(identifier == (int)DefaultCommandsEnum::DefaultResponse)
	{
		return gcnew CommandEntity(address, function, subfunction, hassubfunction, data, datacount, 0);
	}
	else
	{
		return gcnew CommandEntity(address, function, subfunction, hassubfunction, data, datacount, 0, identifier);
	}
}
