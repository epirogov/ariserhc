#include "StdAfx.h"
#include "CustomFunctionResponse.h"

using namespace LibmodbusCustomCommands;

CustomFunctionResponse::CustomFunctionResponse(System::Byte address, System::Byte function, System::Byte subfunction, System::Byte * data, System::Byte datalength)
{
    this->address = address;
    this->function = function;
    this->subfunction = subfunction;
    this->data = data;
    this->datalength = datalength;
}

CustomFunctionResponse::~CustomFunctionResponse()
{
    delete [] this->data;
}
