#pragma once

namespace LibmodbusCustomCommands
{
	public enum class ReadBufferEnum { channel0 = 0x00, channel1 = 0x01 };
}