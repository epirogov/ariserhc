#include "StdAfx.h"
#include <iostream>
#include "DataTransferMaster.h"
#include "CommandFactory.h"
#include "CommandParser.h"
#include "CommandEntity.h"
#include "DefaultCommandsEnum.h"

using namespace std;
using namespace LibmodbusCustomCommands;
using namespace System;
using namespace System::IO::Ports;

DataTransferMaster::DataTransferMaster(DeviceTypeEnum deviceType, SerialPort ^ serialPort, int listenPeriod)
{	
	responseFactoy = gcnew CommandResponseFactory(listenPeriod);
	this->listen = false;
	List<CustomFunctionMapping ^> ^ mapping = gcnew List<CustomFunctionMapping ^>();
    mapping->Add(gcnew CustomFunctionMapping(SCANCONTROLLER_FUNCTION, 0, SCANCONTROLLER_FUNCTION_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(REGISTERCONTROLLER_FUNCTION, 0, REGISTERCONTROLLER_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DEVICEWASHALTED_FUNCTION, 0, DEVICEWASHALTED_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(REQUESTDATA_FUNCTION, 0, REQUESTDATA_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(SENDDATAPACKET_FUNCTION, 0, SENDDATAPACKET_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DATAPACKETRECEIVED_FUNCTION, 0, DATAPACKETRECEIVED_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DATATRANSACTIONCOMPLITED_FUNCTION, 0, DATATRANSACTIONCOMPLITED_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(BEGINREMOTECONTROL_FUNCTION, 0, BEGINREMOTECONTROL_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(SENDDEVICEFILES_FUNCTION, 0, SENDDEVICEFILES_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(REQUESTDEVICEFILE_FUNCTION, 0, REQUESTDEVICEFILE_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(SAVEDEVICEFILES_FUNCTION, 0, SAVEDEVICEFILES_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DEVICEFILEUPLOAD_FUNCTION, 0, DEVICEFILEUPLOAD_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(COMPLITEREMOTECONTROL_FUNCTION, 0, COMPLITEREMOTECONTROL_FUNCTION_REQUEST_DATA_LENGTH));
	this->provider = gcnew ModbusProvider(deviceType, serialPort, 19200, Parity::None, 8, StopBits::One, mapping);
	this->provider->CreateTables();
	commandFactory= gcnew CommandFactory();
}

DataTransferMaster::DataTransferMaster(SerialPort ^ serialPort, int listenPeriod)
{	
	responseFactoy = gcnew CommandResponseFactory(listenPeriod);
	this->listen = false;
	List<CustomFunctionMapping ^> ^ mapping = gcnew List<CustomFunctionMapping ^>();
    mapping->Add(gcnew CustomFunctionMapping(SCANCONTROLLER_FUNCTION, 0, SCANCONTROLLER_FUNCTION_REQUEST_DATA_LENGTH));
    mapping->Add(gcnew CustomFunctionMapping(REGISTERCONTROLLER_FUNCTION, 0, REGISTERCONTROLLER_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DEVICEWASHALTED_FUNCTION, 0, DEVICEWASHALTED_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(REQUESTDATA_FUNCTION, 0, REQUESTDATA_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(SENDDATAPACKET_FUNCTION, 0, SENDDATAPACKET_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DATAPACKETRECEIVED_FUNCTION, 0, DATAPACKETRECEIVED_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DATATRANSACTIONCOMPLITED_FUNCTION, 0, DATATRANSACTIONCOMPLITED_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(BEGINREMOTECONTROL_FUNCTION, 0, BEGINREMOTECONTROL_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(SENDDEVICEFILES_FUNCTION, 0, SENDDEVICEFILES_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(REQUESTDEVICEFILE_FUNCTION, 0, REQUESTDEVICEFILE_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(SAVEDEVICEFILES_FUNCTION, 0, SAVEDEVICEFILES_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(DEVICEFILEUPLOAD_FUNCTION, 0, DEVICEFILEUPLOAD_FUNCTION_REQUEST_DATA_LENGTH));
	mapping->Add(gcnew CustomFunctionMapping(COMPLITEREMOTECONTROL_FUNCTION, 0, COMPLITEREMOTECONTROL_FUNCTION_REQUEST_DATA_LENGTH));
	this->provider = gcnew ModbusProvider(DeviceTypeEnum::RS485, serialPort, 19200, Parity::None, 8, StopBits::One, mapping);
	this->provider->CreateTables();
	commandFactory = gcnew CommandFactory();
}

DataTransferMaster::~DataTransferMaster()
{
	delete this->provider;
}

void DataTransferMaster::Open()
{
	this->provider->Initialize();	
    this->listen = true;
}

void DataTransferMaster::Close()
{
	if(this->listen)
    {
        this->listen = false;
		this->thread->Join();        
        this->provider->Close();
    }
}

void DataTransferMaster::SendResponse(CommandEntity ^ responseCommand)
{
	try
	{
		System::Byte rawresponse[256];
		int rawresponselength = 0;
		responseCommand->Build(rawresponse, rawresponselength);
		this->provider->modbus_send(rawresponse, rawresponselength);
	}
	catch(Exception ^ ex)
	{
	}
}


int DataTransferMaster::RetriveResponse(CommandEntity ^ entity)
{
	CommandEntity ^ response = gcnew CommandEntity();
	try
	{
		response = this->SendWithResponse(entity, this->provider, false);	
		DataSent(this, gcnew TypedEventArgs<CommandEntity ^>(entity));
		CompliteTemplate(this, gcnew TypedEventArgs<CommandEntity ^>(response));	
		return response->GetFunction();
		//throw gcnew Exception(response->GetFunction().ToString());
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::RetriveLongResponse(CommandEntity ^ entity)
{	
	try
	{
		System::Byte query[256];
		int querylength = 0;
		entity->Build(query, querylength);		
		//provider->receive_msg(responselength, response, realResponseLength, true);		
		provider->modbus_send(query, querylength);	    
		DataSent(this, gcnew TypedEventArgs<CommandEntity ^>(entity));
		return entity->GetFunction();
	}
	catch(Exception ^ ex)
	{
	}
    return 0;
}

int DataTransferMaster::ScanController(unsigned char address)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateScanController(address);	
		return RetriveResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::ScanControllerRequest(unsigned char address)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateScanController(address);	
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}
    return 0;
}

int DataTransferMaster::RegisterController(unsigned char address, unsigned char deviceType)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateRegisterController(address,deviceType);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::DeviceWasHalted(unsigned char address)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateDeviceWasHalted(address);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::RequestData(unsigned char address, DateTime ^ startDate, TimeSpan ^ duration)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateRequestData(address, startDate, duration);	
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::SendDataPacket(unsigned char address, int offset, unsigned char count, unsigned char * data, int datalength)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateSendDataPacket(address, offset, count, data, datalength);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}
    return 0;
}

int DataTransferMaster::DataTransactionComplited(unsigned char address)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateDataTransactionComplited(address);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::BeginRemoteControl(unsigned char address)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateBeginRemoteControl(address);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::SendDeviceFiles(unsigned char address, int fileMask)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateSendDeviceFiles(address, fileMask);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}

    return 0;
}

int DataTransferMaster::RequestDeviceFile(unsigned char address, int fileCode)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateRequestDeviceFile(address, fileCode);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}
    return 0;
}

int DataTransferMaster::SaveDeviceFiles(unsigned char address, int fileMask)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateSaveDeviceFiles(address, fileMask);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}
    return 0;
}

int DataTransferMaster::DeviceFileUpload(unsigned char address, int fileCode)
{
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateDeviceFileUpload(address, fileCode);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}
    return 0;
}

int DataTransferMaster::CompliteRemoteControl(unsigned char address)
{
	CommandEntity ^ entity =  gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateCompliteRemoteControl(address);
		return RetriveLongResponse(entity);
	}
	catch(Exception ^ ex)
	{
	}
    return 0;
}

int DataTransferMaster::DataPackedReseived(unsigned char address)
{	
	CommandEntity ^ entity = gcnew CommandEntity();
	try
	{
		entity =  commandFactory->CreateDataPacketReceived(address, NULL, 0);
		return RetriveLongResponse(entity);
	}
	catch (Exception ^ ex)
	{
	}

    return 0;
}