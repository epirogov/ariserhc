#pragma once
#include "FileReaderBase.h"

namespace LibmodbusCustomCommands
{
	public ref class FileReader : public FileReaderBase
	{
	    private :
	    char * body;
		delegate bool ConditionMethod(char * buffer);

	    public :
	    FileReader();
	    FileReader(const char * filename);

	    char * GetContent();
	    void ReadToEnd();
	    void ReadFirstLine();

	    private :
	    void ReadStaticSize();
	    void ReadLineStaticSize();
	    void ReadBuffered();
	    void ReadLineBuffered();
		void ReadBufferedCondition(ConditionMethod ^ condition);
	    static bool EofCondition(char * buffer);
	    static bool EolCondition(char * buffer);  
		protected :
		virtual void SetNoData() override;
	};
}