#pragma once
#include <stdio.h>
#include "CommandEntity.h"
#include "CommandFactory.h"

using namespace System;

namespace LibmodbusCustomCommands
{
	public ref class CommandResponseFactory
	{
	    private :	    	    
	    System::Byte * buffer;				
		int dataDuration;
		TimeSpan ^ dataDurationSpan;
		CommandFactory ^ commandFactory;
		public :
		delegate void NoDataEventHandler (Object ^ sender, EventArgs ^ e);
		event NoDataEventHandler ^ NoData;

		public :
	    CommandResponseFactory(const char * filename);
		CommandResponseFactory(int dataDuration);
	    ~CommandResponseFactory();

	    void Open();
	    void Close();

	    CommandEntity ^  Build(CommandEntity ^ request);
	};
}