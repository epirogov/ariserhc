#pragma once

namespace LibmodbusCustomCommands
{
	public struct timespec
	{
	public:
		timespec();
		timespec(unsigned long long tv_sec, unsigned long long tv_nsec);
		unsigned long long tv_sec;
		unsigned long long tv_nsec;
	};
}