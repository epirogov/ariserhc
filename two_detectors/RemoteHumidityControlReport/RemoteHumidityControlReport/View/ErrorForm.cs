﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RemoteHumidityControlReport.View
{
    public partial class ErrorForm : Form
    {
        public ErrorForm(Exception ex)
        {
            InitializeComponent();

            this.richTextBox1.Text = ex.Message;
            this.richTextBox2.Text = ex.StackTrace;
        }
    }
}
