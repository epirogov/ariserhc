﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RemoteHumidityControlReport.View
{
    public partial class ReportChoiceForm : Form
    {
        private int first;
        private int second;

        public ReportChoiceForm(int first, int second)
        {
            InitializeComponent();
            this.first = first;
            this.second = second;
            this.radioButton1.Text = first.ToString();
            this.radioButton2.Text = second.ToString();
        }

        public bool IsLeft
        {
            get
            {
                return this.radioButton1.Checked;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
