﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.StateManagement.Content;
using RemoteHumidityControl.Entity;

namespace RemoteHumidityControl.ViewInterface
{
    public interface IMainView : IContentAction
    {
        void PrintData(int controllerId, IEnumerable<ControllerGeneralDataEntity> data);        
    }
}
