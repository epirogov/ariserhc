﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.Entity;

namespace RemoteHumidityControl.ViewInterface
{
    public interface IOneChartDeviceView
    {
        void PrintData(int controllerId, IEnumerable<ControllerGeneralDataEntity> data);
    }
}
