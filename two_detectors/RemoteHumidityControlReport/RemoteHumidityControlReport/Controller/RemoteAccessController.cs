﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 20:36
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;

using STD4408Device.Logic;
using System.Windows.Forms;
using RemoteHumidityControl.ViewInterface;
using RemoteHumidityControl.Logic;

namespace RemoteHumidityControl.Controller
{
    public class RemoteAccessController
    {        
        const int DISPLAY_WIDTH = 20;
		const int DISPLAY_HEIGHT = 16;
		
		private STD4408KeyEnum oneKey = STD4408KeyEnum.None;
		private List<STD4408KeyEnum> keys = new List<STD4408KeyEnum>();
		
		private STD4408MainFormStateController formStateController;
		private STD4408Adapter keyboardAdapter;
		private bool prepareToSend;
		string displayText;
		
		public event EventHandler<EventArgs> ButtonPerformed;
        public RemoteAccessController(IRemoteAccessView view)
		{			
			this.keyboardAdapter = new STD4408Adapter();
			this.View = view;
			this.formStateController = new STD4408MainFormStateController();
			this.formStateController.NewCOMPortsStateEvent += new EventHandler<EventArgs<STD4408StateEnum>>(MainFormController_NewCOMPortsStateEvent);
			this.formStateController.NewKeyScenarioStateEvent += new EventHandler<EventArgs<KeyScenarioStateEnum>>(MainFormController_NewKeyScenarioStateEvent);
			this.formStateController.Initialize(STD4408StateEnum.DisconnectedCOMPorts, KeyScenarioStateEnum.OneKey);						
			
			this.keyboardAdapter.KeyboardScaning += new EventHandler<EventArgs<KeyboardScan>>(MainFormController_KeyboardScaning);
			this.keyboardAdapter.PrintXY += new EventHandler<EventArgs<PrintedText>>(MainFormController_PrintXY);
			this.keyboardAdapter.SetCursor += new EventHandler<EventArgs<CursorPosition>>(MainFormController_SetCursor);
			this.keyboardAdapter.ClrScr += new EventHandler<EventArgs<ClrScr>>(MainFormController_ClrScr);
		}
		
		public void OnLoad()
		{
			this.ClrScr();
		}

		void MainFormController_SetCursor(object sender, EventArgs<CursorPosition> e)
		{
			int selectedStart = (DISPLAY_WIDTH + 1) * e.Value1.PosY +  e.Value1.PosX;
			this.View.EnableCursor(selectedStart);
		}

		void MainFormController_PrintXY(object sender, EventArgs<PrintedText> e)
		{			
			this.PasteText(e.Value1);
			this.View.DisplayText(this.displayText);
		}
		
		void MainFormController_ClrScr(object sender, EventArgs<ClrScr> e)
		{
			this.ClrScr();			
		}		
		
		void ClrScr()
		{
			this.InitializeDisplay();
			this.View.DisplayText(this.displayText);
		}
		
		void InitializeDisplay()
		{
			StringBuilder displayTextBuilder = new StringBuilder();
			
			for(int i = 0; i < DISPLAY_HEIGHT; i++)
			{
				for(int j = 0; j < DISPLAY_WIDTH; j++)
				{
					displayTextBuilder.Append(" ");
				}
				if(i < DISPLAY_HEIGHT - 1)
				{
					displayTextBuilder.Append(Environment.NewLine);
				}
			}
			this.displayText = displayTextBuilder.ToString();
		}
		
		
		//int maxLength = 20;
		void PasteText(PrintedText printedText)
		{	
			/*
			if(maxLength < printedText.Text.Length + printedText.PosX)
			{
				maxLength = printedText.Text.Length + printedText.PosX;
				MessageBox.Show((printedText.Text.Length + printedText.PosX).ToString());				
			}
			*/
			
			StringBuilder dispalayTextBuilder = new StringBuilder();
			int startPosition = printedText.PosX + printedText.PosY * (DISPLAY_WIDTH + Environment.NewLine.Length);
			dispalayTextBuilder.Append(this.displayText.Substring(0, startPosition));
			dispalayTextBuilder.Append(printedText.Text);
			dispalayTextBuilder.Append(this.displayText.Substring(startPosition + printedText.Text.Length, this.displayText.Length - (startPosition + printedText.Text.Length)));
			/*
			if(this.displayText.Length != dispalayTextBuilder.ToString().Length)
			{
				throw new Exception();
			}
			*/
			
			this.displayText = dispalayTextBuilder.ToString();
			
		}

		void MainFormController_KeyboardScaning(object sender, EventArgs<KeyboardScan> e)
		{
			bool performed = false;
			if(this.formStateController.KeyScenarioState == KeyScenarioStateEnum.OneKey)
			{
				this.keyboardAdapter.SendKey(this.oneKey);
				if(this.oneKey != STD4408KeyEnum.None)
				{
					oneKey = STD4408KeyEnum.None;
					performed = true;
				}
			}
			else
			{
				if(keys.Count == 3)
				{
					this.keyboardAdapter.SendThreeKeys(keys[0], keys[1], keys[2]);
					keys.Clear();
					performed = true;
				}
				else
				{
					prepareToSend = true;
				}
			}
			
			if(performed && this.ButtonPerformed != null)
			{
				this.ButtonPerformed(this, new EventArgs());
			}			
		}

		
		
		void MainFormController_NewKeyScenarioStateEvent(object sender, EventArgs<KeyScenarioStateEnum> e)
		{
			keys.Clear();
		}
		
		public IRemoteAccessView View
		{
			get;
			set;
		}

		void MainFormController_NewCOMPortsStateEvent(object sender, EventArgs<STD4408StateEnum> e)
		{
			switch(e.Value1)
			{
				case STD4408StateEnum.ConnectedCOMPorts:
                    this.keyboardAdapter.ConnectRemoteControl();
                    this.View.ConnectRemoteControl();
					break;
				case STD4408StateEnum.DisconnectedCOMPorts:
                    this.keyboardAdapter.DisconnectRemoteControl();
                    this.View.DisconnectRemoteControl();
					break;
				default:
					break;
			}
		}
		
		public void PerformSTD4408Key(STD4408KeyEnum key)
		{
			if(this.formStateController.COMPortsState == STD4408StateEnum.ConnectedCOMPorts)
			{
				if (this.formStateController.KeyScenarioState == KeyScenarioStateEnum.OneKey)
				{
					this.oneKey = key;
				}
				else
				{
					if(keys.Count == 3)
					{						
						if(this.prepareToSend)
						{
							this.keyboardAdapter.SendThreeKeys(keys[0], keys[1], keys[2]);
							this.prepareToSend = false;
						}
						keys.Clear();
					}
					keys.Add(key);					
				}
			}
		}
		
		public void PerformSwitchRemoteControl()
		{			
			this.formStateController.SetNextSate();
		}
		
		public void SetOneKeyScenario()
		{
			this.formStateController.SetOneKeyScenario();
		}
		
		public void SetThreeKeyScenario()
		{
			this.formStateController.SetThreeKeyScenario();
		}
    }
}
