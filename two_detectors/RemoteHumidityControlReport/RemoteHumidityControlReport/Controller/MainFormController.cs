﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.StateManagement.Content;
using RemoteHumidityControl.ViewInterface;
using RemoteHumidityControl.Logic.DeviceControl;

using EventArgs2 = RemoteHumidityControl.Utility.EventArgs<System.Collections.Generic.IEnumerable<RemoteHumidityControl.Entity.ControllerGeneralDataEntity>>;

namespace RemoteHumidityControl.Controller
{
    public class MainFormController : IContentAction, IController
    {
        private readonly HumidityWatcher deviceWatcher = new HumidityWatcher(new TimeSpan(0, 0, 5));

        public MainFormController(IMainView view)
        {
            this.View = view;
            this.ContentStateManager = new ContentStateManager();
            this.ContentStateManager.OpenContent += new EventHandler<EventArgs2>(ContentStateManager_OpenContent);
            deviceWatcher.DataReceived += new EventHandler<STD4408Device.Logic.EventArgs<IEnumerable<Entity.ControllerGeneralDataEntity>, int>>(deviceWatcher_OnDataReceived);            
        }

        public void deviceWatcher_OnDataReceived(object sender, STD4408Device.Logic.EventArgs<IEnumerable<Entity.ControllerGeneralDataEntity>, int> e)
        {
            this.View.PrintData(e.Value2, e.Value1);
        }

        public IMainView View
        {
            get;
            private set;
        }

        void ContentStateManager_OpenContent(object sender, RemoteHumidityControl.Utility.EventArgs<ContentStateEnum> e)
        {
            switch(e.Value1)
            {
                case ContentStateEnum.Archive:
                    this.View.OpenArchive();
                    break;
                case ContentStateEnum.Charts:
                    this.View.OpenCharts();
                    break;
                case ContentStateEnum.RemoteControl:
                    this.View.OpenRemoteControl();
                    break;
                case ContentStateEnum.Report:
                    this.View.OpenReport();
                    break;
            }
        }

        public ContentStateManager ContentStateManager
        {
            get;
            private set;
        }

        public void OpenArchive()
        {
            this.ContentStateManager.OpenArchive();
        }

        public void OpenCharts()
        {
            deviceWatcher.RegisterController(1);
            deviceWatcher.RegisterController(2);

            deviceWatcher.Start();

            this.ContentStateManager.OpenCharts();
        }

        public void OpenRemoteControl()
        {
            this.ContentStateManager.OpenRemoteControl();
        }

        public void OpenReport()
        {
            this.ContentStateManager.OpenReport();
        }



        public void OnLoad()
        {
            
        }

        public void OnHide()
        {
            
        }

        public void OnShow()
        {
            
        }

        public void OnClose()
        {
            
        }
    }
}
