﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 19.04.2010
 * Time: 17:38
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Timers;
using LibmodbusCustomCommands;
using EventArgs1 = RemoteHumidityControl.Utility.EventArgs<LibmodbusCustomCommands.CommandEntity>;
using EventArgs2 = RemoteHumidityControl.Utility.EventArgs<byte>;
using Timer = System.Timers.Timer;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO.Ports;
using RemoteHumidityControlReport.Logic.Exceptions;
using RemoteHumidityControlReport.Logic.DeviceControl;
using System.Text;
using LibraryBusinessLogic;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace RemoteHumidityControl.DataAccess
{
	/// <summary>
	/// Description of ControllerConnector.
	/// </summary>
	public class ControllerConnector
	{
		const int ScanController = 0x1;
		const int RegisterController = 0x2;
		const int DeviceWasHalted = 0x4;
		const int RequiestData = 0x8;
		const int DataPacketReceived = 0x10;
		const int DataPacketBrocken = 0x20;
		const int DataTransactionComplited = 0x40;
		const int BeginDataSend = 0x80;
		const int SendDataPacket = 0x100;
		const int EndDataSend = 0x200;
		const int NoMedium = 0x400;

		private Timer timer = new Timer();		
		private DataTransferMaster master;
		volatile bool isAsynchronousResponse;
		volatile byte controllerId;
		
		private readonly object WaitScanning = new object();
		private readonly object WaitResponse = new object();        
		private int period = 30;
		private List<int> requiredDevices = new List<int>();
		//private int controllerIndex;

		public event EventHandler<EventArgs1> DataSent;
		public event EventHandler<EventArgs1> DataReceived;
		public event EventHandler<EventArgs2> ControllerFounded;
		public event EventHandler<EventArgs2> ControllerUnsubscribe;
		public event EventHandler<EventArgs> ControllerRequest;
		public readonly CommandFactory commandFactory = new CommandFactory();
		

		public SerialPort serialPort
		{
			get;
			set;
		}

		public ControllerConnector(SerialPort serialPort, List<SPM3DeviceInfo> Controllers, int period)
		{
			File.WriteAllText(@"C:\rhc.log", "Data" + DateTime.Now.ToLongTimeString() + Environment.NewLine);
			this.period = period;
			this.serialPort = serialPort;
			this.serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
			master = new DataTransferMaster(serialPort, 30 * 1000);
			//master.Open();                
			ControllerIDs = Controllers;

			Thread thread = new Thread(new ThreadStart(ReadPack));
			thread.IsBackground = true;
			thread.Start();
			master.DataSent += new CompliteTemplateEventHandler(master_DataSent);
			master.CompliteTemplate += new CompliteTemplateEventHandler(master_CompliteTemplate);

			string addresses = ConfigurationManager.AppSettings["RequredDeviceAddresses"];			
			int address1 = 0;
			if (addresses.Split(';').All(address => int.TryParse(address, out address1)))
			{
				requiredDevices = addresses.Split(';').Select(address => int.Parse(address)).ToList();
			}
		}

		
        private readonly object syncDataRead = new object();

		void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
            if (Monitor.TryEnter(syncDataRead))
            {
                //File.AppendAllText(@"C:\rhc.log", "Data" + DateTime.Now.ToLongTimeString() + Environment.NewLine);
                //Thread thread = new Thread(new ThreadStart(delegate()
                //    {
                try
                {
                    if (isAsynchronousResponse)
                    {
                        byte[] bufferUnion = new byte[0];
                        bool registerController = false;

                        int length = 0;
                        int length1 = 0;

                        ReleaseData();
                        try
                        {
                            byte[] buffer = new byte[256];
                            byte[] buffer1 = new byte[256];

                            Thread.Sleep(500);
                            length = this.serialPort.Read(buffer, 0, 256);
                            buffer = buffer.Take(length).ToArray();
                            registerController = (requiredDevices.Count > 0 ||
                            ControllerIDs.Any(controller => controller.State == true)) &&
                            buffer
                            .Select((value, i) => new { Value = value, Index = i })
                            .Any(data =>
                            (requiredDevices.Contains(data.Value) ||
                            ControllerIDs.Any(controller => controller.Address == data.Value)) &&
                            buffer.Count() > data.Index + 2 &&
                            buffer[data.Index + 1] == 0x51);
                            if (!registerController && length < 7)
                            {
                                Thread.Sleep(100);
                                if (this.serialPort.BytesToRead == 0)
                                {
                                    Monitor.Exit(syncDataRead);
                                    return;
                                }
                                length1 = this.serialPort.Read(buffer1, 0, 256);
                                buffer1 = buffer1.Take(length1).ToArray();
                                bufferUnion = buffer.Union(buffer1).ToArray();

                                registerController = (requiredDevices.Count > 0 ||
                                ControllerIDs.Any(controller => controller.State == true)) &&
                                bufferUnion
                                .Select((value, i) => new { Value = value, Index = i })
                                .Any(data =>
                                (requiredDevices.Contains(data.Value) ||
                                ControllerIDs.Any(controller => controller.Address == data.Value)) &&
                                bufferUnion.Count() > data.Index + 2 &&
                                bufferUnion[data.Index + 1] == 0x51);
                            }
                            else
                            {
                                bufferUnion = buffer;
                            }
                        }
                        catch (TimeoutException)
                        {
                        }
                        catch (InvalidOperationException ex)
                        {
                            ErrorNotifier.SetError(ex);
                        }

                        if (registerController)
                        {
                            int registeredController = bufferUnion
                                .Select((v, i) => new { V = v, I = i })
                                .Where(value => value.I > 0 &&
                                    (requiredDevices.Contains(value.V) ||
                                    ControllerIDs.Any(controller => controller.Address == value.V)) &&
                                    bufferUnion.Count() > value.I + 2 &&
                                    bufferUnion[value.I + 1] == 0x51)
                                .Select(value => value.V)
                                .FirstOrDefault();

                            if (registeredController > 0)
                            {
                                if (requiredDevices.Contains(registeredController) ||
                            	    ControllerIDs.Any(controller => controller.State.GetValueOrDefault()))
                                {
                                    if (requiredDevices.Contains(registeredController))
                                    {
                                        requiredDevices.Remove(registeredController);
                                    }
                                    

                                    if (ControllerFounded != null)
                                    {
                                        ControllerFounded(this, new EventArgs2((byte)registeredController));
                                    }
                                    if (ControllerIDs.Any(controller => controller.Address == registeredController))
                                    {
                                        ControllerIDs.First(controller => controller.Address == registeredController).State = null;
                                    }                                    
                                }
                            }
                            Monitor.Exit(syncDataRead);
                            return;
                        }

                        int index = bufferUnion
                                .Select((v, i) => new { V = v, I = i })
                                .Where(value => value.I > 0 &&                                    
                                    ControllerIDs.Any(c => c.Address == bufferUnion[value.I - 1]) &&
                                    value.V == 0x54)
                                .Min(value => value.I);

                        if (index == 0 || !ControllerIDs.Any(c => c.Address == bufferUnion[index - 1]) ||
                            bufferUnion.Count() < 5 + index)
                        {
                            Monitor.Exit(syncDataRead);
                            return;
                        }

                        byte address = (byte)bufferUnion[index - 1];
                        
                        var result = bufferUnion.Skip(index + 1).Take(4);
                        //bufferUnion
                        //    .Select((v, i) => new { V = v, I = i})
                        //    .Where(value => value.V == 0x54)
                        //    .Min(value => value.I) + 1);

                        ErrorNotifier.SetError(
                                        new ObviousException(
                                            string.Format(
                                            "Data Packet Received adr [{0}] {1} ",
                                            address,
                                            string.Join(",", result.Select(value => value.ToString())))));
                        if (DataReceived != null)
                        {
                            unsafe
                            {
                                fixed (byte* entityData = result.ToArray())
                                {
                                    DataReceived(this,
                                        new EventArgs1(
                                            commandFactory
                                            .CreateDataPacketReceived(
                                            address,
                                            entityData,
                                            4)));
                                }
                            }
                        }

                    }
                }
                catch (Exception)
                {
                }
                //}));
                //thread.IsBackground = true;
                //thread.Start();            
                //if (thread.IsAlive && !thread.Join(10000))
                //{
                //    this.serialPort.DiscardInBuffer();
                //    //thread.Interrupt();                
                //}

                Monitor.Exit(syncDataRead);
            }

			
		}

		void master_DataSent(object sender, TypedEventArgs<CommandEntity> e)
		{
			if (DataSent != null)
			{
				DataSent(this, new EventArgs1(e.Value1));
			}
		}

		public List<SPM3DeviceInfo> ControllerIDs 
		{
			get;
			set;
		}

		private void timer_Elapsed()
		{
			//while (true)
			//{
			//    Thread.Sleep(1200);
			//    if (this.serialPort.IsOpen && this.ControllerIDs.Count > 0)
			//    {
			//        ReadPack((byte)this.ControllerIDs[0].Address);
			//    }
			//}
		}
		
		public void SetInterval(long interval)
		{
			//timer.Stop();			
			//timer.Interval =  interval;
			//timer.Start();
		}
		
		public void Star()
		{
			if(timer.Interval != 0)
			{
				timer.Start();
			}
		}
		
		public void Stop()
		{
            this.master.Close();
			//timer.Stop();
		}
		
		public bool StartScaning()
		{
			//bool result = false;

			isAsynchronousResponse = false;
			for (byte i = 1; i < 6; i++)
			{
				if (ControllerRequest != null)
				{
					ControllerRequest(this, new EventArgs());
				}
				try
				{
					master.ScanController(i);
					WaitScaning();
					//if (WaitScaning() && requiredDevices.Contains(i))
					//{
					//   ControllerIDs.Add(new SPM3DeviceInfo() { Address = i, State = true });
					//};
				}
				catch (TimeoutException)
				{
					try
					{
						master.ScanController(i);
						WaitScaning();
						//if (!WaitScaning() && requiredDevices.Contains(i))
						//{
						//   ControllerIDs.Add(new SPM3DeviceInfo() { Address = i, State = true });
						//}                        
					}  
					catch(Exception)
					{
					}
				}
				catch (InvalidOperationException ex)
				{
					ErrorNotifier.SetError(ex);
				}                
			}
			isAsynchronousResponse = true;

			return true;
		}     
		

		private bool WaitData(out TimeSpan waitTime)
		{
			TimeSpan periodTime = new TimeSpan(0, 0, period);
			DateTime time = DateTime.Now;
			Monitor.Enter(WaitResponse);
			bool result = Monitor.Wait(WaitResponse, periodTime);
			Monitor.Exit(WaitResponse);
			waitTime = DateTime.Now - time;
			return waitTime < periodTime;
		}

		private void ReleaseData()
		{
			Monitor.Enter(WaitResponse);
			Monitor.PulseAll(WaitResponse);
			Monitor.Exit(WaitResponse);
		}

		private bool WaitScaning()
		{
			Monitor.Enter(WaitScanning);
			bool result = Monitor.Wait(WaitScanning, 4 * 1000);
			Monitor.Exit(WaitScanning);
			return result;
		}

		private void ReleaseScaning()
		{
			Monitor.Enter(WaitScanning);
			Monitor.PulseAll(WaitScanning);
			Monitor.Exit(WaitScanning);
		}

		private void TimedOperation(Action action)
		{
			bool isTimeout = false;
			do
			{
				try
				{
					action();
					isTimeout = false;
				}
				catch (TimeoutException)
				{
					isTimeout = true;
				}
			}
			while (isTimeout);
		}

		public void ReadPack()
		{
			isAsynchronousResponse = true;

			while (true)
			{

				if (ControllerIDs.Count <= controllerId && requiredDevices.Count <= controllerId)
				{
					controllerId = 0;
				}
				
				if (!isAsynchronousResponse || requiredDevices.Count == 0 && ControllerIDs.Count == 0)
				{
					Thread.Sleep(5000);
					continue;
				}
				
				//ReleaseScaning();

				//Action requiestData = () => master.RequestData(controllerId, 0, 50 * 1000);
				//TimedOperation(requiestData);            

				//WaitScaning();
				
				unsafe
				{
					TimeSpan waitTime = new TimeSpan();
					Action sendDataPacket = () =>
					{
						fixed (byte* data = new byte[0])
						{
							try
							{
								bool isRequest = false;
								if (ControllerIDs.Count > controllerId && ControllerIDs[controllerId].State != true)
								{
									master.SendDataPacket((byte)ControllerIDs[controllerId].Address, 0, 0, data, 0);
									if (!WaitData(out waitTime))
									{
										ControllerIDs[controllerId].State = ControllerIDs[controllerId].State.HasValue;
										//MessageBox.Show("Failed " + ControllerIDs[controllerId].State.ToString());
										if (ControllerIDs[controllerId].State == true)
										{
											isRequest = true;
										}
									}
									else
									{
										ControllerIDs[controllerId].State = null;                                
									}
								}
								else
								{
									isRequest = true;
								}

								if (isRequest || requiredDevices.Count > controllerId)
								{
									try
									{
										if (ControllerRequest != null)
										{
											ControllerRequest(this, new EventArgs());
										}
										if(ControllerIDs.Count() > controllerId && ControllerIDs[controllerId].State == true)
										{
											master.ScanControllerRequest((byte)ControllerIDs[controllerId].Address);
											ErrorNotifier.SetError(
											new ObviousException(
												string.Format(
												"Request Controller {0} ", (byte)ControllerIDs[controllerId].Address
												)));
                                            Thread.Sleep(1000);
										}
										else
										{
											Thread.Sleep(5000);
											master.ScanControllerRequest((byte)requiredDevices[controllerId]);
											ErrorNotifier.SetError(
											new ObviousException(
												string.Format(
												"Request Controller {0} ", (byte)requiredDevices[controllerId]
												)));
											Thread.Sleep(5000);
										}
										
									}
									catch (TimeoutException)
									{
									}
									catch (InvalidOperationException ex)
									{
										ErrorNotifier.SetError(ex);
									}
								}
							}
							catch (TimeoutException)
							{
							}
							catch (InvalidOperationException ex)
							{
								ErrorNotifier.SetError(ex);
							}
						}
					};

					sendDataPacket();
					Thread.Sleep(new TimeSpan(0, 0, period) - waitTime);
					//while (!transactionComplited)
					//{
					//    master.SendDataPacket(controllerId, 0, 0, data, 0);
					//    WaitScaning();
					//}

				}
				controllerId++;
			}
		}

		void master_CompliteTemplate(object sender, TypedEventArgs<CommandEntity> e)
		{
			StringBuilder b = new StringBuilder();
			//MessageBox.Show(e.Value1.identifier.ToString());
			switch (e.Value1.identifier)
			{
				case  RegisterController:
					ErrorNotifier.SetError(new ObviousException(string.Format("Register Controller {0}", e.Value1.GetAddress())));
					requiredDevices.Remove(e.Value1.GetAddress());
					if (ControllerFounded != null)
					{   
						ControllerFounded(this, new EventArgs2(e.Value1.GetAddress()));                        
					}
					ReleaseScaning();
					if (ControllerIDs.Any(controller => controller.Address == e.Value1.GetAddress()))
					{
                        ControllerIDs.First(controller => controller.Address == e.Value1.GetAddress()).State = null;
					}
					break;
				case DeviceWasHalted:
					ErrorNotifier.SetError(new ObviousException("Device Was Halted"));
					ControllerUnsubscribe(this, new EventArgs2(e.Value1.GetAddress()));
					break;
				case RequiestData:
					ErrorNotifier.SetError(new ObviousException("Requiest Data"));
					ReleaseScaning();
					break;
				case DataPacketReceived:
					ReleaseScaning();
					break;
				case DataTransactionComplited:
					ErrorNotifier.SetError(new ObviousException("Data Transaction Complited"));                    
					ReleaseScaning();
					break;
				case EndDataSend:
					ErrorNotifier.SetError(new ObviousException("End Data Send"));
					break;
				case NoMedium:
					ErrorNotifier.SetError(new ObviousException("No Medium"));					
					ReleaseScaning();
					break;
				default :
					//ErrorNotifier.SetError(
					//    new ObviousException( 
					//        string.Format(
					//        "Identifier{0}, Function {1}, Has SubFunction {2}, SubFunction{3}, Data length {4}",
					//        e.Value1.identifier, 
					//        e.Value1.GetFunction(), 
					//        e.Value1.HasSubfunction(), 
					//        e.Value1.GetSubFunction(), 
					//        e.Value1.datalength)));                    
					ReleaseScaning();
					break;
			}
		}
	}
}
