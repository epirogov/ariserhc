﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.Entity;
using Microsoft.Office.Interop.Excel;

namespace RemoteHumidityControlReport.DataAccess
{
    public static class ReportWriter
    {
        public static void WriteReport(string filename, IEnumerable<ControllerGeneralDataEntity> data)        
        {
            if(data.Count() == 0)
                return;

            _Application oXL;
            _Workbook oWB;
            _Worksheet oSheet;

            oXL = new Application() { Visible = true};
            oWB = (_Workbook)oXL.Workbooks.Add();
            oSheet = (_Worksheet)oWB.ActiveSheet;

            oSheet.Cells[1, 1] = "Отчет влагомер SPM3";
            oSheet.Cells[2, 1] = string.Format("Прибор № {0} ",
                data.First().Address);
            oSheet.Cells[3, 1] = string.Format("Период от {0} до {1} ", 
            data.Min(dataEntity => dataEntity.Date), 
            data.Max(dataEntity => dataEntity.Date));

            oSheet.Cells[4, 1] = "Дата";
            oSheet.Cells[4, 2] = "Влажность";
            oSheet.Cells[4, 3] = "Температура";
            int index = 6;
            data.ToList().ForEach(dataEntity => 
            {
                oSheet.Cells[index, 1] = dataEntity.Date;
                oSheet.Cells[index, 2] = dataEntity.Humidity;
                oSheet.Cells[index++, 3] = dataEntity.Temperature;                
            });

            oXL.SaveWorkspace(filename);
        }
    }
}
