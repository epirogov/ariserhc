﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.Entity;

namespace RemoteHumidityControl.DataAccess
{
    public static class FakeControllerConnector
    {
        public static float AA
        {
            get;
            set;
        }

        public static float BB
        {
            get;
            set;
        }

        private static readonly List<ControllerGeneralDataEntity> storedData = new List<ControllerGeneralDataEntity>();

        public static IEnumerable<ControllerGeneralDataEntity> ReadConlrollerData(int controllerId, TimeSpan period)
        {
            AA = 25;
            BB = 10;
            List<ControllerGeneralDataEntity> fakeData = new List<ControllerGeneralDataEntity>();

            DateTime startDate = DateTime.Now.Subtract(period);
            Random rand = new Random();
            for (int i = 0; i < period.TotalSeconds / 12; i++)
            {
                fakeData.Add(new ControllerGeneralDataEntity() { Humidity = AA + rand.Next((int)BB / 2) - (BB / 2), Temperature = rand.Next(5) + 60, Date = startDate.AddSeconds(12 * i) });
            }

            storedData.AddRange(fakeData.ToArray());
            return fakeData;
        }

        public static IEnumerable<ControllerGeneralDataEntity> GetDataForPeriod(DateTime from, DateTime to)
        {            
            return storedData.Where(hd => hd.Date >= from && hd.Date <= to).OrderBy(hd => hd.Date);
        }
    }
}
