﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 20:50
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of STD4408KeyEnum.
	/// </summary>
	public enum STD4408KeyEnum
	{
		None,
		Shift, Zero_Star, Enter, Equal,
		One_F1, Two_Down, Three_F2, Plus,
		Four_Left, Five_F3, Six_Right, Minus,
		Seven_F4, Eight_Up, Nine_F5, Multiply		
	}
}
