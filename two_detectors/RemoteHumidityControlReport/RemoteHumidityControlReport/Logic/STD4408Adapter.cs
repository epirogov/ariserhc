﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using STD4408Device.Logic;

using ALCHEVSKRemoteControlCore;

namespace RemoteHumidityControl.Logic
{
    public class STD4408Adapter : ReadDataListener
    {
        private readonly object syncRoot = new Object();

        private RemoteControlCore core;

        private volatile bool dataPresent;
        private byte[] readData;

        public event EventHandler<EventArgs<KeyboardScan>> KeyboardScaning;
        public event EventHandler<EventArgs<PrintedText>> PrintXY;
        public event EventHandler<EventArgs<CursorPosition>> SetCursor;
        public event EventHandler<EventArgs<ClrScr>> ClrScr;

        public STD4408Adapter()
        {
            core = new RemoteControlCore(this);            
            core.SendData += new RemoteControlCore.SaveDataEventHandler(core_SendData);
            core.SendSignal += new RemoteControlCore.SendSignalEventHandler(core_SendSignal);
        }

        void core_SendSignal(object sender, TypedEventArgs<WriteData> e)
        {
            byte[] bytes = e.Value1.GetRequestdata();            
            switch (COMSignalAnalyser.Detect(bytes))
            {
                case COMSignalTypeEnum.KeyboardScan:
                    OnKeyboardScan(COMCommandFactory.BuildKeyboardScan(bytes));
                    break;
                case COMSignalTypeEnum.SetCursor:
                    OnSetCursor(COMCommandFactory.BuildSetCursor(bytes));
                    break;
                case COMSignalTypeEnum.PrintXY:
                    OnPrintXY(COMCommandFactory.BuildPrintXY(bytes));
                    break;
                case COMSignalTypeEnum.ClrScr:
                    OnClrScr(COMCommandFactory.BuildClrScr(bytes));
                    break;
                default:
                    break;
            }
        }

        void core_SendData(object sender, TypedEventArgs<StoreData> e)
        {
            
        }

        public void ConnectRemoteControl()
        {
            var thread = new Thread(new ThreadStart(delegate() 
                {
                    core.Start();
                    this.OnSetCursor(new CursorPosition(20,20));
                }));
            thread.IsBackground = true;            
            thread.Start();
        }

        public void CommitRemoteControl()
        {
        }

        public void DisconnectRemoteControl()
        {
            core.Stop();
        }

        private byte[] TranslateKeys(STD4408KeyEnum key1, STD4408KeyEnum key2, STD4408KeyEnum key3)
        {
            byte[] array1 = TranslateKey(key1);
            byte[] array2 = TranslateKey(key2);
            byte[] array3 = TranslateKey(key3);

            byte[] arrayComposition = new byte[2] { (byte)(array1[0] | array2[0] | array3[0]), (byte)(array1[1] | array2[1] | array3[1]) };

            return arrayComposition;
        }

        private byte[] TranslateKey(STD4408KeyEnum key)
        {
            byte[] array = new byte[2];
            switch (key)
            {
                case STD4408KeyEnum.Zero_Star:
                    array[0] = 0x0;
                    array[1] = 0x10;
                    break;
                case STD4408KeyEnum.One_F1:
                    array[0] = 0x0;
                    array[1] = 0x02;
                    break;
                case STD4408KeyEnum.Two_Down:
                    array[0] = 0x0;
                    array[1] = 0x20;
                    break;
                case STD4408KeyEnum.Three_F2:
                    array[0] = 0x02;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.Four_Left:
                    array[0] = 0x00;
                    array[1] = 0x04;
                    break;
                case STD4408KeyEnum.Five_F3:
                    array[0] = 0x00;
                    array[1] = 0x40;
                    break;
                case STD4408KeyEnum.Six_Right:
                    array[0] = 0x04;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.Seven_F4:
                    array[0] = 0x00;
                    array[1] = 0x08;
                    break;
                case STD4408KeyEnum.Eight_Up:
                    array[0] = 0x00;
                    array[1] = 0x80;
                    break;
                case STD4408KeyEnum.Shift:
                    array[0] = 0x00;
                    array[1] = 0x01;
                    break;
                case STD4408KeyEnum.Enter:
                    array[0] = 0x01;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.Multiply:
                    array[0] = 0x80;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.Minus:
                    array[0] = 0x40;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.Plus:
                    array[0] = 0x20;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.Equal:
                    array[0] = 0x10;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.Nine_F5:
                    array[0] = 0x08;
                    array[1] = 0x00;
                    break;
                case STD4408KeyEnum.None:
                    array[0] = 0x00;
                    array[1] = 0x00;
                    break;
            }

            return array;
        }

        private void CalculateCrc(byte[] array, out byte CRCbyteHIGH, out byte CRCbyteLOW)
        {
            CRCbyteLOW = 0xFF;
            CRCbyteHIGH = 0xFF;

            for (int i = 0; i < array.Length - 2; i++)
            {
                int index = CRCbyteHIGH ^ array[i];
                CRCbyteHIGH = (byte)(CRCbyteLOW ^ CrcTable.crc_table[index]);
                CRCbyteLOW = CrcTable.crc_table[index + 256];
            }
        }

        public void SendKey(STD4408KeyEnum key)
        {
            SendKey(TranslateKey(key));
        }

        
        private void SendKey(byte[] keyCodes)
        {
            byte[] array = new byte[4 + keyCodes.Length];
            array[0] = 0x2;
            array[1] = 0x30;
            int index = 2;
            foreach (byte keyCode in keyCodes)
            {
                array[index++] = keyCode;
            }

            byte CRCbyteHIGH = 0;
            byte CRCbyteLOW = 0;
            CalculateCrc(array, out CRCbyteHIGH, out  CRCbyteLOW);
            array[index++] = CRCbyteHIGH;
            array[index++] = CRCbyteLOW;

            lock (syncRoot)
            {                
                this.dataPresent = true;
                readData = array;
                //COMPort.Write(array, 0, array.Length);
            }
        }

        public void SendThreeKeys(STD4408KeyEnum key1, STD4408KeyEnum key2, STD4408KeyEnum key3)
        {
            SendKey(TranslateKeys(key1, key2, key3));
        }

        private void OnPrintXY(PrintedText printedText)
        {
            if (this.PrintXY != null)
            {
                this.PrintXY(this, new EventArgs<PrintedText>(printedText));
            }
        }

        private void OnSetCursor(CursorPosition cursorPosition)
        {
            if (this.SetCursor != null)
            {
                this.SetCursor(this, new EventArgs<CursorPosition>(cursorPosition));
            }
        }

        private void OnKeyboardScan(KeyboardScan keyboardScan)
        {
            if (this.KeyboardScaning != null)
            {
                this.KeyboardScaning(this, new EventArgs<KeyboardScan>(keyboardScan));
            }
        }

        private void OnClrScr(ClrScr clrScr)
        {
            if (this.ClrScr != null)
            {
                this.ClrScr(this, new EventArgs<ClrScr>(clrScr));
            }
        }

        public override void ReadFile(ALCHEVSKRemoteControlCore.COMPort hCom, Byte [] requestdata)
        {
            if (!this.dataPresent)
            {
                //Thread.Sleep(50);
            }

            if (this.dataPresent)
            {
                for (int i = 0; i < this.readData.Length; i++)
                {
                    requestdata[i] = this.readData[i];
                }

                this.dataPresent = false;
            }
        }
    }
}
