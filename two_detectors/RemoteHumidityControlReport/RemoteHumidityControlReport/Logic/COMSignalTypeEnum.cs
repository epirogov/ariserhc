﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.03.2010
 * Time: 12:45
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of COMSygnalTypeEnum.
	/// </summary>
	public enum COMSignalTypeEnum
	{
		Unknown,
		CMODE,
		ReadBuffer,
		GetRegistersGeneral,
		
		KeyboardScan,
		SetCursor,
		PrintXY,
		ClrScr,
			
		SetRegisters,
		WriteToEEPROM,
		SetSpeed,
		SendId		
	}
}
