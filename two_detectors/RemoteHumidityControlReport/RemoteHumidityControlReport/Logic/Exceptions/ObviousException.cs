﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RemoteHumidityControlReport.Logic.Exceptions
{
    public class ObviousException : Exception
    {
        public ObviousException()
            : base()
        {
        }

        public ObviousException(string message)
            : base(message)
        {
        }

        public ObviousException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ObviousException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
