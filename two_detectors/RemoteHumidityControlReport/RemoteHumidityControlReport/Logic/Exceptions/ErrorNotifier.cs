﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using RemoteHumidityControl.Utility;

namespace RemoteHumidityControlReport.Logic.Exceptions
{
    public static class ErrorNotifier
    {
        public static event EventHandler<EventArgs<Exception>> OnError;

        public static void SetError(Exception ex)
        {
        	if (LogToFile /*&& !(ex is ObviousException)*/)
        	{
        		File.AppendAllText(@"c:\exceptions.txt", string.Format("{0}{1}{2}",ex.Message, Environment.NewLine, ex.StackTrace));
        	}
        	
            if (OnError != null)
            {
                OnError(new Object(), new EventArgs<Exception>(ex));
            }
        }
        
        public static bool LogToFile
        {
        	get;
        	set;
        }        	
    }
}
