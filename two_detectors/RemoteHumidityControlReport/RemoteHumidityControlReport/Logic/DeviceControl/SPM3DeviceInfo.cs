﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControlReport.Logic.DeviceControl
{
    public class SPM3DeviceInfo
    {
        public int Address
        {
            get;
            set;
        }

        public string DeviceName
        {
            get
            {
                return string.Format("Устройство № {0}", Address);
            }
        }

        public bool? State
        {
            get;
            set;
        }
    }
}
