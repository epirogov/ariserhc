﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using STD4408Device.Logic;
using RemoteHumidityControl.Entity;
using RemoteHumidityControl.DataAccess;

using LibmodbusCustomCommands;
using EventArgs2 = RemoteHumidityControl.Utility.EventArgs<System.Collections.Generic.IEnumerable<RemoteHumidityControl.Entity.ControllerGeneralDataEntity>>;
using EventArgs1 = RemoteHumidityControl.Utility.EventArgs<LibmodbusCustomCommands.CommandEntity>;
using System.IO.Ports;
using RemoteHumidityControlReport.Logic.DeviceControl;
using System.Windows.Forms;
using LibraryBusinessLogic;
using System.Data.SqlClient;
using RemoteHumidityControlReport.Logic.Exceptions;
using System.Configuration;
namespace RemoteHumidityControl.Logic.DeviceControl
{
    public class HumidityWatcher
    {        
    	private ControllerConnector connector;
    	
        private TimeSpan duration;
        private int Period;

        private readonly List<SPM3DeviceInfo> controllers = new List<SPM3DeviceInfo>();
        private int controllerIndex = 0;
        public event EventHandler<EventArgs1> DataSent;
        public event EventHandler<EventArgs2> DataReceived;
        public event EventHandler<Utility.EventArgs<byte>> ControllerFounded;
        public event EventHandler<EventArgs> ControllerRequest;

        public HumidityWatcher()
        {
            Period = 30;
            int.TryParse(ConfigurationManager.AppSettings["Period"], out Period);
        }

        public HumidityWatcher(SerialPort serialPort, TimeSpan duration)
        {
            Period = 30;
            int.TryParse(ConfigurationManager.AppSettings["Period"], out Period);

            connector = new ControllerConnector(serialPort, controllers, Period);
            this.duration = duration;
            connector.DataSent += new EventHandler<EventArgs1>(connector_DataSent);
            connector.DataReceived += new EventHandler<EventArgs1>(OnDataReceived);
            connector.ControllerFounded += new EventHandler<Utility.EventArgs<byte>>(connector_ControllerFounded);
            connector.ControllerUnsubscribe += new EventHandler<Utility.EventArgs<byte>>(connector_ControllerUnsubscribe);
            connector.ControllerRequest += new EventHandler<EventArgs>(connector_ControllerRequest);
        }

        void connector_ControllerRequest(object sender, EventArgs e)
        {
            if (ControllerRequest != null)
            {
                ControllerRequest(this, e);
            }           
        }

        public List<SPM3DeviceInfo> Controllers
        {
            get
            {
                return controllers;
            }
        }

        void connector_DataSent(object sender, EventArgs1 e)
        {
            if (DataSent != null)
            {
                DataSent(this, e);
            }
        }
        
        private void OnDataReceived( object sender, EventArgs1 data)
        {
        	unsafe
        	{
        		IntPtr pointer = new IntPtr(data.Value1.data);

                byte[] byteArray = new byte[data.Value1.datalength];
        		
        		for(int i = 0; i < data.Value1.datalength; i++)
        		{
        			byteArray[i] = data.Value1.data[i];
        		}

                float humidity = byteArray[0] + byteArray[1] / 100.0f;
                float temperature = byteArray[2] + byteArray[3] / 100.0f;
                
	        	if(this.DataReceived != null)
                {                    
		        	this.DataReceived(
		                    this,
                            new EventArgs2(new ControllerGeneralDataEntity []
                    {
                        new ControllerGeneralDataEntity(data.Value1.GetAddress(),
                                                    (float)humidity,
                                                    (float)temperature,
                                                    DateTime.Now)
                    }));

                    try
                    {
                        Repository.NoQuery("WriteControllerData", new List<SqlParameter> { 
                                new SqlParameter() { DbType = System.Data.DbType.Int32, Direction = System.Data.ParameterDirection.Input, ParameterName = "@AddressId", SqlDbType = System.Data.SqlDbType.Int, Value = data.Value1.GetAddress()},
                                new SqlParameter() { DbType = System.Data.DbType.Double, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Humidity", SqlDbType = System.Data.SqlDbType.Float, Value = humidity},
                                new SqlParameter() { DbType = System.Data.DbType.Double, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Temperature", SqlDbType = System.Data.SqlDbType.Float, Value = temperature},
                                new SqlParameter() { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Date", SqlDbType = System.Data.SqlDbType.DateTime, Value = DateTime.Now}});                        
                    }
                    catch (Exception ex1)
                    {
                        ErrorNotifier.SetError(ex1);
                    }
                }
        	}
	        controllerIndex = (controllerIndex + 1) % controllers.Count;
        }
        
        private bool RegisterController(int contollerId)
        {
            if (!controllers.Any(controller => controller.Address == contollerId))
            {                
                controllers.Add(new SPM3DeviceInfo() { Address = contollerId });
                return true;
            }
            return false;
        }

        private void UnsubscribeController(int contollerId)
        {
            if (controllers.Any(controller => controller.Address == contollerId))
            {
                controllers.Remove(controllers.SingleOrDefault(controller => controller.Address == contollerId));
            }
        }

        public void Start()
        {
            if (controllers.Count > 0)
            {
	            connector.SetInterval((long)this.duration.TotalMilliseconds);
	            connector.Star();
	            controllerIndex = 0;                                
            }
        }        

        public void Stop()
        {
            connector.Stop();            
        }

        public bool ScanControllers()
        {
            return connector.StartScaning();
        }

        private void connector_ControllerUnsubscribe(object sender, Utility.EventArgs<byte> e)
        {
            UnsubscribeController(e.Value1);            
        }

        private void connector_ControllerFounded(object sender, Utility.EventArgs<byte> e)
        {
            if (RegisterController(e.Value1) && ControllerFounded != null)
            {
                ControllerFounded(this, e);
            }
        }

        public SerialPort serialport
        {
            get
            {
                return connector.serialPort;
            }
        }

        internal void RequestData(SPM3DeviceInfo sPM3DeviceInfo)
        {
            connector.ReadPack();
        }
    }
}
