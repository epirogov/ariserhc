﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.03.2010
 * Time: 12:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of BaseCommandData.
	/// </summary>
	public class BaseCommandData
	{
		private static int version;
		
		public BaseCommandData()
		{
			this.Version = CreateVersion();
		}
		
		public int Version
		{
			get;
			private set;
		}
		
		private static int CreateVersion()
		{
			return version++;
		}
	}
}
