﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.03.2010
 * Time: 12:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of PrintedText.
	/// </summary>
	public class PrintedText : CursorPosition
	{
		public PrintedText(string text, int posX, int posY) : base(posX, posY)
		{
			this.Text = text;
		}
		
		public string Text
		{
			get;
			private set;
		}
	}
}
