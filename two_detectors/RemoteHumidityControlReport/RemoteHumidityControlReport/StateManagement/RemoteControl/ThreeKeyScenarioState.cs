﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 01.03.2010
 * Time: 21:59
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of ThreeKeyScenarioState.
	/// </summary>
	public class ThreeKeyScenarioState : BaseKeyScenarioState, IKeyScenarioState
	{
		public ThreeKeyScenarioState()
		{
			this.CreateInternals(null);
		}
		
		public ThreeKeyScenarioState(STD4408MainFormStateController controller)
		{
			this.CreateInternals(controller);
		}
		
		private void CreateInternals(STD4408MainFormStateController controller)
		{
			base.CreateInternals(controller, KeyScenarioStateEnum.ThreeKey);
		}
		
		public void SetOneKeyScenario()
		{
			base.Controller.SetKeyScenarioState<OneKeyScenarioState>();
		}
		
		public void SetThreeKeyScenario()
		{
			throw new NotImplementedException();
		}
	}
}
