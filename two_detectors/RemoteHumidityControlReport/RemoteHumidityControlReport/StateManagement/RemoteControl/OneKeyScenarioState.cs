﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 01.03.2010
 * Time: 21:59
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of OneKeyScenarioState.
	/// </summary>
	public class OneKeyScenarioState : BaseKeyScenarioState, IKeyScenarioState
	{
		public OneKeyScenarioState()
		{
			this.CreateInternals(null);
		}
		
		public OneKeyScenarioState(STD4408MainFormStateController controller)
		{
			this.CreateInternals(controller);
		}		
		
		private void CreateInternals(STD4408MainFormStateController controller)
		{
			base.CreateInternals(controller, KeyScenarioStateEnum.OneKey);
		}
		
		public void SetOneKeyScenario()
		{
			
		}
		
		public void SetThreeKeyScenario()
		{
			base.Controller.SetKeyScenarioState<ThreeKeyScenarioState>();
		}
	}
}
