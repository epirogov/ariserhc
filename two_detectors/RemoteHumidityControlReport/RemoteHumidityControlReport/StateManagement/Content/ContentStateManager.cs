﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RemoteHumidityControl.Utility;

namespace RemoteHumidityControl.StateManagement.Content
{
    public class ContentStateManager : IContentAction
    {
        public event EventHandler<EventArgs<ContentStateEnum>> OpenContent;        
 
        public ContentStateManager()
        {
            this.State = new ChartsState();
            this.State.StateManager = this;            
        }

        public IContentState State
        {
            get;
            set;
        }        

        public void  OpenArchive()
        {
            this.State.OpenArchive();
        }

        public void  OpenCharts()
        {
            this.State.OpenCharts();
        }

        public void  OpenRemoteControl()
        {
            this.State.OpenRemoteControl();
        }

        public void  OpenReport()
        {
            this.State.OpenReport();
        }

        public void OnOpenContent()
        {
            if (this.OpenContent != null)
            {
                this.OpenContent(this, new EventArgs<ContentStateEnum>(this.State.StateValue));
            }
        }
    }
}
