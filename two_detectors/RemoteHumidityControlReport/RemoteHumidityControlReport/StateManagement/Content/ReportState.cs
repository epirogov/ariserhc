﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.StateManagement.Content
{
    public class ReportState : BaseContentState
    {
        public ReportState()
            : base(ContentStateEnum.Report)
        {
        }

        public override void OpenArchive()
        {
            this.SetState<ArchiveState>();
        }

        public override void OpenCharts()
        {
            this.SetState<ChartsState>();
        }

        public override void OpenRemoteControl()
        {
            this.SetState<RemoteControlState>();
        }

        public override void OpenReport()
        {
        }
    }
}
