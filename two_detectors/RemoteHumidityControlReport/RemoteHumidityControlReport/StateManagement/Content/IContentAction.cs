﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.StateManagement.Content
{
    public interface IContentAction
    {
        void OpenArchive();
        void OpenCharts();
        void OpenRemoteControl();
        void OpenReport();
    }
}
