﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.StateManagement.Content
{
    public class ChartsState : BaseContentState
    {
        public ChartsState()
            : base(ContentStateEnum.Charts)
        {
        }

        public override void OpenArchive()
        {
            this.SetState<ArchiveState>();
        }

        public override void OpenCharts()
        {
            this.StateManager.OnOpenContent();
        }

        public override void OpenRemoteControl()
        {
            this.SetState<RemoteControlState>();
        }

        public override void OpenReport()
        {
            this.SetState<ReportState>();
        }
    }
}
