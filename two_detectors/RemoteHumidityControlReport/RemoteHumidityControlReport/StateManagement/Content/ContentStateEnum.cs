﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.StateManagement.Content
{
    public enum ContentStateEnum
    {
        Charts,
        Archive,
        Report,
        RemoteControl
    }
}
