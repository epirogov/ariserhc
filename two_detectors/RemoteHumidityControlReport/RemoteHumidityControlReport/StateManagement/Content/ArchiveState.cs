﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.StateManagement.Content
{
    public class ArchiveState : BaseContentState
    {
        public ArchiveState()
            : base(ContentStateEnum.Archive)
        {
        }

        public override void OpenArchive()
        {
        }

        public override void OpenCharts()
        {
            this.SetState<ChartsState>();
        }

        public override void OpenRemoteControl()
        {
            this.SetState<RemoteControlState>();
        }

        public override void OpenReport()
        {
            this.SetState<ReportState>();
        }
    }
}
