﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.Utility;

namespace RemoteHumidityControl.StateManagement.Content
{
    public abstract class BaseContentState : IContentState
    {
        public BaseContentState(ContentStateEnum state)
        {
            this.StateValue = state;
        }

        public ContentStateEnum StateValue
        {
            get;
            set;
        }

        public ContentStateManager StateManager
        {
            get;
            set;
        }

        abstract public void OpenArchive();
        abstract public void OpenCharts();
        abstract public void OpenRemoteControl();
        abstract public void OpenReport();

        public void SetState<StateType>() where StateType : IContentState, new()
        {
            StateType newState = new StateType();
            newState.StateManager = this.StateManager;            
            newState.StateManager.State = newState;
            newState.StateManager.OnOpenContent();            
        }
    }
}
