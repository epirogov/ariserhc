﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using RemoteHumidityControlReport.View;
using LibraryBusinessLogic;
using System.Configuration;
using RemoteHumidityControlReport.Logic.Exceptions;
using System.Threading;

namespace RemoteHumidityControlReport
{
    static class Program
    {
        public static MainForm mainForm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
           Application.EnableVisualStyles();
           Application.SetCompatibleTextRenderingDefault(false);

           AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
           Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
           
            
            Repository.ConnectionString = ConfigurationManager.ConnectionStrings["SPM3Database"].ConnectionString;
            string[] portNames = SerialPort.GetPortNames();
                //MessageBox.Show(ConfigurationManager.AppSettings["SerialPortName"]);
                string portname = ConfigurationManager.AppSettings["SerialPortName"];
                bool isReport = string.Compare(ConfigurationManager.AppSettings["SerialPortName"], "DENY", true) == 0;
            try{
                	if(!portNames.Contains(portname))
                	{
		                AboutBox1 form = new AboutBox1(portNames, isReport);
		                form.ShowDialog();

                        portname = form.SelectedPort;
                        isReport = isReport && string.IsNullOrEmpty(portname);       	                    
                	}
                }
                catch (Exception)
                {
//                    DisableScanning();
//                    return;
                }


            mainForm = new MainForm(portname, isReport);
            Application.Run(mainForm);
        }


        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            if (ex == null || (!(ex is ObviousException) &&
                ex.Message.Contains("Retrieving the COM class factory for component with CLSID")))
            {
                //File.WriteAllText(@"c:\exception.txt", ex.Message + " " + ex.StackTrace);

                ErrorNotifier.SetError((Exception)e.ExceptionObject);                
            }
        }


        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            if (!(e.Exception is ObviousException) &&
                !e.Exception.Message.Contains("Retrieving the COM class factory for component with CLSID"))
            {
                ErrorNotifier.SetError(e.Exception);
            }
            else
            {
                ErrorNotifier.SetError(new ObviousException( e.Exception.Message ));                
            }
        }
    }
}
