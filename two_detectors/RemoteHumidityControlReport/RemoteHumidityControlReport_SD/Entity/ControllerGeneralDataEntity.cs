﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.Entity
{
    public class ControllerGeneralDataEntity
    {        
        public ControllerGeneralDataEntity()
        {
        }

        public ControllerGeneralDataEntity(byte address, float humidity, float temperature, DateTime date)
        {
            this.Address = address;
            this.Humidity = humidity;
            this.Temperature = temperature;
            this.Date = date;
        }

        public byte Address
        {
            get;
            set;
        }

        public float Humidity
        {
            get;
            set;
        }

        public float Temperature
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

    }
}
