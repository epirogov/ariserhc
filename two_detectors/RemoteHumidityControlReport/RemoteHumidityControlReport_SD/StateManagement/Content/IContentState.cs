﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.StateManagement.Content
{
    public interface IContentState : IContentAction
    {
        ContentStateEnum StateValue
        {
            get;
            set;
        }

        ContentStateManager StateManager
        {
            get;
            set;
        }        
    }
}
