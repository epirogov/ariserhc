﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.StateManagement.Content
{
    public class RemoteControlState : BaseContentState
    {
        public RemoteControlState()
            : base(ContentStateEnum.RemoteControl)
        {
        }

        public override void OpenArchive()
        {
            this.SetState<ArchiveState>();
        }

        public override void OpenCharts()
        {
            this.SetState<ChartsState>();
        }

        public override void OpenRemoteControl()
        {
        }

        public override void OpenReport()
        {
            this.SetState<ReportState>();
        }
    }
}
