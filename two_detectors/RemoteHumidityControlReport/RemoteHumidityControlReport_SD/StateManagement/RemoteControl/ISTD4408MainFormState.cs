﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 19:46
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of ISTD4408MainFormState.
	/// </summary>
	public interface ISTD4408MainFormState
	{
		STD4408MainFormStateController Controller
		{
			get;
			set;			
		}
		
		STD4408StateEnum StateValue
		{
			get;			
		}
		
		void ConnectCOMPorts();
		void DisconnectCOMPorts();		
	}
}
