﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 01.03.2010
 * Time: 21:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of BaseKeysScenarioState.
	/// </summary>
	public class BaseKeyScenarioState
	{
		public BaseKeyScenarioState()
		{
		}
		
		public STD4408MainFormStateController Controller {
			get;
			set;
		}
		
		public KeyScenarioStateEnum StateValue
		{
			get;
			set;
		}
		
		protected void CreateInternals(STD4408MainFormStateController controller, KeyScenarioStateEnum stateValue)
		{
			this.Controller = controller;			
			this.StateValue = stateValue;
		}
	}
}
