﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 20:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of DisconnectedCOMPortsState.
	/// </summary>
	public class DisconnectedCOMPortsState : BaseCOMPortsState, ISTD4408MainFormState
	{
		public DisconnectedCOMPortsState()
		{
			CreateInternals(null);
		}	
		
		public DisconnectedCOMPortsState(STD4408MainFormStateController controller)
		{
			CreateInternals(controller);
		}	
		
		public void ConnectCOMPorts()
		{
			this.Controller.SetCOMPortsState<ConnectedCOMPortsState>();
		}
		
		public void DisconnectCOMPorts()
		{
			throw new NotImplementedException();
		}
		
		private void CreateInternals(STD4408MainFormStateController controller)
		{
			base.CreateInternals(controller, STD4408StateEnum.DisconnectedCOMPorts);			
		}
	}
}
