﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using RemoteHumidityControlReport.View;
using LibraryBusinessLogic;
using System.Configuration;

namespace RemoteHumidityControlReport
{
	static class Program
	{
		public static MainForm mainForm;
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
		   Application.EnableVisualStyles();
		   Application.SetCompatibleTextRenderingDefault(false);
			
		   Repository.ConnectionString = ConfigurationManager.ConnectionStrings["SPM3Database"].ConnectionString;
		   string[] portNames = SerialPort.GetPortNames();
				
		   string portname = ConfigurationManager.AppSettings["SerialPortName"];
		   bool isReport = string.Compare(ConfigurationManager.AppSettings["SerialPortName"], "DENY", true) == 0;
		   try{

				AboutBox1 form = new AboutBox1(portNames, isReport);
				form.ShowDialog();
				
				
					if (!string.IsNullOrEmpty(portname))
					{
						portname = form.SelectedPort;
					}
					else
					{
				 		isReport = true;	   
					}
				}
				catch
				{
//					DisableScanning();
//					return;
				}


			mainForm = new MainForm(portname, isReport);
			Application.Run(mainForm);
		}	 
		
	}
}
