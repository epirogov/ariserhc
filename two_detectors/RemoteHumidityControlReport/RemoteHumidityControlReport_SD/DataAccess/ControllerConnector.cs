﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 19.04.2010
 * Time: 17:38
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;

using LibmodbusCustomCommands;
using EventArgs1 = RemoteHumidityControl.Utility.EventArgs<LibmodbusCustomCommands.CommandEntity>;
using EventArgs2 = RemoteHumidityControl.Utility.EventArgs<byte>;
using Timer = System.Timers.Timer;

using System.Threading;

using System.Windows.Forms;
using System.Diagnostics;
using System.IO.Ports;
using RemoteHumidityControlReport.Logic.Exceptions;
using RemoteHumidityControlReport.Logic.DeviceControl;
using System.Text;
using LibraryBusinessLogic;
using System.Data.SqlClient;
using System.IO;
namespace RemoteHumidityControl.DataAccess
{
	/// <summary>
	/// Description of ControllerConnector.
	/// </summary>
	public class ControllerConnector
	{
        const int ScanController = 0x1;
        const int RegisterController = 0x2;
        const int DeviceWasHalted = 0x4;
        const int RequiestData = 0x8;
        const int DataPacketReceived = 0x10;
        const int DataPacketBrocken = 0x20;
        const int DataTransactionComplited = 0x40;
        const int BeginDataSend = 0x80;
        const int SendDataPacket = 0x100;
        const int EndDataSend = 0x200;
        const int NoMedium = 0x400;

		private Timer timer = new Timer();		
		private DataTransferMaster master;
        volatile bool isAsynchronousResponse;
        volatile byte controllerId;
        
        private readonly object WaitScanning = new object();
        private readonly object WaitResponse = new object();        
        private int period = 30;
        //private int controllerIndex;

        public event EventHandler<EventArgs1> DataSent;
		public event EventHandler<EventArgs1> DataReceived;
        public event EventHandler<EventArgs2> ControllerFounded;
        public event EventHandler<EventArgs2> ControllerUnsubscribe;
        public event EventHandler<EventArgs> ControllerRequest;
        public readonly CommandFactory commandFactory = new CommandFactory();

        public SerialPort serialPort
        {
            get;
            set;
        }

        public ControllerConnector(SerialPort serialPort, List<SPM3DeviceInfo> Controllers, int period)
		{
            File.WriteAllText(@"C:\rhc.log", "Data" + DateTime.Now.ToLongTimeString() + Environment.NewLine);
            this.period = period;
            this.serialPort = serialPort;
            this.serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
            master = new DataTransferMaster(serialPort, 30 * 1000);
            //master.Open();                
            ControllerIDs = Controllers;

            Thread thread = new Thread(new ThreadStart(ReadPack));
            thread.IsBackground = true;
            thread.Start();
            master.DataSent += new CompliteTemplateEventHandler(master_DataSent);
            master.CompliteTemplate += new CompliteTemplateEventHandler(master_CompliteTemplate);
            
		}

        
        void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            File.AppendAllText(@"C:\rhc.log", "Data" + DateTime.Now.ToLongTimeString() + Environment.NewLine);
            //Thread thread = new Thread(new ThreadStart(delegate()
            //    {
                    try
                    {
                        if (isAsynchronousResponse)
                        {
                            if (ControllerIDs[controllerId].State.HasValue)
                            {
                                ControllerIDs[controllerId].State = null;
                                StartScaning();
                            }
                            ReleaseData();
                            byte[] buffer = new byte[256];
                            byte[] buffer1 = new byte[256];
                            int length = 0;
                            int length1 = 0;
                            try
                            {
                                length = this.serialPort.Read(buffer, 0, 256);
                                Thread.Sleep(500);
                                length1 = this.serialPort.Read(buffer1, 0, 256);
                            }
                            catch (TimeoutException)
                            {
                            }
                            catch (InvalidOperationException ex)
                            {
                                ErrorNotifier.SetError(ex);
                            }


                            var bufferUnion = buffer.Take(length).Union(buffer1.Take(length1)).ToArray();

                            int indexComplited = bufferUnion
                                .Select(
                                (value, i) => new { Value = value, Index = i })
                                .Where(data => ControllerIDs.Select(controller => controller.Address).Contains((int)data.Value)
                                    && bufferUnion[data.Index + 1] == 85)
                                .Select(data1 => data1.Index + 2).FirstOrDefault();

                            if (indexComplited > 0)
                            {
                                return;
                            }

                            int index = bufferUnion
                                .Select(
                                (value, i) => new { Value = value, Index = i })
                                .Where(data => ControllerIDs.Select(controller => controller.Address).Contains((int)data.Value)
                                    && bufferUnion[data.Index + 1] == 84)
                                .Select(data1 => data1.Index + 2).FirstOrDefault();

                            if (index > 2)
                            {
                                var result = bufferUnion.Skip(index);
                                result = result.Take(4);
                                ErrorNotifier.SetError(
                                                new ObviousException(
                                                    string.Format(
                                                    "Data Packet Received {0} ",
                                                    string.Join(",", result.Select(value => value.ToString())))));                                
                                if (DataReceived != null)
                                {
                                    
                                    unsafe
                                    {
                                        fixed (byte* entityData = result.ToArray())
                                        {                                            
                                            DataReceived(this,
                                                new EventArgs1(
                                                    commandFactory
                                                    .CreateDataPacketReceived(
                                                    (byte)bufferUnion[index - 2],
                                                    entityData,
                                                    (byte)result.Count())));
                                        }
                                    }
                                }
                            }

                        }                        
                    }
                catch (Exception)
                {
                }
                //}));
            //thread.IsBackground = true;
            //thread.Start();            
            //if (thread.IsAlive && !thread.Join(10000))
            //{
            //    this.serialPort.DiscardInBuffer();
            //    //thread.Interrupt();                
            //}
        }

        void master_DataSent(object sender, TypedEventArgs<CommandEntity> e)
        {
            if (DataSent != null)
            {
                DataSent(this, new EventArgs1(e.Value1));
            }
        }

        public List<SPM3DeviceInfo> ControllerIDs 
		{
			get;
			set;
		}

		private void timer_Elapsed()
		{
            //while (true)
            //{
            //    Thread.Sleep(1200);
            //    if (this.serialPort.IsOpen && this.ControllerIDs.Count > 0)
            //    {
            //        ReadPack((byte)this.ControllerIDs[0].Address);
            //    }
            //}
		}
		
		public void SetInterval(long interval)
		{
            //timer.Stop();			
            //timer.Interval =  interval;
            //timer.Start();
		}
		
		public void Star()
		{
			if(timer.Interval != 0)
			{
				timer.Start();
			}
		}
		
		public void Stop()
		{            
			//timer.Stop();
		}
		
		public bool StartScaning()
		{
            //bool result = false;

            isAsynchronousResponse = false;
            for (byte i = 1; i < 6; i++)
            {
                if (ControllerRequest != null)
                {
                    ControllerRequest(this, new EventArgs());
                }
                try
                {
                    master.ScanController(i);
                    WaitScaning();
                }
                catch (TimeoutException)
                {
                    try
                    {
                        master.ScanController(i);
                        WaitScaning();
                    }
                    catch (Exception)
                    {
                    }
                }
                catch (InvalidOperationException ex)
                {
                    ErrorNotifier.SetError(ex);
                }                
            }
            isAsynchronousResponse = true;

            return true;
		}     
        

        private bool WaitData(out TimeSpan waitTime)
        {
            TimeSpan periodTime = new TimeSpan(0, 0, period);
            DateTime time = DateTime.Now;
            Monitor.Enter(WaitResponse);
            bool result = Monitor.Wait(WaitResponse, periodTime);
            Monitor.Exit(WaitResponse);
            waitTime = DateTime.Now - time;
            return waitTime < periodTime;
        }

        private void ReleaseData()
        {
            Monitor.Enter(WaitResponse);
            Monitor.PulseAll(WaitResponse);
            Monitor.Exit(WaitResponse);
        }

        private bool WaitScaning()
        {
            Monitor.Enter(WaitScanning);
            bool result = Monitor.Wait(WaitScanning, 4 * 1000);
            Monitor.Exit(WaitScanning);
            return result;
        }

        private void ReleaseScaning()
        {
            Monitor.Enter(WaitScanning);
            Monitor.PulseAll(WaitScanning);
            Monitor.Exit(WaitScanning);
        }

        private void TimedOperation(Action action)
        {
            bool isTimeout = false;
            do
            {
                try
                {
                    action();
                    isTimeout = false;
                }
                catch (TimeoutException)
                {
                    isTimeout = true;
                }
            }
            while (isTimeout);
        }

        public void ReadPack()
        {
            isAsynchronousResponse = true;

            while (true)
            {              

                if (!isAsynchronousResponse || ControllerIDs.Count == 0)
                {
                    Thread.Sleep(5000);
                    continue;
                }
                if (ControllerIDs.Count <= controllerId)
                {
                    controllerId = 0;
                }
                //ReleaseScaning();

                //Action requiestData = () => master.RequestData(controllerId, 0, 50 * 1000);
                //TimedOperation(requiestData);            

                //WaitScaning();
                
                unsafe
                {
                    TimeSpan waitTime = new TimeSpan();
                    Action sendDataPacket = () =>
                    {
                        fixed (byte* data = new byte[0])
                        {
                            try
                            {
                                master.SendDataPacket((byte)ControllerIDs[controllerId].Address, 0, 0, data, 0);
                                if (!WaitData(out waitTime))
                                {
                                    ControllerIDs[controllerId].State = ControllerIDs[controllerId].State.HasValue;
                                    //MessageBox.Show("Failed " + ControllerIDs[controllerId].State.ToString());
                                    if (ControllerIDs[controllerId].State == true)
                                    {                                        
                                        try
                                        {
                                            if (ControllerRequest != null)
                                            {
                                                ControllerRequest(this, new EventArgs());
                                            }                                            
                                            master.ScanControllerRequest((byte)ControllerIDs[controllerId].Address);
                                            ErrorNotifier.SetError(
                                                new ObviousException(
                                                    string.Format(
                                                    "Request Controller {0} ", (byte)ControllerIDs[controllerId].Address
                                                    )));                                
                                        }
                                        catch (TimeoutException)
                                        {
                                        }
                                        catch (InvalidOperationException ex)
                                        {
                                            ErrorNotifier.SetError(ex);
                                        }                    
                                    }
                                }
                                else
                                {
                                    ControllerIDs[controllerId].State = null;
                                }
                            }
                            catch (TimeoutException)
                            {
                            }
                            catch (InvalidOperationException ex)
                            {
                                ErrorNotifier.SetError(ex);
                            }
                        }
                    };

                    sendDataPacket();
                    Thread.Sleep(new TimeSpan(0, 0, period) - waitTime);
                    //while (!transactionComplited)
                    //{
                    //    master.SendDataPacket(controllerId, 0, 0, data, 0);
                    //    WaitScaning();
                    //}

                }
                controllerId++;
            }
        }

        void master_CompliteTemplate(object sender, TypedEventArgs<CommandEntity> e)
        {
            StringBuilder b = new StringBuilder();
            //MessageBox.Show(e.Value1.identifier.ToString());
            switch (e.Value1.identifier)
            {
                case  RegisterController:
                    ErrorNotifier.SetError(new ObviousException("Register Controller"));
                    if (ControllerFounded != null)
                    {
                        ReleaseScaning();
                        ControllerFounded(this, new EventArgs2(e.Value1.GetAddress()));
                        if (ControllerIDs.Count > e.Value1.GetAddress())
                        {
                            ControllerIDs[controllerId].State = null;
                        }
                    }
                    break;
                case DeviceWasHalted:
                    ErrorNotifier.SetError(new ObviousException("Device Was Halted"));
                    ControllerUnsubscribe(this, new EventArgs2(e.Value1.GetAddress()));
                    break;
                case RequiestData:
                    ErrorNotifier.SetError(new ObviousException("Requiest Data"));
                    ReleaseScaning();
                    break;
                case DataPacketReceived:

                    try
                    {
                        master.DataPackedReseived(e.Value1.GetAddress());
                    }
                    catch (TimeoutException)
                    {
                    }
                    catch (InvalidOperationException ex)
                    {
                        ErrorNotifier.SetError(ex);
                    }

                    unsafe
                    {                       
                        for(int i =0; i< e.Value1.datalength; i++)
                        {
                            b.Append((char)e.Value1.data[i]);                            
                        }

                        ErrorNotifier.SetError(
                                new ObviousException(
                                    string.Format(
                                    "Data Packet Received {0} {1} {2} {3}",
                                    e.Value1.data[0],
                                    e.Value1.data[1],
                                    e.Value1.data[2],
                                    e.Value1.data[3])));
                    }

                    
                    DataReceived(this, new EventArgs1(e.Value1));
                    ReleaseScaning();
                    break;
                case DataTransactionComplited:
                    ErrorNotifier.SetError(new ObviousException("Data Transaction Complited"));                    
                    ReleaseScaning();
                    break;
                case EndDataSend:
                    ErrorNotifier.SetError(new ObviousException("End Data Send"));
                    break;
                case NoMedium:
                    ErrorNotifier.SetError(new ObviousException("No Medium"));
                    DataReceived(this, new EventArgs1(e.Value1));
                    ReleaseScaning();
                    break;
                default :
                    //ErrorNotifier.SetError(
                    //    new ObviousException( 
                    //        string.Format(
                    //        "Identifier{0}, Function {1}, Has SubFunction {2}, SubFunction{3}, Data length {4}",
                    //        e.Value1.identifier, 
                    //        e.Value1.GetFunction(), 
                    //        e.Value1.HasSubfunction(), 
                    //        e.Value1.GetSubFunction(), 
                    //        e.Value1.datalength)));                    
                    ReleaseScaning();
                    break;
            }
        }
	}
}
