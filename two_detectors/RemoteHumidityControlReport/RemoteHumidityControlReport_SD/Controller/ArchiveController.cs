﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 25.04.2010
 * Time: 13:12
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using RemoteHumidityControl.ViewInterface;
using System;
using System.Linq;
using RemoteHumidityControl.Logic.DeviceControl;
using System.Collections.Generic;
using System.IO.Ports;
using RemoteHumidityControlReport.View;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;
using RemoteHumidityControlReport.Logic.DeviceControl;
using RemoteHumidityControl.Utility;
using RemoteHumidityControlReport.Logic.Exceptions;

namespace RemoteHumidityControlPreview.Controller
{
    /// <summary>
    /// Description of ArchiveController.
    /// </summary>
    public class ArchiveController
    {
        public event EventHandler<EventArgs<SPM3DeviceInfo>> ControllerFounded;        

        [Browsable(false)]
        public HumidityWatcher watcher
        {
            get;
            set;
        }        

        public ArchiveController(IDataUserControl control, SerialPort serialPort)
        {
            ArciveView = control;
            ArciveView.SetController(this);
            if (serialPort != null)
            {
                watcher = new HumidityWatcher(serialPort, new TimeSpan(0, 0, 12));
                watcher.ControllerFounded += new EventHandler<EventArgs<byte>>(watcher_ControllerFounded);
                watcher.DataReceived += new EventHandler<EventArgs<IEnumerable<RemoteHumidityControl.Entity.ControllerGeneralDataEntity>>>(watcher_DataReceived);
                watcher.ControllerRequest += new EventHandler<EventArgs>(watcher_ControllerRequest);
            }
        }

        void watcher_ControllerRequest(object sender, EventArgs e)
        {
            ArciveView.OnControllerRequest();
        }

        void watcher_DataReceived(object sender, EventArgs<IEnumerable<RemoteHumidityControl.Entity.ControllerGeneralDataEntity>> e)
        {
            ArciveView.BindData(e.Value1);            
        }

        void watcher_ControllerFounded(object sender, EventArgs<byte> e)
        {
            if (ControllerFounded != null)
            {
                ControllerFounded(this, new EventArgs<SPM3DeviceInfo>(new SPM3DeviceInfo() { Address = e.Value1 }));
            }
        }
        
        public IDataUserControl ArciveView
        {
            get;
            private set;
        }

        public bool ScanDevices()
        {
            bool result = watcher.ScanControllers();
            if (result)
            {
                watcher.Start();
            }
            return result;
        }

        public List<SPM3DeviceInfo> Controllers
        {
            get
            {
                return this.watcher.Controllers;
            }
        }

        public bool OpenPort(string serialPortName)
        {            
            this.watcher.serialport.PortName = serialPortName;            
            if (this.watcher.serialport.IsOpen)
            {                    
                this.watcher.serialport.Close();
            }
            
            if (!SerialPort.GetPortNames().Any(port => serialPortName == port))
            {
                return false;
            }

            //Thread thread = new Thread(new ThreadStart(delegate()
            //{
                try
                {
                    if (!this.watcher.serialport.IsOpen)
                    {                        
                        this.watcher.serialport.Open();                    
                    };
                }
                catch (Exception ex)
                {                    
                    ErrorNotifier.SetError(ex);
                }
            //}));
            //thread.IsBackground = true;
            //thread.Start();
            //if(thread.IsAlive && !thread.Join(10000))
            //{
            //    this.watcher.serialport.DiscardInBuffer();
            //    //thread.Interrupt();                
            //}

            return true;
        }

        public void SelectSerialPort()
        {
            SelectSerialPortForm form = new SelectSerialPortForm(SerialPort.GetPortNames());
            form.ShowDialog();
            if (form.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                OpenPort(form.SelectedPort);
            }
        }

        internal void RequestData(SPM3DeviceInfo sPM3DeviceInfo)
        {
            watcher.RequestData(sPM3DeviceInfo);
        }
    }
}
