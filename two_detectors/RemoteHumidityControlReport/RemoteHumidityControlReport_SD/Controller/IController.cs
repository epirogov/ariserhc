﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControl.Controller
{
    public interface IController
    {
        void OnLoad();
        void OnHide();
        void OnShow();
        void OnClose();
    }
}
