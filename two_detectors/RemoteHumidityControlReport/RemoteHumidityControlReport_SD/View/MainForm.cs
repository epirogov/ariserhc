﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using RemoteHumidityControlPreview.Controller;
using System.Globalization;
using System.Threading;
using RemoteHumidityControlReport.Logic.Exceptions;
using System.Reflection;
using RemoteHumidityControlReport.View;
using System.Configuration;
using RemoteHumidityControlReport.Logic.DeviceControl;
using RemoteHumidityControl.Utility;
using System.Collections;
using LibmodbusCustomCommands;
using LibraryBusinessLogic;
using System.Data.SqlClient;

namespace RemoteHumidityControlReport
{
    public partial class MainForm : Form
    {
        ArchiveController controller;
        readonly List<Exception> exceptions = new List<Exception>();
        
        private string portname;
        private bool isReport;
        public MainForm(string portname,bool isReport)
        {
        	this.portname = portname;
        	this.isReport = isReport;
            InitializeComponent();
            AppDomain.CurrentDomain.UnhandledException +=new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);            
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);            
            ErrorNotifier.OnError += new EventHandler<EventArgs<Exception>>(ErrorNotifier_OnError);
            bindingSourceErrors.DataSource = exceptions;
            
            this.dataGridViewErrors.DataSource = bindingSourceErrors;
            splitContainer.Panel2Collapsed = true;
        }

        void ErrorNotifier_OnError(object sender, EventArgs<Exception> e)
        {
            PrintError(e.Value1);
        }

        public SerialPort serialPort
        {
            get;
            set;
        }

        void Watcher_DataSent(object sender, RemoteHumidityControl.Utility.EventArgs<LibmodbusCustomCommands.CommandEntity> e)
        {
            //if (e.Value1 == null)
            //{
            //    return;
            //}
            //unsafe{
            //    fixed (byte** command = new byte * [1])
            //    {
            //        fixed(byte * buffer = new byte[256])
            //        {
            //            command[0] = buffer;
            //            int commandlength = 0;
            //            e.Value1.Build(command, &commandlength);

            //            StringBuilder b = new StringBuilder();
            //            for(int i =0; i<commandlength; i++)
            //            {
            //                b.Append((*command)[i].ToString("X2"));
            //                b.Append(", ");
            //            }
                        
            //            Action printRequest = () => this.rtbRequest.Text = b.ToString() + Environment.NewLine + this.rtbRequest.Text;
            //            if (rtbRequest.InvokeRequired)
            //            {
            //                rtbRequest.Invoke(new MethodInvoker(delegate()
            //                {
            //                    printRequest();
            //                }));
            //            }
            //            else
            //            {
            //                printRequest();
            //            }
            //        }
            //    }            
            
            //};
            //e.Value1
        }
        
        private void btnScanController_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(delegate()
                {
                    bool result = this.reportUserControl.ScanDevices();
                    //MessageBox.Show(result.ToString());
                    if (!result)
                    {
                        exceptions.Add(new ObviousException("Devices not founded"));
                        this.stateUserControl.SetYellow();
                    }
                    else
                    {                        
                        this.stateUserControl.SetGreen();
                    }

                    
                }));
            
            thread.IsBackground = true;
            thread.Start();
        }
        
        private void lvDevices_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private int count;
        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //this.rtbData.Text = string.Join(",  ", "41 54 20 30 32 20 31 20 31 20 30 30 30 2E 30 20 30 30 2E 30 30 20 24 BE".Split(' ').Select(value => toBits(byte.Parse(value, NumberStyles.HexNumber))).ToArray());
            
            //byte[] data = new byte[0];
            //try
            //{
            //    data = serialPort.ReadExisting().Select(value => (byte)value).ToArray();
            //}
            //catch (TimeoutException)
            //{
            //    throw;
            //}
            //Action printResponse = () => this.rtbData.Text = string.Join(", ", data.Select((value, i) => value.ToString("X2")
            //    + (((i + count) % 16 == 0) ? Environment.NewLine : string.Empty))) + this.rtbData.Text;
            ////.ToString("X2")
            ////toBits(value)
            //if (this.rtbData.InvokeRequired)
            //{
            //    this.rtbData.Invoke(new MethodInvoker(delegate
            //    {
            //        printResponse();
            //    }));
            //}
            //else
            //{
            //    printResponse();
            //}

            //this.rtbData.Text = ModbusProvider.check_crc16(serialPort.ReadExisting().Select(value => (byte)value).ToArray()).ToString() ;            
            //short crc = ModbusProvider.crc16(data, (short)(data.Length - 2));


            //right reading
            //byte[] data = new byte[256];
            //int ret1 = 0;
            //ret1 = serialPort.Read(data, 0, 256);
            //data = data.Take(ret1).ToArray();   
            //short crc  = ModbusProvider.crc16(data, (short)(data.Length -2));

            //this.rtbData.Text = string.Format("{0} {1} = {2}, {3}", (crc & 0xFF00).ToString("x"), (crc & 0xFF).ToString("x"), crc, data.Length);
            //this.rtbData.Text = string.Join(", ", data.Select(value => value.ToString("x")).Take(ret1).ToArray());


            //this.rtbData.Text = this.rtbData.Text + data.Length + " " + (crc >> 8).ToString("x") + " " + (crc & 0xFF).ToString("x");
            //this.rtbData.Text += (count++).ToString();
            
            //count += data.Length;
            this.Invoke(new MethodInvoker(delegate(){
            this.rtbData.Text = string.Format("{0}: {1}\r\n{2}", ++count, serialPort.BytesToRead,  this.rtbData.Text);
                                          }));
        }

        string toBits(byte value)
        {
            StringBuilder b = new StringBuilder();
            while (value > 0)
            {
                b.Append(value % 2);
                value /= 2;
            }

            return new string(b.ToString().Reverse().ToArray());
        }


        private void serialPort_ErrorReceived(object sender, System.IO.Ports.SerialErrorReceivedEventArgs e)
        {
            Action serialPortErrorPrint = () => exceptions.Add(new ObviousException(Enum.GetName(typeof(SerialError), e.EventType)));
            if (this.dataGridViewErrors.InvokeRequired)
            {
                this.dataGridViewErrors.Invoke(new MethodInvoker(delegate()
                    {
                        serialPortErrorPrint();
                    }));
            }
            else
            {
                serialPortErrorPrint();
            }            
        }

        private void serialPort_PinChanged(object sender, System.IO.Ports.SerialPinChangedEventArgs e)
        {
            //MessageBox.Show(Enum.GetName( typeof(SerialPinChange), e.EventType));
        }

        private void trackerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            splitContainer.Panel2Collapsed = !splitContainer.Panel2Collapsed;
        }

        private void PrintError(Exception ex)
        {
            exceptions.Add(ex);
            bindingSourceErrors.ResetBindings(false);

            try
            {
                Repository.NoQuery("WriteError", new List<SqlParameter> { 
                                new SqlParameter() { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@Message", SqlDbType = System.Data.SqlDbType.NVarChar, Size = 1000, Value = ex.Message},
                                new SqlParameter() { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@StackTrace", SqlDbType = System.Data.SqlDbType.NVarChar, Size = Int32.MaxValue, Value = (ex.StackTrace ?? string.Empty)},
                                new SqlParameter() { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@ExceptionType", SqlDbType = System.Data.SqlDbType.NVarChar, Size = 1000, Value = ex.GetType().FullName},
                                new SqlParameter() { DbType = System.Data.DbType.String, Direction = System.Data.ParameterDirection.Input, ParameterName = "@FullMessage", SqlDbType = System.Data.SqlDbType.NVarChar, Size = Int32.MaxValue, Value = ex.ToString()} });
            }
            catch(Exception ex1)
            {
                exceptions.Add(ex1);
            }
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            if (ex == null || (!(ex is ObviousException) &&
                ex.Message.Contains("Retrieving the COM class factory for component with CLSID")))
            {
                stateUserControl.SetRed();
            }
            if (e.ExceptionObject is Exception)
            {
                PrintError((Exception) e.ExceptionObject);
            }            
        }

        void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            if (!(e.Exception is ObviousException) &&
                !e.Exception.Message.Contains("Retrieving the COM class factory for component with CLSID"))
            {
                stateUserControl.SetRed();
            }
            
            PrintError(e.Exception);                       
        }


        private void MainForm_Load(object sender, EventArgs e)
        {

            if (!this.DesignMode)
            {
            		this.btnScanController.Enabled = false;
            		
                	if(!isReport)
                	{

                        serialPort = new SerialPort(portname, 9600, Parity.None, 8, StopBits.One);
                        Thread.Sleep(200);
                        serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
                        serialPort.RtsEnable = true;
                        serialPort.ReceivedBytesThreshold = 1;
                        serialPort.ReadTimeout = 20000;
                	}
                	else
                	{                		
                	}
                	
                	controller = new ArchiveController(this.reportUserControl, this.serialPort);
            		lvDevices.ValueMember = "Address";
                    lvDevices.DisplayMember = "DeviceName";
                    if (!isReport)
                    {
                        bindingSourceDevices.DataSource = controller.Controllers;
                        lvDevices.DataSource = bindingSourceDevices;
                        this.reportUserControl.Watcher.DataSent += new EventHandler<RemoteHumidityControl.Utility.EventArgs<LibmodbusCustomCommands.CommandEntity>>(Watcher_DataSent);
                    }                	
                	
                    controller.ControllerFounded += new EventHandler<EventArgs<SPM3DeviceInfo>>(controller_ControllerFounded);

                if (!isReport)
                    {       
            	        new Thread(new ThreadStart(delegate()
            	                                       {        
                                                           controller.OpenPort(portname); 
                                                           this.btnScanController.Invoke(new MethodInvoker(delegate()
                                                                {                                                                  
                                                                	this.btnScanController.Enabled = true;
                                                                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SerialPortName"]))
                                                                    {
                                                                        //controller.ScanDevices();                                                                        
                                                                    }
                                                                }));
                                                       })) { IsBackground = true}.Start();
                        
                    };
//                this.stateUserControl.Invoke(new MethodInvoker(delegate()
//                                                                {
//                 exceptions.Add(ex);
//                 stateUserControl.SetYellow();}));
            }            
            
        }

        
        void controller_ControllerFounded(object sender, EventArgs<SPM3DeviceInfo> e)
        {   
            this.lvDevices.Invoke(new MethodInvoker(delegate()
            {
        	      bindingSourceDevices.ResetBindings(false);
                this.reportUserControl.Address1 = controller.Controllers[0].Address;
                if (controller.Controllers.Count > 1)
                {
                    this.reportUserControl.Address2 = controller.Controllers[0].Address;
                }
            }));            
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void dataGridViewErrors_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        { 
            if (e.RowIndex > 0 && e.RowIndex < dataGridViewErrors.Rows.Count)
            {
                Color color = Color.Honeydew;
                e.CellStyle.BackColor = Color.LightGray;

                Type exceptionType = dataGridViewErrors.Rows[e.RowIndex].DataBoundItem.GetType();
                if (dataGridViewErrors.Rows[e.RowIndex].DataBoundItem is Exception &&
                    (dataGridViewErrors.Rows[e.RowIndex].DataBoundItem as Exception).Message.Contains("Retrieving the COM class factory for component with CLSID"))
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }
                else  if (exceptionType.Assembly != Assembly.GetExecutingAssembly())
                {
                    e.CellStyle.BackColor = Color.Red;
                }                
            }
        }

        private void serialPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.SelectSerialPort();
        }

        private void receiveDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.lvDevices.SelectedIndex != -1)
            {
                controller.RequestData((SPM3DeviceInfo)this.lvDevices.Items[this.lvDevices.SelectedIndex]);
                
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.DesignMode)
            {
                bool hasDevices = lvDevices.Items.Count > 0;
                if (hasDevices &&
                    MessageBox.Show(
                    "Вы действительно уверены что необходимо остановить запись данных?",
                    "Предупреждение",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (reportUserControl.Controller != null)
                    {
                        reportUserControl.Controller.watcher.Stop();
                    }
                    //if (serialPort != null && serialPort.IsOpen)
                    //{
                    //    serialPort.DiscardOutBuffer();
                    //    serialPort.Close();
                    //}
                }
                else
                {
                    e.Cancel = hasDevices;
                }
            }
        }

        private void cmsDeviceBounds_Opening(object sender, CancelEventArgs e)
        {

        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Visible = !this.Visible;
            if (this.Visible)
            {
                Show();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void SetControllerProgress()
        {
        	Action setProgress = () =>{ this.toolStripProgressBar1.Increment(1);
            if (this.toolStripProgressBar1.Value > 10)
            {
                this.toolStripProgressBar1.Increment(-this.toolStripProgressBar1.Value);
            };};
        	if(InvokeRequired)
        		this.Invoke(new MethodInvoker(delegate() {setProgress();}));
        	else
        	{
        		setProgress();
        	}
        		
            
        }
    }
}
