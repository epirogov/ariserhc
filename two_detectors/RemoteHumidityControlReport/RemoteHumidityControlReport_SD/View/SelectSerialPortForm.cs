﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RemoteHumidityControlReport.View
{
    public partial class SelectSerialPortForm : Form
    {
        public SelectSerialPortForm(string [] portNames)
        {
            InitializeComponent();
            lvSerialPortList.Items.AddRange(portNames.Select(portName => new ListViewItem(portName)).ToArray());
        }

        public string SelectedPort
        {
            get
            {
                return this.lvSerialPortList.SelectedItems[0].Text;
            }
        }
        
        private void SelectSerialPortForm_FormClosing(object sender, FormClosingEventArgs e)
        {            
            if (DialogResult != System.Windows.Forms.DialogResult.OK||
                this.lvSerialPortList.SelectedItems.Count == 0 && DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                DialogResult = DialogResult.Ignore;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

    }
}
