﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RemoteHumidityControlReport.VievInterface;

namespace RemoteHumidityControlReport.View
{
    public partial class StateUserControl : UserControl, ISemaphoreControl
    {
        private Color[] colors = new Color[3];
        public StateUserControl()
        {
            InitializeComponent();
            colors[0] = this.panel1.BackColor;
            colors[1] = this.panel2.BackColor;
            colors[2] = this.panel3.BackColor;
        }


        public void ResetOthers(int state)
        {
            Panel[] panels = new Panel[3] { this.panel1, this.panel2, this.panel3 };
            this.panel1.BackColor = colors[0];
            this.panel2.BackColor = colors[1];
            this.panel3.BackColor = colors[2];

            //panels.Where((value, i) => (i + 1) != state).ToList().ForEach(panel => panel.BackColor = SystemColors.Control);            
        }

        public void SetGreen()
        {            
            Action setState = () =>{
                ResetOthers(1);
                this.panel1.BackColor = Color.Green;                
            };
            if (this.panel1.InvokeRequired)
            {
                this.panel1.Invoke(new MethodInvoker(delegate
                {
                    setState();
                }));
            }
            else
            {
                setState();
            }
        }

        public void SetYellow()
        {
            Action setState = () =>
            {
                ResetOthers(2);
                this.panel2.BackColor = Color.Yellow;                
            };

            if (this.panel2.InvokeRequired)
            {
                this.panel2.Invoke(new MethodInvoker( delegate {
                setState(); }));
            }
            else
            {
                setState();
            }
        }

        public void SetRed()
        {
            Action setState = () => {
                ResetOthers(3);
                this.panel3.BackColor = Color.Red;
                
            };

            if (this.panel3.InvokeRequired)
            {
                this.panel3.Invoke(new MethodInvoker(delegate
                {
                    setState();
                }));
            }
            else
            {
                setState();
            }            
        }

        public void Reset()
        {
            SetGreen();
        }
    }
}
