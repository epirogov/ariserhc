﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RemoteHumidityControl.ViewInterface;
using RemoteHumidityControlPreview.Controller;
using RemoteHumidityControl.Logic.DeviceControl;
using RemoteHumidityControlReport.Logic.ViewEntities;
using RemoteHumidityControl.Entity;
using RemoteHumidityControlReport.Logic.Exceptions;
using LibraryBusinessLogic;
using System.Data.SqlClient;
using System.IO;
using RemoteHumidityControlReport.DataAccess;

namespace RemoteHumidityControlReport.View
{
    public partial class ReportUserControl : UserControl, IDataUserControl
    {
        private readonly List<ControllerGeneralDataEntity> dataEntities1 = new List<ControllerGeneralDataEntity>();
        private readonly List<ControllerGeneralDataEntity> dataEntities2 = new List<ControllerGeneralDataEntity>();

        public ReportUserControl()
        {
            InitializeComponent();
            bindingSource.DataSource = dataEntities1;
            bindingSource1.DataSource = dataEntities2;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.DataSource = bindingSource1;            
            this.dataGridView1.DataSource = bindingSource;
            this.dataGridView2.DataSource = bindingSource1;            
        }

        [Browsable(false)]
        public ArchiveController Controller
        {
            get;
            set;
        }

        [Browsable(false)]
        public HumidityWatcher Watcher
        {
            get
            {
                if (!this.DesignMode)
                {
                    return Controller.watcher;
                }
                else
                {
                    return null;
                }
            }
            set
            {
            }
        }

        public void BindData(IEnumerable<RemoteHumidityControl.Entity.ControllerGeneralDataEntity> datapack)
        {
            try
            {
                //ErrorNotifier.SetError(new ObviousException(datapack.FirstOrDefault().Address.ToString()));
                if (datapack.FirstOrDefault().Address == Controller.Controllers[0].Address)
                {
                    dataEntities1.InsertRange(0, datapack.Reverse());
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate() { bindingSource.ResetBindings(false); }));
                    }
                    else
                    {
                        bindingSource.ResetBindings(false);
                    }
                }
                else
                {
                    dataEntities2.InsertRange(0, datapack.Reverse());
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate() { bindingSource1.ResetBindings(false); }));
                    }
                    else
                    {
                        bindingSource1.ResetBindings(false);
                    }
                }

                if (dataEntities1.Count > 500)
                {
                    DateTime lastHour = dataEntities1.First().Date.AddHours(-1);

                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate() { dataEntities1.RemoveAll(entity => entity.Date < lastHour); bindingSource.ResetBindings(false); }));
                    }
                    else
                    {
                        bindingSource.ResetBindings(false);
                    }
                }

                if (dataEntities2.Count > 500)
                {
                    DateTime lastHour = dataEntities2.First().Date.AddHours(-1);

                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate() { dataEntities2.RemoveAll(entity => entity.Date < lastHour); bindingSource1.ResetBindings(false); }));
                    }
                    else
                    {
                        dataEntities2.RemoveAll(entity => entity.Date < lastHour);
                        bindingSource1.ResetBindings(false);
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorNotifier.SetError(ex);
            }
        }

        public void SetController(ArchiveController controller)
        {
            Controller = controller;
        }

        public bool ScanDevices()
        {
            return Controller.ScanDevices();
        }

        private void cbCurrent_CheckedChanged(object sender, EventArgs e)
        {
            this.gbInterval.Visible = !this.cbCurrent.Checked;
            if (this.gbInterval.Visible)
            {
                BindArchive();
            }
            else
            {
                bindingSource.DataSource = dataEntities1;
                bindingSource1.DataSource = dataEntities2;            
            }
        }

        private void BindArchive()
        {
            byte address1 = (byte)Address1;
            byte address2 = (byte)Address2;
            List<ControllerGeneralDataEntity> data = null;
            try
            {
                data = Repository.Select<ControllerGeneralDataEntity>("GetControllerData", new List<SqlParameter> { 
                                new SqlParameter() { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@DateFrom", SqlDbType = System.Data.SqlDbType.DateTime, Value = this.dateTimePicker1.Value},
                                new SqlParameter() { DbType = System.Data.DbType.DateTime, Direction = System.Data.ParameterDirection.Input, ParameterName = "@DateTo", SqlDbType = System.Data.SqlDbType.DateTime, Value = this.dateTimePicker2.Value}
                                 });
            }
            catch (Exception ex)
            {
                ErrorNotifier.SetError(ex);
            }

            if (data != null)
            {
                var entityGroups = data
                    .Where(entity => entity.Address == address1 || entity.Address == address2)
                    .OrderBy(entity => entity.Address).GroupBy(entity => entity.Address);
                if (entityGroups.Count() > 0)
                {
                    bindingSource.DataSource = entityGroups.First();
                }
                if (entityGroups.Count() > 1)
                {
                    bindingSource1.DataSource = entityGroups.Last();
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            BindArchive();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            BindArchive();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //  MessageBox.Show("Отчет по первому устройству?", "Отчет", MessageBoxButtons., MessageBoxIcon.Question) == DialogResult.Yes
            //StreamWriter writer = new StreamWriter(saveFileDialog1.FileName);


            //if(data.Count() == 0)
            //{
            //    writer.Close();
            //    return;
            //}
            //writer.Write(data.First().Address.ToString());
            //writer.Write("\r\n");
            //writer.Write("Влажность %, Температура Среды C, Время");
            //writer.Write("\r\n");
            //writer.Write(string.Join("\r\n", data.Select(entity => string.Format("{0},{1},{2}", entity.Humidity.ToString().Replace(",", "."), entity.Temperature.ToString().Replace(",", "."), entity.Date))));
            //writer.Close();

            ReportChoiceForm form = new ReportChoiceForm((int)this.nudAddress1.Value, (int)this.nudAddress2.Value );
            if(form.ShowDialog() == DialogResult.OK)
            {
                bool isLeft = form.IsLeft;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    IEnumerable<ControllerGeneralDataEntity> data = isLeft ? (IEnumerable<ControllerGeneralDataEntity>)bindingSource.DataSource : (IEnumerable<ControllerGeneralDataEntity>)bindingSource1.DataSource;
                    ReportWriter.WriteReport(saveFileDialog1.FileName, data);                    
                }
            }            
        }

        public int Address1 { get { return (int)nudAddress1.Value; } set { nudAddress1.Value = value; } }

        public int Address2 { get { return (int)nudAddress2.Value; } set { nudAddress2.Value = value; } }
        
        public void OnControllerRequest()
        {
            Program.mainForm.SetControllerProgress();
        }
    }
}
