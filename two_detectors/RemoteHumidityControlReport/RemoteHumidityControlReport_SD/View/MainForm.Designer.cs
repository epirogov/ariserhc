﻿namespace RemoteHumidityControlReport
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnScanController = new System.Windows.Forms.Button();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.reportUserControl = new RemoteHumidityControlReport.View.ReportUserControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpResponse = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.rtbData = new System.Windows.Forms.RichTextBox();
            this.tpRequest = new System.Windows.Forms.TabPage();
            this.rtbRequest = new System.Windows.Forms.RichTextBox();
            this.tpErrors = new System.Windows.Forms.TabPage();
            this.dataGridViewErrors = new System.Windows.Forms.DataGridView();
            this.cmsSystem = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.trackerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serialPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.базыДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.периодТекущихПоказанийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lvDevices = new System.Windows.Forms.ListBox();
            this.cmsDeviceBounds = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.receiveDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bindingSourceErrors = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceDevices = new System.Windows.Forms.BindingSource(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stateUserControl = new RemoteHumidityControlReport.View.StateUserControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpResponse.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tpRequest.SuspendLayout();
            this.tpErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewErrors)).BeginInit();
            this.cmsSystem.SuspendLayout();
            this.cmsDeviceBounds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDevices)).BeginInit();
            this.contextMenuStripNotify.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnScanController
            // 
            this.btnScanController.BackColor = System.Drawing.SystemColors.Control;
            this.btnScanController.Location = new System.Drawing.Point(12, 29);
            this.btnScanController.Name = "btnScanController";
            this.btnScanController.Size = new System.Drawing.Size(121, 23);
            this.btnScanController.TabIndex = 0;
            this.btnScanController.Text = "Сканировать";
            this.btnScanController.UseVisualStyleBackColor = false;
            this.btnScanController.Click += new System.EventHandler(this.btnScanController_Click);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(139, 1);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.reportUserControl);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer.Size = new System.Drawing.Size(641, 554);
            this.splitContainer.SplitterDistance = 277;
            this.splitContainer.TabIndex = 4;
            // 
            // reportUserControl
            // 
            this.reportUserControl.Address1 = 0;
            this.reportUserControl.Address2 = 0;
            this.reportUserControl.Controller = null;
            this.reportUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportUserControl.Location = new System.Drawing.Point(0, 0);
            this.reportUserControl.Name = "reportUserControl";
            this.reportUserControl.Size = new System.Drawing.Size(641, 277);
            this.reportUserControl.TabIndex = 0;
            this.reportUserControl.Watcher = null;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpResponse);
            this.tabControl1.Controls.Add(this.tpRequest);
            this.tabControl1.Controls.Add(this.tpErrors);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(641, 273);
            this.tabControl1.TabIndex = 0;
            // 
            // tpResponse
            // 
            this.tpResponse.Controls.Add(this.statusStrip1);
            this.tpResponse.Controls.Add(this.rtbData);
            this.tpResponse.Location = new System.Drawing.Point(4, 22);
            this.tpResponse.Name = "tpResponse";
            this.tpResponse.Padding = new System.Windows.Forms.Padding(3);
            this.tpResponse.Size = new System.Drawing.Size(633, 247);
            this.tpResponse.TabIndex = 0;
            this.tpResponse.Text = "Ответ";
            this.tpResponse.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(3, 222);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(627, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.MarqueeAnimationSpeed = 10;
            this.toolStripProgressBar1.Maximum = 10;
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar1.Step = 1;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(80, 17);
            this.toolStripStatusLabel1.Text = "Сканирование";
            // 
            // rtbData
            // 
            this.rtbData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbData.Location = new System.Drawing.Point(3, 3);
            this.rtbData.Name = "rtbData";
            this.rtbData.ReadOnly = true;
            this.rtbData.Size = new System.Drawing.Size(627, 241);
            this.rtbData.TabIndex = 5;
            this.rtbData.Text = "";
            // 
            // tpRequest
            // 
            this.tpRequest.Controls.Add(this.rtbRequest);
            this.tpRequest.Location = new System.Drawing.Point(4, 22);
            this.tpRequest.Name = "tpRequest";
            this.tpRequest.Padding = new System.Windows.Forms.Padding(3);
            this.tpRequest.Size = new System.Drawing.Size(633, 247);
            this.tpRequest.TabIndex = 1;
            this.tpRequest.Text = "Посылка";
            this.tpRequest.UseVisualStyleBackColor = true;
            // 
            // rtbRequest
            // 
            this.rtbRequest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbRequest.Location = new System.Drawing.Point(3, 3);
            this.rtbRequest.Name = "rtbRequest";
            this.rtbRequest.ReadOnly = true;
            this.rtbRequest.Size = new System.Drawing.Size(627, 241);
            this.rtbRequest.TabIndex = 0;
            this.rtbRequest.Text = "";
            // 
            // tpErrors
            // 
            this.tpErrors.Controls.Add(this.dataGridViewErrors);
            this.tpErrors.Location = new System.Drawing.Point(4, 22);
            this.tpErrors.Name = "tpErrors";
            this.tpErrors.Size = new System.Drawing.Size(633, 247);
            this.tpErrors.TabIndex = 2;
            this.tpErrors.Text = "Ошибки";
            this.tpErrors.UseVisualStyleBackColor = true;
            // 
            // dataGridViewErrors
            // 
            this.dataGridViewErrors.AllowUserToAddRows = false;
            this.dataGridViewErrors.AllowUserToDeleteRows = false;
            this.dataGridViewErrors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewErrors.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewErrors.Name = "dataGridViewErrors";
            this.dataGridViewErrors.ReadOnly = true;
            this.dataGridViewErrors.Size = new System.Drawing.Size(633, 247);
            this.dataGridViewErrors.TabIndex = 0;
            this.dataGridViewErrors.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewErrors_CellFormatting);
            // 
            // cmsSystem
            // 
            this.cmsSystem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trackerToolStripMenuItem,
            this.serialPortToolStripMenuItem,
            this.базыДанныхToolStripMenuItem,
            this.периодТекущихПоказанийToolStripMenuItem});
            this.cmsSystem.Name = "contextMenuStrip";
            this.cmsSystem.Size = new System.Drawing.Size(228, 92);
            // 
            // trackerToolStripMenuItem
            // 
            this.trackerToolStripMenuItem.Name = "trackerToolStripMenuItem";
            this.trackerToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.trackerToolStripMenuItem.Text = "Служебные данные";
            this.trackerToolStripMenuItem.Click += new System.EventHandler(this.trackerToolStripMenuItem_Click);
            // 
            // serialPortToolStripMenuItem
            // 
            this.serialPortToolStripMenuItem.Name = "serialPortToolStripMenuItem";
            this.serialPortToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.serialPortToolStripMenuItem.Text = "Порт";
            this.serialPortToolStripMenuItem.Click += new System.EventHandler(this.serialPortToolStripMenuItem_Click);
            // 
            // базыДанныхToolStripMenuItem
            // 
            this.базыДанныхToolStripMenuItem.Name = "базыДанныхToolStripMenuItem";
            this.базыДанныхToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.базыДанныхToolStripMenuItem.Text = "Базы данных";
            this.базыДанныхToolStripMenuItem.Visible = false;
            // 
            // периодТекущихПоказанийToolStripMenuItem
            // 
            this.периодТекущихПоказанийToolStripMenuItem.Name = "периодТекущихПоказанийToolStripMenuItem";
            this.периодТекущихПоказанийToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.периодТекущихПоказанийToolStripMenuItem.Text = "Период текущих показаний";
            this.периодТекущихПоказанийToolStripMenuItem.Visible = false;
            // 
            // lvDevices
            // 
            this.lvDevices.ContextMenuStrip = this.cmsDeviceBounds;
            this.lvDevices.FormattingEnabled = true;
            this.lvDevices.Location = new System.Drawing.Point(13, 77);
            this.lvDevices.Name = "lvDevices";
            this.lvDevices.Size = new System.Drawing.Size(120, 485);
            this.lvDevices.TabIndex = 5;
            // 
            // cmsDeviceBounds
            // 
            this.cmsDeviceBounds.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.receiveDataToolStripMenuItem});
            this.cmsDeviceBounds.Name = "cmsDeviceBounds";
            this.cmsDeviceBounds.Size = new System.Drawing.Size(150, 26);
            this.cmsDeviceBounds.Opening += new System.ComponentModel.CancelEventHandler(this.cmsDeviceBounds_Opening);
            // 
            // receiveDataToolStripMenuItem
            // 
            this.receiveDataToolStripMenuItem.Name = "receiveDataToolStripMenuItem";
            this.receiveDataToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.receiveDataToolStripMenuItem.Text = "Receive Data";
            this.receiveDataToolStripMenuItem.Visible = false;
            this.receiveDataToolStripMenuItem.Click += new System.EventHandler(this.receiveDataToolStripMenuItem_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStripNotify;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("с")));
            this.notifyIcon.Text = "Измерение влажности";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // contextMenuStripNotify
            // 
            this.contextMenuStripNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.contextMenuStripNotify.Name = "contextMenuStripNotify";
            this.contextMenuStripNotify.Size = new System.Drawing.Size(119, 26);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // stateUserControl
            // 
            this.stateUserControl.ContextMenuStrip = this.cmsSystem;
            this.stateUserControl.Location = new System.Drawing.Point(-3, 1);
            this.stateUserControl.Name = "stateUserControl";
            this.stateUserControl.Size = new System.Drawing.Size(108, 21);
            this.stateUserControl.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 558);
            this.Controls.Add(this.lvDevices);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.stateUserControl);
            this.Controls.Add(this.btnScanController);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowInTaskbar = false;
            this.Text = "Удаленный Отчет";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpResponse.ResumeLayout(false);
            this.tpResponse.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tpRequest.ResumeLayout(false);
            this.tpErrors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewErrors)).EndInit();
            this.cmsSystem.ResumeLayout(false);
            this.cmsDeviceBounds.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDevices)).EndInit();
            this.contextMenuStripNotify.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnScanController;
        private View.StateUserControl stateUserControl;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.RichTextBox rtbData;
        private System.Windows.Forms.TabPage tpRequest;
        private System.Windows.Forms.TabPage tpResponse;
        private System.Windows.Forms.TabPage tpErrors;
        private System.Windows.Forms.ContextMenuStrip cmsSystem;
        private System.Windows.Forms.ToolStripMenuItem trackerToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewErrors;
        private System.Windows.Forms.RichTextBox rtbRequest;
        private System.Windows.Forms.ToolStripMenuItem serialPortToolStripMenuItem;
        private View.ReportUserControl reportUserControl;
        private System.Windows.Forms.ListBox lvDevices;
        private System.Windows.Forms.ContextMenuStrip cmsDeviceBounds;
        private System.Windows.Forms.ToolStripMenuItem базыДанныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem периодТекущихПоказанийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiveDataToolStripMenuItem;
        private System.Windows.Forms.BindingSource bindingSourceErrors;
        private System.Windows.Forms.BindingSource bindingSourceDevices;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripNotify;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

