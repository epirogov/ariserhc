﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.03.2010
 * Time: 12:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of CursorPosition.
	/// </summary>
	public class CursorPosition : BaseCommandData
	{
		public CursorPosition(int posX, int posY)
		{
			this.PosX = posX;
			this.PosY = posY;
		}
		
		public int PosX
		{
			get;
			protected set;
		}
		
		public int PosY
		{
			get;
			protected set;
		}
	}
}
