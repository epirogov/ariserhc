﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteHumidityControl.Utility;

namespace RemoteHumidityControlReport.Logic.Exceptions
{
    public static class ErrorNotifier
    {
        public static event EventHandler<EventArgs<Exception>> OnError;

        public static void SetError(Exception ex)
        {
            if (OnError != null)
            {
                OnError(new Object(), new EventArgs<Exception>(ex));
            }
        }
    }
}
