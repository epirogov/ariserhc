﻿using RemoteHumidityControlPreview.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibmodbusCustomCommands;
using RemoteHumidityControl.Logic.DeviceControl;

namespace RemoteHumidityControl.ViewInterface
{
    public interface IDataUserControl
    {
        void BindData(IEnumerable<RemoteHumidityControl.Entity.ControllerGeneralDataEntity> datapack);
        HumidityWatcher Watcher
        {
            get;
            set;
        }
        void SetController(ArchiveController controller);
        bool ScanDevices();
        void OnControllerRequest();
        
    }
}
