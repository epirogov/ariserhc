﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteHumidityControlReport.VievInterface
{
    interface ISemaphoreControl
    {
        void SetGreen();
        void SetYellow();
        void SetRed();
        void ResetOthers(int state);
        void Reset();
    }
}
