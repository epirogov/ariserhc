﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 20:47
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO.Ports;
using STD4408Device.Controller;


namespace STD4408Device
{
	/// <summary>
	/// Description of IMainForm.
	/// </summary>
	public interface IMainForm
	{
			MainFormController Controller
			{
				get;
				set;
			}
			
			SerialPort COM1Port
			{
				get;		
			}
			
			SerialPort COM3Port
			{
				get;		
			}
			
			void SetComPort1(string comPort);
			void SetComPort3(string comPort);
			void ConnectComPorts();
			void DisconnectComPorts();
			void PrintSTD4408Input(string text);
			void SetShift();
			void UnsetShift();		
			void DisplayText(string text);
			void EnableCursor(int selectionStart);
			void DisableCursor();
			void SetTestText(string text);
			void SetNoData(int address);
            void SetHasData(int address);

            void IndicateData(int address);            

	}
}
