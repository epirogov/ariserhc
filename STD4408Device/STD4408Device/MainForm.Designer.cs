﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.02.2010
 * Time: 17:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace STD4408Device
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.rtbScreen = new System.Windows.Forms.RichTextBox();
            this.lbLeftMetrics = new System.Windows.Forms.Label();
            this.lbTopMetrics = new System.Windows.Forms.Label();
            this.btn7F4 = new System.Windows.Forms.Button();
            this.btn8Up = new System.Windows.Forms.Button();
            this.btn9F5 = new System.Windows.Forms.Button();
            this.btnMultiply = new System.Windows.Forms.Button();
            this.btnMinus = new System.Windows.Forms.Button();
            this.btn6Rg = new System.Windows.Forms.Button();
            this.btn5F3 = new System.Windows.Forms.Button();
            this.btn4Lf = new System.Windows.Forms.Button();
            this.btnPlus = new System.Windows.Forms.Button();
            this.btn3F2 = new System.Windows.Forms.Button();
            this.btn2Dw = new System.Windows.Forms.Button();
            this.btn1F1 = new System.Windows.Forms.Button();
            this.btnEqual = new System.Windows.Forms.Button();
            this.btnEnter = new System.Windows.Forms.Button();
            this.btn0Star = new System.Windows.Forms.Button();
            this.btnShift = new System.Windows.Forms.Button();
            this.pSTD4408Case = new System.Windows.Forms.Panel();
            this.cbSendThreeKeys = new System.Windows.Forms.CheckBox();
            this.pSTD4408InputTop = new System.Windows.Forms.Panel();
            this.pSTD4408InputBottom = new System.Windows.Forms.Panel();
            this.lbSTD4408Input = new System.Windows.Forms.Label();
            this.pSTD4408CableBase = new System.Windows.Forms.Panel();
            this.pSTD4408Cable6 = new System.Windows.Forms.Panel();
            this.pSTD4408Cable8 = new System.Windows.Forms.Panel();
            this.pSTD4408Cable3 = new System.Windows.Forms.Panel();
            this.pSTD4408Cable7 = new System.Windows.Forms.Panel();
            this.pSTD4408Cable5 = new System.Windows.Forms.Panel();
            this.pSTD4408Cable2 = new System.Windows.Forms.Panel();
            this.pSTD4408Cable1 = new System.Windows.Forms.Panel();
            this.pSTD4408Cable4 = new System.Windows.Forms.Panel();
            this.pR485Connector = new System.Windows.Forms.Panel();
            this.btnSwitchCOMPortsState = new System.Windows.Forms.Button();
            this.pConnectSTD4408Vertical = new System.Windows.Forms.Panel();
            this.pConnectSTD4408Horisontal = new System.Windows.Forms.Panel();
            this.pConnectScreen = new System.Windows.Forms.Panel();
            this.serialPortCOM1 = new System.IO.Ports.SerialPort(this.components);
            this.serialPortCOM3 = new System.IO.Ports.SerialPort(this.components);
            this.btnCopyDisplay = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbCOM3 = new System.Windows.Forms.ComboBox();
            this.cbCOM1 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbSensorData1 = new System.Windows.Forms.RadioButton();
            this.rbSensorData2 = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pSTD4408Case.SuspendLayout();
            this.pSTD4408CableBase.SuspendLayout();
            this.pR485Connector.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbScreen
            // 
            this.rtbScreen.BackColor = System.Drawing.Color.AliceBlue;
            this.rtbScreen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbScreen.Font = new System.Drawing.Font("Lucida Console", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbScreen.Location = new System.Drawing.Point(51, 41);
            this.rtbScreen.Name = "rtbScreen";
            this.rtbScreen.ReadOnly = true;
            this.rtbScreen.Size = new System.Drawing.Size(336, 513);
            this.rtbScreen.TabIndex = 0;
            this.rtbScreen.Text = "";
            this.rtbScreen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbScreen_KeyPress);
            // 
            // lbLeftMetrics
            // 
            this.lbLeftMetrics.Font = new System.Drawing.Font("Lucida Console", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbLeftMetrics.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbLeftMetrics.Location = new System.Drawing.Point(-15, 43);
            this.lbLeftMetrics.Name = "lbLeftMetrics";
            this.lbLeftMetrics.Size = new System.Drawing.Size(60, 511);
            this.lbLeftMetrics.TabIndex = 1;
            this.lbLeftMetrics.Text = "1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n10\r\n11\r\n12\r\n13\r\n14\r\n15\r\n16\r\n";
            this.lbLeftMetrics.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbTopMetrics
            // 
            this.lbTopMetrics.BackColor = System.Drawing.SystemColors.Control;
            this.lbTopMetrics.Font = new System.Drawing.Font("Lucida Console", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbTopMetrics.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbTopMetrics.Location = new System.Drawing.Point(51, 9);
            this.lbTopMetrics.Name = "lbTopMetrics";
            this.lbTopMetrics.Size = new System.Drawing.Size(336, 31);
            this.lbTopMetrics.TabIndex = 2;
            this.lbTopMetrics.Text = "12345678901234567890";
            // 
            // btn7F4
            // 
            this.btn7F4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn7F4.Location = new System.Drawing.Point(3, 26);
            this.btn7F4.Name = "btn7F4";
            this.btn7F4.Size = new System.Drawing.Size(62, 37);
            this.btn7F4.TabIndex = 3;
            this.btn7F4.Tag = "Seven_F4";
            this.btn7F4.Text = "7F4";
            this.btn7F4.UseVisualStyleBackColor = true;
            // 
            // btn8Up
            // 
            this.btn8Up.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn8Up.Location = new System.Drawing.Point(71, 26);
            this.btn8Up.Name = "btn8Up";
            this.btn8Up.Size = new System.Drawing.Size(62, 37);
            this.btn8Up.TabIndex = 4;
            this.btn8Up.Tag = "Eight_Up";
            this.btn8Up.Text = "8Up";
            this.btn8Up.UseVisualStyleBackColor = true;
            // 
            // btn9F5
            // 
            this.btn9F5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn9F5.Location = new System.Drawing.Point(139, 26);
            this.btn9F5.Name = "btn9F5";
            this.btn9F5.Size = new System.Drawing.Size(62, 37);
            this.btn9F5.TabIndex = 5;
            this.btn9F5.Tag = "Nine_F5";
            this.btn9F5.Text = "9F5";
            this.btn9F5.UseVisualStyleBackColor = true;
            // 
            // btnMultiply
            // 
            this.btnMultiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnMultiply.Location = new System.Drawing.Point(206, 26);
            this.btnMultiply.Name = "btnMultiply";
            this.btnMultiply.Size = new System.Drawing.Size(62, 37);
            this.btnMultiply.TabIndex = 6;
            this.btnMultiply.Tag = "Multiply";
            this.btnMultiply.Text = "X";
            this.btnMultiply.UseVisualStyleBackColor = true;
            // 
            // btnMinus
            // 
            this.btnMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnMinus.Location = new System.Drawing.Point(206, 69);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(62, 37);
            this.btnMinus.TabIndex = 10;
            this.btnMinus.Tag = "Minus";
            this.btnMinus.Text = "-";
            this.btnMinus.UseVisualStyleBackColor = true;
            // 
            // btn6Rg
            // 
            this.btn6Rg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn6Rg.Location = new System.Drawing.Point(139, 69);
            this.btn6Rg.Name = "btn6Rg";
            this.btn6Rg.Size = new System.Drawing.Size(62, 37);
            this.btn6Rg.TabIndex = 9;
            this.btn6Rg.Tag = "Six_Right";
            this.btn6Rg.Text = "6Rg";
            this.btn6Rg.UseVisualStyleBackColor = true;
            // 
            // btn5F3
            // 
            this.btn5F3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn5F3.Location = new System.Drawing.Point(71, 69);
            this.btn5F3.Name = "btn5F3";
            this.btn5F3.Size = new System.Drawing.Size(62, 37);
            this.btn5F3.TabIndex = 8;
            this.btn5F3.Tag = "Five_F3";
            this.btn5F3.Text = "5F3";
            this.btn5F3.UseVisualStyleBackColor = true;
            // 
            // btn4Lf
            // 
            this.btn4Lf.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn4Lf.Location = new System.Drawing.Point(3, 69);
            this.btn4Lf.Name = "btn4Lf";
            this.btn4Lf.Size = new System.Drawing.Size(62, 37);
            this.btn4Lf.TabIndex = 7;
            this.btn4Lf.Tag = "Four_Left";
            this.btn4Lf.Text = "4Lf";
            this.btn4Lf.UseVisualStyleBackColor = true;
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPlus.Location = new System.Drawing.Point(206, 112);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(62, 37);
            this.btnPlus.TabIndex = 14;
            this.btnPlus.Tag = "Plus";
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            // 
            // btn3F2
            // 
            this.btn3F2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn3F2.Location = new System.Drawing.Point(139, 112);
            this.btn3F2.Name = "btn3F2";
            this.btn3F2.Size = new System.Drawing.Size(62, 37);
            this.btn3F2.TabIndex = 13;
            this.btn3F2.Tag = "Three_F2";
            this.btn3F2.Text = "3F2";
            this.btn3F2.UseVisualStyleBackColor = true;
            // 
            // btn2Dw
            // 
            this.btn2Dw.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn2Dw.Location = new System.Drawing.Point(71, 112);
            this.btn2Dw.Name = "btn2Dw";
            this.btn2Dw.Size = new System.Drawing.Size(62, 37);
            this.btn2Dw.TabIndex = 12;
            this.btn2Dw.Tag = "Two_Down";
            this.btn2Dw.Text = "2Dn";
            this.btn2Dw.UseVisualStyleBackColor = true;
            // 
            // btn1F1
            // 
            this.btn1F1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn1F1.Location = new System.Drawing.Point(3, 112);
            this.btn1F1.Name = "btn1F1";
            this.btn1F1.Size = new System.Drawing.Size(62, 37);
            this.btn1F1.TabIndex = 11;
            this.btn1F1.Tag = "One_F1";
            this.btn1F1.Text = "1F1";
            this.btn1F1.UseVisualStyleBackColor = true;
            // 
            // btnEqual
            // 
            this.btnEqual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEqual.Location = new System.Drawing.Point(206, 155);
            this.btnEqual.Name = "btnEqual";
            this.btnEqual.Size = new System.Drawing.Size(62, 37);
            this.btnEqual.TabIndex = 18;
            this.btnEqual.Tag = "Equal";
            this.btnEqual.Text = "=";
            this.btnEqual.UseVisualStyleBackColor = true;
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEnter.Location = new System.Drawing.Point(139, 155);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(62, 37);
            this.btnEnter.TabIndex = 17;
            this.btnEnter.Tag = "Enter";
            this.btnEnter.Text = "Enter";
            this.btnEnter.UseVisualStyleBackColor = true;
            // 
            // btn0Star
            // 
            this.btn0Star.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn0Star.Location = new System.Drawing.Point(71, 155);
            this.btn0Star.Name = "btn0Star";
            this.btn0Star.Size = new System.Drawing.Size(62, 37);
            this.btn0Star.TabIndex = 16;
            this.btn0Star.Tag = "Zero_Star";
            this.btn0Star.Text = "0*";
            this.btn0Star.UseVisualStyleBackColor = true;
            // 
            // btnShift
            // 
            this.btnShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShift.Location = new System.Drawing.Point(3, 155);
            this.btnShift.Name = "btnShift";
            this.btnShift.Size = new System.Drawing.Size(62, 37);
            this.btnShift.TabIndex = 15;
            this.btnShift.Tag = "Shift";
            this.btnShift.Text = "Shift";
            this.btnShift.UseVisualStyleBackColor = true;
            // 
            // pSTD4408Case
            // 
            this.pSTD4408Case.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pSTD4408Case.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSTD4408Case.Controls.Add(this.cbSendThreeKeys);
            this.pSTD4408Case.Controls.Add(this.pSTD4408InputTop);
            this.pSTD4408Case.Controls.Add(this.pSTD4408InputBottom);
            this.pSTD4408Case.Controls.Add(this.lbSTD4408Input);
            this.pSTD4408Case.Controls.Add(this.pSTD4408CableBase);
            this.pSTD4408Case.Controls.Add(this.btnShift);
            this.pSTD4408Case.Controls.Add(this.btnEqual);
            this.pSTD4408Case.Controls.Add(this.btn7F4);
            this.pSTD4408Case.Controls.Add(this.btnEnter);
            this.pSTD4408Case.Controls.Add(this.btn8Up);
            this.pSTD4408Case.Controls.Add(this.btn0Star);
            this.pSTD4408Case.Controls.Add(this.btn9F5);
            this.pSTD4408Case.Controls.Add(this.btnMultiply);
            this.pSTD4408Case.Controls.Add(this.btnPlus);
            this.pSTD4408Case.Controls.Add(this.btn4Lf);
            this.pSTD4408Case.Controls.Add(this.btn3F2);
            this.pSTD4408Case.Controls.Add(this.btn5F3);
            this.pSTD4408Case.Controls.Add(this.btn2Dw);
            this.pSTD4408Case.Controls.Add(this.btn6Rg);
            this.pSTD4408Case.Controls.Add(this.btn1F1);
            this.pSTD4408Case.Controls.Add(this.btnMinus);
            this.pSTD4408Case.Location = new System.Drawing.Point(402, 191);
            this.pSTD4408Case.Name = "pSTD4408Case";
            this.pSTD4408Case.Size = new System.Drawing.Size(272, 366);
            this.pSTD4408Case.TabIndex = 19;
            // 
            // cbSendThreeKeys
            // 
            this.cbSendThreeKeys.Location = new System.Drawing.Point(7, 237);
            this.cbSendThreeKeys.Name = "cbSendThreeKeys";
            this.cbSendThreeKeys.Size = new System.Drawing.Size(117, 33);
            this.cbSendThreeKeys.TabIndex = 27;
            this.cbSendThreeKeys.Text = "Send Three Keys";
            this.cbSendThreeKeys.UseVisualStyleBackColor = true;
            this.cbSendThreeKeys.CheckedChanged += new System.EventHandler(this.CbSendThreeKeysCheckedChanged);
            // 
            // pSTD4408InputTop
            // 
            this.pSTD4408InputTop.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pSTD4408InputTop.Location = new System.Drawing.Point(64, 208);
            this.pSTD4408InputTop.Name = "pSTD4408InputTop";
            this.pSTD4408InputTop.Size = new System.Drawing.Size(200, 2);
            this.pSTD4408InputTop.TabIndex = 26;
            // 
            // pSTD4408InputBottom
            // 
            this.pSTD4408InputBottom.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pSTD4408InputBottom.Location = new System.Drawing.Point(64, 228);
            this.pSTD4408InputBottom.Name = "pSTD4408InputBottom";
            this.pSTD4408InputBottom.Size = new System.Drawing.Size(200, 2);
            this.pSTD4408InputBottom.TabIndex = 25;
            // 
            // lbSTD4408Input
            // 
            this.lbSTD4408Input.Location = new System.Drawing.Point(64, 211);
            this.lbSTD4408Input.Name = "lbSTD4408Input";
            this.lbSTD4408Input.Size = new System.Drawing.Size(203, 23);
            this.lbSTD4408Input.TabIndex = 21;
            this.lbSTD4408Input.Text = "[Input]";
            this.lbSTD4408Input.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pSTD4408CableBase
            // 
            this.pSTD4408CableBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable6);
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable8);
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable3);
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable7);
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable5);
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable2);
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable1);
            this.pSTD4408CableBase.Controls.Add(this.pSTD4408Cable4);
            this.pSTD4408CableBase.Location = new System.Drawing.Point(85, 332);
            this.pSTD4408CableBase.Name = "pSTD4408CableBase";
            this.pSTD4408CableBase.Size = new System.Drawing.Size(107, 34);
            this.pSTD4408CableBase.TabIndex = 20;
            // 
            // pSTD4408Cable6
            // 
            this.pSTD4408Cable6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable6.Location = new System.Drawing.Point(66, 9);
            this.pSTD4408Cable6.Name = "pSTD4408Cable6";
            this.pSTD4408Cable6.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable6.TabIndex = 23;
            // 
            // pSTD4408Cable8
            // 
            this.pSTD4408Cable8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable8.Location = new System.Drawing.Point(86, 9);
            this.pSTD4408Cable8.Name = "pSTD4408Cable8";
            this.pSTD4408Cable8.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable8.TabIndex = 23;
            // 
            // pSTD4408Cable3
            // 
            this.pSTD4408Cable3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable3.Location = new System.Drawing.Point(36, 9);
            this.pSTD4408Cable3.Name = "pSTD4408Cable3";
            this.pSTD4408Cable3.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable3.TabIndex = 22;
            // 
            // pSTD4408Cable7
            // 
            this.pSTD4408Cable7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable7.Location = new System.Drawing.Point(76, 9);
            this.pSTD4408Cable7.Name = "pSTD4408Cable7";
            this.pSTD4408Cable7.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable7.TabIndex = 22;
            // 
            // pSTD4408Cable5
            // 
            this.pSTD4408Cable5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable5.Location = new System.Drawing.Point(56, 9);
            this.pSTD4408Cable5.Name = "pSTD4408Cable5";
            this.pSTD4408Cable5.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable5.TabIndex = 22;
            // 
            // pSTD4408Cable2
            // 
            this.pSTD4408Cable2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable2.Location = new System.Drawing.Point(26, 9);
            this.pSTD4408Cable2.Name = "pSTD4408Cable2";
            this.pSTD4408Cable2.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable2.TabIndex = 21;
            // 
            // pSTD4408Cable1
            // 
            this.pSTD4408Cable1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable1.Location = new System.Drawing.Point(16, 9);
            this.pSTD4408Cable1.Name = "pSTD4408Cable1";
            this.pSTD4408Cable1.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable1.TabIndex = 20;
            // 
            // pSTD4408Cable4
            // 
            this.pSTD4408Cable4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pSTD4408Cable4.Location = new System.Drawing.Point(46, 9);
            this.pSTD4408Cable4.Name = "pSTD4408Cable4";
            this.pSTD4408Cable4.Size = new System.Drawing.Size(2, 40);
            this.pSTD4408Cable4.TabIndex = 21;
            // 
            // pR485Connector
            // 
            this.pR485Connector.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pR485Connector.BackColor = System.Drawing.Color.Yellow;
            this.pR485Connector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pR485Connector.Controls.Add(this.btnSwitchCOMPortsState);
            this.pR485Connector.Location = new System.Drawing.Point(623, 43);
            this.pR485Connector.Name = "pR485Connector";
            this.pR485Connector.Size = new System.Drawing.Size(64, 63);
            this.pR485Connector.TabIndex = 21;
            // 
            // btnSwitchCOMPortsState
            // 
            this.btnSwitchCOMPortsState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSwitchCOMPortsState.BackColor = System.Drawing.Color.YellowGreen;
            this.btnSwitchCOMPortsState.ForeColor = System.Drawing.Color.Coral;
            this.btnSwitchCOMPortsState.Location = new System.Drawing.Point(14, 16);
            this.btnSwitchCOMPortsState.Name = "btnSwitchCOMPortsState";
            this.btnSwitchCOMPortsState.Size = new System.Drawing.Size(36, 31);
            this.btnSwitchCOMPortsState.TabIndex = 0;
            this.btnSwitchCOMPortsState.Text = "o";
            this.btnSwitchCOMPortsState.UseVisualStyleBackColor = false;
            this.btnSwitchCOMPortsState.Click += new System.EventHandler(this.BtnSwitchCOMPortsStateClick);
            // 
            // pConnectSTD4408Vertical
            // 
            this.pConnectSTD4408Vertical.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pConnectSTD4408Vertical.BackColor = System.Drawing.Color.Yellow;
            this.pConnectSTD4408Vertical.Location = new System.Drawing.Point(576, 88);
            this.pConnectSTD4408Vertical.Name = "pConnectSTD4408Vertical";
            this.pConnectSTD4408Vertical.Size = new System.Drawing.Size(2, 104);
            this.pConnectSTD4408Vertical.TabIndex = 22;
            // 
            // pConnectSTD4408Horisontal
            // 
            this.pConnectSTD4408Horisontal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pConnectSTD4408Horisontal.BackColor = System.Drawing.Color.Yellow;
            this.pConnectSTD4408Horisontal.Location = new System.Drawing.Point(576, 88);
            this.pConnectSTD4408Horisontal.Name = "pConnectSTD4408Horisontal";
            this.pConnectSTD4408Horisontal.Size = new System.Drawing.Size(50, 2);
            this.pConnectSTD4408Horisontal.TabIndex = 23;
            // 
            // pConnectScreen
            // 
            this.pConnectScreen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pConnectScreen.BackColor = System.Drawing.Color.Yellow;
            this.pConnectScreen.Location = new System.Drawing.Point(387, 58);
            this.pConnectScreen.Name = "pConnectScreen";
            this.pConnectScreen.Size = new System.Drawing.Size(240, 2);
            this.pConnectScreen.TabIndex = 24;
            // 
            // serialPortCOM3
            // 
            this.serialPortCOM3.PortName = "COM3";
            this.serialPortCOM3.ReadTimeout = 100;
            // 
            // btnCopyDisplay
            // 
            this.btnCopyDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyDisplay.Location = new System.Drawing.Point(402, 164);
            this.btnCopyDisplay.Name = "btnCopyDisplay";
            this.btnCopyDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnCopyDisplay.TabIndex = 25;
            this.btnCopyDisplay.Text = "Copy Dispay";
            this.btnCopyDisplay.UseVisualStyleBackColor = true;
            this.btnCopyDisplay.Click += new System.EventHandler(this.BtnCopyDisplayClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(393, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 18);
            this.label1.TabIndex = 26;
            this.label1.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.OliveDrab;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cbCOM3);
            this.panel1.Controls.Add(this.cbCOM1);
            this.panel1.Location = new System.Drawing.Point(623, 112);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(68, 51);
            this.panel1.TabIndex = 27;
            // 
            // cbCOM3
            // 
            this.cbCOM3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCOM3.DropDownWidth = 60;
            this.cbCOM3.FormattingEnabled = true;
            this.cbCOM3.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15",
            "COM16",
            "COM17",
            "COM19",
            "COM20"});
            this.cbCOM3.Location = new System.Drawing.Point(-1, 28);
            this.cbCOM3.MaxDropDownItems = 20;
            this.cbCOM3.Name = "cbCOM3";
            this.cbCOM3.Size = new System.Drawing.Size(64, 21);
            this.cbCOM3.TabIndex = 29;
            this.cbCOM3.SelectedIndexChanged += new System.EventHandler(this.CbCOM2SelectedIndexChanged);
            // 
            // cbCOM1
            // 
            this.cbCOM1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCOM1.DropDownWidth = 60;
            this.cbCOM1.FormattingEnabled = true;
            this.cbCOM1.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15",
            "COM16",
            "COM17",
            "COM19",
            "COM20"});
            this.cbCOM1.Location = new System.Drawing.Point(-1, 3);
            this.cbCOM1.MaxDropDownItems = 20;
            this.cbCOM1.Name = "cbCOM1";
            this.cbCOM1.Size = new System.Drawing.Size(64, 21);
            this.cbCOM1.TabIndex = 28;
            this.cbCOM1.SelectedIndexChanged += new System.EventHandler(this.CbCOM1SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.OliveDrab;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(623, 106);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(68, 10);
            this.panel2.TabIndex = 28;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Yellow;
            this.panel3.Location = new System.Drawing.Point(421, 60);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(2, 47);
            this.panel3.TabIndex = 29;
            // 
            // rbSensorData1
            // 
            this.rbSensorData1.BackColor = System.Drawing.SystemColors.Desktop;
            this.rbSensorData1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbSensorData1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSensorData1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbSensorData1.Location = new System.Drawing.Point(413, 101);
            this.rbSensorData1.Name = "rbSensorData1";
            this.rbSensorData1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.rbSensorData1.Size = new System.Drawing.Size(18, 24);
            this.rbSensorData1.TabIndex = 30;
            this.rbSensorData1.TabStop = true;
            this.rbSensorData1.UseVisualStyleBackColor = false;
            // 
            // rbSensorData2
            // 
            this.rbSensorData2.BackColor = System.Drawing.SystemColors.Desktop;
            this.rbSensorData2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbSensorData2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSensorData2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbSensorData2.Location = new System.Drawing.Point(437, 101);
            this.rbSensorData2.Name = "rbSensorData2";
            this.rbSensorData2.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.rbSensorData2.Size = new System.Drawing.Size(18, 24);
            this.rbSensorData2.TabIndex = 31;
            this.rbSensorData2.TabStop = true;
            this.rbSensorData2.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Yellow;
            this.panel4.Location = new System.Drawing.Point(445, 60);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(2, 47);
            this.panel4.TabIndex = 32;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 566);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.rbSensorData2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCopyDisplay);
            this.Controls.Add(this.pConnectScreen);
            this.Controls.Add(this.pConnectSTD4408Horisontal);
            this.Controls.Add(this.pConnectSTD4408Vertical);
            this.Controls.Add(this.pR485Connector);
            this.Controls.Add(this.pSTD4408Case);
            this.Controls.Add(this.lbTopMetrics);
            this.Controls.Add(this.lbLeftMetrics);
            this.Controls.Add(this.rtbScreen);
            this.Controls.Add(this.rbSensorData1);
            this.Name = "MainForm";
            this.Text = "TM-3 Emulator";
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.pSTD4408Case.ResumeLayout(false);
            this.pSTD4408CableBase.ResumeLayout(false);
            this.pR485Connector.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		private System.Windows.Forms.RadioButton rbSensorData1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.ComboBox cbCOM1;
		private System.Windows.Forms.ComboBox cbCOM3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnCopyDisplay;
		private System.Windows.Forms.CheckBox cbSendThreeKeys;
		private System.IO.Ports.SerialPort serialPortCOM3;
		private System.IO.Ports.SerialPort serialPortCOM1;
		private System.Windows.Forms.Button btn0Star;
		private System.Windows.Forms.Button btn7F4;
		private System.Windows.Forms.Button btn8Up;
		private System.Windows.Forms.Button btn9F5;
		private System.Windows.Forms.Button btnMultiply;
		private System.Windows.Forms.Button btnMinus;
		private System.Windows.Forms.Button btn6Rg;
		private System.Windows.Forms.Button btn5F3;
		private System.Windows.Forms.Button btn4Lf;
		private System.Windows.Forms.Button btnPlus;
		private System.Windows.Forms.Button btn3F2;
		private System.Windows.Forms.Button btn2Dw;
		private System.Windows.Forms.Button btn1F1;
		private System.Windows.Forms.Button btnEqual;
		private System.Windows.Forms.Button btnEnter;
		private System.Windows.Forms.Button btnShift;
		private System.Windows.Forms.Panel pSTD4408Case;
		private System.Windows.Forms.Panel pSTD4408CableBase;
		private System.Windows.Forms.Panel pSTD4408Cable4;
		private System.Windows.Forms.Panel pSTD4408Cable1;
		private System.Windows.Forms.Panel pSTD4408Cable8;
		private System.Windows.Forms.Panel pSTD4408Cable7;
		private System.Windows.Forms.Panel pSTD4408Cable6;
		private System.Windows.Forms.Panel pSTD4408Cable3;
		private System.Windows.Forms.Panel pSTD4408Cable5;
		private System.Windows.Forms.Panel pSTD4408Cable2;
		private System.Windows.Forms.Panel pR485Connector;
		private System.Windows.Forms.Label lbSTD4408Input;
		private System.Windows.Forms.Panel pSTD4408InputTop;
		private System.Windows.Forms.Panel pSTD4408InputBottom;
		private System.Windows.Forms.Button btnSwitchCOMPortsState;
		private System.Windows.Forms.RichTextBox rtbScreen;
		private System.Windows.Forms.Label lbLeftMetrics;
		private System.Windows.Forms.Label lbTopMetrics;
		private System.Windows.Forms.Panel pConnectSTD4408Vertical;
		private System.Windows.Forms.Panel pConnectSTD4408Horisontal;
        private System.Windows.Forms.Panel pConnectScreen;
        private System.Windows.Forms.RadioButton rbSensorData2;
        private System.Windows.Forms.Panel panel4;
	}
}
