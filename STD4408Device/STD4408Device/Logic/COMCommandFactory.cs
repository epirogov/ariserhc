﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.03.2010
 * Time: 13:07
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Linq;
using System.Text;
using LibmodbusCustomCommands;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of COMCommandFactory.
	/// </summary>
	public static class COMCommandFactory
	{		
		public static KeyboardScan BuildKeyboardScan(byte [] rawdata)
		{
			return new KeyboardScan();
		}
		
		public static CursorPosition BuildSetCursor(byte [] rawdata)
		{
			return  new CursorPosition(rawdata[2], rawdata[3]);
		}
		
		public static PrintedText BuildPrintXY(byte [] rawdata)
		{
			byte[] stringArray = rawdata.ToList().GetRange(4, rawdata.Length - 6).ToArray();
			string text = Encoding.GetEncoding(1251).GetString(stringArray);						
			return new PrintedText(text, rawdata[2], rawdata[3]);
		}
		
		public static ClrScr BuildClrScr(byte [] rawdata)
		{
			return new ClrScr();
		}
		
		public unsafe static CommandEntity BuildCustomCommand(byte [] rawdata)
		{
			fixed(byte * array = new byte[rawdata.Length])
			{
				for(int i = 0; i < rawdata.Length; i++)
				{
					array[i] = rawdata[i];
				}
				return CommandParser.Parse(array, (uint)rawdata.Length);			
			}
		}
	}
}
