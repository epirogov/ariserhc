﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.04.2010
 * Time: 13:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of CancelledEventArgs.
	/// </summary>
	public class CancelledEventArgs<TValue> : EventArgs<TValue>
	{
		public CancelledEventArgs(TValue value) : base (value)
		{
			this.Cancelled = false;
		}
		
		public bool Cancelled
		{
			get;
			set;
		}
	}
}
