﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 01.03.2010
 * Time: 22:04
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of IKeyScenarioState.
	/// </summary>
	public interface IKeyScenarioState
	{
		STD4408MainFormStateController Controller
		{
			get;
			set;			
		}
		
		KeyScenarioStateEnum StateValue
		{
			get;			
		}
		
		void SetOneKeyScenario();
		void SetThreeKeyScenario();
	}
}
