﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 22:05
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	public class EventArgs<TValue1> : EventArgs
	{
	    public TValue1 Value1 { get; set; }
	    public EventArgs(TValue1 value1)
	    {
	        Value1 = value1;
	    }
	}
}
