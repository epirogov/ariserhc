﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 19:45
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of STD4408MainFormStateController.
	/// </summary>
	public class STD4408MainFormStateController
	{
		
		private ISTD4408MainFormState currentCOMPortsState;
		private IKeyScenarioState currentKeyScenarioState;
				
		public event EventHandler<CancelledEventArgs<STD4408StateEnum>> NewCOMPortsStateEvent;
		public event EventHandler<EventArgs<KeyScenarioStateEnum>> NewKeyScenarioStateEvent;
		
		public STD4408MainFormStateController()
		{
			
		}
		
		public void Initialize(STD4408StateEnum initCOMPortsState, KeyScenarioStateEnum initKeyScenarioState)
		{
			switch(initCOMPortsState)
			{
				case STD4408StateEnum.ConnectedCOMPorts:
					SetCOMPortsState<ConnectedCOMPortsState>();
					break;
				case STD4408StateEnum.DisconnectedCOMPorts:
					SetCOMPortsState<DisconnectedCOMPortsState>();
					break;
				default:
					break;
			}
			
			switch(initKeyScenarioState)
			{
				case KeyScenarioStateEnum.OneKey:
					SetKeyScenarioState<OneKeyScenarioState>();
					break;
				case KeyScenarioStateEnum.ThreeKey:
					SetKeyScenarioState<ThreeKeyScenarioState>();
					break;
				default:
					break;
			}
		}
		
		public STD4408StateEnum COMPortsState
		{
			get
			{
				return currentCOMPortsState.StateValue;
			}
		}
		
		public KeyScenarioStateEnum KeyScenarioState
		{
			get
			{
				return currentKeyScenarioState.StateValue;
			}
		}
		
		public void SetNextSate()
		{
			switch(currentCOMPortsState.StateValue)
			{
				case STD4408StateEnum.ConnectedCOMPorts :
					SetCOMPortsState<DisconnectedCOMPortsState>();
					break;
				case STD4408StateEnum.DisconnectedCOMPorts :
					SetCOMPortsState<ConnectedCOMPortsState>();
					break;
				default:
					break;
			}
		}
		
		public void SetCOMPortsState<T>() where T : ISTD4408MainFormState, new()
		{
			var oldstate = this.currentCOMPortsState;
			this.currentCOMPortsState = new T() {Controller = this } ;
			if(this.NewCOMPortsStateEvent != null)
			{
				var e = new CancelledEventArgs<STD4408StateEnum>(this.currentCOMPortsState.StateValue);
				this.NewCOMPortsStateEvent(this, e);
				if(e.Cancelled)
				{
					this.currentCOMPortsState = oldstate;
					var e1 = new CancelledEventArgs<STD4408StateEnum>(this.currentCOMPortsState.StateValue);
					this.NewCOMPortsStateEvent(this, e1);
				}
			}
		}
		
		public void SetKeyScenarioState<T>() where T : IKeyScenarioState, new()
		{
			this.currentKeyScenarioState = new T() {Controller = this } ;
			if(this.NewKeyScenarioStateEvent != null)
			{
				this.NewKeyScenarioStateEvent(this, new EventArgs<KeyScenarioStateEnum>(this.currentKeyScenarioState.StateValue));
			}
		}
		
		public void SetOneKeyScenario()
		{
			this.currentKeyScenarioState.SetOneKeyScenario();
		}
		
		public void SetThreeKeyScenario()
		{
			this.currentKeyScenarioState.SetThreeKeyScenario();
		}
	}
}
