﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.03.2010
 * Time: 12:44
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of COMSygnalAnalyser.
	/// </summary>
	public static class COMSignalAnalyser
	{
		public static COMSignalTypeEnum Detect(byte [] rawData)
		{
			if(rawData[0] == 0x2 && rawData[1] == 0x44 && rawData[2] == 0x0 && rawData[3] == 0x30 && rawData[4] == 0x1)
			{
				return COMSignalTypeEnum.KeyboardScan;
			}
			
			if(rawData[0] == 0x1 && rawData[1] == 0x44)
			{
				return COMSignalTypeEnum.GetRegistersGeneral;
			}
			
			if(rawData[0] == 0x2 && rawData[1] == 0x67)
			{
				return COMSignalTypeEnum.SetCursor;
			}
			
			if(rawData[0] == 0x2 && rawData[1] == 0x68)
			{
				return COMSignalTypeEnum.PrintXY;
			}			
			
			if(rawData[0] == 0x2 && rawData[1] == 0x66)
			{
				return COMSignalTypeEnum.ClrScr;
			}
				
			return COMSignalTypeEnum.Unknown;
		}
	}
}
