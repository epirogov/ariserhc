﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 20:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of BaseCOMPortsState.
	/// </summary>
	public class BaseCOMPortsState
	{
		public BaseCOMPortsState()
		{
		}
		
		public STD4408MainFormStateController Controller {
			get;
			set;
		}
		
		public STD4408StateEnum StateValue {
			get;
			private set;
		}
		
		protected void CreateInternals(STD4408MainFormStateController controller, STD4408StateEnum stateValue)
		{
			this.Controller = controller;			
			this.StateValue = stateValue;
		}
	}
}
