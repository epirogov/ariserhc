﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 20:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace STD4408Device.Logic
{
	/// <summary>
	/// Description of ConnectedCOMPortsState.
	/// </summary>
	public class ConnectedCOMPortsState : BaseCOMPortsState, ISTD4408MainFormState
	{
		public ConnectedCOMPortsState()
		{			
			CreateInternals(null);
		}
		
		public ConnectedCOMPortsState(STD4408MainFormStateController controller)
		{			
			CreateInternals(controller);
		}	
		
		public void ConnectCOMPorts()
		{			
		}
		
		public void DisconnectCOMPorts()
		{
			this.Controller.SetCOMPortsState<DisconnectedCOMPortsState>();
		}
		
		private void CreateInternals(STD4408MainFormStateController controller)
		{
			base.CreateInternals(controller, STD4408StateEnum.ConnectedCOMPorts);			
		}
	}
}
