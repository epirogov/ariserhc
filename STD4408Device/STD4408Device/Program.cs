﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.02.2010
 * Time: 17:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Linq;
using System.Windows.Forms;

namespace STD4408Device
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			byte [] data = new byte [14] {205, 229, 242, 32, 231, 224, 239, 238, 235, 237, 229, 237, 232, 255};
			string dataStr = UnicodeEncoding.Unicode.GetString(data);
			string dataStr1 = Convert.ToBase64String(data);
			string dataStr2 = ASCIIEncoding.ASCII.GetString(data);
			
			//&#1053;&#1077;&#1090;&#32;&#1079;&#1072;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1103;
			byte [] data1 = new Byte [6] {0x1D, 0x4, 0x35, 0x4, 0x42, 0x4};
			string dataStr3 = UnicodeEncoding.Unicode.GetString(data1);
			string dataStr4 = "Нет";
			byte [] data2 = UnicodeEncoding.Unicode.GetBytes(dataStr4);
			byte [] data3 = Encoding.Convert(UnicodeEncoding.Unicode, ASCIIEncoding.ASCII, data1);
			string dataStr5 = UnicodeEncoding.Unicode.GetString(data);
			string UnicodeStr = Encoding.GetEncoding(1251).GetString(data,0,data.Length);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
		
	}
}
