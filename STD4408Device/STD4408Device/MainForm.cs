﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 27.02.2010
 * Time: 17:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using STD4408Device.Logic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Windows.Forms;
using STD4408Device.Controller;
using System.ComponentModel;
using System.Threading;

namespace STD4408Device
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form, IMainForm
	{
        private Dictionary<int, RadioButton> sensors;
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

            sensors = new Dictionary<int, RadioButton>();
            sensors.Add(1, rbSensorData1);
            sensors.Add(2, rbSensorData2);

			this.Controller = new MainFormController(this);		
			
			InitializeSTD4408Buttons();

            
		}
		
		
		
		public MainFormController Controller {
			get;
			set;
		}
		
		public SerialPort COM1Port
		{
			get
			{
				return this.serialPortCOM1;
			}
		}
		
		public SerialPort COM3Port
		{
			get
			{
				return this.serialPortCOM3;
			}
		}
		
		public void ConnectComPorts()
		{
			this.cbCOM1.Enabled = false;
			this.cbCOM3.Enabled = false;
			this.btnSwitchCOMPortsState.BackColor = Color.YellowGreen;
		}
		
		public void DisconnectComPorts()
		{
			this.SetHasData(1);
            this.SetHasData(2);
            this.cbCOM1.Enabled = true;
			this.cbCOM3.Enabled = true;
			this.btnSwitchCOMPortsState.BackColor = Color.Gray;
		}
		
		public void PrintSTD4408Input(string text)
		{
			this.lbSTD4408Input.Text += text;
		}
		
		public void SetShift()
		{			
			this.btnShift.BackColor = SystemColors.ButtonHighlight;
		}
		
		public void UnsetShift()
		{
			this.btnShift.BackColor = SystemColors.ButtonFace;
		}
		
		public void InitializeSTD4408Buttons()
		{
			foreach(Control button in this.pSTD4408Case.Controls)
			{
				if(Enum.IsDefined(typeof(STD4408KeyEnum),button.Tag ?? string.Empty))
				{
					button.Click += new EventHandler(STD4408Button_Click);
				}
			}
		}
		
		private Button performedButton;

		void STD4408Button_Click(object sender, EventArgs e)
		{
            PerformSTD4008Button((Button)sender);
		}

        private void PerformSTD4008Button(Button button)
        {
            if (this.performedButton != null)
            {
                this.OnButtonPerformed();

            }

            Button buttonToPerform = button;
            buttonToPerform.BackColor = Color.Blue;
            this.performedButton = buttonToPerform;

            this.Controller.PerformSTD4408Key((STD4408KeyEnum)Enum.Parse(typeof(STD4408KeyEnum), (string)(button.Tag)));
        }
		
		private void OnButtonPerformed()
		{
			if(this.performedButton != null)
			{
				ControlInvokeDelegate func = delegate()
				{
					this.performedButton.BackColor = SystemColors.Control;
				};
				
				if(this.performedButton.InvokeRequired)
				{
					this.performedButton.Invoke(new MethodInvoker(func));
				}
				else
				{
					func();
				}
			}
		}
		
		void BtnSwitchCOMPortsStateClick(object sender, EventArgs e)
		{			
			this.Controller.PerformSwitchCOMPorts();
		}		
		
		void CbSendThreeKeysCheckedChanged(object sender, EventArgs e)
		{
			if(this.cbSendThreeKeys.Checked)
			{
				this.Controller.SetThreeKeyScenario();
			}
			else
			{
				this.Controller.SetOneKeyScenario();
			}
		}
		
		public void DisplayText(string text)
		{
			ControlInvokeDelegate func = delegate()
			{
				this.rtbScreen.Text = text;						
				SetSelection();
			};
			
			if(this.rtbScreen.InvokeRequired)
			{
				this.rtbScreen.Invoke(new MethodInvoker(func));
			}
			else
			{
				func();
			}
		}
		
		private int selectionStart;
		private Color initialSelectetBackColor;
		
		private void SetSelection()
		{				
			this.rtbScreen.SuspendLayout();
			this.rtbScreen.SelectionStart = 0;
			this.rtbScreen.SelectionLength = this.rtbScreen.Text.Length;
			this.rtbScreen.SelectionBackColor = this.initialSelectetBackColor;
			
			this.label1.Text = string.Format("{0} {1}", (selectionStart % 22).ToString(), (selectionStart / 22).ToString());
			this.rtbScreen.SelectionStart = this.selectionStart;			                      	
			this.rtbScreen.SelectionLength = 1;
			this.rtbScreen.SelectionBackColor = Color.Blue;			                        
			this.rtbScreen.ResumeLayout();
		}
		
		delegate void ControlInvokeDelegate();
		public void EnableCursor(int selectionStart)
		{
			this.selectionStart = selectionStart;
			
			ControlInvokeDelegate func = () =>
			{
				SetSelection();
			};
			
			if(this.rtbScreen.InvokeRequired)
			{
				this.rtbScreen.Invoke(new MethodInvoker(func));
			}
			else
			{
				func();
			}
		}
		
		public void DisableCursor()
		{
			this.selectionStart = 0;
			this.rtbScreen.Invoke(new MethodInvoker(delegate ()
			                      {
									SetSelection();
			                                        })
			                     );
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			this.initialSelectetBackColor = this.rtbScreen.SelectionBackColor;
			this.Controller.OnLoad();
			this.Controller.ButtonPerformed += new EventHandler<EventArgs>(MainForm_ButtonPerformed);
			
			cbCOM1.SelectedItem = this.cbCOM1.Items[0];			
			cbCOM3.SelectedItem = this.cbCOM3.Items[2];
			//this.Controller.ChangeComPort1((string)this.cbCOM1.Items[0]);
			//this.Controller.ChangeComPort3((string)this.cbCOM2.Items[0]);
		}

		void MainForm_ButtonPerformed(object sender, EventArgs e)
		{
			this.OnButtonPerformed();
		}
		
		
		void BtnCopyDisplayClick(object sender, EventArgs e)
		{			
			string test = (this.rtbScreen.Text.Replace("\n", "\r\n"));
			try
			{
					Clipboard.SetText(test);
			}
			catch(Exception)
			{
				try
				{
					Clipboard.SetText(test);
				}
				catch(Exception)
				{
				
				}
			}
		}
		
		public void SetComPort1(string comPort)
		{
			this.serialPortCOM1.PortName = comPort;
		}
		
		public void SetComPort3(string comPort)
		{
			this.serialPortCOM3.PortName = comPort;
		}
		
		void CbCOM1SelectedIndexChanged(object sender, EventArgs e)
		{
			this.Controller.ChangeComPort1(this.cbCOM1.Text);			
		}
		
		void CbCOM2SelectedIndexChanged(object sender, EventArgs e)
		{
			this.Controller.ChangeComPort3(this.cbCOM3.Text);
		}
		
		public void SetTestText(string text)
		{
			ControlInvokeDelegate func = () =>
			{
				this.label1.Text = text;
			};
			
			if(this.rtbScreen.InvokeRequired)
			{
				this.rtbScreen.Invoke(new MethodInvoker(func));
			}
			else
			{
				func();
			}			
		}
		
		public void SetNoData(int address)
		{
            if (sensors.ContainsKey(address))
                sensors[address].BackColor = Color.ForestGreen;
		}

        public void SetHasData(int address)
		{            
            if (sensors.ContainsKey(address))
                sensors[address].BackColor = Color.Azure;
		}

        public void IndicateData(int address)
        {
            if (sensors.ContainsKey(address))
                sensors[address].BackColor = Color.Tan;
            BackgroundWorker b = new BackgroundWorker();
            b.DoWork += (s, e) => { Thread.Sleep(500); };
            b.RunWorkerCompleted += (s, e) => { sensors[address].BackColor = Color.Azure; };
            b.RunWorkerAsync();
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {   
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {        
        }

        private void rtbScreen_KeyPress(object sender, KeyPressEventArgs e)
        {
            STD4408KeyEnum key = STD4408KeyEnum.None;
            e.Handled = true;            
            switch (e.KeyChar)
            {
                case '0':
                    PerformSTD4008Button(this.btn0Star);
                    key = STD4408KeyEnum.Zero_Star;
                    break;
                case '1':
                    PerformSTD4008Button(this.btn1F1);
                    key = STD4408KeyEnum.One_F1;
                    break;
                case '2':
                    PerformSTD4008Button(this.btn2Dw);
                    key = STD4408KeyEnum.Two_Down;
                    break;
                case '3':
                    PerformSTD4008Button(this.btn3F2);
                    key = STD4408KeyEnum.Three_F2;
                    break;
                case '4':
                    PerformSTD4008Button(this.btn4Lf);
                    key = STD4408KeyEnum.Four_Left;
                    break;
                case '5':
                    PerformSTD4008Button(this.btn5F3);
                    key = STD4408KeyEnum.Five_F3;
                    break;
                case '6':
                    PerformSTD4008Button(this.btn6Rg);
                    key = STD4408KeyEnum.Six_Right;
                    break;
                case '7':
                    PerformSTD4008Button(this.btn7F4);
                    key = STD4408KeyEnum.Seven_F4;
                    break;
                case '8':
                    PerformSTD4008Button(this.btn8Up);
                    key = STD4408KeyEnum.Eight_Up;
                    break;
                case '9':
                    PerformSTD4008Button(this.btn9F5);
                    key = STD4408KeyEnum.Nine_F5;
                    break;
                case 's':
                    PerformSTD4008Button(this.btnShift);
                    key = STD4408KeyEnum.Shift;
                    break;
                case 'e':
                    PerformSTD4008Button(this.btnEnter);
                    key = STD4408KeyEnum.Enter;
                    break;
                case '=':
                    PerformSTD4008Button(this.btnEqual);
                    key = STD4408KeyEnum.Equal;
                    break;
                case '+':
                    PerformSTD4008Button(this.btnPlus);
                    key = STD4408KeyEnum.Plus;
                    break;
                case '-':
                    PerformSTD4008Button(this.btnMinus);
                    key = STD4408KeyEnum.Minus;
                    break;
                case '*':
                    PerformSTD4008Button(this.btnMultiply);
                    key = STD4408KeyEnum.Multiply;
                    break;
            }
        }
	}
}
