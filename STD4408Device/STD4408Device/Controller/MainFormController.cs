﻿/*
 * Created by SharpDevelop.
 * User: Eugene
 * Date: 28.02.2010
 * Time: 20:36
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using LibmodbusCustomCommands;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Windows.Forms;
using STD4408Device.Logic;

namespace STD4408Device.Controller
{
	/// <summary>
	/// Description of MainFormController.
	/// </summary>
	public class MainFormController
	{
		const int DISPLAY_WIDTH = 20;
		const int DISPLAY_HEIGHT = 16;
		
		private STD4408KeyEnum oneKey = STD4408KeyEnum.None;
		private List<STD4408KeyEnum> keys = new List<STD4408KeyEnum>();
		
		private STD4408MainFormStateController formStateController;
		private STD4408Adapter keyboardAdapter;
		private bool prepareToSend;
		string displayText;
		
		CommandResponseFactory responseReader;
        CommandResponseFactory responseReaderSeccond;
		
		public event EventHandler<EventArgs> ButtonPerformed;
		public unsafe MainFormController(IMainForm view)
		{	
			ModbusProvider.CreateTables();
            
            InitializeReader();

			this.keyboardAdapter = new STD4408Adapter(view.COM3Port);
			this.View = view;
			this.formStateController = new STD4408MainFormStateController();
			this.formStateController.NewCOMPortsStateEvent += new EventHandler<CancelledEventArgs<STD4408StateEnum>>(MainFormController_NewCOMPortsStateEvent);
			this.formStateController.NewKeyScenarioStateEvent += new EventHandler<EventArgs<KeyScenarioStateEnum>>(MainFormController_NewKeyScenarioStateEvent);
			this.formStateController.Initialize(STD4408StateEnum.DisconnectedCOMPorts, KeyScenarioStateEnum.OneKey);						
			
			this.keyboardAdapter.KeyboardScaning += new EventHandler<EventArgs<KeyboardScan>>(MainFormController_KeyboardScaning);
			this.keyboardAdapter.PrintXY += new EventHandler<EventArgs<PrintedText>>(MainFormController_PrintXY);
			this.keyboardAdapter.SetCursor += new EventHandler<EventArgs<CursorPosition>>(MainFormController_SetCursor);
			this.keyboardAdapter.ClrScr += new EventHandler<EventArgs<ClrScr>>(MainFormController_ClrScr);
			this.keyboardAdapter.CustomCommand += new EventHandler<EventArgs<CommandEntity>>(MainFormController_CustomCommand);
		}

        private void InitializeReader()
        {
            string path = ConfigurationManager.AppSettings["SensorResponseFile_1"];
            CheckFiles(path);
            if (!string.IsNullOrEmpty(path))
                this.responseReader = CreateResponseReader(path);
            else
                this.responseReader = new CommandResponseFactory(6000);

            path = ConfigurationManager.AppSettings["SensorResponseFile_2"];
            CheckFiles(path);
            if (!string.IsNullOrEmpty(path))
                this.responseReaderSeccond = CreateResponseReader(path);
            else
                this.responseReaderSeccond = new CommandResponseFactory(6000);
            this.responseReader.address = 1;
            this.responseReaderSeccond.address = 2;
        }
        
        
        private void CheckFiles(string path)
        {
        	var dir = Path.GetDirectoryName(path);
            if(Directory.Exists(dir) && !File.Exists(path))
            {
            	var s = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Format("STD4408Device.Resources.{0}", Path.GetFileName(path)));
            	var m = new BinaryReader(s);
            	File.WriteAllBytes(path,m.ReadBytes((int)s.Length) );
            	s.Close();
            }
        }
        

        private CommandResponseFactory CreateResponseReader(string path)
        {
            CommandResponseFactory reader;
            unsafe
            {
                fixed (sbyte* sensorfilepath = new sbyte[path.Length])
                {
                    for (int i = 0; i < path.Length; i++)
                    {
                        sensorfilepath[i] = Convert.ToSByte(path[i]);
                    }

                    reader = new CommandResponseFactory(sensorfilepath);
                    reader.NoData += new CommandResponseFactory.NoDataEventHandler(MainFormController_NoData);
                    
                }
            }

            return reader;
        }

		void MainFormController_NoData(object sender, EventArgs e)
		{
            this.View.SetNoData(((CommandResponseFactory)sender).address);			
		}

		unsafe void MainFormController_CustomCommand(object sender, EventArgs<CommandEntity> e)
		{
			//int startAddress = e.Value1.data[0] * 256 + e.Value1.data[1];
			//int registersCount = e.Value1.data[2];
			
			//this.View.SetTestText(string.Format("{0}, {1}",e.Value1.data[0] * 256 + e.Value1.data[1], e.Value1.data[2]));			

            
			CommandEntity responseCommand = this.responseReader.Build(e.Value1);
            if (responseCommand == null)
            {                
                return;
            }
               
			var val = ModbusProvider.table_crc_hi;
            byte[] array;
            int rawresponselength = 0;
            fixed (byte* rawresponse = new byte[256])
            {                
                responseCommand.Build(rawresponse, &rawresponselength);
                array = new byte[rawresponselength];
                for (int i = 0; i < rawresponselength; i++)
                {
                    array[i] = rawresponse[i];
                }
            }            

			this.keyboardAdapter.SendRawData(array);			
			StringBuilder builder = new StringBuilder();
			for(int i =0; i < array.Length; i++)
			{
				builder.Append(array[i]);
				builder.Append(" ");
			}
			//MessageBox.Show(builder.ToString());
			this.View.SetTestText(builder.ToString());

            this.View.IndicateData(e.Value1.GetAddress());
			//this.View.SetTestText(array.Length.ToString());
		}
		
		public void OnLoad()
		{
			this.ClrScr();
		}

		void MainFormController_SetCursor(object sender, EventArgs<CursorPosition> e)
		{
			int selectedStart = (DISPLAY_WIDTH + 1) * e.Value1.PosY +  e.Value1.PosX;
			this.View.EnableCursor(selectedStart);
		}

		void MainFormController_PrintXY(object sender, EventArgs<PrintedText> e)
		{			
			this.PasteText(e.Value1);
			this.View.DisplayText(this.displayText);
		}
		
		void MainFormController_ClrScr(object sender, EventArgs<ClrScr> e)
		{
			this.ClrScr();
			this.ClrScr();
		}		
		
		void ClrScr()
		{
			this.InitializeDisplay();
			this.View.DisplayText(this.displayText);
		}
		
		void InitializeDisplay()
		{
			StringBuilder displayTextBuilder = new StringBuilder();
			
			for(int i = 0; i < DISPLAY_HEIGHT; i++)
			{
				for(int j = 0; j < DISPLAY_WIDTH; j++)
				{
					displayTextBuilder.Append(" ");
				}
				if(i < DISPLAY_HEIGHT - 1)
				{
					displayTextBuilder.Append(Environment.NewLine);
				}
			}
			this.displayText = displayTextBuilder.ToString();
		}
		
		
		//int maxLength = 20;
		void PasteText(PrintedText printedText)
		{	
			/*
			if(maxLength < printedText.Text.Length + printedText.PosX)
			{
				maxLength = printedText.Text.Length + printedText.PosX;
				MessageBox.Show((printedText.Text.Length + printedText.PosX).ToString());				
			}
			*/
			
			StringBuilder dispalayTextBuilder = new StringBuilder();
			int startPosition = printedText.PosX + printedText.PosY * (DISPLAY_WIDTH + Environment.NewLine.Length);
			dispalayTextBuilder.Append(this.displayText.Substring(0, startPosition));
			dispalayTextBuilder.Append(printedText.Text);
			dispalayTextBuilder.Append(this.displayText.Substring(startPosition + printedText.Text.Length, this.displayText.Length - (startPosition + printedText.Text.Length)));
			/*
			if(this.displayText.Length != dispalayTextBuilder.ToString().Length)
			{
				throw new Exception();
			}
			*/
			
			this.displayText = dispalayTextBuilder.ToString();
			
		}

		void MainFormController_KeyboardScaning(object sender, EventArgs<KeyboardScan> e)
		{
			bool performed = false;
			if(this.formStateController.KeyScenarioState == KeyScenarioStateEnum.OneKey)
			{
				this.keyboardAdapter.SendKey(this.oneKey);
				if(this.oneKey != STD4408KeyEnum.None)
				{
					oneKey = STD4408KeyEnum.None;
					performed = true;
				}
			}
			else
			{
				if(keys.Count == 3)
				{
					this.keyboardAdapter.SendThreeKeys(keys[0], keys[1], keys[2]);
					keys.Clear();
					performed = true;
				}
				else
				{
					prepareToSend = true;
				}
			}
			
			if(performed && this.ButtonPerformed != null)
			{
				this.ButtonPerformed(this, new EventArgs());
			}			
		}

		
		
		void MainFormController_NewKeyScenarioStateEvent(object sender, EventArgs<KeyScenarioStateEnum> e)
		{
			keys.Clear();
		}
		
		public IMainForm View
		{
			get;
			set;
		}

		void MainFormController_NewCOMPortsStateEvent(object sender, CancelledEventArgs<STD4408StateEnum> e)
		{
			switch(e.Value1)
			{
				case STD4408StateEnum.ConnectedCOMPorts:
					e.Cancelled = !this.keyboardAdapter.StartCOMPort();
					this.View.ConnectComPorts();
					this.responseReader.Open();
                    this.responseReaderSeccond.Open();
					break;
				case STD4408StateEnum.DisconnectedCOMPorts:
					this.keyboardAdapter.StopCOMPort();
					this.View.DisconnectComPorts();
					this.responseReader.Close();
                    this.responseReaderSeccond.Close();
					break;
				default:
					break;
			}
		}
		
		public void PerformSTD4408Key(STD4408KeyEnum key)
		{
			if(this.formStateController.COMPortsState == STD4408StateEnum.ConnectedCOMPorts)
			{
				if (this.formStateController.KeyScenarioState == KeyScenarioStateEnum.OneKey)
				{
					this.oneKey = key;
				}
				else
				{
					if(keys.Count == 3)
					{						
						if(this.prepareToSend)
						{
							this.keyboardAdapter.SendThreeKeys(keys[0], keys[1], keys[2]);
							this.prepareToSend = false;
						}
						keys.Clear();
					}
					keys.Add(key);					
				}
			}
		}
		
		public void PerformSwitchCOMPorts()
		{			
			this.formStateController.SetNextSate();
		}
		
		public void SetOneKeyScenario()
		{
			this.formStateController.SetOneKeyScenario();
		}
		
		public void SetThreeKeyScenario()
		{
			this.formStateController.SetThreeKeyScenario();
		}
		
		public void ChangeComPort1(string comPort)
		{
			this.View.SetComPort1(comPort);			
		}
		
		public void ChangeComPort3(string comPort)
		{
			this.View.SetComPort3(comPort);
		}
	}
}
