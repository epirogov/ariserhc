#include "StdAfx.h"
#include <iostream>
#include <string.h>
#include "CommandResponseFactory.h"
#include "CustomFunctionMapping.h"
#include "CMODEEnum.h"
#include "ReadBufferEnum.h"
#include "SpeedEnum.h"
#include "SMP3RegisterEnum.h"
#include "Spm3ModmusMaster.h"
#include "CommandFactory.h"
#include "ControllerTypeEnum.h"

using namespace std;
using namespace System;


using namespace LibmodbusCustomCommands;

CommandResponseFactory::CommandResponseFactory(const char * filename)
{
	commandFactory = gcnew CommandFactory();	
}

CommandResponseFactory::CommandResponseFactory(int dataDuration)
{
	this->dataDuration = dataDuration;
	this->dataDurationSpan = gcnew TimeSpan(dataDuration * 10);
	commandFactory = gcnew CommandFactory();	
}

CommandResponseFactory::~CommandResponseFactory()
{
    Close();
}

void CommandResponseFactory::Open()
{
	//NoData(this, gcnew EventArgs());
	//this->noData = true;
}

void CommandResponseFactory::Close()
{	
}

CommandEntity ^ CommandResponseFactory::Build(CommandEntity ^ request)
{
	//throw gcnew Exception("Register controller" + request->GetFunction().ToString());
    CommandEntity ^ response;
    System::Byte * data = NULL;
    int datalength = 0;
    switch(request->GetFunction())
    {
        case SMP3_CMODE :
            if(request->HasSubfunction() && request->datalength == 0)
            {
                switch(request->GetSubFunction())
                {
				case CMODEEnum::normal :
				case CMODEEnum::oneCycle:
				case CMODEEnum::reset_mcu:
                        response = gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, NULL, 0, 0);
                        break;
                    default:
                        response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_value, true, NULL, 0, 0);
                        break;
                }
            }
            else
            {
                response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_address, true, NULL, 0, 0);
            }
            break;
        case SMP3_READ_BUFFER :
            if(request->HasSubfunction() && request->datalength == 0)
            {
                switch(request->GetSubFunction())
                {
				case ReadBufferEnum::channel0:
				case ReadBufferEnum::channel1:
                        datalength = 128;
                        data = new System::Byte[datalength];
                        memset(data, 0, datalength);
                        response = gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, data, datalength, 0);
                        break;
                    default:
                        response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_value, true, NULL, 0, 0);
                        break;
                }
            }
            else
            {
                response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_address, true, NULL, 0, 0);
            }
            break;
        case SMP3_GET_REGISTERS :
            if(request->datalength == 3)
            {
                System::Byte startAddressHight = request->data[0];
                System::Byte startAddressLow = request->data[1];
                System::Byte count = request->data[2];
                short startAddress = 0;
                System::Byte realBytesCount = 0;
                System::Byte * startData = NULL;

                datalength = 34;
                data = new System::Byte[datalength * 2];

                Spm3ModbusMaster::CalculateStartAddressAndSizeRegisters(data, startAddressHight, startAddressLow, count, startAddress, realBytesCount, startData);

                if(startAddress + request->data[2] <= 30)
                {
					int * channeldata = new int[datalength - 4];
                    //float * channeldata = new float[datalength - 4];
					int count = 0;

					/*this->noData = true;
					this->NoData(this, gcnew EventArgs());*/
					

                    int index = 0;
                    for(int i =0; i < datalength - 4; i++)
                    {
                        //cout << i << ":" << channeldata[i] << endl;
                        if(i < 23 || i > 26)
                        {
                            data[index * 2] = (((int)channeldata[i] >> 8) & 0xFF);
                            data[index * 2 + 1] = ((int)channeldata[i] & 0xFF);
                            //cout << index * 2 << " : " << (int)data[index * 2] << endl;
                            //cout << index * 2 + 1 << " : " << (int)data[index * 2 + 1] << endl;
                            index++;
                        }
                        else
                        {
                            data[index * 2] = ((int)channeldata[i] >> 24) & 0xFF;
                            data[index * 2 + 1] = ((int)channeldata[i] >> 16) & 0xFF;
                            data[index * 2 + 2] = ((int)channeldata[i] >> 8) & 0xFF;
                            data[index * 2 + 3] = (int)channeldata[i] & 0xFF;

                            //cout << index * 2 << " : " << (int)data[index * 2] << endl;
                            //cout << index * 2 + 1 << " : " << (int)data[index * 2 + 1] << endl;
                            //cout << index * 2 + 2 << " : " << (int)data[index * 2 + 2] << endl;
                            //cout << index * 2 + 3 << " : " << (int)data[index * 2 + 3] << endl;
                            index+=2;
                        }
                    }

                    response = gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, startData, realBytesCount, 0);
                }
                else
                {
                    response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_value, true, NULL, 0, 0);
                }
            }
            else
            {
                response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_address, true, NULL, 0, 0);
            }
            break;
        case SMP3_SET_REGISTERS :
            if(request->datalength >= 5 && !((request->datalength - 3) & 0x01))
            {
                System::Byte startAddressHight = request->data[0];
                System::Byte startAddressLow = request->data[1];
                System::Byte count = request->data[2];
                short startAddress = 0;
                System::Byte realBytesCount = 0;
                System::Byte * startData = NULL;

                Spm3ModbusMaster::CalculateStartAddressAndSizeRegisters(request->data + 3, startAddressHight, startAddressLow, count, startAddress, realBytesCount, startData);

                if(request->data[0] *0xFF + request->data[1] + request->data[2] <= 30 && request->datalength - 3 == realBytesCount)
                {
                    response = gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, NULL, 0, 0);
                }
                else
                {
                    response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_value, true, NULL, 0, 0);
                }
            }
            else
            {
                response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_address, true, NULL, 0, 0);
            }
            break;
        case SMP3_WRITE_EEPROM :
            if(request->datalength == 0)
            {
                response = gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, NULL, 0, 0);
            }
            else
            {
                response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_address, true, NULL, 0, 0);
            }
            break;
        case SMP3_SET_SPEED :
            if(request->HasSubfunction() && request->datalength == 0)
            {
                switch(request->GetSubFunction())
                {
				case SpeedEnum::Speed_1200:
				case SpeedEnum::Speed_1800:
				case SpeedEnum::Speed_2400:
				case SpeedEnum::Speed_4800:
				case SpeedEnum::Speed_7200:
				case SpeedEnum::Speed_9600:
				case SpeedEnum::Speed_14400:
				case SpeedEnum::Speed_19200:
                        response = gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, NULL, 0, 0);
                        break;
                    default:
                        response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_value, true, NULL, 0, 0);
                        break;
                }
            }
            else
            {
                response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_address, true, NULL, 0, 0);
            }
            break;
        case SMP3_SEND_ID :
            if(request->datalength == 0)
            {
                datalength = 13;
                data = new System::Byte[datalength];
                memcpy(data, "MVT-SPM3v1.00", datalength);
                response = gcnew CommandEntity(request->GetAddress(), request->GetFunction(), 0, false, data, datalength, 0);
            }
            else
            {
                response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_data_address, true, NULL, 0, 0);
            }
            break;
		case SCANCONTROLLER_FUNCTION :
			response = commandFactory->CreateRegisterController(request->GetAddress(), (int)ControllerTypeEnum::HumidityController);			
			break;
		case REGISTERCONTROLLER_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case DEVICEWASHALTED_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case REQUESTDATA_FUNCTION :
			response = commandFactory->CreateBeginDataSend(request->GetAddress(), this->dataDurationSpan);
			break;
		case SENDDATAPACKET_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case DATATRANSACTIONCOMPLITED_FUNCTION :
			if(!request->HasSubfunction())
			{
				response = commandFactory->CreateEndDataSend(request->GetAddress(), true);
			}
			else
			{
				throw gcnew InvalidOperationException("No response for request");
			}
			break;
		case BEGINREMOTECONTROL_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case SENDDEVICEFILES_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case REQUESTDEVICEFILE_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case SAVEDEVICEFILES_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case DEVICEFILEUPLOAD_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
		case COMPLITEREMOTECONTROL_FUNCTION :
			response = commandFactory->CreateDefaultResponse(request);
			break;
        default:
            //error response illegal_function
            response = gcnew CommandEntity(request->GetAddress(), ERROR_FUNCTION, illegal_function, true, NULL, 0, 0);
            break;
    }
    return response;
}
														