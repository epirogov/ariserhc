#include "StdAfx.h"
#include "BooleanParser.h"

#include <iostream>
#include <string.h>

using namespace std;
using namespace LibmodbusCustomCommands;

bool BooleanParser::Parse(const char * value)
{
    bool result = false;
    if(!BooleanParser::TryParse(value,result))
    {
        throw new exception();
    }

    return result;
}


bool BooleanParser::TryParse(const char * value, bool & result)
{
    if(strcmp(value, "false") == 0 || strcmp(value, "False") == 0 ||strcmp(value, "FALSE") == 0)
    {
        result = false;
        return true;
    }

    if(strcmp(value, "true") == 0 || strcmp(value, "True") == 0 ||strcmp(value, "TRUE") == 0)
    {
        result = true;
        return true;
    }

    return false;
}
