#include "StdAfx.h"
#include <memory.h>
#include <stdio.h>
#include "PrivateMessanger.h"
#include "CommandFactory.h"

using namespace LibmodbusCustomCommands;
using namespace System::IO::Ports;

CommandEntity ^ PrivateMessanger::SendWithResponse(CommandEntity ^ queryentity, ModbusProvider ^ provider, bool longRespone)
{
    System::Byte query[256];
    int querylength = 0;
    queryentity->Build(query, querylength);
	static System::Byte response[256];
	memset(response, 0, 256);
    System::Byte responselength = queryentity->responselength;
    System::Byte realResponselength = 0;
    provider->SendData(query, querylength, response, responselength, realResponselength, longRespone);
	delete [] query; 	
	if(realResponselength == 0 || realResponselength > 255)
	{
		throw gcnew TimeoutException();
	}
	
    return commandFactory->CreateResponse(response, realResponselength);       
}
