#include "StdAfx.h"
#include <list>
#include <string.h>
#include "BooleanArrayParser.h"
#include "BooleanParser.h"

using namespace std;
using namespace LibmodbusCustomCommands;

void BooleanArrayParser::Parse(const char * content, bool * & array, int & lenght)
{
    list<char *> * values = TrimString(content, '\n');

    array = new bool[values->size()];

    list<char *>::iterator current = values->begin();

    for(int i =0; i<values->size(); i++)
    {
        bool result;
        array[i] = BooleanParser::Parse(*current);
    }

    current = values->begin();
    for(int i =0; i<values->size(); i++)
    {
        delete [] *current;
    }

    delete values;
}

bool BooleanArrayParser::Validate(const char * content)
{
    list<char *> * values = TrimString(content, '\n');

    list<char *>::iterator current = values->begin();

    for(int i =0; i<values->size(); i++)
    {
        bool result;
        if(!BooleanParser::TryParse(*current, result))
        {
            return false;
        }
    }

    current = values->begin();
    for(int i =0; i<values->size(); i++)
    {
        delete [] *current;
    }

    delete values;

    return  true;
}


list<char*> * BooleanArrayParser::TrimString(const char * source, char delimiter)
{
    list<char*> * trimList = new list<char*>();

    char * begin = (char *)source;

    char * current = NULL;
    do
    {
        current = strchr(begin, delimiter);
        if(current - begin > 0)
        {
            char * value = NULL;
            value = new char[current - begin + 1];
            strncpy(value, begin, current - begin);
            value[current - begin] = 0;
            trimList->push_back(value);
        }
        begin = current+1;
    }
    while(strlen(current) > 0);

    return trimList;
}
