#pragma once

using namespace System;

namespace LibmodbusCustomCommands {
	generic <typename TValue1> public ref class TypedEventArgs :
	public EventArgs
	{
	public :
		TValue1 Value1;
	public:
		
		TypedEventArgs(TValue1 value1)
		{
			this->Value1 = value1;
		}

		TValue1 GetValue()
		{
			return this->Value1;
		}
	};

}