#pragma once
#include "CommandEntity.h"
#include "CMODEEnum.h"
#include "SpeedEnum.h"
#include "ReadBufferEnum.h"

using namespace System;

namespace LibmodbusCustomCommands
{
	public ref class CommandFactory
	{
		private :		
		Object ^ syncRoot;
	    public :

		CommandFactory();
		~CommandFactory();
	    CommandEntity ^ CreateCMODE(Byte address, CMODEEnum mode);
	    CommandEntity ^ CreateReadBuffer(Byte address, ReadBufferEnum channel);
	    CommandEntity ^ CreateGetRegisters(Byte address, short startAddress, Byte count);
	    CommandEntity ^ CreateSetRegisters(Byte address, short startAddress, Byte count, const Byte * registers, Byte registerslength);
	    CommandEntity ^ CreateWriteToEEPROM(Byte address);
	    CommandEntity ^ CreateSetSpeed(System::Byte address, SpeedEnum mode);
	    CommandEntity ^ CreateSendId(System::Byte address);
	    CommandEntity ^ CreateResponse(System::Byte * rawresponse, int count);
		
		CommandEntity ^ CreateScanController(Byte address);
		CommandEntity ^ CreateRegisterController(Byte address, Byte deviceType);
		CommandEntity ^ CreateDeviceWasHalted(Byte address);		
		
		CommandEntity ^ CreateRequestData(Byte address, DateTime ^ startDate, TimeSpan ^ duration);
		CommandEntity ^ CreateDataPacketReceived(Byte address, System::Byte * data, System::Byte datacount);
		CommandEntity ^ CreateDataPacketBrocken(Byte address);
		CommandEntity ^ CreateDataTransactionComplited(Byte address);
		CommandEntity ^ CreateBeginDataSend(Byte address, TimeSpan ^ duration);
		CommandEntity ^ CreateSendDataPacket(Byte address, int offset, Byte count, Byte * data, int datalength);
		CommandEntity ^ CreateEndDataSend(Byte address, bool hasData);
		CommandEntity ^ CreateNoMedium(Byte address);
		
		CommandEntity ^ CreateBeginRemoteControl(Byte address);
		CommandEntity ^ CreateRequestDeviceFile(Byte address, int fileCode);
		CommandEntity ^ CreateSaveDeviceFiles(Byte address, int fileMask);
		CommandEntity ^ CreateDeviceFileUpload(Byte address, int fileCode);
		CommandEntity ^ CreateCompliteRemoteControl(Byte address);
		CommandEntity ^ CreateSendDeviceFiles(Byte address, int fileMask);

		CommandEntity ^ CreateResponseTemplate(CommandEntity ^ request);

		CommandEntity ^ CreateDefaultResponse(CommandEntity ^ request);
	};
}