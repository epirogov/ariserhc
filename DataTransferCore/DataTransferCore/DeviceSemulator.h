#pragma once
#include "ModbusProvider.h"
#include "DeviceTypeEnum.h"
#include "PrivateMessanger.h"
#include "CommandResponseFactory.h"

using namespace System;
using namespace System::Threading;
using namespace System::IO::Ports;

namespace LibmodbusCustomCommands
{
	public ref class DeviceSemulator : public PrivateMessanger
	{
	    private :
	    ModbusProvider ^ provider;
	    volatile bool listen;
	    Thread ^ thread;
	    CommandResponseFactory ^ responseFactory;

	    public :
	    DeviceSemulator(DeviceTypeEnum deviceType, SerialPort ^ serialPort, const char * datafilename);
		DeviceSemulator(SerialPort ^ serialPort, const char * datafilename);
	    ~DeviceSemulator();

	    void Start();
	    void Stop();

	    private :
	    static void WatchCycle(Object ^ arg);    
	};
}