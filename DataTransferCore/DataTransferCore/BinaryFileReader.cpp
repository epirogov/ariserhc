#include "StdAfx.h"
#include "BinaryFileReader.h"

using namespace System;
using namespace LibmodbusCustomCommands;

BinaryFileReader::BinaryFileReader() : FileReaderBase("", STATICFILE)
{
}

BinaryFileReader::BinaryFileReader(const char * filename) : FileReaderBase(filename, STATICFILE)
{
}

BinaryFileReader::~BinaryFileReader()
{
}

void BinaryFileReader::ReadToEnd(Byte * & array, int & length)
{
    this->OnlyOnceRead();

    if(this->file == NULL)
    {
        length = 0;
        return;
    }

    length = GetFileSize();
    array = new Byte[length];

    if(fread(array, length, 1, this->file) == -1)
    {
        perror("fread");
    }
}
