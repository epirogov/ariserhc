#include "StdAfx.h"
#include "CustomFunctionMapping.h"

using namespace LibmodbusCustomCommands;

CustomFunctionMapping::CustomFunctionMapping(System::Byte function, System::Byte headerlength, System::Byte datalength)
{
    this->function = function;
    this->headerlength = headerlength;
    this->datalength = datalength;
}

CustomFunctionMapping::~CustomFunctionMapping()
{
}
